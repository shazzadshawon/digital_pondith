<link rel='stylesheet' id='wcu' href='assets/css/wcu.0ee40939d27ffea6.0ee40939d27ffea6.0ee40939d27ffea6.css' type='text/css' media='all'/>

<section id="why_choose_us">
    <div class="view x40-widget widget   text-bg" id="layers-widget-skrollex-section-7"
         data-text-effect-selector="h1,h3,h4" data-text-effect="effect-a-animated">
        <div data-src="assets/images/bg-stocksnap-3F7D411CC8.8138c6013c9bc6cc.8138c6013c9bc6cc.5acf6089459ec422.jpg" data-alt="" class="bg-holder"></div>
        <div data-src="assets/images/bg-stocksnap-3F7D411CC8.8138c6013c9bc6cc.8138c6013c9bc6cc.5acf6089459ec422.jpg" data-alt="" class="bg-holder"></div>
        <div id="team" class="fg">
            <div class="layout-boxed section-top wcu_header"><h3 class="heading-section-title">Why Choose Us</h3>
                <p class="lead">This is the texture of our Culture of life and the system for all choices made inside these walls.
                    Heads up, they have a tendency to be contagious.</p>
            </div>
            <div class="section-cols layout-boxed wcu">
                <div class="pure-g">
                    <ul class="our-values">
                        <li class="value scroll-in-animation" data-animation="fadeInLeft">
                            <h2 class="word">Authenticity.</h2>
                            <p class="description">To be genuine, be vulnerable.</p>
                        </li>
                        <li class="value scroll-in-animation" data-animation="fadeInRight">
                            <h2 class="word">Simplicity.</h2>
                            <p class="description">Distill to the meaningful and balanced.</p>
                        </li>
                        <li class="value scroll-in-animation" data-animation="fadeInLeft">
                            <h2 class="word">Drive.</h2>
                            <p class="description">Do what you love.</p>
                        </li>
                        <li class="value scroll-in-animation" data-animation="fadeInRight">
                            <h2 class="word">Adventure.</h2>
                            <p class="description">Take risks and embrace where they take you.</p>
                        </li>
                        <li class="value scroll-in-animation" data-animation="fadeInLeft">
                            <h2 class="word">Mindfulness.</h2>
                            <p class="description">Exercise a nuanced, articulate understanding.</p>
                        </li>
                        <li class="value scroll-in-animation" data-animation="fadeInRight">
                            <h2 class="word">Appreciation.</h2>
                            <p class="description">Dwell on the good.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>