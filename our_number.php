<div class="view x40-widget widget  " id="layers-widget-skrollex-section-13">
    <div id="numbers" class="fg colors-d ">
        <div class="layout-boxed section-top"><h4 class="heading-subsection-title">Our <span>Numbers</span></h4>
            <p class="subsection-details"><span>Some of The</span> Cool Facts About Us</p>
        </div>
        <div class="section-cols layout-fullwidth">
            <div class="pure-g">
                <div class="layers-widget-skrollex-section-55d50056042d8357324106 pure-u-12-24 pure-u-md-6-24  ">
                    <div class="counter background-f heading-f">
                        <div class="count player background-10-light">200</div>
                        <div class="caption heading-f">Projects</div>
                    </div>
                </div>
                <div class="layers-widget-skrollex-section-55d504c904a53344045984 pure-u-12-24 pure-u-md-6-24  ">
                    <div class="counter background-g heading-g">
                        <div class="count player background-10-light">150</div>
                        <div class="caption heading-g">Clients</div>
                    </div>
                </div>
                <div class="layers-widget-skrollex-section-55d504ce404de389549092 pure-u-12-24 pure-u-md-6-24  ">
                    <div class="counter background-f heading-f">
                        <div class="count player background-10-light">5310</div>
                        <div class="caption heading-f">Followers</div>
                    </div>
                </div>
                <div class="layers-widget-skrollex-section-55d504dcd6135845189246 pure-u-12-24 pure-u-md-6-24  ">
                    <div class="counter background-g heading-g">
                        <div class="count player background-10-light">2</div>
                        <div class="caption heading-g">Years</div>
                    </div>
                </div>
                <div class="layers-widget-skrollex-section-55d505793b346054205997 pure-u-1 pure-u-lg-24-24  ">
                    <p class="narrow text-center">Mauris venenatis vulputate ligula eu finibus. Donec pretium
                        libero lacus, vitae maximus purus dapibus. Proin gravida, est sed vestibulum cursus.</p>
                </div>
            </div>
        </div>
    </div>
</div>