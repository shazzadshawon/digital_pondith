<!-- META TAGS -->
<?php require('meta_home.php'); ?>
<!-- META TAGS -->



<link rel='dns-prefetch' href='http://maps.googleapis.com/'/>
<link rel='dns-prefetch' href='http://fonts.googleapis.com/'/>
<link rel='dns-prefetch' href='http://s.w.org/'/>
<link rel="alternate" type="application/rss+xml" title="DigitalPondith &raquo; Feed" href="feed/index.html"/>
<link rel="shortcut icon" type="image/x-icon" href="assets/favicon.2d1cacb3e497e65b.2d1cacb3e497e65b.2d1cacb3e497e65b.ico">

<link rel='stylesheet' id='layers-google-fonts-css' href='http://fonts.googleapis.com/css?family=Raleway%3Aregular%2C700%2C100%2C200%2C300%2C500%2C600%2C800%2C900%7COswald%3Aregular%2C700%2C300&amp;ver=2.0.0' type='text/css' media='all'/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<!--    <link rel='stylesheet' id='contact-form-7-css' href='plugins/contact-form-7/includes/css/styles4906.css?ver=4.7' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='layers-framework-css' href='assets/layerswp/assets/css/framework001e.05951e3cbbb7ec25.05951e3cbbb7ec25.05951e3cbbb7ec25.05951e3cbbb7ec25.css?ver=2.0.0' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='layers-components-css' href='assets/layerswp/assets/css/components001e.b549f7b3562eb962.b549f7b3562eb962.b549f7b3562eb962.b549f7b3562eb962.css?ver=2.0.0' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='layers-responsive-css' href='assets/layerswp/assets/css/responsive001e.e83e06663b6ba805.e83e06663b6ba805.e83e06663b6ba805.e83e06663b6ba805.css?ver=2.0.0' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='layers-icon-fonts-css' href='assets/layerswp/assets/css/layers-icons001e.bc7bea8d33a7a758.bc7bea8d33a7a758.bc7bea8d33a7a758.bc7bea8d33a7a758.css?ver=2.0.0' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='layers-font-awesome-css' href='assets/layerswp/core/assets/plugins/font-awesome/font-awesome.min001e.b6af08e93d3861f8.b6af08e93d3861f8.b6af08e93d3861f8.b6af08e93d3861f8.css?ver=2.0.0' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='animate.css-css' href='assets/lib/bower_components/animate/animate.minf4ef.60aca30e688004a8.60aca30e688004a8.60aca30e688004a8.css.60aca30e688004a8.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='purecss-css' href='assets/lib/pure/pure-minf4ef.9a108ac6ff91842e.9a108ac6ff91842e.9a108ac6ff91842e.9a108ac6ff91842e.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='purecss-grids-responsive-css' href='assets/lib/pure/grids-responsive-minf4ef.4fa7b3ff27b2fd96.4fa7b3ff27b2fd96.4fa7b3ff27b2fd96.4fa7b3ff27b2fd96.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='linecons-css' href='assets/lib/linecons/stylef4ef.9d1e660aa16f655b.9d1e660aa16f655b.9d1e660aa16f655b.9d1e660aa16f655b.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='custom-mit-code-css' href='assets/lib/mit-code/stylef4ef.94b60875c741c132.94b60875c741c132.94b60875c741c132.94b60875c741c132.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='custom-gnu-code-css' href='assets/lib/gnu-code/stylef4ef.de8bb18335d6b904.de8bb18335d6b904.de8bb18335d6b904.de8bb18335d6b904.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='minicolors-css' href='assets/lib/bower_components/minicolors/jquery.minicolorsf4ef.776a49397b3d6e54.776a49397b3d6e54.776a49397b3d6e54.776a49397b3d6e54.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='skrollex_child_styles-css' href='assets/css/stylef4ef.675474117f5682e2.675474117f5682e2.675474117f5682e2.675474117f5682e2.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='theme-color-schemes-css' href='assets/css/colors-preset-maryc719.04cf60e6ad1e08de.04cf60e6ad1e08de.04cf60e6ad1e08de.04cf60e6ad1e08de.css?ver=1494117369' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='layers-style-css' href='style001e.css?ver=2.0.0' type='text/css' media='all'/>-->
<!--    <link rel='stylesheet' id='fancybox-css' href='plugins/easy-fancybox/fancybox/jquery.fancybox-1.3.8.min82fc.css?ver=1.5.8.2' type='text/css' media='screen'/>-->

<!--    <link rel='stylesheet' id='theme-custome-css' href='assets/css/custom.463ee11b3f2b4ce6.463ee11b3f2b4ce6.463ee11b3f2b4ce6.463ee11b3f2b4ce6.css' type='text/css' media='all'/>-->
<!--    <link rel="stylesheet" href="assets/css/trailler.0b816e2cc39bb35b.0b816e2cc39bb35b.0b816e2cc39bb35b.0b816e2cc39bb35b.css?v=2.0" type="text/css" media="screen" />-->
<link rel="stylesheet" id="mainCss" href="assets/css/digitalPondith.6a06082efaaeec79.6a06082efaaeec79.6a06082efaaeec79.css?v=2.0" type="text/css" media="all" />




<!--    <script type='text/javascript' src='assets/js/jquery/jqueryb8ff.99d28e47dc25f263.99d28e47dc25f263.99d28e47dc25f263.99d28e47dc25f263.js?ver=1.12.4'></script>-->
<!--    <script type='text/javascript' src='assets/js/jquery/jquery-migrate.min330a.7121994eec5320fb.7121994eec5320fb.7121994eec5320fb.7121994eec5320fb.js?ver=1.4.1'></script>-->
<!--    <script type='text/javascript' src='assets/layerswp/assets/js/plugins001e.8c93736bbd962e2d.8c93736bbd962e2d.8c93736bbd962e2d.8c93736bbd962e2d.js?ver=2.0.0'></script>-->

<script type='text/javascript'>
    var layers_script_settings = {"header_sticky_breakpoint": "270"};
    var isMobile = false;
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
</script>

<!--    <script type='text/javascript' src='assets/layerswp/assets/js/layers.framework001e.8c402b75b4f47a00.8c402b75b4f47a00.8c402b75b4f47a00.8c402b75b4f47a00.js?ver=2.0.0'></script>-->

<!--<link rel='https://api.w.org/' href='wp-json/index.html'/>-->
<!--<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="assets/wlwmanifest.xml"/>-->

<link rel="canonical" href="index.php"/>
<link rel='shortlink' href='index.php'/>

<style type="text/css" id="layers-inline-styles-header">
    body {
        font-family: "Raleway", Helvetica, sans-serif;
    }

    h1, h2, h3, h4, h5, h6, .heading {
        font-family: "Oswald", Helvetica, sans-serif;
    }

    button, .button, input[type=submit] {
        font-family: "Oswald", Helvetica, sans-serif;
    }
</style>

<style type="text/css">.recentcomments a {
    display: inline !important;
    padding: 0 !important;
    margin: 0 !important;
}</style>

<style type="text/css">
    .border_mail{
        position: absolute;
        left: 2%;
    }
    .border_hotline{
        position: absolute;
        right: 2%;
    }
    .background_yellow{
        background: #ffbb42 !important;
    }
    .background_yello_dark{
        background: #d29934 !important;
    }
    .background_blueLight{
        background: #90fffa !important;
    }
    .background_blueDark{
        background: #75cecb;
    }
    .text_black{
        color: #000 !important;
    }
    .preloader_background{
        color: #ffffff !important;
    }
    .preloaderbar_color{
        background: #ffffff !important;
    }
</style>

<!--    THREE JS-->
<!--    <script type='text/javascript' src='assets/js/three.min.8d27c7b809c3f79a.8d27c7b809c3f79a.8d27c7b809c3f79a.8d27c7b809c3f79a.js'></script>-->
<!--    <script type="text/javascript" src="assets/js/OrbitControls.4c51b0422232dfc1.4c51b0422232dfc1.4c51b0422232dfc1.4c51b0422232dfc1.js?v=2.0"></script>-->
<!--    <script type='text/javascript' src="assets/js/Projector.e0ca8d67a365d508.e0ca8d67a365d508.e0ca8d67a365d508.e0ca8d67a365d508.js?v=2.0"></script>-->
<!--    <script type='text/javascript' src="assets/js/CanvasRenderer.9f9e69b31fdac173.9f9e69b31fdac173.9f9e69b31fdac173.9f9e69b31fdac173.js?v=2.0"></script>-->
<!--    <script type='text/javascript' src='assets/js/dat.gui.min.12e1fc5c836b6a53.12e1fc5c836b6a53.12e1fc5c836b6a53.12e1fc5c836b6a53.js'></script>-->

<!--Inline 1-->
<script type="text/javascript">
    var fb_timeout=null,fb_opts={overlayShow:!0,hideOnOverlayClick:!0,showCloseButton:!0,margin:20,centerOnScroll:!0,enableEscapeButton:!0,autoScale:!0},easy_fancybox_handler=function(){var o='a[href*=".jpg"]:not(.nolightbox,li.nolightbox>a), area[href*=".jpg"]:not(.nolightbox), a[href*=".jpeg"]:not(.nolightbox,li.nolightbox>a), area[href*=".jpeg"]:not(.nolightbox), a[href*=".png"]:not(.nolightbox,li.nolightbox>a), area[href*=".png"]:not(.nolightbox)';jQuery(o).addClass("fancybox image");var a=jQuery("div.gallery ");a.each(function(){jQuery(this).find(o).attr("rel","gallery-"+a.index(this))}),jQuery("a.fancybox, area.fancybox, li.fancybox a").fancybox(jQuery.extend({},fb_opts,{transitionIn:"elastic",easingIn:"easeOutBack",transitionOut:"elastic",easingOut:"easeInBack",opacity:!1,hideOnContentClick:!1,titleShow:!0,titlePosition:"over",titleFromAlt:!0,showNavArrows:!0,enableKeyboardNav:!0,cyclic:!1}))},easy_fancybox_auto=function(){setTimeout(function(){jQuery("#fancybox-auto").trigger("click")},1e3)};
</script>