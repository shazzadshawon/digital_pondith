<section id="info">
    <div class="section-cols layout-boxed">
        <div class="pure-g">
            <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
                <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">

                    <h1 class="post-title">
                        <a href="#">Some <span>FAQ</span></a>
                    </h1>
                    <div class="copy">
                        <p class="excerpt">When people have the sense of hearing the term social media their minds usually head towards Twitter and Facebook, however, there are many platforms of social media out there. Finding the ones where you can connect with your target audience is the real key.</p>
                    </div>
                </article>
            </div>
        </div>

        <div class="pure-g">
            <div class="layers-widget-skrollex-section-55ca79e2d9085825707768 pure-u-12-24 pure-u-md-6-24  col-padding col-style-decorated">
                <h5 class="heading-col-title">What’s included<br/>
                    in our SM advertising packages?</h5>
                <p><span><h5>Business Page Optimization</h5></span></p>
                <p>We will optimize your business pages based on principles and put into practices, before running any paid Social Media campaigns.</p>

                <p><span><h5>Conversion Tracking Setup</h5></span></p>
                <p>It is important to have the right metrics and our first step is to make sure that we can properly and truthfully track the performance of your ads, to evaluate the success of any campaign.</p>

                <p><span><h5>Campaign Optimization</h5></span></p>
                <p>We will run a number of tests to optimize your campaigns and ads and increase the ROI during the month.</p>
            </div>
            <div class="layers-widget-skrollex-section-55ca79e2d9091546317451 pure-u-12-24 pure-u-md-6-24  col-padding col-style-decorated">
                <h5 class="heading-col-title">Why Startup, SME, MNC<br/>
                    businesses need social media packages?</h5>
                <ul>
                    <li>Find new target custome</li>
                    <li>Extend the word about your products and services</li>
                    <li>Brand awareness, acknowledgment & Trust</li>
                    <li>Promote your business, products or services</li>
                    <li>Create a community and loyal target following group</li>
                    <li>It’s good for SEO</li>
                </ul>
            </div>
            <div class="layers-widget-skrollex-section-55ca79e2d9096487365395 pure-u-12-24 pure-u-md-6-24  col-padding col-style-decorated">
                <h5 class="heading-col-title">What is the difference<br/>
                    between ‘social media packages’ and ‘social media advertising packages’?</h5>
                <p><span>If you want a professional to update your social media pages with high-quality content and get new target audience, then choose one of our social media packages. If you want to run Paid Ads on any social network and maximize you’re ROI, then choose one of our social media advertising packages</span></p>
                <p>If you want a combination of both, then Contact us for select Customize, our experts will create a package that is appropriate for your business goals and budget.</p>
            </div> <div class="layers-widget-skrollex-section-55ca8337ec9d8357985403 pure-u-12-24 pure-u-md-6-24  col-padding col-style-decorated">
                <h5 class="heading-col-title">How you choose<br/>
                    your package?</h5>
                <p><span>Select the package that is closer to your budget and needs. Submit the form by clicking the Get Started button.</span></p>
                <p>One of our social media specialists or our marketing team will contact you to discuss the full process and details dictation about your channel and then we start your campaign.</p>
                <p>If you are not sure which package is best for your business don’t worry our social media experts will guide you from side to side the process and help you to make the best decision for your business. Just select any package and get in touch with us and we will touch the rest.</p>
            </div>
        </div>
        <div class="pure-g">
            <div class="layers-widget-skrollex-section-55ca79e2d9085825707768 pure-u-12-24 pure-u-md-6-24  col-padding col-style-decorated">
                <h5 class="heading-col-title">What to expect from <br/>
                    our social media packages?</h5>
                <p><span>Increase in the number up to 20% of your target followers on all social networks.</span></p>

                <p><span>Proper Management of your social media pages and campaigns, according to best values and practice</span></p>

                <p><span>Full Guidance from a social media expert on how to make the most of the social media.</span></p>
            </div>
            <div class="layers-widget-skrollex-section-55ca79e2d9085825707768 pure-u-12-24 pure-u-md-6-24  col-padding col-style-decorated">
                <h5 class="heading-col-title">Want more than <br/>
                    social media marketing?</h5>
                <p><span>Combine social media marketing with Search Engine Optimization (SEO). Check out our SEO Packages and Mobile Marketing for more details.</span></p>
            </div>
            <div class="layers-widget-skrollex-section-55ca79e2d9085825707768 pure-u-12-24 pure-u-md-6-24  col-padding col-style-decorated">
                <h5 class="heading-col-title">Terms and <br/>
                    Conditions</h5>
                <p><span>We like to keep things simple and we are confident about our services and the result we deliver. In the unlikely event that you are not satisfied with our work, you can cancel any time (at the end of month) without any fees or penalties. There is also no minimum contact period.</span></p>
            </div>
        </div>
    </div>
</section>
