module.exports = function(grunt){

    // configure Main project setting
    grunt.initConfig({
        imagemin: {
            png: {
                options: {
                    optimizationLevel: 3,
                    svgoPlugins: [{removeViewBox: false}]
                },
                files: [
                    {
                        expand: true,
                        cwd: 'assets/',
                        src: ['**/*.{png,jpg,gif}'],
                        dest: 'assets/compressed/'
                    }
                ]
            }
        },

        cache_control: {
            your_target: {
                source: "*.php",
                options: {
                    version: "2.0",
                    links: true,
                    scripts: true,
                    replace: true,
                    filesToIgnore: ["ovc/counter.js"],
                }
            }
        },

        cacheBust: {
            taskName: {
                options: {
                    assets: ['assets/**']
                },
                src: ['*.php']
            }
        },

        // Basic Settings and info about plugins
        pkg: grunt.file.readJSON('package.json'),

        // Name of Plugins
        cssmin: {
            combine: {
                files: {
                    'assets/css/digitalPondith.css': ['plugins/contact-form-7/includes/css/styles4906.css',
                        'assets/layerswp/assets/css/framework001e.css',
                        'assets/layerswp/assets/css/components001e.css',
                        'assets/layerswp/assets/css/responsive001e.css',
                        'assets/layerswp/assets/css/layers-icons001e.css',
                        'assets/layerswp/core/assets/plugins/font-awesome/font-awesome.min001e.css',
                        'assets/lib/bower_components/animate.css/animate.minf4ef.css',
                        'assets/lib/pure/pure-minf4ef.css',
                        'assets/lib/pure/grids-responsive-minf4ef.css',
                        'assets/lib/linecons/stylef4ef.css',
                        'assets/lib/mit-code/stylef4ef.css',
                        'assets/lib/gnu-code/stylef4ef.css',
                        'assets/lib/bower_components/minicolors/jquery.minicolorsf4ef.css',
                        'assets/css/stylef4ef.css',
                        'assets/css/colors-preset-maryc719.css',
                        'style001e.css',
                        'plugins/easy-fancybox/fancybox/jquery.fancybox-1.3.8.min82fc.css',
                        'assets/css/custom.css',
                        'assets/css/trailler.css',
                        'landing_section.css']
                }
            }
        },

        uglify: {
            dist1: {
                files: {
                    'assets/js/digitalPondith.min.js': ['assets/js/jquery/jqueryb8ff.js',
                        'assets/js/jquery/jquery-migrate.min330a.js',
                        'assets/layerswp/assets/js/plugins001e.js',
                        'assets/layerswp/assets/js/layers.framework001e.js',
                        'assets/js/three.min.js',
                        'assets/js/OrbitControls.js',
                        'assets/js/Projector.js',
                        'assets/js/CanvasRenderer.js',
                        'assets/js/dat.gui.min.js',
                        'plugins/contact-form-7/includes/js/jquery.form.mind03d.js',
                        'plugins/contact-form-7/includes/js/scripts4906.js',
                        'assets/lib/bower_components/jquery-cookie/jquery.cookief4ef.js',
                        'assets/js/imagesloaded.min55a0.js',
                        'assets/js/masonry.mind617.js',
                        'assets/lib/bower_components/less.js/dist/less.minf4ef.js',
                        'assets/lib/tween/tween.minf4ef.js',
                        'assets/lib/bower_components/modernizr/modernizrf4ef.js',
                        // 'assets/lib/bower_components/vimeo-player-js/dist/player.minf4ef.js',
                        'assets/lib/bower_components/snap.svg/dist/snap.svg-minf4ef.js',
                        'assets/lib/bower_components/minicolors/jquery.minicolors.minf4ef.js',
                        'assets/lib/bower_components/textillate/assets/jquery.letteringf4ef.js',
                        'assets/lib/bower_components/textillate/assets/jquery.fittextf4ef.js',
                        'assets/lib/bower_components/textillate/jquery.textillatef4ef.js',
                        'assets/lib/stringencoders-v3.10.3/javascript/base64f4ef.js',
                        // 'assets/js/script-bundle.minf4ef.js',
                        // 'assets/js/wp-embed.min66f2.js',
                        'plugins/easy-fancybox/fancybox/jquery.fancybox-1.3.8.min82fc.js',
                        'plugins/easy-fancybox/js/jquery.easing.min9e1e.js',
                        'plugins/easy-fancybox/js/jquery.mousewheel.min4830.js',
                        'assets/sitescript.js',
                        'assets/js/pera_removal.js',
                        'assets/js/DM_trailler.js',
                        'assets/js/landing_section.js']
                }
            },

            dist3: {
                files: {
                    'ovc/counter.min.js': ['ovc/counter.js']
                }
            }
        }
    });

    // Load the Plugin
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-cache-control');
    grunt.loadNpmTasks('grunt-cache-bust');
    // grunt.loadNpmTasks('grunt-contrib-imagemin');

    // Exicute The Task
    grunt.registerTask('default', ['cache_control', 'cacheBust', 'cssmin', 'uglify:dist1', 'uglify:dist3']);
    // grunt.registerTask('default', ['cache_control', 'cssmin', 'uglify:dist1', 'uglify:dist3']);
    // grunt.registerTask('default', ['imagemin']);
};