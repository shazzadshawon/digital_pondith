<div class="view x40-widget widget   text-bg" id="layers-widget-skrollex-section-9" data-text-effect-selector="h1,h2,h3,h4" data-text-effect="effect-a-animated">
    <div data-src="assets/images/bg_service.6e9706d9f607906d.6e9706d9f607906d.6e9706d9f607906d.jpg" data-alt="" class="bg-holder"></div>
    <div data-src="assets/images/bg_service.6e9706d9f607906d.6e9706d9f607906d.6e9706d9f607906d.jpg" data-alt="" class="bg-holder"></div>
    <div id="solutions" class="fg colors-c ">
        <div class="layout-boxed section-top"><h3 class="heading-section-title">Our Solutions</h3>
            <p class="header-details"><span>We Are Here</span> For You</p>
            <p class="lead">There are great deals of organizations that do what we do. They share a similar what and how, yet our accomplice works with us for our why and our who. We're wits and marketers with marketing prudence and innovative slashes, set out to associate individuals with what is important most — the experience. And more, we spend every day doing as such by honing the tools of the advanced Digital Marketing Trade.</p>
        </div>
        <div class="section-cols layout-boxed">
            <div class="pure-g">
                <div class="layers-widget-skrollex-section-55ca79e2d9085825707768 pure-u-24-24 pure-u-md-8-24  col-padding col-style-decorated scroll-in-animation" data-animation="fadeInLeft" style="background: #ccc;">
                    <h5 class="heading-col-title">Individual / Startup<br/>
                        Business</h5>
                    <p><span>Build your Brand, Target Customers, Market Segmentation, Monetize Market Demand, Increase Product Sells</span></p>
                    <p>Build your Brand, Target Customers, Market Segmentation, Monetize Market Demand, Increase Product Sells</p>
                    <ul>
                        <li>Responsive design</li>
                        <li>Template development</li>
                        <li>Rich media banners</li>
                        <li>Frontend development</li>
                        <li>Backend development</li>
                        <li>Content creation</li>
                        <li>Content audit</li>
                        <li>Copywriting</li>
                        <li>Photography</li>
                    </ul>
                </div>
                <div class="layers-widget-skrollex-section-55ca79e2d9091546317451 pure-u-24-24 pure-u-md-8-24  col-padding col-style-decorated scroll-in-animation" data-animation="fadeInUp" style="background: #fdd8d8;">
                    <h5 class="heading-col-title">Small and Medium Size<br/>
                         Enterprise (SME)</h5>
                    <p><span>Developed Brand, Target Customer, Increased sales, Compete your Competitors, Analysis</span></p>
                    <p>Developed Brand, Target Customer, Increased sales, Compete your Competitors, Analysis</p>
                    <ul>
                        <li>Facebook advertising</li>
                        <li>SEO and SEM</li>
                        <li>Facebook apps</li>
                        <li>Context advertising</li>
                        <li>Rich media banners</li>
                        <li>Game development</li>
                        <li>Content creation</li>
                        <li>Testing</li>
                    </ul>
                </div>
                <div class="layers-widget-skrollex-section-55ca79e2d9096487365395 pure-u-24-24 pure-u-md-8-24  col-padding col-style-decorated scroll-in-animation" data-animation="fadeInRight" style="background: #cde8e7;">
                    <h5 class="heading-col-title">MNC<br/>
                        Multi National Company</h5>
                    <p><span>Value Increase, More Sales, Growth Social Awareness, Be the Brand</span></p>
                    <p>Value Increase, More Sales, Growth Social Awareness, Be the Brand</p>
                    <ul>
                        <li>Rich media banners</li>
                        <li>Audio production</li>
                        <li>Photography</li>
                        <li>Design</li>
                        <li>Content creation</li>
                        <li>Content audit</li>
                        <li>Project management</li>
                        <li>Technical requirements</li>
                        <li>Testing</li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>