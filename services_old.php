<link rel="stylesheet" type="text/css" href="assets/css/service.9300b0c83579906f.9300b0c83579906f.9300b0c83579906f.9300b0c83579906f.css?v=2.0" />

<div class="view x40-widget widget   text-bg" id="layers-widget-skrollex-section-9" data-text-effect-selector="h1,h2,h3,h4" data-text-effect="effect-a-animated">
<!--    <div data-src="http://skrollex2.x40.ru/mary/wp-content/uploads/sites/42/2015/11/bg-pexels-coffee-coffee-machine-cup-3042.jpg" data-alt="" class="bg-holder"></div>-->
<!--    <div data-src="http://skrollex2.x40.ru/mary/wp-content/uploads/sites/42/2015/11/bg-pexels-coffee-coffee-machine-cup-3042.jpg" data-alt="" class="bg-holder"></div>-->
    <div data-src="assets/images/bg_service.6e9706d9f607906d.6e9706d9f607906d.6e9706d9f607906d.6e9706d9f607906d.jpg" data-alt="" class="bg-holder"></div>
    <div data-src="assets/images/bg_service.6e9706d9f607906d.6e9706d9f607906d.6e9706d9f607906d.6e9706d9f607906d.jpg" data-alt="" class="bg-holder"></div>
    <div id="services" class="fg colors-c ">
        <div class="layout-boxed section-top"><h3 class="heading-section-title">Services</h3>
            <p class="header-details"><span>We Are Here</span> For You</p>
            <p class="lead">There are great deals of organizations that do what we do. They share a similar what and how, yet our accomplice works with us for our why and our who. We're wits and marketers with marketing prudence and innovative slashes, set out to associate individuals with what is important most — the experience. And more, we spend every day doing as such by honing the tools of the advanced Digital Marketing Trade.</p>
        </div>
        <div class="section-cols layout-boxed">
            <div class="pure-g">
                <div class="layers-widget-skrollex-section-55ca79e2d9085825707768 pure-u-24-24 pure-u-md-24-24">
                    <div id="service_cubes" class="service_cubes">
                        <!--SMM Button-->
                        <div class="cube flip-to-top service_button_left smm">
                            <div class="default-state cube_shadow">
                                <h2>SMM</h2>
                            </div>
                            <div class="active-state cube_shadow">
                                <h2>Social Media Marketing</h2>
                            </div>
                        </div>
                        <!--SMM Button-->

                        <!--WEB Button-->
                        <div class="cube flip-to-top service_button_right web">
                            <div class="default-state cube_shadow">
                                <h2>WEB</h2>
                            </div>
                            <div class="active-state cube_shadow">
                                <h2>Web Marketing</h2>
                            </div>
                        </div>
                        <!--WEB Button-->

                        <!--Mobile Button-->
                        <div class="cube flip-to-bottom service_button_bottom mobile">
                            <div class="default-state cube_shadow">
                                <h2>MM</h2>
                            </div>
                            <div class="active-state cube_shadow">
                                <h2>Mobile Marketing</h2>
                            </div>
                        </div>
                        <!--Mobile Button-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--FLIP BUTTON-->
<script type="text/javascript">
    jQuery('.active-state').mousedown(function(e){
        jQuery(this).removeClass('cube_shadow').addClass('click_shadow');
    }).mouseup(function(e){
        jQuery(this).addClass('cube_shadow').removeClass('click_shadow');
    });

    jQuery('.smm .active-state').click(function(){
        window.location.href = "service_smm.php";
    });
    jQuery('.web .active-state').click(function(){
        window.location.href = "service_web.php";
    });
    jQuery('.mobile .active-state').click(function(){
//        window.location.href = "service_mobile.php";
        window.location.href = "#";
    });
</script>
<!--FLIP BUTTON-->


<!--CUBE ANIMATION START-->
<script>

    var container;
    var camera, scene, renderer, controls;
    var cube, plane;
    var targetRotation = 0;
    var targetRotationOnMouseDown = 0;

    var mouseX = 0;
    var mouseXOnMouseDown = 0;

    var objects = new Array();
    var mouse;

    var cube_container_h;
    var cube_container_w;

    var windowHalfX;
    var windowHalfY;

    var rotSpeed = 0.02;

    init();
    animate();

    function init() {

        container = document.getElementById( 'service_cubes' );

        cube_container_h = jQuery(container).height();
        cube_container_w = jQuery(container).width();

        windowHalfX = cube_container_w / 2;
        windowHalfY = cube_container_h / 2;


        camera = new THREE.PerspectiveCamera( 70, cube_container_w / cube_container_h, 1, 1000 );
        camera.position.y = 150;
        camera.position.z = 500;

        mouse = new THREE.Vector2();

        scene = new THREE.Scene();
        camera.lookAt(scene.position);

        var cubeMesh = new THREE.CubeGeometry( 250, 250, 250, 4, 4, 4 );

        //var materialcube = new THREE.MeshBasicMaterial( { vertexColors: THREE.FaceColors, overdraw: 0.5 } );
        var textureLoader = new THREE.TextureLoader();
        var texture0 = textureLoader.load( 'assets/data/services/web.697344616b7299b2.697344616b7299b2.697344616b7299b2.697344616b7299b2.jpg' );
        var texture1 = textureLoader.load( 'assets/data/services/owl.be81f09e58a652f4.be81f09e58a652f4.be81f09e58a652f4.be81f09e58a652f4.png' );
        var texture2 = textureLoader.load( 'assets/data/services/dark.4f432d9234988a5f.4f432d9234988a5f.4f432d9234988a5f.4f432d9234988a5f.gif' );
        var texture3 = textureLoader.load( 'assets/data/services/dark.4f432d9234988a5f.4f432d9234988a5f.4f432d9234988a5f.4f432d9234988a5f.gif' );
        var texture4 = textureLoader.load( 'assets/data/services/mob.6dc29e7f0e06059e.6dc29e7f0e06059e.6dc29e7f0e06059e.6dc29e7f0e06059e.jpg' );
        var texture5 = textureLoader.load( 'assets/data/services/social.8ddedd694c5616ca.8ddedd694c5616ca.8ddedd694c5616ca.8ddedd694c5616ca.jpg' );
        var cubeTexture = [
            new THREE.MeshPhongMaterial( { map: texture0, polygonOffset: true, polygonOffsetFactor: 1, polygonOffsetUnits: 1} ),
            new THREE.MeshPhongMaterial( { map: texture1, polygonOffset: true, polygonOffsetFactor: 1, polygonOffsetUnits: 1} ),
            new THREE.MeshPhongMaterial( { vertexColors: THREE.FaceColors, overdraw: 0.5, polygonOffset: true, polygonOffsetFactor: 1, polygonOffsetUnits: 1} ),
            new THREE.MeshPhongMaterial( { vertexColors: THREE.FaceColors, overdraw: 0.5, polygonOffset: true, polygonOffsetFactor: 1, polygonOffsetUnits: 1} ),
            new THREE.MeshPhongMaterial( { map: texture4, polygonOffset: true, polygonOffsetFactor: 1, polygonOffsetUnits: 1} ),
            new THREE.MeshPhongMaterial( { map: texture5, polygonOffset: true, polygonOffsetFactor: 1, polygonOffsetUnits: 1} )
        ];
        var materialcube = new THREE.MeshFaceMaterial( cubeTexture );
        cube = new THREE.Mesh( cubeMesh, materialcube );
        cube.name = "serviceCube";
        cube.id = "1";
        cube.position.y = 80;
        objects.push(cube);
        scene.add( cube );

//        var geo = new THREE.WireframeGeometry( cube.geometry );
        var geo = new THREE.EdgesGeometry( cube.geometry );
        var mat = new THREE.LineBasicMaterial( { color: 0xc8c8c8, linewidth: 2 } );
        var wireframe = new THREE.LineSegments( geo, mat );
        wireframe.scale.x = 1.2;
        wireframe.scale.y = 1.2;
        wireframe.scale.z = 1.2;
        cube.add( wireframe );

        // Plane

        var planeMesh = new THREE.PlaneBufferGeometry( 250, 250);
        planeMesh.rotateX( - Math.PI / 2 );
        var materialPlane = new THREE.MeshBasicMaterial( { color: 0xe0e0e0, overdraw: 0.5 } );
        plane = new THREE.Mesh( planeMesh, materialPlane );
        plane.position.y = -80;
        plane.name = "servicePlane";
        plane.id = "2";
        scene.add( plane );

        var PI2 = Math.PI * 2;

        control = new function () {
            this.rotSpeed = 0.01;
            this.scale = 1;
        };
        addControls(control);

        renderer = new THREE.CanvasRenderer({ alpha: true });
//        renderer = new THREE.WebGLRenderer();
        renderer.setClearColor( 0x000000, 0);
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setSize( cube_container_w, cube_container_h );
        container.appendChild( renderer.domElement );

        container.addEventListener( 'mousedown', onDocumentMouseDown, false );
        container.addEventListener( 'touchstart', onDocumentTouchStart, false );
        container.addEventListener( 'touchmove', onDocumentTouchMove, false );
        //
        window.addEventListener( 'resize', onWindowResize, false );

    }

    function addControls(controlObject) {
        var gui = new dat.GUI();
        gui.add(controlObject, 'rotSpeed', -0.1, 0.1);
    }

    function onWindowResize() {
        windowHalfX = cube_container_w / 2;
        windowHalfY = cube_container_h / 2;
        camera.aspect = cube_container_w / cube_container_h;
        camera.updateProjectionMatrix();
        renderer.setSize( cube_container_w, cube_container_h );
    }

    //

    function onDocumentMouseDown( event ) {
        event.preventDefault();
        container.addEventListener( 'mousemove', onDocumentMouseMove, false );
        container.addEventListener( 'mouseup', onDocumentMouseUp, false );
        container.addEventListener( 'mouseout', onDocumentMouseOut, false );
        mouseXOnMouseDown = event.clientX - windowHalfX;
        targetRotationOnMouseDown = targetRotation;
    }

    function onDocumentMouseMove( event ) {
        mouseX = event.clientX - windowHalfX;
        mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
        targetRotation = targetRotationOnMouseDown + ( mouseX - mouseXOnMouseDown ) * 0.02;
    }

    function onDocumentMouseUp( event ) {
        container.removeEventListener( 'mousemove', onDocumentMouseMove, false );
        container.removeEventListener( 'mouseup', onDocumentMouseUp, false );
        container.removeEventListener( 'mouseout', onDocumentMouseOut, false );

    }

    function onDocumentMouseOut( event ) {
        container.removeEventListener( 'mousemove', onDocumentMouseMove, false );
        container.removeEventListener( 'mouseup', onDocumentMouseUp, false );
        container.removeEventListener( 'mouseout', onDocumentMouseOut, false );

    }

    function onDocumentTouchStart( event ) {
        if ( event.touches.length === 1 ) {
            event.preventDefault();
            mouseXOnMouseDown = event.touches[ 0 ].pageX - windowHalfX;
            targetRotationOnMouseDown = targetRotation;
        }
    }

    function onDocumentTouchMove( event ) {

        if ( event.touches.length === 1 ) {

            event.preventDefault();

            mouseX = event.touches[ 0 ].pageX - windowHalfX;
            targetRotation = targetRotationOnMouseDown + ( mouseX - mouseXOnMouseDown ) * 0.05;

        }

    }

    function rotateRight(){
        var radians = THREE.Math.degToRad( 90 );
        var cartesian = Math.ceil(THREE.Math.radToDeg(cube.rotation.y) % 360);
        targetRotation += radians;
        console.log(cartesian);
    }

    function rotateLeft(){
        var radians = THREE.Math.degToRad( 90 );
        var cartesian = Math.ceil(THREE.Math.radToDeg(cube.rotation.y) % 360);
        targetRotation -= radians;
        console.log(cartesian);
    }

    function animate() {
        requestAnimationFrame( animate );
        render();
    }

    function render() {
        var x = camera.position.x;
        var z = camera.position.z;
        camera.position.x = x * Math.cos(control.rotSpeed) + z * Math.sin(control.rotSpeed);
        camera.position.z = z * Math.cos(control.rotSpeed) - x * Math.sin(control.rotSpeed);
        camera.lookAt(scene.position);

        plane.rotation.y = cube.rotation.y += ( targetRotation - cube.rotation.y ) * 0.05;
        renderer.clear();
        renderer.render( scene, camera );
    }

</script>
<!--CUBE ANIMATION END-->

