<style type="text/css">
    .textBlack{
        color: #000 !important;
    }
</style>

<div class="view x40-widget widget  peraRemoval" id="layers-widget-skrollex-section-6">
    <div id="how-we-work" class="fg colors-d background_blueDark text_black">
        <div class="layout-boxed section-top"><h4 class="heading-subsection-title"><span>How</span> <span class="text_black">We Work</span> </h4>
            <p class="narrow">This is the texture of our Culture of life and the system for all choices made inside these walls.
                Heads up, they have a tendency to be contagious.</p>
        </div>
        <div class="section-cols layout-boxed">
            <div class="pure-g">
                <div class="layers-widget-skrollex-section-55caa2c2d9be3056910152 pure-u-12-24 pure-u-md-6-24  col-padding circle-icon">
                    <p class="text-center"><i class="fa fa-list-ol border-lite circle scroll-in-animation player"
                                              data-animation="fadeInUp"></i></p>
                    <p class="circle-caption textBlack">Planning &amp; analysis stage</p>
                    <p class="text-justify">This step to planning out faithfully what needs to achieve and the time scales concerned. Where we focus on your target audience and target industry</p>
                </div>
                <div class="layers-widget-skrollex-section-55caa59f4525f360669205 pure-u-12-24 pure-u-md-6-24  col-padding circle-icon">
                    <p class="text-center"><i class="li_like border-lite circle scroll-in-animation player"
                                              data-animation="fadeInUp"></i></p>
                    <p class="circle-caption textBlack">Implementation</p>
                    <p class="text-justify">Where we prepare all marketing plan which we have done and perform. In this step, we will find out what you really need and what we do.</p>
                </div>
                <div class="layers-widget-skrollex-section-55caa5b8021c8599175159 pure-u-12-24 pure-u-md-6-24  col-padding circle-icon">
                    <p class="text-center"><i
                            class="li_paperplane border-lite circle scroll-in-animation player"
                            data-animation="fadeInUp"></i></p>
                    <p class="circle-caption textBlack">Optimization</p>
                    <p class="text-justify">Continuous development of your project is essential to maintain high-quality end products. This is a continuous process.</p>
                </div>
                <div class="layers-widget-skrollex-section-55caa5bcc8666509544289 pure-u-12-24 pure-u-md-6-24  col-padding circle-icon">
                    <p class="text-center"><i class="li_lab border-lite circle scroll-in-animation player"
                                              data-animation="fadeInUp"></i></p>
                    <p class="circle-caption textBlack">Digital Marketing</p>
                    <p class="text-justify">This is steps we apply push and pull strategy for your idea and start generating sales. This stage is very important to the growth of your business</p>
                </div>

<!--                <div class="layers-widget-skrollex-section-55caa5bcc8666509544289 pure-u-12-24 pure-u-md-6-24  col-padding circle-icon">-->
<!--                    <p class="text-center"><i class="li_lab border-lite circle scroll-in-animation player"-->
<!--                                              data-animation="fadeInUp"></i></p>-->
<!--                    <p class="circle-caption">Reporting</p>-->
<!--                    <p class="text-center">We will report you and keep update the procedure. We provide deep analysis when it comes to your web analytics.</p>-->
<!--                </div>-->
            </div>
        </div>
    </div>
</div>