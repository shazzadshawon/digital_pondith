<div class="gate colors-o">
    <div class="gate-content background_blueDark">
        <div class="gate-bar preloaderbar_color"></div>
        <div class="preloader">
            <div class="preloader-container">
                <div class="circleOne border-heading preloader_background"></div>
                <div class="circleTwo border-heading preloader_background"></div>
                <div class="circleThree border-heading preloader_background"></div>
                <div class="circleFour border-heading preloader_background"></div>
                <div class="circleFive border-heading preloader_background"></div>
                <div class="circleSix border-heading preloader_background"></div>
            </div>
        </div>
    </div>
</div>