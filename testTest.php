<!DOCTYPE html>
<html lang="en-US" class="state2 page-is-gated scroll-bar site-decoration-b">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no">
    <title>SMM &#8211; Digital|Pondith</title>
    <link rel='dns-prefetch' href='http://maps.googleapis.com/'/>
    <link rel='dns-prefetch' href='http://fonts.googleapis.com/'/>
    <link rel='dns-prefetch' href='http://s.w.org/'/>
    <link rel="alternate" type="application/rss+xml" title="DigitalPondith &raquo; Feed" href="index.php"/>
    <style>#wp-admin-bar-layers-edit-layout .ab-icon:before{font-family:"layers-interface"!important;content:"\e62f"!important;font-size:16px!important;}</style>
    <link rel='stylesheet' id='contact-form-7-css' href='assets/css/styles4906.c0ce8c75d8b06b3b.c0ce8c75d8b06b3b.c0ce8c75d8b06b3b.c0ce8c75d8b06b3b.css?ver=4.7' type='text/css' media='all'/>
    <link rel='stylesheet' id='layers-google-fonts-css' href='http://fonts.googleapis.com/css?family=Raleway%3Aregular%2C700%2C100%2C200%2C300%2C500%2C600%2C800%2C900%7COswald%3Aregular%2C700%2C300&amp;ver=2.0.0' type='text/css' media='all'/>
    <link rel='stylesheet' id='layers-framework-css' href='assets/css/framework001e.05951e3cbbb7ec25.05951e3cbbb7ec25.05951e3cbbb7ec25.05951e3cbbb7ec25.css?ver=2.0.0' type='text/css' media='all'/>
    <link rel='stylesheet' id='layers-components-css' href='assets/css/components001e.b549f7b3562eb962.b549f7b3562eb962.b549f7b3562eb962.b549f7b3562eb962.css?ver=2.0.0' type='text/css' media='all'/>
    <link rel='stylesheet' id='layers-responsive-css' href='assets/css/responsive001e.e83e06663b6ba805.e83e06663b6ba805.e83e06663b6ba805.e83e06663b6ba805.css?ver=2.0.0' type='text/css' media='all'/>
    <link rel='stylesheet' id='layers-icon-fonts-css' href='assets/css/layers-icons001e.bc7bea8d33a7a758.bc7bea8d33a7a758.bc7bea8d33a7a758.bc7bea8d33a7a758.css?ver=2.0.0' type='text/css' media='all'/>
    <link rel='stylesheet' id='layers-font-awesome-css' href='assets/css/font-awesome/font-awesome.min001e.b6af08e93d3861f8.b6af08e93d3861f8.b6af08e93d3861f8.b6af08e93d3861f8.css?ver=2.0.0' type='text/css' media='all'/>
    <link rel='stylesheet' id='animate.css-css' href='assets/css/animate.minf4ef.60aca30e688004a8.60aca30e688004a8.60aca30e688004a8.60aca30e688004a8.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>
    <link rel='stylesheet' id='purecss-css' href='assets/css/pure-minf4ef.9a108ac6ff91842e.9a108ac6ff91842e.9a108ac6ff91842e.9a108ac6ff91842e.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>
    <link rel='stylesheet' id='purecss-grids-responsive-css' href='assets/css/grids-responsive-minf4ef.4fa7b3ff27b2fd96.4fa7b3ff27b2fd96.4fa7b3ff27b2fd96.4fa7b3ff27b2fd96.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>
    <link rel='stylesheet' id='linecons-css' href='assets/css/linecons/stylef4ef.9d1e660aa16f655b.9d1e660aa16f655b.9d1e660aa16f655b.9d1e660aa16f655b.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>
    <link rel='stylesheet' id='custom-mit-code-css' href='assets/css/mit-code/stylef4ef.94b60875c741c132.94b60875c741c132.94b60875c741c132.94b60875c741c132.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>
    <link rel='stylesheet' id='custom-gnu-code-css' href='assets/css/gnu-code/stylef4ef.de8bb18335d6b904.de8bb18335d6b904.de8bb18335d6b904.de8bb18335d6b904.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>
    <link rel='stylesheet' id='minicolors-css' href='assets/css/bower_components/minicolors/jquery.minicolorsf4ef.776a49397b3d6e54.776a49397b3d6e54.776a49397b3d6e54.776a49397b3d6e54.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>
    <link rel='stylesheet' id='skrollex_child_styles-css' href='assets/css/stylef4ef.675474117f5682e2.675474117f5682e2.675474117f5682e2.675474117f5682e2.css?ver=27e1af5b410de903825f58485ec0fd40' type='text/css' media='all'/>
    <link rel='stylesheet' id='theme-color-schemes-css' href='assets/css/colors-preset-maryc719.04cf60e6ad1e08de.04cf60e6ad1e08de.04cf60e6ad1e08de.04cf60e6ad1e08de.css?ver=1494117369' type='text/css' media='all'/>
    <link rel='stylesheet' id='layers-style-css' href='assets/css/style001e.d41d8cd98f00b204.d41d8cd98f00b204.d41d8cd98f00b204.d41d8cd98f00b204.css?ver=2.0.0' type='text/css' media='all'/>
    <link rel='stylesheet' id='fancybox-css' href='assets/css/fancybox/jquery.fancybox-1.3.8.min82fc.018cdd8501941cff.018cdd8501941cff.018cdd8501941cff.018cdd8501941cff.css?ver=1.5.8.2' type='text/css' media='screen'/>
    <script type='text/javascript' src='assets/js/jquery/jqueryb8ff.99d28e47dc25f263.99d28e47dc25f263.99d28e47dc25f263.99d28e47dc25f263.js?ver=1.12.4'></script>
    <script type='text/javascript' src='assets/js/jquery/jquery-migrate.min330a.7121994eec5320fb.7121994eec5320fb.7121994eec5320fb.7121994eec5320fb.js?ver=1.4.1'></script>
    <script type='text/javascript' src='/assets/js/plugins001e.8c93736bbd962e2d.8c93736bbd962e2d.8c93736bbd962e2d.8c93736bbd962e2d.js?ver=2.0.0'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var layers_script_settings = {"header_sticky_breakpoint":"270"};
        /* ]]> */
    </script>
    <script type='text/javascript' src='assets/js/layers.framework001e.8c402b75b4f47a00.8c402b75b4f47a00.8c402b75b4f47a00.8c402b75b4f47a00.js?ver=2.0.0'></script>
    <style type="text/css" id="layers-inline-styles-header">body{font-family:"Raleway",Helvetica,sans-serif;}h1,h2,h3,h4,h5,h6,.heading{font-family:"Oswald",Helvetica,sans-serif;}button,.button,input[type=submit]{font-family:"Oswald",Helvetica,sans-serif;}</style> <meta property="og:title" content="&raquo; Blog"/>
    <meta property="og:description" content="SMM DM"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content=""/>
    <style type="text/css">.recentcomments a{display:inline!important;padding:0!important;margin:0!important;}</style>

    <script type="text/javascript">
        /* <![CDATA[ */
        var fb_timeout = null;
        var fb_opts = { 'overlayShow' : true, 'hideOnOverlayClick' : true, 'showCloseButton' : true, 'margin' : 20, 'centerOnScroll' : true, 'enableEscapeButton' : true, 'autoScale' : true };
        var easy_fancybox_handler = function(){
            /* IMG */
            var fb_IMG_select = 'a[href*=".jpg"]:not(.nolightbox,li.nolightbox>a), area[href*=".jpg"]:not(.nolightbox), a[href*=".jpeg"]:not(.nolightbox,li.nolightbox>a), area[href*=".jpeg"]:not(.nolightbox), a[href*=".png"]:not(.nolightbox,li.nolightbox>a), area[href*=".png"]:not(.nolightbox)';
            jQuery(fb_IMG_select).addClass('fancybox image');
            var fb_IMG_sections = jQuery('div.gallery ');
            fb_IMG_sections.each(function() { jQuery(this).find(fb_IMG_select).attr('rel', 'gallery-' + fb_IMG_sections.index(this)); });
            jQuery('a.fancybox, area.fancybox, li.fancybox a').fancybox( jQuery.extend({}, fb_opts, { 'transitionIn' : 'elastic', 'easingIn' : 'easeOutBack', 'transitionOut' : 'elastic', 'easingOut' : 'easeInBack', 'opacity' : false, 'hideOnContentClick' : false, 'titleShow' : true, 'titlePosition' : 'over', 'titleFromAlt' : true, 'showNavArrows' : true, 'enableKeyboardNav' : true, 'cyclic' : false }) );
        }
        var easy_fancybox_auto = function(){
            /* Auto-click */
            setTimeout(function(){jQuery('#fancybox-auto').trigger('click')},1000);
        }
        /* ]]> */
    </script>
</head>
<body id="skrollex-body" class="blog no-colors-label background-k body-header-logo-left">
<div class="gate colors-o">
    <div class="gate-content">
        <div class="gate-bar background-highlight"></div>
        <div class="preloader">
            <div class="preloader-container">
                <div class="circleOne border-heading"></div>
                <div class="circleTwo border-heading"></div>
                <div class="circleThree border-heading"></div>
                <div class="circleFour border-heading"></div>
                <div class="circleFive border-heading"></div>
                <div class="circleSix border-heading"></div>
            </div>
        </div> </div>
</div>
<div class="page-border  heading top colors-a main-navigation"></div>
<div class="page-border  heading bottom colors-a main-navigation"><a href="#top" class="to-top hover-effect">To <span>Top</span></a><a href="#scroll-down" class="scroll-down hover-effect">Scroll <span>Down</span></a></div>
<div class="page-border  heading left colors-a main-navigation border-pad"></div>
<div class="page-border  heading right colors-a main-navigation border-pad"></div>
<div class="page-border  heading left colors-a main-navigation">
    <ul>
        <li><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a></li><li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li><li><a href="http://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a></li><li><a href="https://vimeo.com/" target="_blank"><i class="fa fa-vimeo-square"></i></a></li><li><a href="https://www.linkedin.com/" target="_blank"><i class="fa fa-linkedin"></i></a></li> </ul>
</div>
<div class="page-border  heading right colors-a main-navigation"></div>
<section id="top-nav" class="page-transition main-navigation heading colors-a top-nav-logo-left" data-colors-1="colors-a" data-colors-2="colors-b">
    <div class="layout-boxed top-nav-inner clearfix">
        <span class="menu-toggle ext-nav-toggle" data-target=".ext-nav"><span></span></span>
        <nav class="nav nav-horizontal">
            <ul id="menu-skrollex-menu-1" class="menu"><li id="menu-item-796" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-796"><a href="../index.html#home">Home</a></li>
                <li id="menu-item-797" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-797"><a href="../index.html#about">About</a></li>
                <li id="menu-item-798" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-798"><a href="../index.html#team">Team</a></li>
                <li id="menu-item-799" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-799"><a href="../index.html#services">Services</a></li>
                <li id="menu-item-800" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-800"><a href="../index.html#work">Work</a></li>
                <li id="menu-item-801" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-801"><a href="../index.html#skills">Skills</a></li>
                <li id="menu-item-802" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-802"><a href="index.html">Blog</a></li>
                <li id="menu-item-803" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-803"><a href="../index.html#contact">Contact</a></li>
            </ul> <a class="responsive-nav" data-toggle="#off-canvas-right" data-toggle-class="open">
                <span class="l-menu"></span>
            </a> </nav>
        <div class="logo">
            <div class="site-description">
                <h3 class="sitename sitetitle"><a href="http://skrollex2.x40.ru/mary">SKROLL<span>EX</span></a></h3>
            </div>
        </div>
    </div>
</section>
<ul id="dot-scroll" class="colors-a no-colors-label"></ul>
<div class="overlay-window gallery-overlay colors-q" data-overlay-zoom=".fg">
    <div class="overlay-control">
        <a class="previos" href="#"></a>
        <a class="next" href="#"></a>
        <a class="cross" href="#"></a>
    </div>
    <div class="overlay-view scroll-bar">
        <div class="layout-boxed overlay-content">
        </div>
    </div>
    <ul class="loader">
        <li class="background-highlight"></li>
        <li class="background-highlight"></li>
        <li class="background-highlight"></li>
    </ul>
</div>
<div class="overlay-window map-overlay colors-q">
    <div class="overlay-control">
        <a class="cross" href="#"></a>
    </div>
    <div class="overlay-view scroll-bar">
    </div>
</div>
<div class="wrapper invert off-canvas-right scroll-bar colors-k" id="off-canvas-right">
    <a class="close-canvas" data-toggle="#off-canvas-right" data-toggle-class="open">
        <i class="l-close"></i>
        Close
    </a>
    <div class="content-box nav-mobile page-transition">
        <nav class="nav nav-vertical"><ul id="menu-skrollex-menu-2" class="menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-796"><a href="../index.html#home">Home</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-797"><a href="../index.html#about">About</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-798"><a href="../index.html#team">Team</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-799"><a href="../index.html#services">Services</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-800"><a href="../index.html#work">Work</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-801"><a href="../index.html#skills">Skills</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-802"><a href="index.html">Blog</a></li>
                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-803"><a href="../index.html#contact">Contact</a></li>
            </ul></nav> </div>
</div>
<section class="wrapper-site">
    <div class="ext-nav scroll-bar page-transition heading non-preloading background-t">
        <div class="view half-height">
            <img alt class="bg static" src="../wp-content/themes/skrollex/assets/preset-images/bg-john-kraus-2.jpg"/>
            <div class="fg no-top-padding no-bottom-padding  full-height">
                <div class="full-height">
                    <div class="pure-g full-height">
                        <a href="../theme-help/index.html" class="position-relative pure-u-1 pure-u-sm-12-24 colors-r full-height">
                            <div>
                                <span class="side-label highlight">Manual</span>
                                <span class="side-title heading">Help</span>
                            </div>
                        </a>
                        <a href="../index.html#process" class="position-relative pure-u-1 pure-u-sm-12-24 colors-s full-height">
                            <div>
                                <span class="side-label highlight">Our Workflow</span>
                                <span class="side-title heading">Process</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="half-height">
            <div class="pure-g full-height">
                <a href="../index.html#numbers" class="position-relative pure-u-1 pure-u-sm-12-24 pure-u-lg-8-24 colors-t full-height border-bottom border-right border-lite-t">
                    <div>
                        <span class="side-label highlight">Some Facts</span>
                        <span class="side-title heading">Numbers</span>
                    </div>
                </a>
                <a href="../index.html#how-we-work" class="position-relative pure-u-1 pure-u-sm-12-24 pure-u-lg-8-24 colors-t full-height border-bottom border-right border-lite-t">
                    <div>
                        <span class="side-label highlight">Awesome</span>
                        <span class="side-title heading">How We Work</span>
                    </div>
                </a>
                <a href="../index.html#who-we-are" class="position-relative pure-u-1 pure-u-sm-12-24 pure-u-lg-8-24 colors-t full-height border-bottom border-right border-lite-t">
                    <div>
                        <span class="side-label highlight">We Are Designers</span>
                        <span class="side-title heading">Who We Are</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <section id="wrapper-content" class="wrapper-content"> <div class="view x40-widget widget  " id="layers-widget-skrollex-section-2">
            <div data-src="http://skrollex2.x40.ru/mary/wp-content/uploads/sites/42/2015/11/bg-stocksnap-37450BE2D6.jpg" data-alt="" class="bg-holder"></div>
            <div data-src="http://skrollex2.x40.ru/mary/wp-content/uploads/sites/42/2015/11/bg-stocksnap-37450BE2D6.jpg" data-alt="" class="bg-holder"></div>
            <div class="fg colors-u ">
                <div class="layout-boxed section-top"><h3 class="heading-section-title">Our <span>Blog</span></h3>
                    <p class="header-details"><span>Skrollex</span> Theme</p>
                    <p class="header-caption">We are <span>Creative Team</span> located in Kalura, Bovlandia. Tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </div> </div>
        </div>
        <img class="bg" src="../wp-content/uploads/sites/42/2015/11/bg-picjumbo.com_IMG_4563.jpg" alt=""/>
        <img class="bg" src="../wp-content/uploads/sites/42/2015/11/bg-picjumbo.com_IMG_4563.jpg" alt=""/>
        <div class="default-page-wrapper background-v">
            <section class="layout-boxed archive">
                <div class="pure-g">
                    <div class="pure-u-1 pure-u-md-18-24">
                        <div class="pure-g">
                            <div class="post-meta pure-u-1 pure-u-md-6-24 text-right background-transparent colors-v">
                                <a href="../2016/04/23/post-about-life/index.html">
                                    <div class="post-day heading"><span>23</span></div>
                                    <div class="post-year heading">Apr 2016</div>
                                </a>
                                <div class="post-author"><i class="fa fa-user"></i> <a href="../author/admin/index.html" title="View all posts by admin" rel="author">admin</a></div>
                                <div class="post-comments"><i class="fa fa-comments-o"></i> <a href="../2016/04/23/post-about-life/index.html#comments">2 Comments</a></div>
                                <div class="post-categories"><i class="l-folder-open-o"></i> <a href="../category/web-design/index.html" title="View all posts in Web Design">Web Design</a></div> <div class="post-permalink"><i class="fa fa-link"></i> <a href="../2016/04/23/post-about-life/index.html" class="page-transition">Permalink</a></div>
                            </div>
                            <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
                                <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">
                                    <div class="post-image push-bottom"><a href="../2016/04/23/post-about-life/index.html"><img width="660" height="337" src="../wp-content/uploads/sites/42/2015/09/blog-AlagichKatya-13589984035_6eefe4a12b_o.jpg" class="attachment-large size-large" alt=""/></a></div> <h1 class="post-title"><a href="../2016/04/23/post-about-life/index.html">Post <span>About Life</span></a></h1>
                                    <div class="copy">
                                        <p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat [&hellip;]</p>
                                    </div>
                                    <p><a href="../2016/04/23/post-about-life/index.html" class="page-transition post-read-more">Read More...</a></p>
                                    <p class="post-tags"><i class="l-tags"></i> <a class="page-transition" href="../tag/design/index.html" title="View all posts tagged Design">Design</a>, <a class="page-transition" href="../tag/life/index.html" title="View all posts tagged Life">Life</a>, <a class="page-transition" href="../tag/music/index.html" title="View all posts tagged Music">Music</a></p> </article>
                            </div>
                        </div> <div class="pure-g">
                            <div class="post-meta pure-u-1 pure-u-md-6-24 text-right background-transparent colors-v">
                                <a href="../2016/04/23/my-next-post-3/index.html">
                                    <div class="post-day heading"><span>23</span></div>
                                    <div class="post-year heading">Apr 2016</div>
                                </a>
                                <div class="post-author"><i class="fa fa-user"></i> <a href="../author/admin/index.html" title="View all posts by admin" rel="author">admin</a></div>
                                <div class="post-comments"><i class="fa fa-comments-o"></i> <a href="../2016/04/23/my-next-post-3/index.html#respond">No Comments</a></div>
                                <div class="post-categories"><i class="l-folder-open-o"></i> <a href="../category/web-design/index.html" title="View all posts in Web Design">Web Design</a></div> <div class="post-permalink"><i class="fa fa-link"></i> <a href="../2016/04/23/my-next-post-3/index.html" class="page-transition">Permalink</a></div>
                            </div>
                            <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
                                <article id="post-240" class="post-240 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-life">
                                    <div class="post-image push-bottom"><a href="../2016/04/23/my-next-post-3/index.html"><img width="660" height="337" src="../wp-content/uploads/sites/42/2015/09/blog-AlagichKatya-13590018253_4a67596733_o.jpg" class="attachment-large size-large" alt=""/></a></div> <h1 class="post-title"><a href="../2016/04/23/my-next-post-3/index.html">My Next <span>Post</span></a></h1>
                                    <div class="copy">
                                        <p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat [&hellip;]</p>
                                    </div>
                                    <p><a href="../2016/04/23/my-next-post-3/index.html" class="page-transition post-read-more">Read More...</a></p>
                                    <p class="post-tags"><i class="l-tags"></i> <a class="page-transition" href="../tag/life/index.html" title="View all posts tagged Life">Life</a></p> </article>
                            </div>
                        </div> <div class="pure-g">
                            <div class="post-meta pure-u-1 pure-u-md-6-24 text-right background-transparent colors-v">
                                <a href="../2016/04/23/simple-example/index.html">
                                    <div class="post-day heading"><span>23</span></div>
                                    <div class="post-year heading">Apr 2016</div>
                                </a>
                                <div class="post-author"><i class="fa fa-user"></i> <a href="../author/admin/index.html" title="View all posts by admin" rel="author">admin</a></div>
                                <div class="post-comments"><i class="fa fa-comments-o"></i> <a href="../2016/04/23/simple-example/index.html#comments">1 Comment</a></div>
                                <div class="post-categories"><i class="l-folder-open-o"></i> <a href="../category/web-design/index.html" title="View all posts in Web Design">Web Design</a></div> <div class="post-permalink"><i class="fa fa-link"></i> <a href="../2016/04/23/simple-example/index.html" class="page-transition">Permalink</a></div>
                            </div>
                            <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
                                <article id="post-604" class="post-604 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-life tag-video">
                                    <div class="post-image push-bottom"><a href="../2016/04/23/simple-example/index.html"><img width="660" height="337" src="../wp-content/uploads/sites/42/2015/10/blog-AlagichKatya-14643751662_cc1f19e06b_o.jpg" class="attachment-large size-large" alt=""/></a></div> <h1 class="post-title"><a href="../2016/04/23/simple-example/index.html">Simple <span>Example</span></a></h1>
                                    <div class="copy">
                                        <p class="excerpt">Mauris in tincidunt sem. Vivamus eu lectus dictum, venenatis nunc sit amet, semper leo. Morbi ac sagittis sem. Vestibulum sit amet gravida tortor. Aenean placerat, ante et sodales vestibulum, ante mauris elementum felis, sit amet porta leo orci at ante. Fusce dictum elementum libero, non scelerisque ligula maximus nec. Nam porta condimentum ultrices. Curabitur suscipit [&hellip;]</p>
                                    </div>
                                    <p><a href="../2016/04/23/simple-example/index.html" class="page-transition post-read-more">Read More...</a></p>
                                    <p class="post-tags"><i class="l-tags"></i> <a class="page-transition" href="../tag/life/index.html" title="View all posts tagged Life">Life</a>, <a class="page-transition" href="../tag/video/index.html" title="View all posts tagged Video">Video</a></p> </article>
                            </div>
                        </div> <div class="pure-g">
                            <div class="post-meta pure-u-1 pure-u-md-6-24 text-right background-transparent colors-v">
                                <a href="../2016/04/23/next-important-text/index.html">
                                    <div class="post-day heading"><span>23</span></div>
                                    <div class="post-year heading">Apr 2016</div>
                                </a>
                                <div class="post-author"><i class="fa fa-user"></i> <a href="../author/admin/index.html" title="View all posts by admin" rel="author">admin</a></div>
                                <div class="post-comments"><i class="fa fa-comments-o"></i> <a href="../2016/04/23/next-important-text/index.html#respond">No Comments</a></div>
                                <div class="post-categories"><i class="l-folder-open-o"></i> <a href="../category/web-design/index.html" title="View all posts in Web Design">Web Design</a></div> <div class="post-permalink"><i class="fa fa-link"></i> <a href="../2016/04/23/next-important-text/index.html" class="page-transition">Permalink</a></div>
                            </div>
                            <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
                                <article id="post-238" class="post-238 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design">
                                    <div class="post-image push-bottom"><a href="../2016/04/23/next-important-text/index.html"><img width="660" height="337" src="../wp-content/uploads/sites/42/2015/09/blog-AlagichKatya-13433599034_67531e7c81_o.jpg" class="attachment-large size-large" alt=""/></a></div> <h1 class="post-title"><a href="../2016/04/23/next-important-text/index.html">Next Important <span>Text</span></a></h1>
                                    <div class="copy">
                                        <p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat [&hellip;]</p>
                                    </div>
                                    <p><a href="../2016/04/23/next-important-text/index.html" class="page-transition post-read-more">Read More...</a></p>
                                </article>
                            </div>
                        </div> <div class="pure-g">
                            <div class="post-meta pure-u-1 pure-u-md-6-24 text-right background-transparent colors-v">
                                <a href="../2016/04/23/simple-post/index.html">
                                    <div class="post-day heading"><span>23</span></div>
                                    <div class="post-year heading">Apr 2016</div>
                                </a>
                                <div class="post-author"><i class="fa fa-user"></i> <a href="../author/admin/index.html" title="View all posts by admin" rel="author">admin</a></div>
                                <div class="post-comments"><i class="fa fa-comments-o"></i> <a href="../2016/04/23/simple-post/index.html#respond">No Comments</a></div>
                                <div class="post-categories"><i class="l-folder-open-o"></i> <a href="../category/music/index.html" title="View all posts in My Music">My Music</a>, <a href="../category/web-design/index.html" title="View all posts in Web Design">Web Design</a></div> <div class="post-permalink"><i class="fa fa-link"></i> <a href="../2016/04/23/simple-post/index.html" class="page-transition">Permalink</a></div>
                            </div>
                            <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
                                <article id="post-236" class="post-236 post type-post status-publish format-standard has-post-thumbnail hentry category-music category-web-design tag-life tag-text">
                                    <div class="post-image push-bottom"><a href="../2016/04/23/simple-post/index.html"><img width="660" height="337" src="../wp-content/uploads/sites/42/2015/09/blog-AlagichKatya-13433599184_b1b9f6515b_o.jpg" class="attachment-large size-large" alt=""/></a></div> <h1 class="post-title"><a href="../2016/04/23/simple-post/index.html">Simple <span>Post</span></a></h1>
                                    <div class="copy">
                                        <p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat [&hellip;]</p>
                                    </div>
                                    <p><a href="../2016/04/23/simple-post/index.html" class="page-transition post-read-more">Read More...</a></p>
                                    <p class="post-tags"><i class="l-tags"></i> <a class="page-transition" href="../tag/life/index.html" title="View all posts tagged Life">Life</a>, <a class="page-transition" href="../tag/text/index.html" title="View all posts tagged Text">Text</a></p> </article>
                            </div>
                        </div> <div class="pure-g">
                            <div class="post-meta pure-u-1 pure-u-md-6-24 text-right background-transparent colors-v">
                                <a href="../2016/04/23/hello-world-2/index.html">
                                    <div class="post-day heading"><span>23</span></div>
                                    <div class="post-year heading">Apr 2016</div>
                                </a>
                                <div class="post-author"><i class="fa fa-user"></i> <a href="../author/admin/index.html" title="View all posts by admin" rel="author">admin</a></div>
                                <div class="post-comments"><i class="fa fa-comments-o"></i> <a href="../2016/04/23/hello-world-2/index.html#comments">24 Comments</a></div>
                                <div class="post-categories"><i class="l-folder-open-o"></i> <a href="../category/life-style/index.html" title="View all posts in Life Style">Life Style</a>, <a href="../category/web-design/index.html" title="View all posts in Web Design">Web Design</a></div> <div class="post-permalink"><i class="fa fa-link"></i> <a href="../2016/04/23/hello-world-2/index.html" class="page-transition">Permalink</a></div>
                            </div>
                            <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
                                <article id="post-63" class="post-63 post type-post status-publish format-standard has-post-thumbnail hentry category-life-style category-web-design">
                                    <div class="post-image push-bottom"><a href="../2016/04/23/hello-world-2/index.html"><img width="660" height="337" src="../wp-content/uploads/sites/42/2015/05/blog-AlagichKatya-13433341543_86ff2cf0cf_o.jpg" class="attachment-large size-large" alt=""/></a></div> <h1 class="post-title"><a href="../2016/04/23/hello-world-2/index.html">Hello <span>World!</span></a></h1>
                                    <div class="copy">
                                        <p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat [&hellip;]</p>
                                    </div>
                                    <p><a href="../2016/04/23/hello-world-2/index.html" class="page-transition post-read-more">Read More...</a></p>
                                </article>
                            </div>
                        </div> <div class="pure-g">
                            <div class="post-meta pure-u-1 pure-u-md-6-24 text-right background-transparent colors-v">
                                <a href="../2016/04/23/just-exampe/index.html">
                                    <div class="post-day heading"><span>23</span></div>
                                    <div class="post-year heading">Apr 2016</div>
                                </a>
                                <div class="post-author"><i class="fa fa-user"></i> <a href="../author/admin/index.html" title="View all posts by admin" rel="author">admin</a></div>
                                <div class="post-comments"><i class="fa fa-comments-o"></i> <a href="../2016/04/23/just-exampe/index.html#respond">No Comments</a></div>
                                <div class="post-categories"><i class="l-folder-open-o"></i> <a href="../category/music/index.html" title="View all posts in My Music">My Music</a>, <a href="../category/web-design/index.html" title="View all posts in Web Design">Web Design</a></div> <div class="post-permalink"><i class="fa fa-link"></i> <a href="../2016/04/23/just-exampe/index.html" class="page-transition">Permalink</a></div>
                            </div>
                            <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
                                <article id="post-234" class="post-234 post type-post status-publish format-standard has-post-thumbnail hentry category-music category-web-design tag-audio">
                                    <div class="post-image push-bottom"><a href="../2016/04/23/just-exampe/index.html"><img width="660" height="337" src="../wp-content/uploads/sites/42/2015/09/blog-AlagichKatya-14006989073_7d5548f83a_o.jpg" class="attachment-large size-large" alt=""/></a></div> <h1 class="post-title"><a href="../2016/04/23/just-exampe/index.html">Just <span>Exampe</span></a></h1>
                                    <div class="copy">
                                        <p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat [&hellip;]</p>
                                    </div>
                                    <p><a href="../2016/04/23/just-exampe/index.html" class="page-transition post-read-more">Read More...</a></p>
                                    <p class="post-tags"><i class="l-tags"></i> <a class="page-transition" href="../tag/audio/index.html" title="View all posts tagged Audio">Audio</a></p> </article>
                            </div>
                        </div> <div class="pure-g">
                            <div class="post-meta pure-u-1 pure-u-md-6-24 text-right background-transparent colors-v">
                                <a href="../2016/04/23/next-post-example/index.html">
                                    <div class="post-day heading"><span>23</span></div>
                                    <div class="post-year heading">Apr 2016</div>
                                </a>
                                <div class="post-author"><i class="fa fa-user"></i> <a href="../author/admin/index.html" title="View all posts by admin" rel="author">admin</a></div>
                                <div class="post-comments"><i class="fa fa-comments-o"></i> <a href="../2016/04/23/next-post-example/index.html#respond">No Comments</a></div>
                                <div class="post-categories"><i class="l-folder-open-o"></i> <a href="../category/music/index.html" title="View all posts in My Music">My Music</a>, <a href="../category/web-design/index.html" title="View all posts in Web Design">Web Design</a></div> <div class="post-permalink"><i class="fa fa-link"></i> <a href="../2016/04/23/next-post-example/index.html" class="page-transition">Permalink</a></div>
                            </div>
                            <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
                                <article id="post-232" class="post-232 post type-post status-publish format-standard has-post-thumbnail hentry category-music category-web-design tag-audio tag-photo tag-video">
                                    <div class="post-image push-bottom"><a href="../2016/04/23/next-post-example/index.html"><img width="660" height="337" src="../wp-content/uploads/sites/42/2015/09/blog-AlagichKatya-13433598114_a8461c586a_o.jpg" class="attachment-large size-large" alt=""/></a></div> <h1 class="post-title"><a href="../2016/04/23/next-post-example/index.html">Next Post <span>Example</span></a></h1>
                                    <div class="copy">
                                        <p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat [&hellip;]</p>
                                    </div>
                                    <p><a href="../2016/04/23/next-post-example/index.html" class="page-transition post-read-more">Read More...</a></p>
                                    <p class="post-tags"><i class="l-tags"></i> <a class="page-transition" href="../tag/audio/index.html" title="View all posts tagged Audio">Audio</a>, <a class="page-transition" href="../tag/photo/index.html" title="View all posts tagged Photo">Photo</a>, <a class="page-transition" href="../tag/video/index.html" title="View all posts tagged Video">Video</a></p> </article>
                            </div>
                        </div> <div class="pure-g">
                            <div class="post-meta pure-u-1 pure-u-md-6-24 text-right background-transparent colors-v">
                                <a href="../2016/04/23/post-example-2/index.html">
                                    <div class="post-day heading"><span>23</span></div>
                                    <div class="post-year heading">Apr 2016</div>
                                </a>
                                <div class="post-author"><i class="fa fa-user"></i> <a href="../author/admin/index.html" title="View all posts by admin" rel="author">admin</a></div>
                                <div class="post-comments"><i class="fa fa-comments-o"></i> <a href="../2016/04/23/post-example-2/index.html#respond">No Comments</a></div>
                                <div class="post-categories"><i class="l-folder-open-o"></i> <a href="../category/web-design/index.html" title="View all posts in Web Design">Web Design</a></div> <div class="post-permalink"><i class="fa fa-link"></i> <a href="../2016/04/23/post-example-2/index.html" class="page-transition">Permalink</a></div>
                            </div>
                            <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
                                <article id="post-230" class="post-230 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-audio tag-photo tag-text tag-video">
                                    <div class="post-image push-bottom"><a href="../2016/04/23/post-example-2/index.html"><img width="660" height="337" src="../wp-content/uploads/sites/42/2015/09/blog-AlagichKatya-14097477996_7b7cfb3a20_o.jpg" class="attachment-large size-large" alt=""/></a></div> <h1 class="post-title"><a href="../2016/04/23/post-example-2/index.html">Post <span>Example</span></a></h1>
                                    <div class="copy">
                                        <p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat [&hellip;]</p>
                                    </div>
                                    <p><a href="../2016/04/23/post-example-2/index.html" class="page-transition post-read-more">Read More...</a></p>
                                    <p class="post-tags"><i class="l-tags"></i> <a class="page-transition" href="../tag/audio/index.html" title="View all posts tagged Audio">Audio</a>, <a class="page-transition" href="../tag/photo/index.html" title="View all posts tagged Photo">Photo</a>, <a class="page-transition" href="../tag/text/index.html" title="View all posts tagged Text">Text</a>, <a class="page-transition" href="../tag/video/index.html" title="View all posts tagged Video">Video</a></p> </article>
                            </div>
                        </div> <div class="pure-g">
                            <div class="post-meta pure-u-1 pure-u-md-6-24 text-right background-transparent colors-v">
                                <a href="../2016/04/23/my-blog-post/index.html">
                                    <div class="post-day heading"><span>23</span></div>
                                    <div class="post-year heading">Apr 2016</div>
                                </a>
                                <div class="post-author"><i class="fa fa-user"></i> <a href="../author/admin/index.html" title="View all posts by admin" rel="author">admin</a></div>
                                <div class="post-comments"><i class="fa fa-comments-o"></i> <a href="../2016/04/23/my-blog-post/index.html#respond">No Comments</a></div>
                                <div class="post-categories"><i class="l-folder-open-o"></i> <a href="../category/web-design/index.html" title="View all posts in Web Design">Web Design</a></div> <div class="post-permalink"><i class="fa fa-link"></i> <a href="../2016/04/23/my-blog-post/index.html" class="page-transition">Permalink</a></div>
                            </div>
                            <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
                                <article id="post-228" class="post-228 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-audio tag-design tag-music tag-photo tag-text tag-video">
                                    <div class="post-image push-bottom"><a href="../2016/04/23/my-blog-post/index.html"><img width="660" height="337" src="../wp-content/uploads/sites/42/2015/09/blog-AlagichKatya-16400357806_498e97166e_o.jpg" class="attachment-large size-large" alt=""/></a></div> <h1 class="post-title"><a href="../2016/04/23/my-blog-post/index.html">My Blog <span>Post</span></a></h1>
                                    <div class="copy">
                                        <p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat [&hellip;]</p>
                                    </div>
                                    <p><a href="../2016/04/23/my-blog-post/index.html" class="page-transition post-read-more">Read More...</a></p>
                                    <p class="post-tags"><i class="l-tags"></i> <a class="page-transition" href="../tag/audio/index.html" title="View all posts tagged Audio">Audio</a>, <a class="page-transition" href="../tag/design/index.html" title="View all posts tagged Design">Design</a>, <a class="page-transition" href="../tag/music/index.html" title="View all posts tagged Music">Music</a>, <a class="page-transition" href="../tag/photo/index.html" title="View all posts tagged Photo">Photo</a>, <a class="page-transition" href="../tag/text/index.html" title="View all posts tagged Text">Text</a>, <a class="page-transition" href="../tag/video/index.html" title="View all posts tagged Video">Video</a></p> </article>
                            </div>
                        </div> </div>
                    <div class="colors-v background-transparent sidebar pure-u-1 pure-u-md-6-24">
                        <aside id="recent-posts-3" class="content well push-bottom-large widget widget_recent_entries"> <h5 class="section-nav-title">Recent Posts</h5> <ul>
                                <li>
                                    <a href="../2016/04/23/post-about-life/index.html">Post About Life</a>
                                </li>
                                <li>
                                    <a href="../2016/04/23/my-next-post-3/index.html">My Next Post</a>
                                </li>
                                <li>
                                    <a href="../2016/04/23/simple-example/index.html">Simple Example</a>
                                </li>
                                <li>
                                    <a href="../2016/04/23/next-important-text/index.html">Next Important Text</a>
                                </li>
                                <li>
                                    <a href="../2016/04/23/simple-post/index.html">Simple Post</a>
                                </li>
                            </ul>
                        </aside> <aside id="search-3" class="content well push-bottom-large widget widget_search"><form role="search" method="get" class="search-form" action="http://skrollex2.x40.ru/mary/">
                                <label>
                                    <span class="screen-reader-text">Search for:</span>
                                    <input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s"/>
                                </label>
                                <input type="submit" class="search-submit" value="Search"/>
                            </form></aside><aside id="categories-3" class="content well push-bottom-large widget widget_categories"><h5 class="section-nav-title">Categories</h5> <ul>
                                <li class="cat-item cat-item-4"><a href="../category/life-style/index.html">Life Style</a>
                                </li>
                                <li class="cat-item cat-item-6"><a href="../category/music/index.html">My Music</a>
                                </li>
                                <li class="cat-item cat-item-5"><a href="../category/web-design/index.html">Web Design</a>
                                </li>
                            </ul>
                        </aside><aside id="tag_cloud-1" class="content well push-bottom-large widget widget_tag_cloud"><h5 class="section-nav-title">Tags</h5><div class="tagcloud"><a href='../tag/audio/index.html' class='tag-link-8 tag-link-position-1' title='4 topics' style='font-size: 22pt;'>Audio</a>
                                <a href='../tag/design/index.html' class='tag-link-9 tag-link-position-2' title='2 topics' style='font-size: 8pt;'>Design</a>
                                <a href='../tag/life/index.html' class='tag-link-13 tag-link-position-3' title='4 topics' style='font-size: 22pt;'>Life</a>
                                <a href='../tag/music/index.html' class='tag-link-10 tag-link-position-4' title='2 topics' style='font-size: 8pt;'>Music</a>
                                <a href='../tag/photo/index.html' class='tag-link-11 tag-link-position-5' title='3 topics' style='font-size: 15.636363636364pt;'>Photo</a>
                                <a href='../tag/text/index.html' class='tag-link-7 tag-link-position-6' title='4 topics' style='font-size: 22pt;'>Text</a>
                                <a href='../tag/video/index.html' class='tag-link-12 tag-link-position-7' title='4 topics' style='font-size: 22pt;'>Video</a></div>
                        </aside><aside id="calendar-1" class="content well push-bottom-large widget widget_calendar"><div id="calendar_wrap" class="calendar_wrap"><table id="wp-calendar">
                                    <caption>August 2017</caption>
                                    <thead>
                                    <tr>
                                        <th scope="col" title="Monday">M</th>
                                        <th scope="col" title="Tuesday">T</th>
                                        <th scope="col" title="Wednesday">W</th>
                                        <th scope="col" title="Thursday">T</th>
                                        <th scope="col" title="Friday">F</th>
                                        <th scope="col" title="Saturday">S</th>
                                        <th scope="col" title="Sunday">S</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <td colspan="3" id="prev"><a href="../2016/04/index.html">&laquo; Apr</a></td>
                                        <td class="pad">&nbsp;</td>
                                        <td colspan="3" id="next" class="pad">&nbsp;</td>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <tr>
                                        <td colspan="1" class="pad">&nbsp;</td><td>1</td><td id="today">2</td><td>3</td><td>4</td><td>5</td><td>6</td>
                                    </tr>
                                    <tr>
                                        <td>7</td><td>8</td><td>9</td><td>10</td><td>11</td><td>12</td><td>13</td>
                                    </tr>
                                    <tr>
                                        <td>14</td><td>15</td><td>16</td><td>17</td><td>18</td><td>19</td><td>20</td>
                                    </tr>
                                    <tr>
                                        <td>21</td><td>22</td><td>23</td><td>24</td><td>25</td><td>26</td><td>27</td>
                                    </tr>
                                    <tr>
                                        <td>28</td><td>29</td><td>30</td><td>31</td>
                                        <td class="pad" colspan="3">&nbsp;</td>
                                    </tr>
                                    </tbody>
                                </table></div></aside> </div>
                </div>
                <div class="colors-v background-transparent">
                    <nav class="navigation pagination" role="navigation">
                        <h2 class="screen-reader-text">Posts navigation</h2>
                        <div class="nav-links"><span class='page-numbers current'>1</span>
                            <a class='page-numbers' href='page/2/index.html'>2</a>
                            <a class='page-numbers' href='page/3/index.html'>3</a>
                            <a class="next page-numbers" href="page/2/index.html">Next</a></div>
                    </nav> </div>
            </section>
        </div>
    </section>
    <footer id="footer" class="animated page-transition non-preloading">
        <div class="footer-widgets   clearfix">
            <div class="pure-g">
                <div class="pure-u-1 pure-u-md-24-24">
                    <div class="view x40-widget widget  " id="layers-widget-skrollex-section-1">
                        <div class="fg colors-o  no-bottom-padding">
                            <div class="section-cols layout-boxed">
                                <div class="pure-g"> <div class="layers-widget-skrollex-section-5605431a66a7f615865511 pure-u-12-24 pure-u-md-6-24  col-padding">
                                        <h5 class="heading-col-title">Skroll<span>ex</span></h5>
                                        <p>Mauris metus tellus, lacinia nec efficitur quis, rhoncus et mi.</p>
                                        <h5 class="heading-col-title"><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i> <i class="fa fa-twitter"></i></a> <a href="http://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a> <a href="https://github.com/" target="_blank"><i class="fa fa-github"></i></a> <a href="https://vimeo.com/"><i class="fa fa-vimeo-square"></i></a> <a href="https://www.linkedin.com/"><i class="fa linkedin"></i></a></h5>
                                    </div> <div class="layers-widget-skrollex-section-5605450875fbd690551588 pure-u-12-24 pure-u-md-6-24  col-padding">
                                        <h6 class="heading-block-title">Additional Links</h6>
                                        <ul>
                                            <li><a href="http://themeforest.net/user/x40/portfolio?ref=x40" target="_blank">Our Portfolio on Envato</a></li>
                                            <li><a href="http://themeforest.net/user/x40?ref=x40" target="_blank">Contact Us on ThemeForest</a></li>
                                            <li><a href="feed://themeforest.net/feeds/users/x40.atom?ref=x40" target="_blank">x40&#8217;s new item RSS feed</a></li>
                                            <li><a href="http://www.x40.ru/" target="_blank">Our Other Site</a></li>
                                            <li><a href="mailto:address@gmail.com">Our Email</a></li>
                                        </ul>
                                    </div> <div class="layers-widget-skrollex-section-5605450f3001b812805779 pure-u-12-24 pure-u-md-6-24  col-padding">
                                        <h6 class="heading-block-title">Recent Posts</h6>
                                        <ul><li><a href="../2016/04/23/post-about-life/index.html">Post About Life</a></li><li><a href="../2016/04/23/my-next-post-3/index.html">My Next Post</a></li><li><a href="../2016/04/23/simple-example/index.html">Simple Example</a></li><li><a href="../2016/04/23/next-important-text/index.html">Next Important Text</a></li><li><a href="../2016/04/23/simple-post/index.html">Simple Post</a></li><li><a href="index.html">More...</a></li></ul>
                                    </div> <div class="layers-widget-skrollex-section-56054514d6dd9579769748 pure-u-12-24 pure-u-md-6-24  col-padding">
                                        <h6 class="heading-block-title">Photo Stream</h6>
                                        <p class="photo-stream"><a href="https://www.flickr.com/photos/we-are-envato/12333983853" target="_blank"><img alt="" src="../../../farm8.staticflickr.com/7367/12333983853_8fc462a494_q.jpg" style="width: 81px; height: 81px; margin: 3px;"/></a><a href="https://www.flickr.com/photos/we-are-envato/13090549273" target="_blank"><img alt="" src="../../../farm8.staticflickr.com/7325/13090549273_ee5c732ce1_q.jpg" style="margin: 3px; width: 81px; height: 81px;"/></a><a href="https://www.flickr.com/photos/we-are-envato/12333834195" target="_blank"><img alt="" src="../../../farm4.staticflickr.com/3798/12333834195_7dcb472dd5_q.jpg" style="margin: 3px; width: 81px; height: 81px;"/></a><a href="https://www.flickr.com/photos/we-are-envato/13090422515" target="_blank"><img alt="" src="../../../farm3.staticflickr.com/2279/13090422515_cabc9c447c_q.jpg" style="margin: 3px; width: 81px; height: 81px;"/></a><a href="https://www.flickr.com/photos/we-are-envato/13090762874" target="_blank"><img alt="" src="../../../farm4.staticflickr.com/3596/13090762874_5f9d99f4e5_q.jpg" style="margin: 3px; width: 81px; height: 81px;"/></a><a href="https://www.flickr.com/photos/we-are-envato/13090456565" target="_blank"><img alt="" src="../../../farm4.staticflickr.com/3715/13090456565_df81e68627_q.jpg" style="margin: 3px; width: 81px; height: 81px;"/></a></p>
                                    </div></div> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom colors-p">
                <div class="text-center"> Made at the tip of Africa. &copy;</div>
            </div>
        </div>
    </footer>
</section>
<script type='text/javascript' src='../wp-content/plugins/contact-form-7/includes/js/jquery.form.mind03d.js?ver=3.51.0-2014.06.20'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpcf7 = {"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
    /* ]]> */
</script>
<script type='text/javascript' src='assets/js/scripts4906.b64315176b0dd79e.b64315176b0dd79e.b64315176b0dd79e.b64315176b0dd79e.js?ver=4.7'></script>
<script type='text/javascript' src='assets/css/bower_components/jquery-cookie/jquery.cookief4ef.16387a76475a91af.16387a76475a91af.16387a76475a91af.16387a76475a91af.js?ver=27e1af5b410de903825f58485ec0fd40'></script>
<script type='text/javascript' src='assets/js/imagesloaded.min55a0.d0c2c0d7e37652e6.d0c2c0d7e37652e6.d0c2c0d7e37652e6.d0c2c0d7e37652e6.js?ver=3.2.0'></script>
<script type='text/javascript' src='assets/js/masonry.mind617.5420b6516c14245b.5420b6516c14245b.5420b6516c14245b.5420b6516c14245b.js?ver=3.3.2'></script>
<script type='text/javascript' src='assets/lib/bower_components/less/dist/less.minf4ef.ed926034cc41d320.ed926034cc41d320.ed926034cc41d320.js.ed926034cc41d320.js?ver=27e1af5b410de903825f58485ec0fd40'></script>
<script type='text/javascript' src='assets/lib/tween/tween.minf4ef.0333be8d916c9e91.0333be8d916c9e91.0333be8d916c9e91.0333be8d916c9e91.js?ver=27e1af5b410de903825f58485ec0fd40'></script>
<script type='text/javascript' src='assets/lib/bower_components/modernizr/modernizrf4ef.e741a78646adb336.e741a78646adb336.e741a78646adb336.e741a78646adb336.js?ver=27e1af5b410de903825f58485ec0fd40'></script>
<script type='text/javascript' src='assets/lib/bower_components/vimeo-player-js/dist/player.minf4ef.2efb021ec4088975.2efb021ec4088975.2efb021ec4088975.2efb021ec4088975.js?ver=27e1af5b410de903825f58485ec0fd40'></script>
<script type='text/javascript' src='assets/lib/bower_components/snap.svg/dist/snap.svg-minf4ef.b92f25bc9c56c69a.b92f25bc9c56c69a.b92f25bc9c56c69a.b92f25bc9c56c69a.js?ver=27e1af5b410de903825f58485ec0fd40'></script>
<script type='text/javascript' src='assets/lib/bower_components/minicolors/jquery.minicolors.minf4ef.9e5315b8ba5ce983.9e5315b8ba5ce983.9e5315b8ba5ce983.9e5315b8ba5ce983.js?ver=27e1af5b410de903825f58485ec0fd40'></script>
<script type='text/javascript' src='assets/lib/bower_components/textillate/assets/jquery.letteringf4ef.4434237a4aaf85c2.4434237a4aaf85c2.4434237a4aaf85c2.4434237a4aaf85c2.js?ver=27e1af5b410de903825f58485ec0fd40'></script>
<script type='text/javascript' src='assets/lib/bower_components/textillate/assets/jquery.fittextf4ef.17871d671095bc41.17871d671095bc41.17871d671095bc41.17871d671095bc41.js?ver=27e1af5b410de903825f58485ec0fd40'></script>
<script type='text/javascript' src='assets/lib/bower_components/textillate/jquery.textillatef4ef.7bce45002ec5ec6d.7bce45002ec5ec6d.7bce45002ec5ec6d.7bce45002ec5ec6d.js?ver=27e1af5b410de903825f58485ec0fd40'></script>
<script type='text/javascript' src='http://maps.googleapis.com/maps/api/js?ver=27e1af5b410de903825f58485ec0fd40'></script>
<script type='text/javascript' src='assets/lib/stringencoders-v3.10.3/javascript/base64f4ef.a3f04aa72c4525a4.a3f04aa72c4525a4.a3f04aa72c4525a4.a3f04aa72c4525a4.js?ver=27e1af5b410de903825f58485ec0fd40'></script>
<script type='text/javascript' src='assets/js/script-bundle.minf4ef.e48abb28eac1a191.e48abb28eac1a191.e48abb28eac1a191.e48abb28eac1a191.js?ver=27e1af5b410de903825f58485ec0fd40'></script>
<script type='text/javascript' src='assets/lib/easy-fancybox/fancybox/jquery.fancybox-1.3.8.min82fc.5fec00649cf6e3fb.5fec00649cf6e3fb.5fec00649cf6e3fb.5fec00649cf6e3fb.js?ver=1.5.8.2'></script>
<script type='text/javascript' src='assets/lib/easy-fancybox/js/jquery.easing.min9e1e.508803480aeda9d6.508803480aeda9d6.508803480aeda9d6.508803480aeda9d6.js?ver=1.3.2'></script>
<script type='text/javascript' src='assets/lib/easy-fancybox/js/jquery.mousewheel.min4830.639d1c35a685d111.639d1c35a685d111.639d1c35a685d111.639d1c35a685d111.js?ver=3.1.12'></script>
<script type="text/javascript">
    jQuery(document).on('ready post-load', function(){ jQuery('.nofancybox,a.pin-it-button,a[href*="pinterest.com/pin/create/button"]').addClass('nolightbox'); });
    jQuery(document).on('ready post-load',easy_fancybox_handler);
    jQuery(document).on('ready',easy_fancybox_auto);</script>
</body>

<!-- Mirrored from skrollex2.x40.ru/mary/blog/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 02 Aug 2017 07:36:21 GMT -->
</html>