<div class="view x40-widget widget   hide-on-small-devices" id="layers-widget-skrollex-section-4">
    <!--    <div data-src="assets/images/bg_under_top.f254d2d066a29939.f254d2d066a29939.f254d2d066a29939.f254d2d066a29939.jpg" data-alt="" class="bg-holder"></div>-->
    <div class="fg colors-none  no-top-padding no-bottom-padding">
        <div class="section-cols layout-fullwidth">
            <div class="pure-g">
                <div class="layers-widget-skrollex-section-55d8cbf47a484111710171 pure-u-1 pure-u-md-8-24 individual_business">
                    <div class="banner-box background-l heading-l">
                        <div class="banner-cell">
                            <div class="banner-subtitle">Build your Brand, Target Customers, Market Segmentation, Monetize Market Demand, Increase Product Sells</div>
                        </div>
                        <div class="banner-cell success-background"><h2>Individual / Startup</h2>
                            <h4>Business</h4>
                        </div>
                    </div>
                </div>
                <div class="layers-widget-skrollex-section-55d8ecd2b32e1520720461 pure-u-1 pure-u-md-8-24  ">
                    <div class="banner-box background-m heading-m">
                        <div class="banner-cell">
                            <div class="banner-subtitle">Developed Brand, Target Customer, Increased sales, Compete your Competitors, Analysis</div>
                        </div>
                        <div class="banner-cell warning-background"><h2>SME</h2>
                            <h4>Small and Medium Size Enterprise</h4>
                        </div>
                    </div>
                </div>
                <div class="layers-widget-skrollex-section-55d8ecd934c82285362295 pure-u-1 pure-u-md-8-24  ">
                    <div class="banner-box background-n heading-n">
                        <div class="banner-cell">
                            <div class="banner-subtitle">Value Increase, More Sales, Growth Social Awareness, Be the Brand</div>
                        </div>
                        <div class="banner-cell info-background"><h2>MNC</h2>
                            <h4>Multi National Company</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>