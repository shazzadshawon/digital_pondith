<section id="our_web" class="layout-boxed archive">
    <div class="pure-g">
        <div class="pure-u-1 pure-u-md-20-24">
            <div class="view x40-widget widget   text-bg peraRemoval" id="layers-widget-skrollex-section-9" data-text-effect-selector="h1,h2,h3,h4" data-text-effect="effect-a-animated">

                <div class="pure-g">
                    <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">

                            <h1 class="post-title">
                                <a href="#">How our <span>Search Engine Marketing</span> team can help</a>
                            </h1>
                            <div class="copy">
                                <p class="excerpt">We are an experienced and brilliant team of passionate mentors who live and breathe search engine marketing. We have urbanized search strategies for leading brands to all kind of business (startup, SME and MNC sized businesses) across many industries in the USA, UK, Canada, Thailand, Malaysia, India, Bangladesh and worldwide. We believe in building long term relationships with our potential clients, based upon shared ideals and success. Our SEO marketing services provides the following and more:</p>
                            </div>
                        </article>
                    </div>
                </div>

                <div class="pure-g">
                    <div class="post-meta pure-u-1 pure-u-md-8-24 text-right background-transparent colors-v">
                        <a href="#">
                            <div class="post-day heading"><span>Search</span></div>
                            <div class="post-year heading">Engine Marketing</div>
                        </a>
                    </div>
                    <div class="colors-w post-body pure-u-1 pure-u-md-16-24 article-blog">
                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">
                            <div class="post-image push-bottom">
                                <a href="#">
                                    <img width="100" height="100" src="assets/data/services/web/web_sem.870ccd6940d92020.870ccd6940d92020.0d8304f6f73706fd.png" class="img-responsive" alt="Social Consultancy icon"/>
                                </a>
                            </div>
                            <!--                                        <h1 class="post-title">-->
                            <!--                                            <a href="#">Post <span>About Life</span></a>-->
                            <!--                                        </h1>-->
                            <div class="copy">
                                <p class="excerpt">We work closely with you to value your unique businesses objectives & challenges, before developing a modified search engine marketing strategy for your brand. We will counsel on the best approach, whether it's PPC, SEO services or a combination.</p>
                            </div>
                        </article>
                    </div>
                </div>

                <div class="pure-g">
                    <div class="post-meta pure-u-1 pure-u-md-8-24 text-right background-transparent colors-v">
                        <a href="#">
                            <div class="post-day heading"><span>Search</span></div>
                            <div class="post-year heading">Engine Optimization</div>
                        </a>
                    </div>
                    <div class="colors-w post-body pure-u-1 pure-u-md-16-24 article-blog">
                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">
                            <div class="post-image push-bottom">
                                <a href="#">
                                    <img width="100" height="100" src="assets/data/services/web/web_seo.e032095d1825beff.e032095d1825beff.b2db1850c5830040.png" class="img-responsive" alt="Social Consultancy icon"/>
                                </a>
                            </div>
                            <div class="copy">
                                <p class="excerpt">Every business wants to be top of the search results, but you have to ought to be there. Our SEO service is intended to make you remarkable, increasing visibility within the organic search results to deliver targeted traffic to your website in the long term.</p>
                            </div>
                        </article>
                    </div>
                </div>

                <div class="pure-g">
                    <div class="post-meta pure-u-1 pure-u-md-8-24 text-right background-transparent colors-v">
                        <a href="#">
                            <div class="post-day heading"><span>Pay-Per-Click</span></div>
                            <div class="post-year heading">(PPC) Management</div>
                        </a>
                    </div>
                    <div class="colors-w post-body pure-u-1 pure-u-md-16-24 article-blog">
                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">
                            <div class="post-image push-bottom">
                                <a href="#">
                                    <img width="100" height="100" src="assets/data/services/web/web_ppc.3512d38672a14a86.3512d38672a14a86.4bbff7580ed4567c.png" class="img-responsive" alt="Social Consultancy icon"/>
                                </a>
                            </div>
                            <div class="copy">
                                <p class="excerpt">PPC advertising gives businesses the prospect to appear within the search results straight away &amp; provides the promotional with complete control over the keywords, budget &amp; adverts. The ability to track translation &amp; attribute value means it is one of the most effective ways of reaching your audience.</p>
                            </div>
                        </article>
                    </div>
                </div>

                <div class="pure-g">
                    <div class="post-meta pure-u-1 pure-u-md-8-24 text-right background-transparent colors-v">
                        <a href="#">
                            <div class="post-day heading"><span>Content Strategy</span></div>
                            <div class="post-year heading">&amp; Marketing</div>
                        </a>
                    </div>
                    <div class="colors-w post-body pure-u-1 pure-u-md-16-24 article-blog">
                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">
                            <div class="post-image push-bottom">
                                <a href="#">
                                    <img width="100" height="100" src="assets/data/services/web/web_csm.2db4ea00ac28478e.2db4ea00ac28478e.fdc801fccb1ce1f3.png" class="img-responsive" alt="Social Consultancy icon"/>
                                </a>
                            </div>
                            <div class="copy">
                                <p class="excerpt">Creating convincing &amp; valuable content, both distributing &amp; onsite it through a multiplicity of online channels, is essential to reach your target audience &amp; persuade them. Our team of creative’s will work with your brand, increasing content strategy built around your own customer's personas &amp; journey.</p>
                            </div>
                        </article>
                    </div>
                </div>

                <div class="pure-g">
                    <div class="post-meta pure-u-1 pure-u-md-8-24 text-right background-transparent colors-v">
                        <a href="#">
                            <div class="post-day heading"><span>Social Media</span></div>
                            <div class="post-year heading">Strategy</div>
                        </a>
                    </div>
                    <div class="colors-w post-body pure-u-1 pure-u-md-16-24 article-blog">
                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">
                            <div class="post-image push-bottom">
                                <a href="#">
                                    <img width="100" height="100" src="assets/data/services/web/web_smm.1c3a935ab7a8765c.1c3a935ab7a8765c.f25f91154326bab6.png" class="img-responsive" alt="Social Consultancy icon"/>
                                </a>
                            </div>
                            <div class="copy">
                                <p class="excerpt">We enable you to set clear social networking targets without taking possession or dealing with your records straightforwardly. This empowers you to draw in with your group of onlookers crosswise over informal organizations and stages, creating important connections and affecting practices for your brand.</p>
                                <p class="excerpt">We help you set clear social media objectives without taking ownership or managing your accounts directly. This enables you to engage with your audience across social networks & platforms, developing meaningful relationships & influencing behaviors for your brand.</p>
                            </div>
                        </article>
                    </div>
                </div>

                <div class="pure-g">
                    <div class="post-meta pure-u-1 pure-u-md-8-24 text-right background-transparent colors-v">
                        <a href="#">
                            <div class="post-day heading"><span>Analysis, Analytics</span></div>
                            <div class="post-year heading">&amp; Reporting</div>
                        </a>
                    </div>
                    <div class="colors-w post-body pure-u-1 pure-u-md-16-24 article-blog">
                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">
                            <div class="post-image push-bottom">
                                <a href="#">
                                    <img width="100" height="100" src="assets/data/services/web/web_aar.b600978c9e56b91f.b600978c9e56b91f.0be5c49fd9b0ef17.png" class="img-responsive" alt="Social Consultancy icon"/>
                                </a>
                            </div>
                            <div class="copy">
                                <p class="excerpt">We love to analyse data to find insights, whether it's about your users, customer journey or attribution. We believe this is the only way to get the very best out of your search engine marketing campaigns & understanding their real value.</p>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>