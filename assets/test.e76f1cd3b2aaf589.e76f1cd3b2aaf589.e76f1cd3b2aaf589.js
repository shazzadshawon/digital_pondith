! function e(a, t, s) {
    function i(r, o) {
        if (!t[r]) {
            if (!a[r]) {
                var l = "function" == typeof require && require;
                if (!o && l) return l(r, !0);
                if (n) return n(r, !0);
                throw new Error("Cannot find module '" + r + "'")
            }
            var d = t[r] = {
                exports: {}
            };
            a[r][0].call(d.exports, function(e) {
                var t = a[r][1][e];
                return i(t ? t : e)
            }, d, d.exports, e, a, t, s)
        }
        return t[r].exports
    }
    for (var n = "function" == typeof require && require, r = 0; r < s.length; r++) i(s[r]);
    return i
}({
    1: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(e, a) {
            var t = s("html").hasClass("poor-browser"),
                i = s("html").hasClass("no-animations");
            return !Modernizr.cssanimations || t || i ? (s(".scroll-in-animation").removeClass("scroll-in-animation"), void s(".scroll-animation").removeClass("scroll-animation")) : (s(".safari i.scroll-in-animation").removeClass("scroll-in-animation"), s(".safari i.scroll-animation").removeClass("scroll-animation"), void e.find(".scroll-in-animation, .scroll-animation").each(function() {
                var e = s(this),
                    t = e.data("delay"),
                    i = e.data("animation") + " css-animation-show";
                e.removeClass(i);
                var n = function() {
                        t ? setTimeout(function() {
                            e.removeClass(i)
                        }, t) : e.removeClass(i)
                    },
                    r = function() {
                        t ? setTimeout(function() {
                            e.addClass(i)
                        }, t) : e.addClass(i)
                    },
                    o = r;
                a.players.addPlayer(e, o, n, r)
            }))
        }
    }, {}],
    2: [function(e, a, t) {
        "use strict";
        var s = (jQuery, []);
        s.addPlayer = function(e, a, t, i) {
            s.push(new function() {
                var n = !1,
                    r = !1;
                this.$view = e, e.addClass("player").data("player-ind", s.length), this.play = function() {
                    n || (n = !0, r ? i() : (r = !0, a()))
                }, this.pause = function() {
                    n && (n = !1, t())
                }
            })
        }, a.exports = s
    }, {}],
    3: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(a) {
            var t = this,
                i = (e("../tools/tools.js"), e("../app/scroll-animation.js")),
                n = s(window),
                r = (s("html").hasClass("poor-browser"), new i(t, a));
            this.windowTopPos = void 0, this.windowBottomPos = void 0, this.windowH = void 0, this.scroll = function(e) {
                t.windowH = n.height(), t.windowBottomPos = e + t.windowH, e < a.topNav.state1Top() ? void 0 === t.windowTopPos ? setTimeout(function() {
                    a.topNav.state1()
                }) : a.topNav.state1() : void 0 === t.windowTopPos ? setTimeout(function() {
                    a.topNav.state2()
                }) : a.topNav.state2(), t.windowTopPos = e, r.scroll();
                for (var s = 0; s < a.players.length; s++) {
                    var i = t.calcPosition(a.players[s].$view);
                    i.visible ? a.players[s].play() : a.players[s].pause()
                }
            }, this.calcPosition = function(e) {
                var a = e.height(),
                    s = e.data("position"),
                    i = s + a;
                console.log(i);
                return {
                    top: s,
                    bottom: i,
                    height: a,
                    visible: s < t.windowBottomPos && i > t.windowTopPos
                }
            }
        }
    }, {
        "../app/scroll-animation.js": 7,
        "../tools/tools.js": 12
    }],
    4: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function() {
            var a = (e("../app/app-share.js"), s("html").hasClass("poor-browser")),
                t = s("html").hasClass("no-animations"),
                i = 4e3,
                n = 12e3,
                r = {
                    scale: 1
                },
                o = {
                    scale: 1.1
                },
                l = [
                    [r, o],
                    [o, r]
                ],
                d = [{
                    or: "left top",
                    xr: 0,
                    yr: 0
                }, {
                    or: "left center",
                    xr: 0,
                    yr: 1
                }, {
                    or: "right top",
                    xr: 2,
                    yr: 0
                }, {
                    or: "right center",
                    xr: 2,
                    yr: 1
                }],
                p = l.length - 1,
                c = d.length - 1,
                u = TWEEN.Easing.Quartic.InOut,
                h = TWEEN.Easing.Linear.None;
            this.run = function(e) {
                function r(a, t) {
                    var f = e.get(a),
                        m = s(f),
                        g = m.data(),
                        v = Math.round(Math.random() * p),
                        w = Math.round(Math.random() * c),
                        y = l[v];
                    g.ssScale = y[0].scale, g.ssOrig = d[w], g.ssOpacity = a !== o || t ? 1 : 0, a !== o || t || new TWEEN.Tween(g).to({
                        ssOpacity: 1
                    }, i).easing(u).onComplete(function() {
                        e.each(function() {
                            s(this).data().ssOpacity = 1
                        })
                    }).start(), new TWEEN.Tween(g).to({
                        ssScale: y[1].scale
                    }, n).easing(h).start(), a > 0 ? new TWEEN.Tween({
                        ssOpacity: 1
                    }).to({
                        ssOpacity: 0
                    }, i).onUpdate(function() {
                        g.ssOpacity = this.ssOpacity
                    }).easing(u).delay(n - i).onStart(function() {
                        r(a - 1)
                    }).start() : new TWEEN.Tween(g).to({}, 0).easing(u).delay(n - i).onStart(function() {
                        r(o)
                    }).start()
                }
                if (!(a || window.skrollexConfig.screenshotMode || t)) {
                    var o = e.length - 1;
                    r(o, !0)
                }
            }
        }
    }, {
        "../app/app-share.js": 5
    }],
    5: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = new function() {
            var e = -1 != navigator.appVersion.indexOf("Windows NT 6.1") || -1 != navigator.appVersion.indexOf("Windows NT 6.0") || -1 != navigator.appVersion.indexOf("Windows NT 5.1") || -1 != navigator.appVersion.indexOf("Windows NT 5.0"),
                a = s("html").hasClass("ie9"),
                t = s("html").hasClass("ie10"),
                i = s("html").hasClass("ie11"),
                n = s("html").hasClass("edge"),
                r = s("html").hasClass("poor-browser"),
                o = s("html").hasClass("mobile"),
                l = function() {
                    return a || t || i && e ? 0 : i ? -.15 : n ? -.15 : r ? 0 : -.25
                }();
            this.force3D = o ? !1 : !0, this.parallaxMargin = function(e, a, t) {
                var s = t - (0 === a ? 0 : e.topNav.state2H);
                return Math.round(l * s)
            }
        }
    }, {}],
    6: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = new function() {
            function a(e) {
                var a, t, s = e.get(0);
                if ("img" === s.tagName.toLowerCase()) a = s.width, t = s.height;
                else if (s.naturalWidth) a = s.naturalWidth, t = s.naturalHeight;
                else {
                    var i = e.width();
                    e.css({
                        width: "",
                        height: ""
                    }), a = e.width(), t = e.height(), e.css({
                        width: i
                    })
                }
                return {
                    w: a,
                    h: t
                }
            }
            var t, i = e("./app-share.js"),
                n = e("./themes.js"),
                r = e("../animation/slide-show.js"),
                o = new r,
                l = s("html").hasClass("poor-browser"),
                d = s("html").hasClass("no-animations"),
                p = s("html").hasClass("mobile"),
                c = s("html").hasClass("site-decoration-b"),
                u = 60,
                h = s("#top-nav, .page-border, #dot-scroll"),
                f = s("#top-nav"),
                m = f.data("state1-colors"),
                g = f.data("state2-colors"),
                v = s("body"),
                w = s(".view");
            this.prepare = function(e) {
                if ("file:" !== window.location.protocol || s("body").hasClass("example-page") || s('<div class="file-protocol-alert alert colors-d background-80 heading fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Upload files to web server and open template from web server. If template is opened from local file system, some links, functions and examples may work incorrectly.</div>').appendTo("body"), i.force3D === !0 && s("html").addClass("force3d"), s(".wrapper-content, .view").each(function() {
                        var e = s(this),
                            a = e.children(l ? ".bg-holder:last" : ".bg-holder");
                        a.each(function() {
                            var e = s(this);
                            e.after('<img alt="' + e.data("alt") + '" class="bg" src="' + e.data("src") + '"/>')
                        })
                    }), s(".bg-holder").remove(), l || d) {
                    var a = s(".wrapper-content>.bg");
                    a.each(function(e) {
                        e === a.length - 1 ? s(this).css("display", "block") : s(this).remove()
                    }), s(".view").each(function() {
                        var e = s(this).children(".bg");
                        e.each(function(e) {
                            0 === e ? s(this).css("display", "block") : s(this).remove()
                        })
                    })
                }
                if (p) {
                    var n = s(".wrapper-content>img.bg"),
                        r = n.length > 0 ? n : s(".view>img.bg");
                    if (r.length > 0) {
                        var o = s(r[0]);
                        s(".view").each(function() {
                            var e = s(this),
                                a = e.children("img.bg");
                            a.length < 1 && o.clone().prependTo(e)
                        })
                    }
                    s(".wrapper-content>img.bg").remove()
                }
                t = s(".bg"), e()
            }, this.setup = function(e) {
                function a(e) {
                    for (var a = 0; a < n.colors; a++) {
                        var t = "colors-" + String.fromCharCode(65 + a).toLowerCase();
                        if (e.hasClass(t)) return t
                    }
                }
                var t = function(e) {
                    var a = e.css("background-color");
                    return a.match(/#/i) || a.match(/rgb\(/i) || a.match(/rgba.*,0\)/i)
                };
                s(".view.section-header").each(function() {
                    var e = s(this),
                        a = e.nextAll(".view").first().children(".fg");
                    a.length > 0 && t(a) && e.children(".fg").addClass("skew-bottom-right")
                }), s(".view.section-footer").each(function() {
                    var e = s(this),
                        a = e.prevAll(".view").first().children(".fg");
                    a.length > 0 && t(a) && e.children(".fg").addClass("skew-top-right")
                }), w.find(".fg").filter(".skew-top-right, .skew-top-left, .skew-bottom-left, .skew-bottom-right").each(function() {
                    var e = s(this),
                        i = e.parent();
                    if (e.hasClass("skew-top-right") || e.hasClass("skew-top-left")) {
                        var n = i.prevAll(".view").first().children(".fg");
                        if (n.length > 0 && t(n)) {
                            var r = e.hasClass("skew-top-right") ? 1 : 2;
                            s('<div class="skew skew-top-' + (1 === r ? "right" : "left") + '"></div>').appendTo(e).css({
                                position: "absolute",
                                top: "0px",
                                width: "0px",
                                height: "0px",
                                "border-top-width": 2 === r ? u + "px" : "0px",
                                "border-right-width": "2880px",
                                "border-bottom-width": 1 === r ? u + "px" : "0px",
                                "border-left-width": "0px",
                                "border-style": "solid solid solid dashed",
                                "border-bottom-color": "transparent",
                                "border-left-color": "transparent"
                            }).addClass(a(n))
                        }
                    }
                    if (e.hasClass("skew-bottom-left") || e.hasClass("skew-bottom-right")) {
                        var o = i.nextAll(".view").first().children(".fg");
                        if (o.length > 0 && t(o)) {
                            var r = e.hasClass("skew-bottom-left") ? 1 : 2;
                            s('<div class="skew skew-bottom-' + (1 === r ? "left" : "right") + '"></div>').appendTo(e).css({
                                position: "absolute",
                                bottom: "0px",
                                width: "0px",
                                height: "0px",
                                "border-top-width": 1 === r ? u + "px" : "0px",
                                "border-right-width": "0px",
                                "border-bottom-width": 2 === r ? u + "px" : "0px",
                                "border-left-width": "2880px",
                                "border-style": "solid dashed solid solid",
                                "border-top-color": "transparent",
                                "border-right-color": "transparent"
                            }).addClass(a(o))
                        }
                    }
                }), e()
            }, this.ungated = function() {
                s(".wrapper-content, .view").each(function() {
                    var e = s(this).children(".bg");
                    e.length > 1 && o.run(e)
                })
            }, this.tick = function() {
                t.each(function() {
                    var e, a, t, n, r = s(this),
                        o = r.data();
                    void 0 !== o.ssOpacity ? (e = o.ssOpacity, a = o.ssOrig.xr, t = o.ssOrig.yr, n = o.ssOrig.or) : (e = 1, a = 1, t = 1, n = "center center");
                    var l = o.normalX + o.zoomXDelta * a,
                        d = o.normalY + o.zoomYDelta * t + (void 0 !== o.parallaxY ? o.parallaxY : 0),
                        p = o.normalScale * (void 0 !== o.ssScale ? o.ssScale : 1);
                    r.css(Modernizr.csstransforms3d && i.force3D ? {
                        transform: "translate3d(" + l + "px, " + d + "px, 0px) scale(" + p + ", " + p + ")",
                        opacity: e,
                        "transform-origin": n + " 0px"
                    } : {
                        transform: "translate(" + l + "px, " + d + "px) scale(" + p + ", " + p + ")",
                        opacity: e,
                        "transform-origin": n
                    })
                })
            }, this.buildSizes = function(e) {
                function t(e, t, s, i) {
                    var n = a(e),
                        r = s / t > n.w / n.h ? s / n.w : t / n.h,
                        o = n.w * r,
                        l = n.h * r,
                        d = (o - n.w) / 2,
                        p = (l - n.h) / 2,
                        c = Math.round((s - o) / 2),
                        u = Math.round((i - l) / 2),
                        h = e.data();
                    h.normalScale = r, h.normalX = c, h.normalY = u, h.zoomXDelta = d, h.zoomYDelta = p
                }
                var n = s(window),
                    r = n.height(),
                    o = n.width(),
                    l = s("#top-nav:visible"),
                    d = c ? r : r - (l.length > 0 ? e.topNav.state2H : 0),
                    p = s(".page-border.bottom:visible"),
                    u = p.length > 0 ? p.height() : 0;
                s(".full-size, .half-size, .one-third-size").each(function() {
                    var e = s(this),
                        a = parseInt(e.css({
                            "padding-top": ""
                        }).css("padding-top").replace("px", "")),
                        t = parseInt(e.css({
                            "padding-bottom": ""
                        }).css("padding-bottom").replace("px", "")),
                        i = c ? d : d - (p.length > 0 ? u : 0),
                        n = Math.ceil(i / 2),
                        r = Math.ceil(i / 3),
                        o = e.hasClass("full-size") ? i : e.hasClass("half-size") ? n : r;
                    e.css({
                        "padding-top": a + "px",
                        "padding-bottom": t + "px"
                    }), (e.hasClass("stretch-height") || e.hasClass("stretch-full-height")) && e.css({
                        height: ""
                    });
                    var l = e.height();
                    if (o > l) {
                        var h = o - l - a - t;
                        0 > h && (h = 0);
                        var f = Math.round(h / 2),
                            m = h - f,
                            g = a + f,
                            v = t + m;
                        e.css({
                            "padding-top": g + "px",
                            "padding-bottom": v + "px"
                        })
                    }
                }), s(".stretch-height").each(function() {
                    var e = s(this),
                        a = e.parent(),
                        t = a.find(".stretch-height");
                    t.css("height", ""), e.outerWidth() < a.innerWidth() && t.css("height", a.innerHeight() + "px")
                }), s(".stretch-full-height").each(function() {
                    var e = s(this),
                        a = e.parent(),
                        t = a.find(".stretch-full-height");
                    if (t.css("height", ""), e.outerWidth() < a.innerWidth()) {
                        var i = a.innerHeight(),
                            n = i > r ? i : r;
                        t.css("height", n + "px")
                    }
                }), w.each(function(a) {
                    var n = s(this),
                        o = n.find(".fg"),
                        l = o.find(".skew.skew-top-right, .skew.skew-top-left"),
                        d = o.find(".skew.skew-bottom-left, .skew.skew-bottom-right"),
                        p = o.width() + "px";
                    d.css({
                        "border-left-width": p
                    }), l.css({
                        "border-right-width": p
                    });
                    var c = n.height(),
                        u = n.width(),
                        h = function() {
                            var t, s = -1 * c,
                                n = 0,
                                o = r - c,
                                l = r,
                                d = i.parallaxMargin(e, a, s),
                                p = i.parallaxMargin(e, a, n),
                                u = i.parallaxMargin(e, a, o),
                                h = i.parallaxMargin(e, a, l),
                                f = function(e, a) {
                                    return a + (e > 0 ? 0 : e)
                                },
                                m = function(e, a) {
                                    var t = e + c;
                                    return -a - (r > t ? 0 : t - r)
                                },
                                g = 0;
                            return t = f(s, d), t > g && (g = t), t = f(n, p), t > g && (g = t), t = f(o, u), t > g && (g = t), t = f(l, h), t > g && (g = t), t = m(s, d), t > g && (g = t), t = m(n, p), t > g && (g = t), t = m(o, u), t > g && (g = t), t = m(l, h), t > g && (g = t), c + 2 * g
                        }();
                    n.children("img.bg").each(function() {
                        t(s(this), h, u, c)
                    }), n.data("position", n.offset().top)
                }), s(".view").each(function() {
                    var e = s(this);
                    e.data("position", e.offset().top)
                }), s(".view>.fg").each(function() {
                    var e = s(this);
                    e.data("position", e.offset().top)
                }), s(".wrapper-content").children("img.bg").each(function() {
                    t(s(this), r, o, r)
                });
                var h = s(".x40-widget, .footer-bottom").last();
                if (h.length > 0) {
                    h.css("padding-bottom", "");
                    var f = parseFloat(window.getComputedStyle(s(".wrapper-site")[0], null).getPropertyValue("height").replace("px", ""));
                    if (!isNaN(f)) {
                        var m = f - Math.floor(f);
                        if (m > 0) {
                            var g = parseFloat(window.getComputedStyle(h[0], null).getPropertyValue("padding-bottom").replace("px", ""));
                            if (!isNaN(g)) {
                                var v = g + (1 - m);
                                h.css("padding-bottom", v + "px")
                            }
                        }
                    }
                }
            }, this.changeSection = function(e, a) {
                var t = s(a),
                    i = t.data("border-colors");
                i ? (h.removeClass(n.colorClasses), h.addClass(i)) : v.hasClass("state2") && g ? (h.removeClass(n.colorClasses), h.addClass(g)) : m && (h.removeClass(n.colorClasses), h.addClass(m))
            }
        }
    }, {
        "../animation/slide-show.js": 4,
        "./app-share.js": 5,
        "./themes.js": 8
    }],
    7: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(a, t) {
            var i = s(".view"),
                n = e("./app-share.js"),
                r = s("html").hasClass("poor-browser");
            this.scroll = function() {
                r || i.each(function(e) {
                    var i = s(this),
                        r = a.calcPosition(i);
                    if (r.visible) {
                        var o = r.top - a.windowTopPos;
                        i.children(".bg:not(.static)").each(function() {
                            var a = s(this).data();
                            a.parallaxY = n.parallaxMargin(t, e, o)
                        })
                    }
                })
            }
        }
    }, {
        "./app-share.js": 5
    }],
    8: [function(e, a, t) {
        "use strict";
        a.exports = new function() {
            var e = this;
            this.colors = 26, this.colorClasses = function() {
                for (var a = "", t = 0; t < e.colors; t++) {
                    var s = 0 === t ? "" : " ";
                    a += s + "colors-" + String.fromCharCode(65 + t).toLowerCase()
                }
                return a
            }()
        }
    }, {}],
    9: [function(e, a, t) {
        ! function() {
            "use strict";

            function e(e) {
                e.fn.swiper = function(a) {
                    var s;
                    return e(this).each(function() {
                        var e = new t(this, a);
                        s || (s = e)
                    }), s
                }
            }
            var a, t = function(e, s) {
                function i(e) {
                    return Math.floor(e)
                }

                function n() {
                    y.autoplayTimeoutId = setTimeout(function() {
                        y.params.loop ? (y.fixLoop(), y._slideNext(), y.emit("onAutoplay", y)) : y.isEnd ? s.autoplayStopOnLast ? y.stopAutoplay() : (y._slideTo(0), y.emit("onAutoplay", y)) : (y._slideNext(), y.emit("onAutoplay", y))
                    }, y.params.autoplay)
                }

                function r(e, t) {
                    var s = a(e.target);
                    if (!s.is(t))
                        if ("string" == typeof t) s = s.parents(t);
                        else if (t.nodeType) {
                            var i;
                            return s.parents().each(function(e, a) {
                                a === t && (i = t)
                            }), i ? t : void 0
                        }
                    return 0 === s.length ? void 0 : s[0]
                }

                function o(e, a) {
                    a = a || {};
                    var t = window.MutationObserver || window.WebkitMutationObserver,
                        s = new t(function(e) {
                            e.forEach(function(e) {
                                y.onResize(!0), y.emit("onObserverUpdate", y, e)
                            })
                        });
                    s.observe(e, {
                        attributes: "undefined" == typeof a.attributes ? !0 : a.attributes,
                        childList: "undefined" == typeof a.childList ? !0 : a.childList,
                        characterData: "undefined" == typeof a.characterData ? !0 : a.characterData
                    }), y.observers.push(s)
                }

                function l(e) {
                    e.originalEvent && (e = e.originalEvent);
                    var a = e.keyCode || e.charCode;
                    if (!y.params.allowSwipeToNext && (y.isHorizontal() && 39 === a || !y.isHorizontal() && 40 === a)) return !1;
                    if (!y.params.allowSwipeToPrev && (y.isHorizontal() && 37 === a || !y.isHorizontal() && 38 === a)) return !1;
                    if (!(e.shiftKey || e.altKey || e.ctrlKey || e.metaKey || document.activeElement && document.activeElement.nodeName && ("input" === document.activeElement.nodeName.toLowerCase() || "textarea" === document.activeElement.nodeName.toLowerCase()))) {
                        if (37 === a || 39 === a || 38 === a || 40 === a) {
                            var t = !1;
                            if (y.container.parents(".swiper-slide").length > 0 && 0 === y.container.parents(".swiper-slide-active").length) return;
                            var s = {
                                    left: window.pageXOffset,
                                    top: window.pageYOffset
                                },
                                i = window.innerWidth,
                                n = window.innerHeight,
                                r = y.container.offset();
                            y.rtl && (r.left = r.left - y.container[0].scrollLeft);
                            for (var o = [
                                [r.left, r.top],
                                [r.left + y.width, r.top],
                                [r.left, r.top + y.height],
                                [r.left + y.width, r.top + y.height]
                            ], l = 0; l < o.length; l++) {
                                var d = o[l];
                                d[0] >= s.left && d[0] <= s.left + i && d[1] >= s.top && d[1] <= s.top + n && (t = !0)
                            }
                            if (!t) return
                        }
                        y.isHorizontal() ? ((37 === a || 39 === a) && (e.preventDefault ? e.preventDefault() : e.returnValue = !1), (39 === a && !y.rtl || 37 === a && y.rtl) && y.slideNext(), (37 === a && !y.rtl || 39 === a && y.rtl) && y.slidePrev()) : ((38 === a || 40 === a) && (e.preventDefault ? e.preventDefault() : e.returnValue = !1), 40 === a && y.slideNext(), 38 === a && y.slidePrev())
                    }
                }

                function d(e) {
                    e.originalEvent && (e = e.originalEvent);
                    var a = y.mousewheel.event,
                        t = 0,
                        s = y.rtl ? -1 : 1;
                    if (e.detail) t = -e.detail;
                    else if ("mousewheel" === a)
                        if (y.params.mousewheelForceToAxis)
                            if (y.isHorizontal()) {
                                if (!(Math.abs(e.wheelDeltaX) > Math.abs(e.wheelDeltaY))) return;
                                t = e.wheelDeltaX * s
                            } else {
                                if (!(Math.abs(e.wheelDeltaY) > Math.abs(e.wheelDeltaX))) return;
                                t = e.wheelDeltaY
                            } else t = Math.abs(e.wheelDeltaX) > Math.abs(e.wheelDeltaY) ? -e.wheelDeltaX * s : -e.wheelDeltaY;
                    else if ("DOMMouseScroll" === a) t = -e.detail;
                    else if ("wheel" === a)
                        if (y.params.mousewheelForceToAxis)
                            if (y.isHorizontal()) {
                                if (!(Math.abs(e.deltaX) > Math.abs(e.deltaY))) return;
                                t = -e.deltaX * s
                            } else {
                                if (!(Math.abs(e.deltaY) > Math.abs(e.deltaX))) return;
                                t = -e.deltaY
                            } else t = Math.abs(e.deltaX) > Math.abs(e.deltaY) ? -e.deltaX * s : -e.deltaY;
                    if (0 !== t) {
                        if (y.params.mousewheelInvert && (t = -t), y.params.freeMode) {
                            var i = y.getWrapperTranslate() + t * y.params.mousewheelSensitivity,
                                n = y.isBeginning,
                                r = y.isEnd;
                            if (i >= y.minTranslate() && (i = y.minTranslate()), i <= y.maxTranslate() && (i = y.maxTranslate()), y.setWrapperTransition(0), y.setWrapperTranslate(i), y.updateProgress(), y.updateActiveIndex(), (!n && y.isBeginning || !r && y.isEnd) && y.updateClasses(), y.params.freeModeSticky ? (clearTimeout(y.mousewheel.timeout), y.mousewheel.timeout = setTimeout(function() {
                                    y.slideReset()
                                }, 300)) : y.params.lazyLoading && y.lazy && y.lazy.load(), 0 === i || i === y.maxTranslate()) return
                        } else {
                            if ((new window.Date).getTime() - y.mousewheel.lastScrollTime > 60)
                                if (0 > t)
                                    if (y.isEnd && !y.params.loop || y.animating) {
                                        if (y.params.mousewheelReleaseOnEdges) return !0
                                    } else y.slideNext();
                                else if (y.isBeginning && !y.params.loop || y.animating) {
                                    if (y.params.mousewheelReleaseOnEdges) return !0
                                } else y.slidePrev();
                            y.mousewheel.lastScrollTime = (new window.Date).getTime()
                        }
                        return y.params.autoplay && y.stopAutoplay(), e.preventDefault ? e.preventDefault() : e.returnValue = !1, !1
                    }
                }

                function p(e, t) {
                    e = a(e);
                    var s, i, n, r = y.rtl ? -1 : 1;
                    s = e.attr("data-swiper-parallax") || "0", i = e.attr("data-swiper-parallax-x"), n = e.attr("data-swiper-parallax-y"), i || n ? (i = i || "0", n = n || "0") : y.isHorizontal() ? (i = s, n = "0") : (n = s, i = "0"), i = i.indexOf("%") >= 0 ? parseInt(i, 10) * t * r + "%" : i * t * r + "px", n = n.indexOf("%") >= 0 ? parseInt(n, 10) * t + "%" : n * t + "px", e.transform("translate3d(" + i + ", " + n + ",0px)")
                }

                function c(e) {
                    return 0 !== e.indexOf("on") && (e = e[0] !== e[0].toUpperCase() ? "on" + e[0].toUpperCase() + e.substring(1) : "on" + e), e
                }
                if (!(this instanceof t)) return new t(e, s);
                var u = {
                        direction: "horizontal",
                        touchEventsTarget: "container",
                        initialSlide: 0,
                        speed: 300,
                        autoplay: !1,
                        autoplayDisableOnInteraction: !0,
                        autoplayStopOnLast: !1,
                        iOSEdgeSwipeDetection: !1,
                        iOSEdgeSwipeThreshold: 20,
                        freeMode: !1,
                        freeModeMomentum: !0,
                        freeModeMomentumRatio: 1,
                        freeModeMomentumBounce: !0,
                        freeModeMomentumBounceRatio: 1,
                        freeModeSticky: !1,
                        freeModeMinimumVelocity: .02,
                        autoHeight: !1,
                        setWrapperSize: !1,
                        virtualTranslate: !1,
                        effect: "slide",
                        coverflow: {
                            rotate: 50,
                            stretch: 0,
                            depth: 100,
                            modifier: 1,
                            slideShadows: !0
                        },
                        flip: {
                            slideShadows: !0,
                            limitRotation: !0
                        },
                        cube: {
                            slideShadows: !0,
                            shadow: !0,
                            shadowOffset: 20,
                            shadowScale: .94
                        },
                        fade: {
                            crossFade: !1
                        },
                        parallax: !1,
                        scrollbar: null,
                        scrollbarHide: !0,
                        scrollbarDraggable: !1,
                        scrollbarSnapOnRelease: !1,
                        keyboardControl: !1,
                        mousewheelControl: !1,
                        mousewheelReleaseOnEdges: !1,
                        mousewheelInvert: !1,
                        mousewheelForceToAxis: !1,
                        mousewheelSensitivity: 1,
                        hashnav: !1,
                        breakpoints: void 0,
                        spaceBetween: 0,
                        slidesPerView: 1,
                        slidesPerColumn: 1,
                        slidesPerColumnFill: "column",
                        slidesPerGroup: 1,
                        centeredSlides: !1,
                        slidesOffsetBefore: 0,
                        slidesOffsetAfter: 0,
                        roundLengths: !1,
                        touchRatio: 1,
                        touchAngle: 45,
                        simulateTouch: !0,
                        shortSwipes: !0,
                        longSwipes: !0,
                        longSwipesRatio: .5,
                        longSwipesMs: 300,
                        followFinger: !0,
                        onlyExternal: !1,
                        threshold: 0,
                        touchMoveStopPropagation: !0,
                        pagination: null,
                        paginationElement: "span",
                        paginationClickable: !1,
                        paginationHide: !1,
                        paginationBulletRender: null,
                        paginationProgressRender: null,
                        paginationFractionRender: null,
                        paginationCustomRender: null,
                        paginationType: "bullets",
                        resistance: !0,
                        resistanceRatio: .85,
                        nextButton: null,
                        prevButton: null,
                        watchSlidesProgress: !1,
                        watchSlidesVisibility: !1,
                        grabCursor: !1,
                        preventClicks: !0,
                        preventClicksPropagation: !0,
                        slideToClickedSlide: !1,
                        lazyLoading: !1,
                        lazyLoadingInPrevNext: !1,
                        lazyLoadingInPrevNextAmount: 1,
                        lazyLoadingOnTransitionStart: !1,
                        preloadImages: !0,
                        updateOnImagesReady: !0,
                        loop: !1,
                        loopAdditionalSlides: 0,
                        loopedSlides: null,
                        control: void 0,
                        controlInverse: !1,
                        controlBy: "slide",
                        allowSwipeToPrev: !0,
                        allowSwipeToNext: !0,
                        swipeHandler: null,
                        noSwiping: !0,
                        noSwipingClass: "swiper-no-swiping",
                        slideClass: "swiper-slide",
                        slideActiveClass: "swiper-slide-active",
                        slideVisibleClass: "swiper-slide-visible",
                        slideDuplicateClass: "swiper-slide-duplicate",
                        slideNextClass: "swiper-slide-next",
                        slidePrevClass: "swiper-slide-prev",
                        wrapperClass: "swiper-wrapper",
                        bulletClass: "swiper-pagination-bullet",
                        bulletActiveClass: "swiper-pagination-bullet-active",
                        buttonDisabledClass: "swiper-button-disabled",
                        paginationCurrentClass: "swiper-pagination-current",
                        paginationTotalClass: "swiper-pagination-total",
                        paginationHiddenClass: "swiper-pagination-hidden",
                        paginationProgressbarClass: "swiper-pagination-progressbar",
                        observer: !1,
                        observeParents: !1,
                        a11y: !1,
                        prevSlideMessage: "Previous slide",
                        nextSlideMessage: "Next slide",
                        firstSlideMessage: "This is the first slide",
                        lastSlideMessage: "This is the last slide",
                        paginationBulletMessage: "Go to slide{{index}}",
                        runCallbacksOnInit: !0
                    },
                    h = s && s.virtualTranslate;
                s = s || {};
                var f = {};
                for (var m in s)
                    if ("object" != typeof s[m] || null === s[m] || s[m].nodeType || s[m] === window || s[m] === document || "undefined" != typeof Dom7 && s[m] instanceof Dom7 || "undefined" != typeof jQuery && s[m] instanceof jQuery) f[m] = s[m];
                    else {
                        f[m] = {};
                        for (var g in s[m]) f[m][g] = s[m][g]
                    }
                for (var v in u)
                    if ("undefined" == typeof s[v]) s[v] = u[v];
                    else if ("object" == typeof s[v])
                        for (var w in u[v]) "undefined" == typeof s[v][w] && (s[v][w] = u[v][w]);
                var y = this;
                if (y.params = s, y.originalParams = f, y.classNames = [], "undefined" != typeof a && "undefined" != typeof Dom7 && (a = Dom7), ("undefined" != typeof a || (a = "undefined" == typeof Dom7 ? window.Dom7 || window.Zepto || window.jQuery : Dom7)) && (y.$ = a, y.currentBreakpoint = void 0, y.getActiveBreakpoint = function() {
                        if (!y.params.breakpoints) return !1;
                        var e, a = !1,
                            t = [];
                        for (e in y.params.breakpoints) y.params.breakpoints.hasOwnProperty(e) && t.push(e);
                        t.sort(function(e, a) {
                            return parseInt(e, 10) > parseInt(a, 10)
                        });
                        for (var s = 0; s < t.length; s++) e = t[s], e >= window.innerWidth && !a && (a = e);
                        return a || "max"
                    }, y.setBreakpoint = function() {
                        var e = y.getActiveBreakpoint();
                        if (e && y.currentBreakpoint !== e) {
                            var a = e in y.params.breakpoints ? y.params.breakpoints[e] : y.originalParams;
                            for (var t in a) y.params[t] = a[t];
                            y.currentBreakpoint = e
                        }
                    }, y.params.breakpoints && y.setBreakpoint(), y.container = a(e), 0 !== y.container.length)) {
                    if (y.container.length > 1) return void y.container.each(function() {
                        new t(this, s)
                    });
                    y.container[0].swiper = y, y.container.data("swiper", y), y.classNames.push("swiper-container-" + y.params.direction), y.params.freeMode && y.classNames.push("swiper-container-free-mode"), y.support.flexbox || (y.classNames.push("swiper-container-no-flexbox"), y.params.slidesPerColumn = 1), y.params.autoHeight && y.classNames.push("swiper-container-autoheight"), (y.params.parallax || y.params.watchSlidesVisibility) && (y.params.watchSlidesProgress = !0), ["cube", "coverflow", "flip"].indexOf(y.params.effect) >= 0 && (y.support.transforms3d ? (y.params.watchSlidesProgress = !0, y.classNames.push("swiper-container-3d")) : y.params.effect = "slide"), "slide" !== y.params.effect && y.classNames.push("swiper-container-" + y.params.effect), "cube" === y.params.effect && (y.params.resistanceRatio = 0, y.params.slidesPerView = 1, y.params.slidesPerColumn = 1, y.params.slidesPerGroup = 1, y.params.centeredSlides = !1, y.params.spaceBetween = 0, y.params.virtualTranslate = !0, y.params.setWrapperSize = !1), ("fade" === y.params.effect || "flip" === y.params.effect) && (y.params.slidesPerView = 1, y.params.slidesPerColumn = 1, y.params.slidesPerGroup = 1, y.params.watchSlidesProgress = !0, y.params.spaceBetween = 0, y.params.setWrapperSize = !1, "undefined" == typeof h && (y.params.virtualTranslate = !0)), y.params.grabCursor && y.support.touch && (y.params.grabCursor = !1), y.wrapper = y.container.children("." + y.params.wrapperClass), y.params.pagination && (y.paginationContainer = a(y.params.pagination), "bullets" === y.params.paginationType && y.params.paginationClickable ? y.paginationContainer.addClass("swiper-pagination-clickable") : y.params.paginationClickable = !1, y.paginationContainer.addClass("swiper-pagination-" + y.params.paginationType)), y.isHorizontal = function() {
                        return "horizontal" === y.params.direction
                    }, y.rtl = y.isHorizontal() && ("rtl" === y.container[0].dir.toLowerCase() || "rtl" === y.container.css("direction")), y.rtl && y.classNames.push("swiper-container-rtl"), y.rtl && (y.wrongRTL = "-webkit-box" === y.wrapper.css("display")), y.params.slidesPerColumn > 1 && y.classNames.push("swiper-container-multirow"), y.device.android && y.classNames.push("swiper-container-android"), y.container.addClass(y.classNames.join(" ")), y.translate = 0, y.progress = 0, y.velocity = 0, y.lockSwipeToNext = function() {
                        y.params.allowSwipeToNext = !1
                    }, y.lockSwipeToPrev = function() {
                        y.params.allowSwipeToPrev = !1
                    }, y.lockSwipes = function() {
                        y.params.allowSwipeToNext = y.params.allowSwipeToPrev = !1
                    }, y.unlockSwipeToNext = function() {
                        y.params.allowSwipeToNext = !0
                    }, y.unlockSwipeToPrev = function() {
                        y.params.allowSwipeToPrev = !0
                    }, y.unlockSwipes = function() {
                        y.params.allowSwipeToNext = y.params.allowSwipeToPrev = !0
                    }, y.params.grabCursor && (y.container[0].style.cursor = "move", y.container[0].style.cursor = "-webkit-grab", y.container[0].style.cursor = "-moz-grab", y.container[0].style.cursor = "grab"), y.imagesToLoad = [], y.imagesLoaded = 0, y.loadImage = function(e, a, t, s, i) {
                        function n() {
                            i && i()
                        }
                        var r;
                        e.complete && s ? n() : a ? (r = new window.Image, r.onload = n, r.onerror = n, t && (r.srcset = t), a && (r.src = a)) : n()
                    }, y.preloadImages = function() {
                        function e() {
                            "undefined" != typeof y && null !== y && (void 0 !== y.imagesLoaded && y.imagesLoaded++, y.imagesLoaded === y.imagesToLoad.length && (y.params.updateOnImagesReady && y.update(), y.emit("onImagesReady", y)))
                        }
                        y.imagesToLoad = y.container.find("img");
                        for (var a = 0; a < y.imagesToLoad.length; a++) y.loadImage(y.imagesToLoad[a], y.imagesToLoad[a].currentSrc || y.imagesToLoad[a].getAttribute("src"), y.imagesToLoad[a].srcset || y.imagesToLoad[a].getAttribute("srcset"), !0, e)
                    }, y.autoplayTimeoutId = void 0, y.autoplaying = !1, y.autoplayPaused = !1, y.startAutoplay = function() {
                        return "undefined" != typeof y.autoplayTimeoutId ? !1 : y.params.autoplay ? y.autoplaying ? !1 : (y.autoplaying = !0, y.emit("onAutoplayStart", y), void n()) : !1
                    }, y.stopAutoplay = function(e) {
                        y.autoplayTimeoutId && (y.autoplayTimeoutId && clearTimeout(y.autoplayTimeoutId), y.autoplaying = !1, y.autoplayTimeoutId = void 0, y.emit("onAutoplayStop", y))
                    }, y.pauseAutoplay = function(e) {
                        y.autoplayPaused || (y.autoplayTimeoutId && clearTimeout(y.autoplayTimeoutId), y.autoplayPaused = !0, 0 === e ? (y.autoplayPaused = !1, n()) : y.wrapper.transitionEnd(function() {
                            y && (y.autoplayPaused = !1, y.autoplaying ? n() : y.stopAutoplay())
                        }))
                    }, y.minTranslate = function() {
                        return -y.snapGrid[0]
                    }, y.maxTranslate = function() {
                        return -y.snapGrid[y.snapGrid.length - 1]
                    }, y.updateAutoHeight = function() {
                        var e = y.slides.eq(y.activeIndex)[0];
                        if ("undefined" != typeof e) {
                            var a = e.offsetHeight;
                            a && y.wrapper.css("height", a + "px")
                        }
                    }, y.updateContainerSize = function() {
                        var e, a;
                        e = "undefined" != typeof y.params.width ? y.params.width : y.container[0].clientWidth, a = "undefined" != typeof y.params.height ? y.params.height : y.container[0].clientHeight, 0 === e && y.isHorizontal() || 0 === a && !y.isHorizontal() || (e = e - parseInt(y.container.css("padding-left"), 10) - parseInt(y.container.css("padding-right"), 10), a = a - parseInt(y.container.css("padding-top"), 10) - parseInt(y.container.css("padding-bottom"), 10), y.width = e, y.height = a, y.size = y.isHorizontal() ? y.width : y.height)
                    }, y.updateSlidesSize = function() {
                        y.slides = y.wrapper.children("." + y.params.slideClass), y.snapGrid = [], y.slidesGrid = [], y.slidesSizesGrid = [];
                        var e, a = y.params.spaceBetween,
                            t = -y.params.slidesOffsetBefore,
                            s = 0,
                            n = 0;
                        "string" == typeof a && a.indexOf("%") >= 0 && (a = parseFloat(a.replace("%", "")) / 100 * y.size), y.virtualSize = -a, y.slides.css(y.rtl ? {
                            marginLeft: "",
                            marginTop: ""
                        } : {
                            marginRight: "",
                            marginBottom: ""
                        });
                        var r;
                        y.params.slidesPerColumn > 1 && (r = Math.floor(y.slides.length / y.params.slidesPerColumn) === y.slides.length / y.params.slidesPerColumn ? y.slides.length : Math.ceil(y.slides.length / y.params.slidesPerColumn) * y.params.slidesPerColumn, "auto" !== y.params.slidesPerView && "row" === y.params.slidesPerColumnFill && (r = Math.max(r, y.params.slidesPerView * y.params.slidesPerColumn)));
                        var o, l = y.params.slidesPerColumn,
                            d = r / l,
                            p = d - (y.params.slidesPerColumn * d - y.slides.length);
                        for (e = 0; e < y.slides.length; e++) {
                            o = 0;
                            var c = y.slides.eq(e);
                            if (y.params.slidesPerColumn > 1) {
                                var u, h, f;
                                "column" === y.params.slidesPerColumnFill ? (h = Math.floor(e / l), f = e - h * l, (h > p || h === p && f === l - 1) && ++f >= l && (f = 0, h++), u = h + f * r / l, c.css({
                                    "-webkit-box-ordinal-group": u,
                                    "-moz-box-ordinal-group": u,
                                    "-ms-flex-order": u,
                                    "-webkit-order": u,
                                    order: u
                                })) : (f = Math.floor(e / d), h = e - f * d), c.css({
                                    "margin-top": 0 !== f && y.params.spaceBetween && y.params.spaceBetween + "px"
                                }).attr("data-swiper-column", h).attr("data-swiper-row", f)
                            }
                            "none" !== c.css("display") && ("auto" === y.params.slidesPerView ? (o = y.isHorizontal() ? c.outerWidth(!0) : c.outerHeight(!0), y.params.roundLengths && (o = i(o))) : (o = (y.size - (y.params.slidesPerView - 1) * a) / y.params.slidesPerView, y.params.roundLengths && (o = i(o)), y.isHorizontal() ? y.slides[e].style.width = o + "px" : y.slides[e].style.height = o + "px"), y.slides[e].swiperSlideSize = o, y.slidesSizesGrid.push(o), y.params.centeredSlides ? (t = t + o / 2 + s / 2 + a, 0 === e && (t = t - y.size / 2 - a), Math.abs(t) < .001 && (t = 0), n % y.params.slidesPerGroup === 0 && y.snapGrid.push(t), y.slidesGrid.push(t)) : (n % y.params.slidesPerGroup === 0 && y.snapGrid.push(t), y.slidesGrid.push(t), t = t + o + a), y.virtualSize += o + a, s = o, n++)
                        }
                        y.virtualSize = Math.max(y.virtualSize, y.size) + y.params.slidesOffsetAfter;
                        var m;
                        if (y.rtl && y.wrongRTL && ("slide" === y.params.effect || "coverflow" === y.params.effect) && y.wrapper.css({
                                width: y.virtualSize + y.params.spaceBetween + "px"
                            }), (!y.support.flexbox || y.params.setWrapperSize) && y.wrapper.css(y.isHorizontal() ? {
                                width: y.virtualSize + y.params.spaceBetween + "px"
                            } : {
                                height: y.virtualSize + y.params.spaceBetween + "px"
                            }), y.params.slidesPerColumn > 1 && (y.virtualSize = (o + y.params.spaceBetween) * r, y.virtualSize = Math.ceil(y.virtualSize / y.params.slidesPerColumn) - y.params.spaceBetween, y.wrapper.css({
                                width: y.virtualSize + y.params.spaceBetween + "px"
                            }), y.params.centeredSlides)) {
                            for (m = [], e = 0; e < y.snapGrid.length; e++) y.snapGrid[e] < y.virtualSize + y.snapGrid[0] && m.push(y.snapGrid[e]);
                            y.snapGrid = m
                        }
                        if (!y.params.centeredSlides) {
                            for (m = [], e = 0; e < y.snapGrid.length; e++) y.snapGrid[e] <= y.virtualSize - y.size && m.push(y.snapGrid[e]);
                            y.snapGrid = m, Math.floor(y.virtualSize - y.size) > Math.floor(y.snapGrid[y.snapGrid.length - 1]) && y.snapGrid.push(y.virtualSize - y.size)
                        }
                        0 === y.snapGrid.length && (y.snapGrid = [0]), 0 !== y.params.spaceBetween && y.slides.css(y.isHorizontal() ? y.rtl ? {
                            marginLeft: a + "px"
                        } : {
                            marginRight: a + "px"
                        } : {
                            marginBottom: a + "px"
                        }), y.params.watchSlidesProgress && y.updateSlidesOffset()
                    }, y.updateSlidesOffset = function() {
                        for (var e = 0; e < y.slides.length; e++) y.slides[e].swiperSlideOffset = y.isHorizontal() ? y.slides[e].offsetLeft : y.slides[e].offsetTop
                    }, y.updateSlidesProgress = function(e) {
                        if ("undefined" == typeof e && (e = y.translate || 0), 0 !== y.slides.length) {
                            "undefined" == typeof y.slides[0].swiperSlideOffset && y.updateSlidesOffset();
                            var a = -e;
                            y.rtl && (a = e), y.slides.removeClass(y.params.slideVisibleClass);
                            for (var t = 0; t < y.slides.length; t++) {
                                var s = y.slides[t],
                                    i = (a - s.swiperSlideOffset) / (s.swiperSlideSize + y.params.spaceBetween);
                                if (y.params.watchSlidesVisibility) {
                                    var n = -(a - s.swiperSlideOffset),
                                        r = n + y.slidesSizesGrid[t],
                                        o = n >= 0 && n < y.size || r > 0 && r <= y.size || 0 >= n && r >= y.size;
                                    o && y.slides.eq(t).addClass(y.params.slideVisibleClass)
                                }
                                s.progress = y.rtl ? -i : i
                            }
                        }
                    }, y.updateProgress = function(e) {
                        "undefined" == typeof e && (e = y.translate || 0);
                        var a = y.maxTranslate() - y.minTranslate(),
                            t = y.isBeginning,
                            s = y.isEnd;
                        0 === a ? (y.progress = 0, y.isBeginning = y.isEnd = !0) : (y.progress = (e - y.minTranslate()) / a, y.isBeginning = y.progress <= 0, y.isEnd = y.progress >= 1), y.isBeginning && !t && y.emit("onReachBeginning", y), y.isEnd && !s && y.emit("onReachEnd", y), y.params.watchSlidesProgress && y.updateSlidesProgress(e), y.emit("onProgress", y, y.progress)
                    }, y.updateActiveIndex = function() {
                        var e, a, t, s = y.rtl ? y.translate : -y.translate;
                        for (a = 0; a < y.slidesGrid.length; a++) "undefined" != typeof y.slidesGrid[a + 1] ? s >= y.slidesGrid[a] && s < y.slidesGrid[a + 1] - (y.slidesGrid[a + 1] - y.slidesGrid[a]) / 2 ? e = a : s >= y.slidesGrid[a] && s < y.slidesGrid[a + 1] && (e = a + 1) : s >= y.slidesGrid[a] && (e = a);
                        (0 > e || "undefined" == typeof e) && (e = 0), t = Math.floor(e / y.params.slidesPerGroup), t >= y.snapGrid.length && (t = y.snapGrid.length - 1), e !== y.activeIndex && (y.snapIndex = t, y.previousIndex = y.activeIndex, y.activeIndex = e, y.updateClasses())
                    }, y.updateClasses = function() {
                        y.slides.removeClass(y.params.slideActiveClass + " " + y.params.slideNextClass + " " + y.params.slidePrevClass);
                        var e = y.slides.eq(y.activeIndex);
                        if (e.addClass(y.params.slideActiveClass), e.next("." + y.params.slideClass).addClass(y.params.slideNextClass), e.prev("." + y.params.slideClass).addClass(y.params.slidePrevClass), y.paginationContainer && y.paginationContainer.length > 0) {
                            var t, s = y.params.loop ? Math.ceil((y.slides.length - 2 * y.loopedSlides) / y.params.slidesPerGroup) : y.snapGrid.length;
                            if (y.params.loop ? (t = Math.ceil(y.activeIndex - y.loopedSlides) / y.params.slidesPerGroup, t > y.slides.length - 1 - 2 * y.loopedSlides && (t -= y.slides.length - 2 * y.loopedSlides), t > s - 1 && (t -= s), 0 > t && "bullets" !== y.params.paginationType && (t = s + t)) : t = "undefined" != typeof y.snapIndex ? y.snapIndex : y.activeIndex || 0,
                                "bullets" === y.params.paginationType && y.bullets && y.bullets.length > 0 && (y.bullets.removeClass(y.params.bulletActiveClass), y.paginationContainer.length > 1 ? y.bullets.each(function() {
                                    a(this).index() === t && a(this).addClass(y.params.bulletActiveClass)
                                }) : y.bullets.eq(t).addClass(y.params.bulletActiveClass)), "fraction" === y.params.paginationType && (y.paginationContainer.find("." + y.params.paginationCurrentClass).text(t + 1), y.paginationContainer.find("." + y.params.paginationTotalClass).text(s)), "progress" === y.params.paginationType) {
                                var i = (t + 1) / s,
                                    n = i,
                                    r = 1;
                                y.isHorizontal() || (r = i, n = 1), y.paginationContainer.find("." + y.params.paginationProgressbarClass).transform("translate3d(0,0,0) scaleX(" + n + ") scaleY(" + r + ")").transition(y.params.speed)
                            }
                            "custom" === y.params.paginationType && y.params.paginationCustomRender && y.paginationContainer.html(y.params.paginationCustomRender(y, t + 1, s))
                        }
                        y.params.loop || (y.params.prevButton && (y.isBeginning ? (a(y.params.prevButton).addClass(y.params.buttonDisabledClass), y.params.a11y && y.a11y && y.a11y.disable(a(y.params.prevButton))) : (a(y.params.prevButton).removeClass(y.params.buttonDisabledClass), y.params.a11y && y.a11y && y.a11y.enable(a(y.params.prevButton)))), y.params.nextButton && (y.isEnd ? (a(y.params.nextButton).addClass(y.params.buttonDisabledClass), y.params.a11y && y.a11y && y.a11y.disable(a(y.params.nextButton))) : (a(y.params.nextButton).removeClass(y.params.buttonDisabledClass), y.params.a11y && y.a11y && y.a11y.enable(a(y.params.nextButton)))))
                    }, y.updatePagination = function() {
                        if (y.params.pagination && y.paginationContainer && y.paginationContainer.length > 0) {
                            var e = "";
                            if ("bullets" === y.params.paginationType) {
                                for (var a = y.params.loop ? Math.ceil((y.slides.length - 2 * y.loopedSlides) / y.params.slidesPerGroup) : y.snapGrid.length, t = 0; a > t; t++) e += y.params.paginationBulletRender ? y.params.paginationBulletRender(t, y.params.bulletClass) : "<" + y.params.paginationElement + ' class="' + y.params.bulletClass + '"></' + y.params.paginationElement + ">";
                                y.paginationContainer.html(e), y.bullets = y.paginationContainer.find("." + y.params.bulletClass), y.params.paginationClickable && y.params.a11y && y.a11y && y.a11y.initPagination()
                            }
                            "fraction" === y.params.paginationType && (e = y.params.paginationFractionRender ? y.params.paginationFractionRender(y, y.params.paginationCurrentClass, y.params.paginationTotalClass) : '<span class="' + y.params.paginationCurrentClass + '"></span> / <span class="' + y.params.paginationTotalClass + '"></span>', y.paginationContainer.html(e)), "progress" === y.params.paginationType && (e = y.params.paginationProgressRender ? y.params.paginationProgressRender(y, y.params.paginationProgressbarClass) : '<span class="' + y.params.paginationProgressbarClass + '"></span>', y.paginationContainer.html(e))
                        }
                    }, y.update = function(e) {
                        function a() {
                            s = Math.min(Math.max(y.translate, y.maxTranslate()), y.minTranslate()), y.setWrapperTranslate(s), y.updateActiveIndex(), y.updateClasses()
                        }
                        if (y.updateContainerSize(), y.updateSlidesSize(), y.updateProgress(), y.updatePagination(), y.updateClasses(), y.params.scrollbar && y.scrollbar && y.scrollbar.set(), e) {
                            var t, s;
                            y.controller && y.controller.spline && (y.controller.spline = void 0), y.params.freeMode ? (a(), y.params.autoHeight && y.updateAutoHeight()) : (t = ("auto" === y.params.slidesPerView || y.params.slidesPerView > 1) && y.isEnd && !y.params.centeredSlides ? y.slideTo(y.slides.length - 1, 0, !1, !0) : y.slideTo(y.activeIndex, 0, !1, !0), t || a())
                        } else y.params.autoHeight && y.updateAutoHeight()
                    }, y.onResize = function(e) {
                        y.params.breakpoints && y.setBreakpoint();
                        var a = y.params.allowSwipeToPrev,
                            t = y.params.allowSwipeToNext;
                        if (y.params.allowSwipeToPrev = y.params.allowSwipeToNext = !0, y.updateContainerSize(), y.updateSlidesSize(), ("auto" === y.params.slidesPerView || y.params.freeMode || e) && y.updatePagination(), y.params.scrollbar && y.scrollbar && y.scrollbar.set(), y.controller && y.controller.spline && (y.controller.spline = void 0), y.params.freeMode) {
                            var s = Math.min(Math.max(y.translate, y.maxTranslate()), y.minTranslate());
                            y.setWrapperTranslate(s), y.updateActiveIndex(), y.updateClasses(), y.params.autoHeight && y.updateAutoHeight()
                        } else y.updateClasses(), ("auto" === y.params.slidesPerView || y.params.slidesPerView > 1) && y.isEnd && !y.params.centeredSlides ? y.slideTo(y.slides.length - 1, 0, !1, !0) : y.slideTo(y.activeIndex, 0, !1, !0);
                        y.params.allowSwipeToPrev = a, y.params.allowSwipeToNext = t
                    };
                    var x = ["mousedown", "mousemove", "mouseup"];
                    window.navigator.pointerEnabled ? x = ["pointerdown", "pointermove", "pointerup"] : window.navigator.msPointerEnabled && (x = ["MSPointerDown", "MSPointerMove", "MSPointerUp"]), y.touchEvents = {
                        start: y.support.touch || !y.params.simulateTouch ? "touchstart" : x[0],
                        move: y.support.touch || !y.params.simulateTouch ? "touchmove" : x[1],
                        end: y.support.touch || !y.params.simulateTouch ? "touchend" : x[2]
                    }, (window.navigator.pointerEnabled || window.navigator.msPointerEnabled) && ("container" === y.params.touchEventsTarget ? y.container : y.wrapper).addClass("swiper-wp8-" + y.params.direction), y.initEvents = function(e) {
                        var t = e ? "off" : "on",
                            i = e ? "removeEventListener" : "addEventListener",
                            n = "container" === y.params.touchEventsTarget ? y.container[0] : y.wrapper[0],
                            r = y.support.touch ? n : document,
                            o = y.params.nested ? !0 : !1;
                        y.browser.ie ? (n[i](y.touchEvents.start, y.onTouchStart, !1), r[i](y.touchEvents.move, y.onTouchMove, o), r[i](y.touchEvents.end, y.onTouchEnd, !1)) : (y.support.touch && (n[i](y.touchEvents.start, y.onTouchStart, !1), n[i](y.touchEvents.move, y.onTouchMove, o), n[i](y.touchEvents.end, y.onTouchEnd, !1)), !s.simulateTouch || y.device.ios || y.device.android || (n[i]("mousedown", y.onTouchStart, !1), document[i]("mousemove", y.onTouchMove, o), document[i]("mouseup", y.onTouchEnd, !1))), window[i]("resize", y.onResize), y.params.nextButton && (a(y.params.nextButton)[t]("click", y.onClickNext), y.params.a11y && y.a11y && a(y.params.nextButton)[t]("keydown", y.a11y.onEnterKey)), y.params.prevButton && (a(y.params.prevButton)[t]("click", y.onClickPrev), y.params.a11y && y.a11y && a(y.params.prevButton)[t]("keydown", y.a11y.onEnterKey)), y.params.pagination && y.params.paginationClickable && (a(y.paginationContainer)[t]("click", "." + y.params.bulletClass, y.onClickIndex), y.params.a11y && y.a11y && a(y.paginationContainer)[t]("keydown", "." + y.params.bulletClass, y.a11y.onEnterKey)), (y.params.preventClicks || y.params.preventClicksPropagation) && n[i]("click", y.preventClicks, !0)
                    }, y.attachEvents = function(e) {
                        y.initEvents()
                    }, y.detachEvents = function() {
                        y.initEvents(!0)
                    }, y.allowClick = !0, y.preventClicks = function(e) {
                        y.allowClick || (y.params.preventClicks && e.preventDefault(), y.params.preventClicksPropagation && y.animating && (e.stopPropagation(), e.stopImmediatePropagation()))
                    }, y.onClickNext = function(e) {
                        e.preventDefault(), (!y.isEnd || y.params.loop) && y.slideNext()
                    }, y.onClickPrev = function(e) {
                        e.preventDefault(), (!y.isBeginning || y.params.loop) && y.slidePrev()
                    }, y.onClickIndex = function(e) {
                        e.preventDefault();
                        var t = a(this).index() * y.params.slidesPerGroup;
                        y.params.loop && (t += y.loopedSlides), y.slideTo(t)
                    }, y.updateClickedSlide = function(e) {
                        var t = r(e, "." + y.params.slideClass),
                            s = !1;
                        if (t)
                            for (var i = 0; i < y.slides.length; i++) y.slides[i] === t && (s = !0);
                        if (!t || !s) return y.clickedSlide = void 0, void(y.clickedIndex = void 0);
                        if (y.clickedSlide = t, y.clickedIndex = a(t).index(), y.params.slideToClickedSlide && void 0 !== y.clickedIndex && y.clickedIndex !== y.activeIndex) {
                            var n, o = y.clickedIndex;
                            if (y.params.loop) {
                                if (y.animating) return;
                                n = a(y.clickedSlide).attr("data-swiper-slide-index"), y.params.centeredSlides ? o < y.loopedSlides - y.params.slidesPerView / 2 || o > y.slides.length - y.loopedSlides + y.params.slidesPerView / 2 ? (y.fixLoop(), o = y.wrapper.children("." + y.params.slideClass + '[data-swiper-slide-index="' + n + '"]:not(.swiper-slide-duplicate)').eq(0).index(), setTimeout(function() {
                                    y.slideTo(o)
                                }, 0)) : y.slideTo(o) : o > y.slides.length - y.params.slidesPerView ? (y.fixLoop(), o = y.wrapper.children("." + y.params.slideClass + '[data-swiper-slide-index="' + n + '"]:not(.swiper-slide-duplicate)').eq(0).index(), setTimeout(function() {
                                    y.slideTo(o)
                                }, 0)) : y.slideTo(o)
                            } else y.slideTo(o)
                        }
                    };
                    var b, C, T, S, k, z, M, E, P, I, j = "input, select, textarea, button",
                        A = Date.now(),
                        $ = [];
                    y.animating = !1, y.touches = {
                        startX: 0,
                        startY: 0,
                        currentX: 0,
                        currentY: 0,
                        diff: 0
                    };
                    var D, O;
                    if (y.onTouchStart = function(e) {
                            if (e.originalEvent && (e = e.originalEvent), D = "touchstart" === e.type, D || !("which" in e) || 3 !== e.which) {
                                if (y.params.noSwiping && r(e, "." + y.params.noSwipingClass)) return void(y.allowClick = !0);
                                if (!y.params.swipeHandler || r(e, y.params.swipeHandler)) {
                                    var t = y.touches.currentX = "touchstart" === e.type ? e.targetTouches[0].pageX : e.pageX,
                                        s = y.touches.currentY = "touchstart" === e.type ? e.targetTouches[0].pageY : e.pageY;
                                    if (!(y.device.ios && y.params.iOSEdgeSwipeDetection && t <= y.params.iOSEdgeSwipeThreshold)) {
                                        if (b = !0, C = !1, T = !0, k = void 0, O = void 0, y.touches.startX = t, y.touches.startY = s, S = Date.now(), y.allowClick = !0, y.updateContainerSize(), y.swipeDirection = void 0, y.params.threshold > 0 && (E = !1), "touchstart" !== e.type) {
                                            var i = !0;
                                            a(e.target).is(j) && (i = !1), document.activeElement && a(document.activeElement).is(j) && document.activeElement.blur(), i && e.preventDefault()
                                        }
                                        y.emit("onTouchStart", y, e)
                                    }
                                }
                            }
                        }, y.onTouchMove = function(e) {
                            if (e.originalEvent && (e = e.originalEvent), !(D && "mousemove" === e.type || e.preventedByNestedSwiper)) {
                                if (y.params.onlyExternal) return y.allowClick = !1, void(b && (y.touches.startX = y.touches.currentX = "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX, y.touches.startY = y.touches.currentY = "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY, S = Date.now()));
                                if (D && document.activeElement && e.target === document.activeElement && a(e.target).is(j)) return C = !0, void(y.allowClick = !1);
                                if (T && y.emit("onTouchMove", y, e), !(e.targetTouches && e.targetTouches.length > 1)) {
                                    if (y.touches.currentX = "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX, y.touches.currentY = "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY, "undefined" == typeof k) {
                                        var t = 180 * Math.atan2(Math.abs(y.touches.currentY - y.touches.startY), Math.abs(y.touches.currentX - y.touches.startX)) / Math.PI;
                                        k = y.isHorizontal() ? t > y.params.touchAngle : 90 - t > y.params.touchAngle
                                    }
                                    if (k && y.emit("onTouchMoveOpposite", y, e), "undefined" == typeof O && y.browser.ieTouch && (y.touches.currentX !== y.touches.startX || y.touches.currentY !== y.touches.startY) && (O = !0), b) {
                                        if (k) return void(b = !1);
                                        if (O || !y.browser.ieTouch) {
                                            y.allowClick = !1, y.emit("onSliderMove", y, e), e.preventDefault(), y.params.touchMoveStopPropagation && !y.params.nested && e.stopPropagation(), C || (s.loop && y.fixLoop(), M = y.getWrapperTranslate(), y.setWrapperTransition(0), y.animating && y.wrapper.trigger("webkitTransitionEnd transitionend oTransitionEnd MSTransitionEnd msTransitionEnd"), y.params.autoplay && y.autoplaying && (y.params.autoplayDisableOnInteraction ? y.stopAutoplay() : y.pauseAutoplay()), I = !1, y.params.grabCursor && (y.container[0].style.cursor = "move", y.container[0].style.cursor = "-webkit-grabbing", y.container[0].style.cursor = "-moz-grabbin", y.container[0].style.cursor = "grabbing")), C = !0;
                                            var i = y.touches.diff = y.isHorizontal() ? y.touches.currentX - y.touches.startX : y.touches.currentY - y.touches.startY;
                                            i *= y.params.touchRatio, y.rtl && (i = -i), y.swipeDirection = i > 0 ? "prev" : "next", z = i + M;
                                            var n = !0;
                                            if (i > 0 && z > y.minTranslate() ? (n = !1, y.params.resistance && (z = y.minTranslate() - 1 + Math.pow(-y.minTranslate() + M + i, y.params.resistanceRatio))) : 0 > i && z < y.maxTranslate() && (n = !1, y.params.resistance && (z = y.maxTranslate() + 1 - Math.pow(y.maxTranslate() - M - i, y.params.resistanceRatio))), n && (e.preventedByNestedSwiper = !0), !y.params.allowSwipeToNext && "next" === y.swipeDirection && M > z && (z = M), !y.params.allowSwipeToPrev && "prev" === y.swipeDirection && z > M && (z = M), y.params.followFinger) {
                                                if (y.params.threshold > 0) {
                                                    if (!(Math.abs(i) > y.params.threshold || E)) return void(z = M);
                                                    if (!E) return E = !0, y.touches.startX = y.touches.currentX, y.touches.startY = y.touches.currentY, z = M, void(y.touches.diff = y.isHorizontal() ? y.touches.currentX - y.touches.startX : y.touches.currentY - y.touches.startY)
                                                }(y.params.freeMode || y.params.watchSlidesProgress) && y.updateActiveIndex(), y.params.freeMode && (0 === $.length && $.push({
                                                    position: y.touches[y.isHorizontal() ? "startX" : "startY"],
                                                    time: S
                                                }), $.push({
                                                    position: y.touches[y.isHorizontal() ? "currentX" : "currentY"],
                                                    time: (new window.Date).getTime()
                                                })), y.updateProgress(z), y.setWrapperTranslate(z)
                                            }
                                        }
                                    }
                                }
                            }
                        }, y.onTouchEnd = function(e) {
                            if (e.originalEvent && (e = e.originalEvent), T && y.emit("onTouchEnd", y, e), T = !1, b) {
                                y.params.grabCursor && C && b && (y.container[0].style.cursor = "move", y.container[0].style.cursor = "-webkit-grab", y.container[0].style.cursor = "-moz-grab", y.container[0].style.cursor = "grab");
                                var t = Date.now(),
                                    s = t - S;
                                if (y.allowClick && (y.updateClickedSlide(e), y.emit("onTap", y, e), 300 > s && t - A > 300 && (P && clearTimeout(P), P = setTimeout(function() {
                                        y && (y.params.paginationHide && y.paginationContainer.length > 0 && !a(e.target).hasClass(y.params.bulletClass) && y.paginationContainer.toggleClass(y.params.paginationHiddenClass), y.emit("onClick", y, e))
                                    }, 300)), 300 > s && 300 > t - A && (P && clearTimeout(P), y.emit("onDoubleTap", y, e))), A = Date.now(), setTimeout(function() {
                                        y && (y.allowClick = !0)
                                    }, 0), !b || !C || !y.swipeDirection || 0 === y.touches.diff || z === M) return void(b = C = !1);
                                b = C = !1;
                                var i;
                                if (i = y.params.followFinger ? y.rtl ? y.translate : -y.translate : -z, y.params.freeMode) {
                                    if (i < -y.minTranslate()) return void y.slideTo(y.activeIndex);
                                    if (i > -y.maxTranslate()) return void y.slideTo(y.slides.length < y.snapGrid.length ? y.snapGrid.length - 1 : y.slides.length - 1);
                                    if (y.params.freeModeMomentum) {
                                        if ($.length > 1) {
                                            var n = $.pop(),
                                                r = $.pop(),
                                                o = n.position - r.position,
                                                l = n.time - r.time;
                                            y.velocity = o / l, y.velocity = y.velocity / 2, Math.abs(y.velocity) < y.params.freeModeMinimumVelocity && (y.velocity = 0), (l > 150 || (new window.Date).getTime() - n.time > 300) && (y.velocity = 0)
                                        } else y.velocity = 0;
                                        $.length = 0;
                                        var d = 1e3 * y.params.freeModeMomentumRatio,
                                            p = y.velocity * d,
                                            c = y.translate + p;
                                        y.rtl && (c = -c);
                                        var u, h = !1,
                                            f = 20 * Math.abs(y.velocity) * y.params.freeModeMomentumBounceRatio;
                                        if (c < y.maxTranslate()) y.params.freeModeMomentumBounce ? (c + y.maxTranslate() < -f && (c = y.maxTranslate() - f), u = y.maxTranslate(), h = !0, I = !0) : c = y.maxTranslate();
                                        else if (c > y.minTranslate()) y.params.freeModeMomentumBounce ? (c - y.minTranslate() > f && (c = y.minTranslate() + f), u = y.minTranslate(), h = !0, I = !0) : c = y.minTranslate();
                                        else if (y.params.freeModeSticky) {
                                            var m, g = 0;
                                            for (g = 0; g < y.snapGrid.length; g += 1)
                                                if (y.snapGrid[g] > -c) {
                                                    m = g;
                                                    break
                                                }
                                            c = Math.abs(y.snapGrid[m] - c) < Math.abs(y.snapGrid[m - 1] - c) || "next" === y.swipeDirection ? y.snapGrid[m] : y.snapGrid[m - 1], y.rtl || (c = -c)
                                        }
                                        if (0 !== y.velocity) d = Math.abs(y.rtl ? (-c - y.translate) / y.velocity : (c - y.translate) / y.velocity);
                                        else if (y.params.freeModeSticky) return void y.slideReset();
                                        y.params.freeModeMomentumBounce && h ? (y.updateProgress(u), y.setWrapperTransition(d), y.setWrapperTranslate(c), y.onTransitionStart(), y.animating = !0, y.wrapper.transitionEnd(function() {
                                            y && I && (y.emit("onMomentumBounce", y), y.setWrapperTransition(y.params.speed), y.setWrapperTranslate(u), y.wrapper.transitionEnd(function() {
                                                y && y.onTransitionEnd()
                                            }))
                                        })) : y.velocity ? (y.updateProgress(c), y.setWrapperTransition(d), y.setWrapperTranslate(c), y.onTransitionStart(), y.animating || (y.animating = !0, y.wrapper.transitionEnd(function() {
                                            y && y.onTransitionEnd()
                                        }))) : y.updateProgress(c), y.updateActiveIndex()
                                    }
                                    return void((!y.params.freeModeMomentum || s >= y.params.longSwipesMs) && (y.updateProgress(), y.updateActiveIndex()))
                                }
                                var v, w = 0,
                                    x = y.slidesSizesGrid[0];
                                for (v = 0; v < y.slidesGrid.length; v += y.params.slidesPerGroup) "undefined" != typeof y.slidesGrid[v + y.params.slidesPerGroup] ? i >= y.slidesGrid[v] && i < y.slidesGrid[v + y.params.slidesPerGroup] && (w = v, x = y.slidesGrid[v + y.params.slidesPerGroup] - y.slidesGrid[v]) : i >= y.slidesGrid[v] && (w = v, x = y.slidesGrid[y.slidesGrid.length - 1] - y.slidesGrid[y.slidesGrid.length - 2]);
                                var k = (i - y.slidesGrid[w]) / x;
                                if (s > y.params.longSwipesMs) {
                                    if (!y.params.longSwipes) return void y.slideTo(y.activeIndex);
                                    "next" === y.swipeDirection && y.slideTo(k >= y.params.longSwipesRatio ? w + y.params.slidesPerGroup : w), "prev" === y.swipeDirection && y.slideTo(k > 1 - y.params.longSwipesRatio ? w + y.params.slidesPerGroup : w)
                                } else {
                                    if (!y.params.shortSwipes) return void y.slideTo(y.activeIndex);
                                    "next" === y.swipeDirection && y.slideTo(w + y.params.slidesPerGroup), "prev" === y.swipeDirection && y.slideTo(w)
                                }
                            }
                        }, y._slideTo = function(e, a) {
                            return y.slideTo(e, a, !0, !0)
                        }, y.slideTo = function(e, a, t, s) {
                            "undefined" == typeof t && (t = !0), "undefined" == typeof e && (e = 0), 0 > e && (e = 0), y.snapIndex = Math.floor(e / y.params.slidesPerGroup), y.snapIndex >= y.snapGrid.length && (y.snapIndex = y.snapGrid.length - 1);
                            var i = -y.snapGrid[y.snapIndex];
                            y.params.autoplay && y.autoplaying && (s || !y.params.autoplayDisableOnInteraction ? y.pauseAutoplay(a) : y.stopAutoplay()), y.updateProgress(i);
                            for (var n = 0; n < y.slidesGrid.length; n++) - Math.floor(100 * i) >= Math.floor(100 * y.slidesGrid[n]) && (e = n);
                            return !y.params.allowSwipeToNext && i < y.translate && i < y.minTranslate() ? !1 : !y.params.allowSwipeToPrev && i > y.translate && i > y.maxTranslate() && (y.activeIndex || 0) !== e ? !1 : ("undefined" == typeof a && (a = y.params.speed), y.previousIndex = y.activeIndex || 0, y.activeIndex = e, y.rtl && -i === y.translate || !y.rtl && i === y.translate ? (y.params.autoHeight && y.updateAutoHeight(), y.updateClasses(), "slide" !== y.params.effect && y.setWrapperTranslate(i), !1) : (y.updateClasses(), y.onTransitionStart(t), 0 === a ? (y.setWrapperTranslate(i), y.setWrapperTransition(0), y.onTransitionEnd(t)) : (y.setWrapperTranslate(i), y.setWrapperTransition(a), y.animating || (y.animating = !0, y.wrapper.transitionEnd(function() {
                                y && y.onTransitionEnd(t)
                            }))), !0))
                        }, y.onTransitionStart = function(e) {
                            "undefined" == typeof e && (e = !0), y.params.autoHeight && y.updateAutoHeight(), y.lazy && y.lazy.onTransitionStart(), e && (y.emit("onTransitionStart", y), y.activeIndex !== y.previousIndex && (y.emit("onSlideChangeStart", y), y.activeIndex > y.previousIndex ? y.emit("onSlideNextStart", y) : y.emit("onSlidePrevStart", y)))
                        }, y.onTransitionEnd = function(e) {
                            y.animating = !1, y.setWrapperTransition(0), "undefined" == typeof e && (e = !0), y.lazy && y.lazy.onTransitionEnd(), e && (y.emit("onTransitionEnd", y), y.activeIndex !== y.previousIndex && (y.emit("onSlideChangeEnd", y), y.activeIndex > y.previousIndex ? y.emit("onSlideNextEnd", y) : y.emit("onSlidePrevEnd", y))), y.params.hashnav && y.hashnav && y.hashnav.setHash()
                        }, y.slideNext = function(e, a, t) {
                            return y.params.loop ? y.animating ? !1 : (y.fixLoop(), y.container[0].clientLeft, y.slideTo(y.activeIndex + y.params.slidesPerGroup, a, e, t)) : y.slideTo(y.activeIndex + y.params.slidesPerGroup, a, e, t)
                        }, y._slideNext = function(e) {
                            return y.slideNext(!0, e, !0)
                        }, y.slidePrev = function(e, a, t) {
                            return y.params.loop ? y.animating ? !1 : (y.fixLoop(), y.container[0].clientLeft, y.slideTo(y.activeIndex - 1, a, e, t)) : y.slideTo(y.activeIndex - 1, a, e, t)
                        }, y._slidePrev = function(e) {
                            return y.slidePrev(!0, e, !0)
                        }, y.slideReset = function(e, a, t) {
                            return y.slideTo(y.activeIndex, a, e)
                        }, y.setWrapperTransition = function(e, a) {
                            y.wrapper.transition(e), "slide" !== y.params.effect && y.effects[y.params.effect] && y.effects[y.params.effect].setTransition(e), y.params.parallax && y.parallax && y.parallax.setTransition(e), y.params.scrollbar && y.scrollbar && y.scrollbar.setTransition(e), y.params.control && y.controller && y.controller.setTransition(e, a), y.emit("onSetTransition", y, e)
                        }, y.setWrapperTranslate = function(e, a, t) {
                            var s = 0,
                                n = 0,
                                r = 0;
                            y.isHorizontal() ? s = y.rtl ? -e : e : n = e, y.params.roundLengths && (s = i(s), n = i(n)), y.params.virtualTranslate || y.wrapper.transform(y.support.transforms3d ? "translate3d(" + s + "px, " + n + "px, " + r + "px)" : "translate(" + s + "px, " + n + "px)"), y.translate = y.isHorizontal() ? s : n;
                            var o, l = y.maxTranslate() - y.minTranslate();
                            o = 0 === l ? 0 : (e - y.minTranslate()) / l, o !== y.progress && y.updateProgress(e), a && y.updateActiveIndex(), "slide" !== y.params.effect && y.effects[y.params.effect] && y.effects[y.params.effect].setTranslate(y.translate), y.params.parallax && y.parallax && y.parallax.setTranslate(y.translate), y.params.scrollbar && y.scrollbar && y.scrollbar.setTranslate(y.translate), y.params.control && y.controller && y.controller.setTranslate(y.translate, t), y.emit("onSetTranslate", y, y.translate)
                        }, y.getTranslate = function(e, a) {
                            var t, s, i, n;
                            return "undefined" == typeof a && (a = "x"), y.params.virtualTranslate ? y.rtl ? -y.translate : y.translate : (i = window.getComputedStyle(e, null), window.WebKitCSSMatrix ? (s = i.transform || i.webkitTransform, s.split(",").length > 6 && (s = s.split(", ").map(function(e) {
                                return e.replace(",", ".")
                            }).join(", ")), n = new window.WebKitCSSMatrix("none" === s ? "" : s)) : (n = i.MozTransform || i.OTransform || i.MsTransform || i.msTransform || i.transform || i.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,"), t = n.toString().split(",")), "x" === a && (s = window.WebKitCSSMatrix ? n.m41 : parseFloat(16 === t.length ? t[12] : t[4])), "y" === a && (s = window.WebKitCSSMatrix ? n.m42 : parseFloat(16 === t.length ? t[13] : t[5])), y.rtl && s && (s = -s), s || 0)
                        }, y.getWrapperTranslate = function(e) {
                            return "undefined" == typeof e && (e = y.isHorizontal() ? "x" : "y"), y.getTranslate(y.wrapper[0], e)
                        }, y.observers = [], y.initObservers = function() {
                            if (y.params.observeParents)
                                for (var e = y.container.parents(), a = 0; a < e.length; a++) o(e[a]);
                            o(y.container[0], {
                                childList: !1
                            }), o(y.wrapper[0], {
                                attributes: !1
                            })
                        }, y.disconnectObservers = function() {
                            for (var e = 0; e < y.observers.length; e++) y.observers[e].disconnect();
                            y.observers = []
                        }, y.createLoop = function() {
                            y.wrapper.children("." + y.params.slideClass + "." + y.params.slideDuplicateClass).remove();
                            var e = y.wrapper.children("." + y.params.slideClass);
                            "auto" !== y.params.slidesPerView || y.params.loopedSlides || (y.params.loopedSlides = e.length), y.loopedSlides = parseInt(y.params.loopedSlides || y.params.slidesPerView, 10), y.loopedSlides = y.loopedSlides + y.params.loopAdditionalSlides, y.loopedSlides > e.length && (y.loopedSlides = e.length);
                            var t, s = [],
                                i = [];
                            for (e.each(function(t, n) {
                                var r = a(this);
                                t < y.loopedSlides && i.push(n), t < e.length && t >= e.length - y.loopedSlides && s.push(n), r.attr("data-swiper-slide-index", t)
                            }), t = 0; t < i.length; t++) y.wrapper.append(a(i[t].cloneNode(!0)).addClass(y.params.slideDuplicateClass));
                            for (t = s.length - 1; t >= 0; t--) y.wrapper.prepend(a(s[t].cloneNode(!0)).addClass(y.params.slideDuplicateClass))
                        }, y.destroyLoop = function() {
                            y.wrapper.children("." + y.params.slideClass + "." + y.params.slideDuplicateClass).remove(), y.slides.removeAttr("data-swiper-slide-index")
                        }, y.fixLoop = function() {
                            var e;
                            y.activeIndex < y.loopedSlides ? (e = y.slides.length - 3 * y.loopedSlides + y.activeIndex, e += y.loopedSlides, y.slideTo(e, 0, !1, !0)) : ("auto" === y.params.slidesPerView && y.activeIndex >= 2 * y.loopedSlides || y.activeIndex > y.slides.length - 2 * y.params.slidesPerView) && (e = -y.slides.length + y.activeIndex + y.loopedSlides, e += y.loopedSlides, y.slideTo(e, 0, !1, !0))
                        }, y.appendSlide = function(e) {
                            if (y.params.loop && y.destroyLoop(), "object" == typeof e && e.length)
                                for (var a = 0; a < e.length; a++) e[a] && y.wrapper.append(e[a]);
                            else y.wrapper.append(e);
                            y.params.loop && y.createLoop(), y.params.observer && y.support.observer || y.update(!0)
                        }, y.prependSlide = function(e) {
                            y.params.loop && y.destroyLoop();
                            var a = y.activeIndex + 1;
                            if ("object" == typeof e && e.length) {
                                for (var t = 0; t < e.length; t++) e[t] && y.wrapper.prepend(e[t]);
                                a = y.activeIndex + e.length
                            } else y.wrapper.prepend(e);
                            y.params.loop && y.createLoop(), y.params.observer && y.support.observer || y.update(!0), y.slideTo(a, 0, !1)
                        }, y.removeSlide = function(e) {
                            y.params.loop && (y.destroyLoop(), y.slides = y.wrapper.children("." + y.params.slideClass));
                            var a, t = y.activeIndex;
                            if ("object" == typeof e && e.length) {
                                for (var s = 0; s < e.length; s++) a = e[s], y.slides[a] && y.slides.eq(a).remove(), t > a && t--;
                                t = Math.max(t, 0)
                            } else a = e, y.slides[a] && y.slides.eq(a).remove(), t > a && t--, t = Math.max(t, 0);
                            y.params.loop && y.createLoop(), y.params.observer && y.support.observer || y.update(!0), y.params.loop ? y.slideTo(t + y.loopedSlides, 0, !1) : y.slideTo(t, 0, !1)
                        }, y.removeAllSlides = function() {
                            for (var e = [], a = 0; a < y.slides.length; a++) e.push(a);
                            y.removeSlide(e)
                        }, y.effects = {
                            fade: {
                                setTranslate: function() {
                                    for (var e = 0; e < y.slides.length; e++) {
                                        var a = y.slides.eq(e),
                                            t = a[0].swiperSlideOffset,
                                            s = -t;
                                        y.params.virtualTranslate || (s -= y.translate);
                                        var i = 0;
                                        y.isHorizontal() || (i = s, s = 0);
                                        var n = y.params.fade.crossFade ? Math.max(1 - Math.abs(a[0].progress), 0) : 1 + Math.min(Math.max(a[0].progress, -1), 0);
                                        a.css({
                                            opacity: n
                                        }).transform("translate3d(" + s + "px, " + i + "px, 0px)")
                                    }
                                },
                                setTransition: function(e) {
                                    if (y.slides.transition(e), y.params.virtualTranslate && 0 !== e) {
                                        var a = !1;
                                        y.slides.transitionEnd(function() {
                                            if (!a && y) {
                                                a = !0, y.animating = !1;
                                                for (var e = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"], t = 0; t < e.length; t++) y.wrapper.trigger(e[t])
                                            }
                                        })
                                    }
                                }
                            },
                            flip: {
                                setTranslate: function() {
                                    for (var e = 0; e < y.slides.length; e++) {
                                        var t = y.slides.eq(e),
                                            s = t[0].progress;
                                        y.params.flip.limitRotation && (s = Math.max(Math.min(t[0].progress, 1), -1));
                                        var i = t[0].swiperSlideOffset,
                                            n = -180 * s,
                                            r = n,
                                            o = 0,
                                            l = -i,
                                            d = 0;
                                        if (y.isHorizontal() ? y.rtl && (r = -r) : (d = l, l = 0, o = -r, r = 0), t[0].style.zIndex = -Math.abs(Math.round(s)) + y.slides.length, y.params.flip.slideShadows) {
                                            var p = t.find(y.isHorizontal() ? ".swiper-slide-shadow-left" : ".swiper-slide-shadow-top"),
                                                c = t.find(y.isHorizontal() ? ".swiper-slide-shadow-right" : ".swiper-slide-shadow-bottom");
                                            0 === p.length && (p = a('<div class="swiper-slide-shadow-' + (y.isHorizontal() ? "left" : "top") + '"></div>'), t.append(p)), 0 === c.length && (c = a('<div class="swiper-slide-shadow-' + (y.isHorizontal() ? "right" : "bottom") + '"></div>'), t.append(c)), p.length && (p[0].style.opacity = Math.max(-s, 0)), c.length && (c[0].style.opacity = Math.max(s, 0))
                                        }
                                        t.transform("translate3d(" + l + "px, " + d + "px, 0px) rotateX(" + o + "deg) rotateY(" + r + "deg)")
                                    }
                                },
                                setTransition: function(e) {
                                    if (y.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), y.params.virtualTranslate && 0 !== e) {
                                        var t = !1;
                                        y.slides.eq(y.activeIndex).transitionEnd(function() {
                                            if (!t && y && a(this).hasClass(y.params.slideActiveClass)) {
                                                t = !0, y.animating = !1;
                                                for (var e = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"], s = 0; s < e.length; s++) y.wrapper.trigger(e[s])
                                            }
                                        })
                                    }
                                }
                            },
                            cube: {
                                setTranslate: function() {
                                    var e, t = 0;
                                    y.params.cube.shadow && (y.isHorizontal() ? (e = y.wrapper.find(".swiper-cube-shadow"), 0 === e.length && (e = a('<div class="swiper-cube-shadow"></div>'), y.wrapper.append(e)), e.css({
                                        height: y.width + "px"
                                    })) : (e = y.container.find(".swiper-cube-shadow"), 0 === e.length && (e = a('<div class="swiper-cube-shadow"></div>'), y.container.append(e))));
                                    for (var s = 0; s < y.slides.length; s++) {
                                        var i = y.slides.eq(s),
                                            n = 90 * s,
                                            r = Math.floor(n / 360);
                                        y.rtl && (n = -n, r = Math.floor(-n / 360));
                                        var o = Math.max(Math.min(i[0].progress, 1), -1),
                                            l = 0,
                                            d = 0,
                                            p = 0;
                                        s % 4 === 0 ? (l = 4 * -r * y.size, p = 0) : (s - 1) % 4 === 0 ? (l = 0, p = 4 * -r * y.size) : (s - 2) % 4 === 0 ? (l = y.size + 4 * r * y.size, p = y.size) : (s - 3) % 4 === 0 && (l = -y.size, p = 3 * y.size + 4 * y.size * r), y.rtl && (l = -l), y.isHorizontal() || (d = l, l = 0);
                                        var c = "rotateX(" + (y.isHorizontal() ? 0 : -n) + "deg) rotateY(" + (y.isHorizontal() ? n : 0) + "deg) translate3d(" + l + "px, " + d + "px, " + p + "px)";
                                        if (1 >= o && o > -1 && (t = 90 * s + 90 * o, y.rtl && (t = 90 * -s - 90 * o)), i.transform(c), y.params.cube.slideShadows) {
                                            var u = i.find(y.isHorizontal() ? ".swiper-slide-shadow-left" : ".swiper-slide-shadow-top"),
                                                h = i.find(y.isHorizontal() ? ".swiper-slide-shadow-right" : ".swiper-slide-shadow-bottom");
                                            0 === u.length && (u = a('<div class="swiper-slide-shadow-' + (y.isHorizontal() ? "left" : "top") + '"></div>'), i.append(u)), 0 === h.length && (h = a('<div class="swiper-slide-shadow-' + (y.isHorizontal() ? "right" : "bottom") + '"></div>'), i.append(h)), u.length && (u[0].style.opacity = Math.max(-o, 0)), h.length && (h[0].style.opacity = Math.max(o, 0))
                                        }
                                    }
                                    if (y.wrapper.css({
                                            "-webkit-transform-origin": "50% 50% -" + y.size / 2 + "px",
                                            "-moz-transform-origin": "50% 50% -" + y.size / 2 + "px",
                                            "-ms-transform-origin": "50% 50% -" + y.size / 2 + "px",
                                            "transform-origin": "50% 50% -" + y.size / 2 + "px"
                                        }), y.params.cube.shadow)
                                        if (y.isHorizontal()) e.transform("translate3d(0px, " + (y.width / 2 + y.params.cube.shadowOffset) + "px, " + -y.width / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + y.params.cube.shadowScale + ")");
                                        else {
                                            var f = Math.abs(t) - 90 * Math.floor(Math.abs(t) / 90),
                                                m = 1.5 - (Math.sin(2 * f * Math.PI / 360) / 2 + Math.cos(2 * f * Math.PI / 360) / 2),
                                                g = y.params.cube.shadowScale,
                                                v = y.params.cube.shadowScale / m,
                                                w = y.params.cube.shadowOffset;
                                            e.transform("scale3d(" + g + ", 1, " + v + ") translate3d(0px, " + (y.height / 2 + w) + "px, " + -y.height / 2 / v + "px) rotateX(-90deg)")
                                        }
                                    var x = y.isSafari || y.isUiWebView ? -y.size / 2 : 0;
                                    y.wrapper.transform("translate3d(0px,0," + x + "px) rotateX(" + (y.isHorizontal() ? 0 : t) + "deg) rotateY(" + (y.isHorizontal() ? -t : 0) + "deg)")
                                },
                                setTransition: function(e) {
                                    y.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), y.params.cube.shadow && !y.isHorizontal() && y.container.find(".swiper-cube-shadow").transition(e)
                                }
                            },
                            coverflow: {
                                setTranslate: function() {
                                    for (var e = y.translate, t = y.isHorizontal() ? -e + y.width / 2 : -e + y.height / 2, s = y.isHorizontal() ? y.params.coverflow.rotate : -y.params.coverflow.rotate, i = y.params.coverflow.depth, n = 0, r = y.slides.length; r > n; n++) {
                                        var o = y.slides.eq(n),
                                            l = y.slidesSizesGrid[n],
                                            d = o[0].swiperSlideOffset,
                                            p = (t - d - l / 2) / l * y.params.coverflow.modifier,
                                            c = y.isHorizontal() ? s * p : 0,
                                            u = y.isHorizontal() ? 0 : s * p,
                                            h = -i * Math.abs(p),
                                            f = y.isHorizontal() ? 0 : y.params.coverflow.stretch * p,
                                            m = y.isHorizontal() ? y.params.coverflow.stretch * p : 0;
                                        Math.abs(m) < .001 && (m = 0), Math.abs(f) < .001 && (f = 0), Math.abs(h) < .001 && (h = 0), Math.abs(c) < .001 && (c = 0), Math.abs(u) < .001 && (u = 0);
                                        var g = "translate3d(" + m + "px," + f + "px," + h + "px) rotateX(" + u + "deg) rotateY(" + c + "deg)";
                                        if (o.transform(g), o[0].style.zIndex = -Math.abs(Math.round(p)) + 1, y.params.coverflow.slideShadows) {
                                            var v = o.find(y.isHorizontal() ? ".swiper-slide-shadow-left" : ".swiper-slide-shadow-top"),
                                                w = o.find(y.isHorizontal() ? ".swiper-slide-shadow-right" : ".swiper-slide-shadow-bottom");
                                            0 === v.length && (v = a('<div class="swiper-slide-shadow-' + (y.isHorizontal() ? "left" : "top") + '"></div>'), o.append(v)), 0 === w.length && (w = a('<div class="swiper-slide-shadow-' + (y.isHorizontal() ? "right" : "bottom") + '"></div>'), o.append(w)), v.length && (v[0].style.opacity = p > 0 ? p : 0), w.length && (w[0].style.opacity = -p > 0 ? -p : 0)
                                        }
                                    }
                                    if (y.browser.ie) {
                                        var x = y.wrapper[0].style;
                                        x.perspectiveOrigin = t + "px 50%"
                                    }
                                },
                                setTransition: function(e) {
                                    y.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e)
                                }
                            }
                        }, y.lazy = {
                            initialImageLoaded: !1,
                            loadImageInSlide: function(e, t) {
                                if ("undefined" != typeof e && ("undefined" == typeof t && (t = !0), 0 !== y.slides.length)) {
                                    var s = y.slides.eq(e),
                                        i = s.find(".swiper-lazy:not(.swiper-lazy-loaded):not(.swiper-lazy-loading)");
                                    !s.hasClass("swiper-lazy") || s.hasClass("swiper-lazy-loaded") || s.hasClass("swiper-lazy-loading") || (i = i.add(s[0])), 0 !== i.length && i.each(function() {
                                        var e = a(this);
                                        e.addClass("swiper-lazy-loading");
                                        var i = e.attr("data-background"),
                                            n = e.attr("data-src"),
                                            r = e.attr("data-srcset");
                                        y.loadImage(e[0], n || i, r, !1, function() {
                                            if (i ? (e.css("background-image", "url(" + i + ")"), e.removeAttr("data-background")) : (r && (e.attr("srcset", r), e.removeAttr("data-srcset")), n && (e.attr("src", n), e.removeAttr("data-src"))), e.addClass("swiper-lazy-loaded").removeClass("swiper-lazy-loading"), s.find(".swiper-lazy-preloader, .preloader").remove(), y.params.loop && t) {
                                                var a = s.attr("data-swiper-slide-index");
                                                if (s.hasClass(y.params.slideDuplicateClass)) {
                                                    var o = y.wrapper.children('[data-swiper-slide-index="' + a + '"]:not(.' + y.params.slideDuplicateClass + ")");
                                                    y.lazy.loadImageInSlide(o.index(), !1)
                                                } else {
                                                    var l = y.wrapper.children("." + y.params.slideDuplicateClass + '[data-swiper-slide-index="' + a + '"]');
                                                    y.lazy.loadImageInSlide(l.index(), !1)
                                                }
                                            }
                                            y.emit("onLazyImageReady", y, s[0], e[0])
                                        }), y.emit("onLazyImageLoad", y, s[0], e[0])
                                    })
                                }
                            },
                            load: function() {
                                var e;
                                if (y.params.watchSlidesVisibility) y.wrapper.children("." + y.params.slideVisibleClass).each(function() {
                                    y.lazy.loadImageInSlide(a(this).index())
                                });
                                else if (y.params.slidesPerView > 1)
                                    for (e = y.activeIndex; e < y.activeIndex + y.params.slidesPerView; e++) y.slides[e] && y.lazy.loadImageInSlide(e);
                                else y.lazy.loadImageInSlide(y.activeIndex);
                                if (y.params.lazyLoadingInPrevNext)
                                    if (y.params.slidesPerView > 1 || y.params.lazyLoadingInPrevNextAmount && y.params.lazyLoadingInPrevNextAmount > 1) {
                                        var t = y.params.lazyLoadingInPrevNextAmount,
                                            s = y.params.slidesPerView,
                                            i = Math.min(y.activeIndex + s + Math.max(t, s), y.slides.length),
                                            n = Math.max(y.activeIndex - Math.max(s, t), 0);
                                        for (e = y.activeIndex + y.params.slidesPerView; i > e; e++) y.slides[e] && y.lazy.loadImageInSlide(e);
                                        for (e = n; e < y.activeIndex; e++) y.slides[e] && y.lazy.loadImageInSlide(e)
                                    } else {
                                        var r = y.wrapper.children("." + y.params.slideNextClass);
                                        r.length > 0 && y.lazy.loadImageInSlide(r.index());
                                        var o = y.wrapper.children("." + y.params.slidePrevClass);
                                        o.length > 0 && y.lazy.loadImageInSlide(o.index())
                                    }
                            },
                            onTransitionStart: function() {
                                y.params.lazyLoading && (y.params.lazyLoadingOnTransitionStart || !y.params.lazyLoadingOnTransitionStart && !y.lazy.initialImageLoaded) && y.lazy.load()
                            },
                            onTransitionEnd: function() {
                                y.params.lazyLoading && !y.params.lazyLoadingOnTransitionStart && y.lazy.load()
                            }
                        }, y.scrollbar = {
                            isTouched: !1,
                            setDragPosition: function(e) {
                                var a = y.scrollbar,
                                    t = y.isHorizontal() ? "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX || e.clientX : "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY || e.clientY,
                                    s = t - a.track.offset()[y.isHorizontal() ? "left" : "top"] - a.dragSize / 2,
                                    i = -y.minTranslate() * a.moveDivider,
                                    n = -y.maxTranslate() * a.moveDivider;
                                i > s ? s = i : s > n && (s = n), s = -s / a.moveDivider, y.updateProgress(s), y.setWrapperTranslate(s, !0)
                            },
                            dragStart: function(e) {
                                var a = y.scrollbar;
                                a.isTouched = !0, e.preventDefault(), e.stopPropagation(), a.setDragPosition(e), clearTimeout(a.dragTimeout), a.track.transition(0), y.params.scrollbarHide && a.track.css("opacity", 1), y.wrapper.transition(100), a.drag.transition(100), y.emit("onScrollbarDragStart", y)
                            },
                            dragMove: function(e) {
                                var a = y.scrollbar;
                                a.isTouched && (e.preventDefault ? e.preventDefault() : e.returnValue = !1, a.setDragPosition(e), y.wrapper.transition(0), a.track.transition(0), a.drag.transition(0), y.emit("onScrollbarDragMove", y))
                            },
                            dragEnd: function(e) {
                                var a = y.scrollbar;
                                a.isTouched && (a.isTouched = !1, y.params.scrollbarHide && (clearTimeout(a.dragTimeout), a.dragTimeout = setTimeout(function() {
                                    a.track.css("opacity", 0), a.track.transition(400)
                                }, 1e3)), y.emit("onScrollbarDragEnd", y), y.params.scrollbarSnapOnRelease && y.slideReset())
                            },
                            enableDraggable: function() {
                                var e = y.scrollbar,
                                    t = y.support.touch ? e.track : document;
                                a(e.track).on(y.touchEvents.start, e.dragStart), a(t).on(y.touchEvents.move, e.dragMove), a(t).on(y.touchEvents.end, e.dragEnd)
                            },
                            disableDraggable: function() {
                                var e = y.scrollbar,
                                    t = y.support.touch ? e.track : document;
                                a(e.track).off(y.touchEvents.start, e.dragStart), a(t).off(y.touchEvents.move, e.dragMove), a(t).off(y.touchEvents.end, e.dragEnd)
                            },
                            set: function() {
                                if (y.params.scrollbar) {
                                    var e = y.scrollbar;
                                    e.track = a(y.params.scrollbar), e.drag = e.track.find(".swiper-scrollbar-drag"), 0 === e.drag.length && (e.drag = a('<div class="swiper-scrollbar-drag"></div>'), e.track.append(e.drag)), e.drag[0].style.width = "", e.drag[0].style.height = "", e.trackSize = y.isHorizontal() ? e.track[0].offsetWidth : e.track[0].offsetHeight, e.divider = y.size / y.virtualSize, e.moveDivider = e.divider * (e.trackSize / y.size), e.dragSize = e.trackSize * e.divider, y.isHorizontal() ? e.drag[0].style.width = e.dragSize + "px" : e.drag[0].style.height = e.dragSize + "px", e.divider >= 1 ? e.track[0].style.display = "none" : e.track[0].style.display = "", y.params.scrollbarHide && (e.track[0].style.opacity = 0)
                                }
                            },
                            setTranslate: function() {
                                if (y.params.scrollbar) {
                                    var e, a = y.scrollbar,
                                        t = (y.translate || 0, a.dragSize);
                                    e = (a.trackSize - a.dragSize) * y.progress, y.rtl && y.isHorizontal() ? (e = -e, e > 0 ? (t = a.dragSize - e, e = 0) : -e + a.dragSize > a.trackSize && (t = a.trackSize + e)) : 0 > e ? (t = a.dragSize + e, e = 0) : e + a.dragSize > a.trackSize && (t = a.trackSize - e), y.isHorizontal() ? (a.drag.transform(y.support.transforms3d ? "translate3d(" + e + "px, 0, 0)" : "translateX(" + e + "px)"), a.drag[0].style.width = t + "px") : (a.drag.transform(y.support.transforms3d ? "translate3d(0px, " + e + "px, 0)" : "translateY(" + e + "px)"), a.drag[0].style.height = t + "px"), y.params.scrollbarHide && (clearTimeout(a.timeout), a.track[0].style.opacity = 1, a.timeout = setTimeout(function() {
                                        a.track[0].style.opacity = 0, a.track.transition(400)
                                    }, 1e3))
                                }
                            },
                            setTransition: function(e) {
                                y.params.scrollbar && y.scrollbar.drag.transition(e)
                            }
                        }, y.controller = {
                            LinearSpline: function(e, a) {
                                this.x = e, this.y = a, this.lastIndex = e.length - 1;
                                var t, s;
                                this.x.length, this.interpolate = function(e) {
                                    return e ? (s = i(this.x, e), t = s - 1, (e - this.x[t]) * (this.y[s] - this.y[t]) / (this.x[s] - this.x[t]) + this.y[t]) : 0
                                };
                                var i = function() {
                                    var e, a, t;
                                    return function(s, i) {
                                        for (a = -1, e = s.length; e - a > 1;) s[t = e + a >> 1] <= i ? a = t : e = t;
                                        return e
                                    }
                                }()
                            },
                            getInterpolateFunction: function(e) {
                                y.controller.spline || (y.controller.spline = y.params.loop ? new y.controller.LinearSpline(y.slidesGrid, e.slidesGrid) : new y.controller.LinearSpline(y.snapGrid, e.snapGrid))
                            },
                            setTranslate: function(e, a) {
                                function s(a) {
                                    e = a.rtl && "horizontal" === a.params.direction ? -y.translate : y.translate, "slide" === y.params.controlBy && (y.controller.getInterpolateFunction(a), n = -y.controller.spline.interpolate(-e)), n && "container" !== y.params.controlBy || (i = (a.maxTranslate() - a.minTranslate()) / (y.maxTranslate() - y.minTranslate()), n = (e - y.minTranslate()) * i + a.minTranslate()), y.params.controlInverse && (n = a.maxTranslate() - n), a.updateProgress(n), a.setWrapperTranslate(n, !1, y), a.updateActiveIndex()
                                }
                                var i, n, r = y.params.control;
                                if (y.isArray(r))
                                    for (var o = 0; o < r.length; o++) r[o] !== a && r[o] instanceof t && s(r[o]);
                                else r instanceof t && a !== r && s(r)
                            },
                            setTransition: function(e, a) {
                                function s(a) {
                                    a.setWrapperTransition(e, y), 0 !== e && (a.onTransitionStart(), a.wrapper.transitionEnd(function() {
                                        n && (a.params.loop && "slide" === y.params.controlBy && a.fixLoop(), a.onTransitionEnd())
                                    }))
                                }
                                var i, n = y.params.control;
                                if (y.isArray(n))
                                    for (i = 0; i < n.length; i++) n[i] !== a && n[i] instanceof t && s(n[i]);
                                else n instanceof t && a !== n && s(n)
                            }
                        }, y.hashnav = {
                            init: function() {
                                if (y.params.hashnav) {
                                    y.hashnav.initialized = !0;
                                    var e = document.location.hash.replace("#", "");
                                    if (e)
                                        for (var a = 0, t = 0, s = y.slides.length; s > t; t++) {
                                            var i = y.slides.eq(t),
                                                n = i.attr("data-hash");
                                            if (n === e && !i.hasClass(y.params.slideDuplicateClass)) {
                                                var r = i.index();
                                                y.slideTo(r, a, y.params.runCallbacksOnInit, !0)
                                            }
                                        }
                                }
                            },
                            setHash: function() {
                                y.hashnav.initialized && y.params.hashnav && (document.location.hash = y.slides.eq(y.activeIndex).attr("data-hash") || "")
                            }
                        }, y.disableKeyboardControl = function() {
                            y.params.keyboardControl = !1, a(document).off("keydown", l)
                        }, y.enableKeyboardControl = function() {
                            y.params.keyboardControl = !0, a(document).on("keydown", l)
                        }, y.mousewheel = {
                            event: !1,
                            lastScrollTime: (new window.Date).getTime()
                        }, y.params.mousewheelControl) {
                        try {
                            new window.WheelEvent("wheel"), y.mousewheel.event = "wheel"
                        } catch (L) {}
                        y.mousewheel.event || void 0 === document.onmousewheel || (y.mousewheel.event = "mousewheel"), y.mousewheel.event || (y.mousewheel.event = "DOMMouseScroll")
                    }
                    y.disableMousewheelControl = function() {
                        return y.mousewheel.event ? (y.container.off(y.mousewheel.event, d), !0) : !1
                    }, y.enableMousewheelControl = function() {
                        return y.mousewheel.event ? (y.container.on(y.mousewheel.event, d), !0) : !1
                    }, y.parallax = {
                        setTranslate: function() {
                            y.container.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function() {
                                p(this, y.progress)
                            }), y.slides.each(function() {
                                var e = a(this);
                                e.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function() {
                                    var a = Math.min(Math.max(e[0].progress, -1), 1);
                                    p(this, a)
                                })
                            })
                        },
                        setTransition: function(e) {
                            "undefined" == typeof e && (e = y.params.speed), y.container.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y]").each(function() {
                                var t = a(this),
                                    s = parseInt(t.attr("data-swiper-parallax-duration"), 10) || e;
                                0 === e && (s = 0), t.transition(s)
                            })
                        }
                    }, y._plugins = [];
                    for (var H in y.plugins) {
                        var N = y.plugins[H](y, y.params[H]);
                        N && y._plugins.push(N)
                    }
                    return y.callPlugins = function(e) {
                        for (var a = 0; a < y._plugins.length; a++) e in y._plugins[a] && y._plugins[a][e](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5])
                    }, y.emitterEventListeners = {}, y.emit = function(e) {
                        y.params[e] && y.params[e](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                        var a;
                        if (y.emitterEventListeners[e])
                            for (a = 0; a < y.emitterEventListeners[e].length; a++) y.emitterEventListeners[e][a](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
                        y.callPlugins && y.callPlugins(e, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5])
                    }, y.on = function(e, a) {
                        return e = c(e), y.emitterEventListeners[e] || (y.emitterEventListeners[e] = []), y.emitterEventListeners[e].push(a), y
                    }, y.off = function(e, a) {
                        var t;
                        if (e = c(e), "undefined" == typeof a) return y.emitterEventListeners[e] = [], y;
                        if (y.emitterEventListeners[e] && 0 !== y.emitterEventListeners[e].length) {
                            for (t = 0; t < y.emitterEventListeners[e].length; t++) y.emitterEventListeners[e][t] === a && y.emitterEventListeners[e].splice(t, 1);
                            return y
                        }
                    }, y.once = function(e, a) {
                        e = c(e);
                        var t = function() {
                            a(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]), y.off(e, t)
                        };
                        return y.on(e, t), y
                    }, y.a11y = {
                        makeFocusable: function(e) {
                            return e.attr("tabIndex", "0"), e
                        },
                        addRole: function(e, a) {
                            return e.attr("role", a), e
                        },
                        addLabel: function(e, a) {
                            return e.attr("aria-label", a), e
                        },
                        disable: function(e) {
                            return e.attr("aria-disabled", !0), e
                        },
                        enable: function(e) {
                            return e.attr("aria-disabled", !1), e
                        },
                        onEnterKey: function(e) {
                            13 === e.keyCode && (a(e.target).is(y.params.nextButton) ? (y.onClickNext(e), y.a11y.notify(y.isEnd ? y.params.lastSlideMessage : y.params.nextSlideMessage)) : a(e.target).is(y.params.prevButton) && (y.onClickPrev(e), y.a11y.notify(y.isBeginning ? y.params.firstSlideMessage : y.params.prevSlideMessage)), a(e.target).is("." + y.params.bulletClass) && a(e.target)[0].click())
                        },
                        liveRegion: a('<span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>'),
                        notify: function(e) {
                            var a = y.a11y.liveRegion;
                            0 !== a.length && (a.html(""), a.html(e))
                        },
                        init: function() {
                            if (y.params.nextButton) {
                                var e = a(y.params.nextButton);
                                y.a11y.makeFocusable(e), y.a11y.addRole(e, "button"), y.a11y.addLabel(e, y.params.nextSlideMessage)
                            }
                            if (y.params.prevButton) {
                                var t = a(y.params.prevButton);
                                y.a11y.makeFocusable(t), y.a11y.addRole(t, "button"), y.a11y.addLabel(t, y.params.prevSlideMessage)
                            }
                            a(y.container).append(y.a11y.liveRegion)
                        },
                        initPagination: function() {
                            y.params.pagination && y.params.paginationClickable && y.bullets && y.bullets.length && y.bullets.each(function() {
                                var e = a(this);
                                y.a11y.makeFocusable(e), y.a11y.addRole(e, "button"), y.a11y.addLabel(e, y.params.paginationBulletMessage.replace(/{{index}}/, e.index() + 1))
                            })
                        },
                        destroy: function() {
                            y.a11y.liveRegion && y.a11y.liveRegion.length > 0 && y.a11y.liveRegion.remove()
                        }
                    }, y.init = function() {
                        y.params.loop && y.createLoop(), y.updateContainerSize(), y.updateSlidesSize(), y.updatePagination(), y.params.scrollbar && y.scrollbar && (y.scrollbar.set(), y.params.scrollbarDraggable && y.scrollbar.enableDraggable()), "slide" !== y.params.effect && y.effects[y.params.effect] && (y.params.loop || y.updateProgress(), y.effects[y.params.effect].setTranslate()), y.params.loop ? y.slideTo(y.params.initialSlide + y.loopedSlides, 0, y.params.runCallbacksOnInit) : (y.slideTo(y.params.initialSlide, 0, y.params.runCallbacksOnInit), 0 === y.params.initialSlide && (y.parallax && y.params.parallax && y.parallax.setTranslate(), y.lazy && y.params.lazyLoading && (y.lazy.load(), y.lazy.initialImageLoaded = !0))), y.attachEvents(), y.params.observer && y.support.observer && y.initObservers(), y.params.preloadImages && !y.params.lazyLoading && y.preloadImages(), y.params.autoplay && y.startAutoplay(), y.params.keyboardControl && y.enableKeyboardControl && y.enableKeyboardControl(), y.params.mousewheelControl && y.enableMousewheelControl && y.enableMousewheelControl(), y.params.hashnav && y.hashnav && y.hashnav.init(), y.params.a11y && y.a11y && y.a11y.init(), y.emit("onInit", y)
                    }, y.cleanupStyles = function() {
                        y.container.removeClass(y.classNames.join(" ")).removeAttr("style"), y.wrapper.removeAttr("style"), y.slides && y.slides.length && y.slides.removeClass([y.params.slideVisibleClass, y.params.slideActiveClass, y.params.slideNextClass, y.params.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-column").removeAttr("data-swiper-row"), y.paginationContainer && y.paginationContainer.length && y.paginationContainer.removeClass(y.params.paginationHiddenClass), y.bullets && y.bullets.length && y.bullets.removeClass(y.params.bulletActiveClass), y.params.prevButton && a(y.params.prevButton).removeClass(y.params.buttonDisabledClass), y.params.nextButton && a(y.params.nextButton).removeClass(y.params.buttonDisabledClass), y.params.scrollbar && y.scrollbar && (y.scrollbar.track && y.scrollbar.track.length && y.scrollbar.track.removeAttr("style"), y.scrollbar.drag && y.scrollbar.drag.length && y.scrollbar.drag.removeAttr("style"))
                    }, y.destroy = function(e, a) {
                        y.detachEvents(), y.stopAutoplay(), y.params.scrollbar && y.scrollbar && y.params.scrollbarDraggable && y.scrollbar.disableDraggable(), y.params.loop && y.destroyLoop(), a && y.cleanupStyles(), y.disconnectObservers(), y.params.keyboardControl && y.disableKeyboardControl && y.disableKeyboardControl(), y.params.mousewheelControl && y.disableMousewheelControl && y.disableMousewheelControl(), y.params.a11y && y.a11y && y.a11y.destroy(), y.emit("onDestroy"), e !== !1 && (y = null)
                    }, y.init(), y
                }
            };
            t.prototype = {
                isSafari: function() {
                    var e = navigator.userAgent.toLowerCase();
                    return e.indexOf("safari") >= 0 && e.indexOf("chrome") < 0 && e.indexOf("android") < 0
                }(),
                isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent),
                isArray: function(e) {
                    return "[object Array]" === Object.prototype.toString.apply(e)
                },
                browser: {
                    ie: window.navigator.pointerEnabled || window.navigator.msPointerEnabled,
                    ieTouch: window.navigator.msPointerEnabled && window.navigator.msMaxTouchPoints > 1 || window.navigator.pointerEnabled && window.navigator.maxTouchPoints > 1
                },
                device: function() {
                    var e = navigator.userAgent,
                        a = e.match(/(Android);?[\s\/]+([\d.]+)?/),
                        t = e.match(/(iPad).*OS\s([\d_]+)/),
                        s = e.match(/(iPod)(.*OS\s([\d_]+))?/),
                        i = !t && e.match(/(iPhone\sOS)\s([\d_]+)/);
                    return {
                        ios: t || i || s,
                        android: a
                    }
                }(),
                support: {
                    touch: window.Modernizr && Modernizr.touch === !0 || function() {
                        return !!("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch)
                    }(),
                    transforms3d: window.Modernizr && Modernizr.csstransforms3d === !0 || function() {
                        var e = document.createElement("div").style;
                        return "webkitPerspective" in e || "MozPerspective" in e || "OPerspective" in e || "MsPerspective" in e || "perspective" in e
                    }(),
                    flexbox: function() {
                        for (var e = document.createElement("div").style, a = "alignItems webkitAlignItems webkitBoxAlign msFlexAlign mozBoxAlign webkitFlexDirection msFlexDirection mozBoxDirection mozBoxOrient webkitBoxDirection webkitBoxOrient".split(" "), t = 0; t < a.length; t++)
                            if (a[t] in e) return !0
                    }(),
                    observer: function() {
                        return "MutationObserver" in window || "WebkitMutationObserver" in window
                    }()
                },
                plugins: {}
            };
            for (var s = ["jQuery", "Zepto", "Dom7"], i = 0; i < s.length; i++) window[s[i]] && e(window[s[i]]);
            var n;
            n = "undefined" == typeof Dom7 ? window.Dom7 || window.Zepto || window.jQuery : Dom7, n && ("transitionEnd" in n.fn || (n.fn.transitionEnd = function(e) {
                function a(n) {
                    if (n.target === this)
                        for (e.call(this, n), t = 0; t < s.length; t++) i.off(s[t], a)
                }
                var t, s = ["webkitTransitionEnd", "transitionend", "oTransitionEnd", "MSTransitionEnd", "msTransitionEnd"],
                    i = this;
                if (e)
                    for (t = 0; t < s.length; t++) i.on(s[t], a);
                return this
            }), "transform" in n.fn || (n.fn.transform = function(e) {
                for (var a = 0; a < this.length; a++) {
                    var t = this[a].style;
                    t.webkitTransform = t.MsTransform = t.msTransform = t.MozTransform = t.OTransform = t.transform = e
                }
                return this
            }), "transition" in n.fn || (n.fn.transition = function(e) {
                "string" != typeof e && (e += "ms");
                for (var a = 0; a < this.length; a++) {
                    var t = this[a].style;
                    t.webkitTransitionDuration = t.MsTransitionDuration = t.msTransitionDuration = t.MozTransitionDuration = t.OTransitionDuration = t.transitionDuration = e
                }
                return this
            })), window.Swiper = t
        }(), "undefined" != typeof a ? a.exports = window.Swiper : "function" == typeof define && define.amd && define([], function() {
                "use strict";
                return window.Swiper
            })
    }, {}],
    10: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(a) {
            function t() {
                w.css({
                    height: E.height() - parseInt(v.css("top").replace("px", "")) - 75 + "px"
                })
            }

            function i() {
                A("skrollexThemeCustomized", "yes")
            }

            function n() {
                A("skrollexThemeCustomized", "")
            }

            function r() {
                var e = $("skrollexThemeCustomized");
                return "yes" === e
            }

            function o() {
                for (var e = 0; z > e; e++) l(String.fromCharCode(65 + e).toLowerCase());
                c('<span><span class="primary-color"></span></span>', ".primary-color", "color", "input.primary-bg", "primary-bg", m), c('<span><span class="out-primary"></span></span>', ".out-primary", "opacity", "input.primary-out", "primary-out", d, p), c('<span><span class="success-color"></span></span>', ".success-color", "color", "input.success-bg", "success-bg", m), c('<span><span class="out-success"></span></span>', ".out-success", "opacity", "input.success-out", "success-out", d, p), c('<span><span class="info-color"></span></span>', ".info-color", "color", "input.info-bg", "info-bg", m), c('<span><span class="out-info"></span></span>', ".out-info", "opacity", "input.info-out", "info-out", d, p), c('<span><span class="warning-color"></span></span>', ".warning-color", "color", "input.warning-bg", "warning-bg", m), c('<span><span class="out-warning"></span></span>', ".out-warning", "opacity", "input.warning-out", "warning-out", d, p), c('<span><span class="danger-color"></span></span>', ".danger-color", "color", "input.danger-bg", "danger-bg", m), c('<span><span class="out-danger"></span></span>', ".out-danger", "opacity", "input.danger-out", "danger-out", d, p)
            }

            function l(e) {
                c('<span class="colors-' + e + '"><span class="bg-color"></span></span>', ".bg-color", "color", "input." + e + "-bg", e + "-bg", m), c('<span class="colors-' + e + '"><span class="text"></span></span>', ".text", "color", "input." + e + "-text", e + "-text", m), c('<span class="colors-' + e + '"><span class="highlight"></span></span>', ".highlight", "color", "input." + e + "-highlight", e + "-highlight", m), c('<span class="colors-' + e + '"><span class="link"></span></span>', ".link", "color", "input." + e + "-link", e + "-link", m), c('<span class="colors-' + e + '"><span class="heading"></span></span>', ".heading", "color", "input." + e + "-heading", e + "-heading", m), c('<span class="colors-' + e + '"><span class="out"></span></span>', ".out", "opacity", "input." + e + "-out", e + "-out", d, p)
            }

            function d(e) {
                return Math.round(100 * (1 - e))
            }

            function p(e) {
                return Math.round(e)
            }

            function c(e, a, t, n, r, o, l) {
                var d = 300,
                    p = s('<span class="getter"></span>').appendTo("body");
                s(e).appendTo(p);
                var c = p.find(a).css(t);
                p.remove(), c && o && (c = o(c)), M.lessVars[r] = c;
                var u = w.find(n);
                if (u.val(c), "color" === t) u.data("getted", c), u.data("color-pane") ? u.minicolors("value", c) : (u.data("color-pane", !0), u.minicolors({
                    control: s(this).attr("data-control") || "hue",
                    defaultValue: s(this).attr("data-defaultValue") || "",
                    inline: "true" === s(this).attr("data-inline"),
                    letterCase: s(this).attr("data-letterCase") || "lowercase",
                    opacity: !1,
                    position: s(this).attr("data-position") || "top left",
                    changeDelay: d,
                    change: function(e, a) {
                        e != u.data("getted") && (u.data("getted", e), i(), M.lessVars[r] = e, h())
                    },
                    show: function() {
                        var e = u.parent(),
                            a = e.children(".minicolors-panel"),
                            t = a.outerHeight(!0),
                            i = a.outerWidth(!0),
                            n = s(window),
                            r = n.width(),
                            o = n.height(),
                            l = a.offset(),
                            d = l.left - s(document).scrollLeft(),
                            p = l.top - s(document).scrollTop();
                        d + i > r && (d = r - i - 5), p + t > o && (p = o - t - 2), 0 > p && (p = 2), a.css({
                            position: "fixed",
                            left: d + "px",
                            top: p + "px"
                        })
                    },
                    hide: function() {
                        u.parent().children(".minicolors-panel").css({
                            position: "",
                            left: "",
                            top: ""
                        })
                    },
                    theme: "bootstrap"
                }));
                else {
                    var f;
                    u.change(function() {
                        var e = s(this),
                            a = e.val();
                        f && clearTimeout(f), i(), M.lessVars[r] = a, h()
                    })
                }
            }

            function u() {
                return M.isShowPanel ? (s(".top-pane .reset").click(function(e) {
                    e.preventDefault(), n(), M.hide(), s("#" + I).remove(), P = !1;
                    var a = w.find(".options-gate");
                    return a.css({
                        visibility: "visible"
                    }), a.css({
                        opacity: 1
                    }), sessionStorage.setItem("lessVars", JSON.stringify({})), M.lessVars = {}, !1
                }), v.css({
                    left: -1 * x + "px"
                }), y.click(function(e) {
                    return e.preventDefault(), skrollexConfig.isCustomizer ? (A("skrollexShowColorPanel", "yes"), window.parent.location = skrollexConfig.permalink) : v.hasClass("on") ? M.hide() : M.show(), !1
                }), w.find(".save-custom-css").click(function(e) {
                    if (e.preventDefault(), b.find(".css-content"), e.shiftKey) {
                        var a = '@import "theme.less";\r\n\r\n';
                        for (var t in M.lessVars) a = a + "@" + t + ": " + M.lessVars[t] + ";\r\n";
                        b.find(".css-content").text(a), new TWEEN.Tween({
                            autoAlpha: 0,
                            x: -450
                        }).to({
                            autoAlpha: 1,
                            x: 0
                        }, 400).onUpdate(function() {
                            b.css({
                                opacity: this.autoAlpha,
                                visibility: this.autoAlpha > 0 ? "visible" : "hidden"
                            }), b.css(Modernizr.csstransforms3d && k.force3D ? {
                                transform: "translate3d(" + this.x + "px, 0px, 0px)"
                            } : {
                                transform: "translate(" + this.x + "px, 0px)"
                            })
                        }).easing(TWEEN.Easing.Quadratic.Out).start()
                    } else if (s("body").hasClass("admin-bar")) {
                        g || h();
                        var a = g.replace(/(\r\n|\r|\n)/g, "\r\n"),
                            i = s("#save-custom-css"),
                            n = s('<input type="hidden" id="content" name="content">').val(a).appendTo(i);
                        i.submit(), n.remove()
                    } else alert("Saving is not allowed in demo mode.");
                    return !1
                }), b.find(".close-panel").click(function(e) {
                    return e.preventDefault(), new TWEEN.Tween({
                        autoAlpha: 1,
                        x: 0
                    }).to({
                        autoAlpha: 0,
                        x: -450
                    }, 400).onUpdate(function() {
                        b.css({
                            opacity: this.autoAlpha,
                            visibility: this.autoAlpha > 0 ? "visible" : "hidden"
                        }), b.css(Modernizr.csstransforms3d && k.force3D ? {
                            transform: "translate3d(" + this.x + "px, 0px, 0px)"
                        } : {
                            transform: "translate(" + this.x + "px, 0px)"
                        })
                    }).easing(TWEEN.Easing.Linear.None).start(), !1
                }), void S.selectTextarea(b.find("textarea"))) : void v.hide()
            }

            function h(e) {
                var a = atob(customLess);
                sessionStorage.setItem("lessVars", JSON.stringify(M.lessVars)), f(a, function(a) {
                    if (!e) {
                        g = a;
                        var t = s("#" + I);
                        t.length < 1 ? s('<style type="text/css" id="' + I + '">\n' + a + "</style>").appendTo("head") : t[0].innerHTML ? t[0].innerHTML = g : t[0].styleSheet.cssText = g
                    }
                })
            }

            function f(e, a) {
                less.render(e, {
                    currentDirectory: "assets/css/src/schemes/",
                    filename: skrollexConfig.themeUri + "assets/css/src/schemes/colors-custom.less",
                    entryPath: "assets/css/src/schemes/",
                    rootpath: "assets/css/src/schemes/",
                    rootFilename: "assets/css/src/schemes/colors-custom.less",
                    relativeUrls: !1,
                    useFileCache: !1,
                    compress: !1,
                    modifyVars: M.lessVars,
                    globalVars: less.globalVars
                }, function(e, t) {
                    a(t.css)
                })
            }

            function m(e) {
                function a(e) {
                    if (isNaN(e)) return "00";
                    var a = parseInt(e).toString(16);
                    return 1 == a.length ? "0" + a : a
                }
                if (-1 === e.indexOf("rgb")) return e;
                var t = e.match(/[^0-9]*([0-9]*)[^0-9]*([0-9]*)[^0-9]*([0-9]*)[^0-9]*/i);
                return "#" + a(t[1]) + a(t[2]) + a(t[3])
            }
            var g, v, w, y, x, b, C, T = e("../app/themes.js"),
                S = e("../tools/tools.js"),
                k = (e("../widgets/loading.js"), e("../app/app-share.js")),
                T = e("../app/themes.js"),
                z = T.colors,
                M = this,
                E = s(window),
                P = !1,
                I = "edit-mode-styles",
                j = (s(".gate .loader"), s("html").hasClass("mobile")),
                A = function(e, a) {
                    sessionStorage.setItem(e, a)
                },
                $ = function(e) {
                    return sessionStorage.getItem(e)
                };
            this.lessVars = {}, this.isShowPanel = function() {
                return j ? !1 : skrollexConfig.isInitColorPanel ? (A("skrollexCustomize", "yes"), !0) : s("html").hasClass("select-theme") ? (A("skrollexCustomize", "yes"), !1) : $("skrollexCustomize") ? !0 : !1
            }(), this.show = function() {
                v.css({
                    left: "0px"
                }), v.addClass("on"), v.transitionEnd(function() {
                    for (var e = 0; e < T.colors; e++) {
                        var a = String.fromCharCode(65 + e),
                            t = a.toLowerCase();
                        s(".colors-" + t + ", .background-" + t + ", .background-lite-" + t + ", .background-hard-" + t).not(".no-colors-label").each(function() {
                            var e = s(this),
                                t = s('<span class="colors-label">Colors ' + a + "</span>"),
                                i = e.offset(),
                                n = e.height(),
                                r = e.width();
                            i.left + r > 200 ? t.css("right", "10%") : t.css("left", "10%"), 30 > n && (i.top > 10 ? t.css("top", "-6px") : t.css("top", "0px")), 400 > n ? t.css("top", "25%") : t.css("top", "100px"), t.hover(function() {
                                e.addClass("light-colors-block")
                            }, function() {
                                e.removeClass("light-colors-block")
                            }).appendTo(e)
                        })
                    }
                    if (!P) {
                        P = !0, h(!0), o();
                        var i = w.find(".options-gate");
                        i.css({
                            opacity: 0,
                            visibility: "hidden"
                        })
                    }
                    s(".colors-label").addClass("show")
                }, 500)
            }, this.hide = function() {
                v.css({
                    left: -1 * x + "px"
                }), v.removeClass("on"), s(".colors-label").off("hover").removeClass("show"), setTimeout(function() {
                    s(".colors-label").remove()
                }, 1e3)
            }, M.isShowPanel ? s('<div id="customize-panel"></div>').appendTo("body").load(skrollexConfig.themeUri + "includes/generated/colors/color-panel.php #customize-panel>*", function(e, i, l) {
                "success" !== i && "notmodified" !== i ? (s("#customize-panel").remove(), a.afterConfigure()) : s.getScript(skrollexConfig.themeUri + "assets/js/custom-less.js?" + (new Date).getTime(), function(e, i, l) {
                    if ("success" !== i && "notmodified" !== i) s("#customize-panel").remove(), a.afterConfigure();
                    else {
                        v = s("#customize-panel"), w = v.find(".options"), y = v.find(".toggle-button"), x = w.outerWidth(), b = v.find(".custom-css"), C = w.find(".colors"), u();
                        var d = $("skrollexLastColors");
                        d && d !== skrollexConfig.colors && n(), r() && (P = !0, M.lessVars = JSON.parse(sessionStorage.getItem("lessVars")), h(), o(), w.find(".options-gate").css({
                            visibility: "hidden"
                        })), E.resize(t), t(), ("yes" === $("skrollexShowColorPanel") && !skrollexConfig.isCustomizer || "yes" === S.getUrlParameter("show-color-panel")) && (A("skrollexShowColorPanel", "no"), M.show()), A("skrollexLastColors", skrollexConfig.colors), a.afterConfigure()
                    }
                })
            }) : a.afterConfigure()
        }
    }, {
        "../app/app-share.js": 5,
        "../app/themes.js": 8,
        "../tools/tools.js": 12,
        "../widgets/loading.js": 19
    }],
    11: [function(require, module, exports) {
        "use strict";
        var $ = jQuery;
        $(function() {
            !new function() {
                function onBodyHeightResize() {
                    buildSizes(), scrolling.scroll(tools.windowYOffset()), calcNavigationLinkTriggers()
                }

                function widgets($context) {
                    new Sliders($context), isMobile || $context.find(".hover-dir").each(function() {
                        $(this).hoverdir({
                            speed: 300
                        })
                    }), $context.find("a").click(function(e) {
                        var a = $(this);
                        if (!a.data("toggle") && !a.hasClass("menu-toggle")) {
                            var t = customizerUrl(document.location.href);
                            return !skrollexConfig.isCustomizer || !a.attr("href") || t !== this.href && t + "/" !== this.href && t !== this.href + "/" || getHash(this.href) ? navigate(this.href, this.hash, e, a) : (tools.scrollTo(0), e.preventDefault(), !1)
                        }
                    }), fluid.setup($context), new Map($context), new Counter($context, me), new ChangeColors($context), new Skillbar($context, me), new TextBg($context, me), new TextMask($context), new TextFit($context), new TextFullscreen($context), new AjaxForm($context), new CssAnimation($context, me), $(".widget-tabs a").click(function(e) {
                        return e.preventDefault(), $(this).tab("show"), !1
                    }), $context.find("video").each(function() {
                        void 0 !== $(this).attr("muted") && (this.muted = !0)
                    }), $context.find(".open-overlay-window").each(function() {
                        var e = $(this),
                            a = $(e.data("overlay-window")),
                            t = new OverlayWindow(a);
                        e.click(function(e) {
                            return e.preventDefault(), t.show(), !1
                        })
                    });
                    var $fbox = $context.find(".fancybox");
                    if ("function" == typeof $fbox.fancybox) {
                        var opts = {
                            overlayShow: !0,
                            hideOnOverlayClick: !0,
                            overlayOpacity: .93,
                            overlayColor: "#000004",
                            showCloseButton: !0,
                            padding: 0,
                            centerOnScroll: !0,
                            enableEscapeButton: !0,
                            autoScale: !0
                        };
                        jQuery(".youtube-popup").addClass("nofancybox").fancybox(jQuery.extend({}, opts, {
                            type: "iframe",
                            width: 1280,
                            height: 720,
                            padding: 0,
                            titleShow: !1,
                            titlePosition: "float",
                            titleFromAlt: !0,
                            onStart: function(e, a, t) {
                                t.href = e[a].href.replace(new RegExp("youtu.be", "i"), "www.youtube.com/embed").replace(new RegExp("watch\\?(.*)v=([a-z0-9_-]+)(&amp;|&|\\?)?(.*)", "i"), "embed/$2?$1$4");
                                var s = t.href.indexOf("?"),
                                    i = s > -1 ? t.href.substring(s) : "";
                                t.allowfullscreen = i.indexOf("fs=0") > -1 ? !1 : !0
                            }
                        }))
                    }
                    if (isMobile) $context.find(".textillate").children(".texts").css({
                        display: "inline"
                    }).children(":not(span:first-of-type)").css({
                        display: "none"
                    });
                    else {
                        var $tlt = $context.find(".textillate");
                        $tlt.textillate(eval("(" + $tlt.data("textillate-options") + ")"))
                    }
                    var columnH = function() {
                        $context.find(".col-height").each(function() {
                            var e = $(this);
                            e.width() === e.parent().width() ? (e.css({
                                "min-height": "0"
                            }), e.children(".position-middle-center").removeClass("position-middle-center").addClass("position-middle-center-mark").css({
                                padding: "20px"
                            })) : (e.css({
                                "min-height": ""
                            }), e.children(".position-middle-center-mark").removeClass("position-middle-center-mark").addClass("position-middle-center").css({
                                padding: ""
                            }))
                        })
                    };
                    $(window).resize(columnH), columnH();
                    var svgOverlays = function() {
                        $context.find("svg.fg-overlay").each(function() {
                            var e = $(this),
                                a = e.parent().width(),
                                t = e.parent().height(),
                                s = a > t ? a : t;
                            e.attr("width", s).attr("height", s)
                        })
                    };
                    if ($(window).resize(svgOverlays), svgOverlays(), skrollexConfig.isCustomizer) {
                        var svgUrlFix = function() {
                            $context.find("svg").each(function() {
                                var e = Snap($(this)[0]),
                                    a = function(e, a) {
                                        var t = window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.search + "#";
                                        e.node.setAttribute(a, e.node.getAttribute(a).replace("url('#", "url('" + t).replace('url("#', 'url("' + t).replace("url(#", "url(" + t))
                                    };
                                e.selectAll("[mask]").forEach(function(e) {
                                    a(e, "mask")
                                }), e.selectAll("[fill]").forEach(function(e) {
                                    a(e, "fill")
                                }), e.selectAll("[filter]").forEach(function(e) {
                                    a(e, "filter")
                                })
                            })
                        };
                        $(window).resize(svgUrlFix), svgUrlFix()
                    }
                    $context.find(".masonry-grd").each(function() {
                        $(this).masonry({
                            itemSelector: ".masonry-item:not(.hidden-item)",
                            layoutMode: "masonry",
                            gutter: 0,
                            percentPosition: !0,
                            isFitWidth: !1
                        })
                    })
                }

                function unwidgets(e) {
                    new Sliders(e, !0), e.find(".player").each(function() {
                        var e = $(this).data("player-ind");
                        me.players[e].pause(), me.players.splice(e, 1)
                    }), e.find(".overlay-content, .loaded-content").empty()
                }

                function navigate(e, a, t, s) {
                    var i = deleteHash(e);
                    if (a && (location === i || location + "/" === i || location === i + "/") && -1 === a.indexOf("!")) {
                        var n = function() {
                            if ("#scroll-down" === a && t) {
                                var e = !1,
                                    s = $(t.target),
                                    i = s.offset().top;
                                return $(".wrapper-content .view").each(function() {
                                    var a = $(this);
                                    return a.offset().top + 100 > i ? (e = a, !1) : void 0
                                }), e && e.length > 0 ? e : (e = $(".wrapper-content .view"), e.length > 1 ? $(e.get(1)) : 1 === e.length && e.offset().top > 300 ? e : null)
                            }
                            return $(a)
                        }();
                        if (t && t.preventDefault(), null !== n && n.length > 0) {
                            var r = n.offset().top - me.topNav.state2H,
                                o = n.get(0).tagName.toLowerCase();
                            ("h1" === o || "h2" === o || "h3" === o || "h4" === o || "h5" === o || "h6" === o) && (r -= 20), 0 > r && (r = 0), tools.scrollTo(r)
                        } else tools.scrollTo("#scroll-down" === a ? Math.round($(window).height() / 2) : 0);
                        return skrollexConfig.isCustomizer && sessionStorage.setItem("navigate", ""), !1
                    }
                    if (t && e !== location + "#") {
                        if (!s.attr("target")) {
                            var l = function() {
                                t.preventDefault();
                                var a = me.topNav.isState1;
                                me.topNav.state2(), loading.gate(function() {
                                    window.location = e
                                }), $(window).one("pageshow popstate", function(e) {
                                    loading.ungate(), a && me.topNav.state1()
                                })
                            };
                            skrollexConfig.isCustomizer ? 0 === e.indexOf(skrollexConfig.homeUri) && sessionStorage.setItem("navigate", e) : s.hasClass("page-transition") ? l() : $pageTransition.each(function() {
                                var e = $(this).get(0);
                                $.contains(e, s[0]) && l()
                            })
                        }
                    } else skrollexConfig.isCustomizer && sessionStorage.setItem("navigate", "")
                }

                function calcNavigationLinkTriggers() {
                    var e = $window.height(),
                        a = e / 3;
                    sectionTriggers = [], $sections.each(function(e) {
                        var t = $(this),
                            s = t.attr("id");
                        if (s) {
                            var i = t.data("position");
                            sectionTriggers.push({
                                hash: "#" + s,
                                triggerOffset: i - a,
                                position: i
                            })
                        }
                    }), trigNavigationLinks(tools.windowYOffset())
                }

                function trigNavigationLinks(e) {
                    for (var a, t = 0; t < sectionTriggers.length; t++) sectionTriggers[t].triggerOffset < e && (0 === t || sectionTriggers[t - 1].position < e) && (a = sectionTriggers[t].hash);
                    if (a != lastActiveSectionHash) {
                        var s = location + a;
                        lastActiveSectionHash = a;
                        var i = [];
                        for ($navLinks.each(function() {
                            var e = $(this);
                            this.href === s && (e.addClass("active"), e.removeClass("target"), i.push(e.data("navigation-group")))
                        }), t = 0; t < i.length; t++) $navLinks.each(function() {
                            var e = $(this);
                            this.href !== s && i[t] === e.data("navigation-group") && e.removeClass("active")
                        });
                        app.changeSection(me, a)
                    }
                }

                function buildSizes() {
                    app.buildSizes(me), maxScrollPosition = $("body").height() - $window.height();
                    for (var e = 0; e < me.players.length; e++) {
                        var a = me.players[e].$view;
                        a.data("position", a.offset().top)
                    }
                }! function() {
                    window.skrollexConfig = eval("(" + $("html").data("skrollex-config") + ")"), window.skrollexConfig.screenshotMode = !1, window.skrollexConfig.animations = !1, window.skrollexConfig.animations && $("html").addClass("no-animations"), $("html").addClass("dom-ready");
                    var disableMobileAnimations = !0,
                        isWin = -1 !== navigator.appVersion.indexOf("Win");
                    isWin && $("html").addClass("win");
                    var ua = navigator.userAgent.toLowerCase(),
                        isEdge = ua.indexOf("edge") > -1;
                    isEdge && $("html").addClass("edge");
                    var isChrome = !isEdge && ua.indexOf("chrome") > -1;
                    isChrome && $("html").addClass("chrome");
                    var isAndroidBrowser4_3minus = ua.indexOf("mozilla/5.0") > -1 && ua.indexOf("android ") > -1 && ua.indexOf("applewebkit") > -1 && !(ua.indexOf("chrome") > -1);
                    isAndroidBrowser4_3minus && $("html").addClass("android-browser-4_3minus");
                    var nua = navigator.userAgent,
                        isAndroidBrowser = nua.indexOf("Mozilla/5.0") > -1 && nua.indexOf("Android ") > -1 && nua.indexOf("AppleWebKit") > -1 && !(nua.indexOf("Chrome") > -1);
                    isAndroidBrowser && $("html").addClass("android-browser");
                    var isSafari = !isChrome && -1 !== ua.indexOf("safari") && ua.indexOf("windows") < 0;
                    isSafari && $("html").addClass("safari");
                    var isMobile = Modernizr.touch;
                    isMobile ? ($("html").addClass("mobile"), disableMobileAnimations && $("html").addClass("poor-browser")) : $("html").addClass("non-mobile"), isWin && isSafari && $("html").addClass("flat-animation"), navigator.userAgent.indexOf("MSIE 9.") > -1 ? $("html").addClass("ie9") : navigator.userAgent.indexOf("MSIE 10.") > -1 ? $("html").addClass("ie10") : navigator.userAgent.match(/Trident.*rv\:11\./) && $("html").addClass("ie11"), window.skrollexConfig.screenshotMode && $("html").addClass("hide-skroll-bar"), window.console ? window.console.log || (window.console.log = function() {}) : window.console = {
                        log: function() {}
                    }, "undefined" == typeof window.atob && (window.atob = function(e) {
                        return base64.decode(e)
                    })
                }();
                var Customize = require("./customize/customize.js"),
                    TopNav = require("./widgets/top-nav.js"),
                    MenuToggle = require("./widgets/menu-toggle.js"),
                    Players = require("./animation/players.js"),
                    Scrolling = require("./animation/scrolling.js"),
                    tools = require("./tools/tools.js"),
                    Gallery = require("./widgets/gallery.js"),
                    fluid = require("./widgets/fluid.js"),
                    Counter = require("./widgets/counter.js"),
                    ChangeColors = require("./widgets/change-colors.js"),
                    Sliders = require("./widgets/sliders.js"),
                    loading = require("./widgets/loading.js"),
                    CssAnimation = require("./animation/css-animation.js"),
                    dotScroll = require("./widgets/dot-scroll.js"),
                    Map = require("./widgets/map.js"),
                    Skillbar = require("./widgets/skillbar.js"),
                    TextBg = require("./widgets/text-bg.js"),
                    TextMask = require("./widgets/text-mask.js"),
                    TextFit = require("./widgets/text-fit.js"),
                    TextFullscreen = require("./widgets/text-fullscreen.js"),
                    AjaxForm = require("./widgets/ajax-form.js"),
                    VimeoBG = require("./widgets/vimeo-bg.js"),
                    VideoBG = require("./widgets/video-bg.js"),
                    app = require("./app/app.js"),
                    OverlayWindow = require("./widgets/overlay-window.js"),
                    isPoorBrowser = $("html").hasClass("poor-browser"),
                    isAndroid43minus = $("html").hasClass("android-browser-4_3minus"),
                    $pageTransition = $(".page-transition, .nav-links, .sidebar, .post-meta, .post-title, .post-image"),
                    me = this,
                    $window = $(window),
                    sectionQ = ".view>.fg",
                    $sections = $(sectionQ),
                    sectionTriggers = [],
                    lastActiveSectionHash, customizerUrl = function(e) {
                        var a = e;
                        if (skrollexConfig.isCustomizer) {
                            var t = "wp-admin/customize.php?url=",
                                s = a.indexOf(t);
                            if (s >= 0) a = decodeURIComponent(a.substring(s + t.length));
                            else {
                                var i = "wp-admin/customize.php",
                                    n = a.indexOf(i);
                                n >= 0 && (a = a.substring(0, n))
                            }
                        }
                        return a
                    },
                    deleteHash = function(e) {
                        var a = e.indexOf("#");
                        return a >= 0 ? e.substr(0, a) : e
                    },
                    getHash = function(e) {
                        var a = e.indexOf("#");
                        return a >= 0 ? e.substr(a) : null
                    },
                    location = deleteHash(customizerUrl(document.location.href)),
                    $navLinks = function() {
                        var e = jQuery();
                        return $("#top-nav nav a").each(function() {
                            var a = $(this);
                            (!this.hash || this.href === location + this.hash && $(sectionQ + this.hash).length > 0) && (a.data("navigation-group", "top-nav"), e = e.add(a))
                        }), e
                    }(),
                    isMobile = $("html").hasClass("mobile"),
                    scrolling, maxScrollPosition, ticker = new function() {
                        var e = this;
                        window.requestAnimFrame = function() {
                            return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(e, a) {
                                    window.setTimeout(e, 1e3 / 60)
                                }
                        }();
                        var a = -1;
                        this.pageIsReady = !1,
                            function t(s) {
                                if (e.pageIsReady) {
                                    var i = tools.windowYOffset();
                                    a !== i && (scrolling.scroll(i), trigNavigationLinks(i)), a = i, TWEEN.update(), app.tick()
                                }
                                loading.queue.length > 0 && loading.queue.pop()(), requestAnimFrame(t)
                            }()
                    };
                this.topNav = void 0, this.players = Players, this.afterConfigure = function() {
                    var e = getHash(customizerUrl(window.location.href));
                    new VimeoBG, new VideoBG, app.prepare(function() {
                        loading.load(function() {
                            $navLinks = $navLinks.add(dotScroll.links()).click(function() {
                                $navLinks.removeClass("target"), $(this).addClass("target")
                            }), me.topNav = new TopNav, new MenuToggle(me), scrolling = new Scrolling(me), $("#footer.animated .section-cols").addClass("scroll-in-animation").attr("data-animation", "fadeInDown"), widgets($("body")), new Gallery(onBodyHeightResize, widgets, unwidgets);
                            var a = $window.width(),
                                t = $window.height();
                            $window.resize(function() {
                                var e = $window.width(),
                                    s = $window.height();
                                (e !== a || s !== t) && (a = e, t = s, fluid.setup($("body")), onBodyHeightResize())
                            }), $(".masonry-grd").each(function() {
                                $(this).masonry("on", "layoutComplete", function() {
                                    onBodyHeightResize()
                                })
                            }), app.setup(function() {
                                var a = function() {
                                        buildSizes(), calcNavigationLinkTriggers(), ticker.pageIsReady = !0, $navLinks.each(function() {
                                            this.href == location && $(this).addClass("active")
                                        }), $(".bigtext").each(function() {
                                            $(this).bigtext()
                                        }), app.ungated(), setTimeout(function() {
                                            loading.ungate();
                                            var a = sessionStorage.getItem("navigate");
                                            skrollexConfig.isCustomizer && a ? navigate(a, getHash(a)) : skrollexConfig.isCustomizer || navigate(customizerUrl(window.location.href), e)
                                        })
                                    },
                                    t = function() {
                                        for (var e = $(".non-preloading, .non-preloading img"), s = $("img").not(e), i = 0; i < s.length; i++)
                                            if (!(!$(s[i]).attr("src") || s[i].width && s[i].height || s[i].naturalWidth && s[i].naturalHeight)) return void setTimeout(t, 100);
                                        a()
                                    };
                                t()
                            })
                        })
                    })
                };
                var animEnd = function(e, a, t, s, i) {
                    var n = 100,
                        r = 1e3;
                    return e.each(function() {
                        var e = this;
                        if (t && !isAndroid43minus) {
                            var o = !1;
                            if ($(e).bind(a, function() {
                                    return o = !0, $(e).unbind(a), s.call(e)
                                }), i >= 0 || void 0 === i) {
                                var l = void 0 === i ? 1e3 : r + n;
                                setTimeout(function() {
                                    o || ($(e).unbind(a), s.call(e))
                                }, l)
                            }
                        } else s.call(e)
                    })
                };
                $.fn.animationEnd = function(e, a) {
                    return animEnd(this, tools.animationEnd, Modernizr.cssanimations, e, a)
                }, $.fn.transitionEnd = function(e, a) {
                    return animEnd(this, tools.transitionEnd, Modernizr.csstransitions, e, a)
                }, $.fn.stopTransition = function() {
                    return this.css({
                        "-webkit-transition": "none",
                        "-moz-transition": "none",
                        "-ms-transition": "none",
                        "-o-transition": "none",
                        transition: "none"
                    })
                }, $.fn.cleanTransition = function() {
                    return this.css({
                        "-webkit-transition": "",
                        "-moz-transition": "",
                        "-ms-transition": "",
                        "-o-transition": "",
                        transition: ""
                    })
                }, $.fn.nonTransition = function(e) {
                    return this.stopTransition().css(e).cleanTransition()
                }, $.fn.transform = function(e, a) {
                    return this.css(tools.transformCss(e, a))
                }, $("video").each(function() {
                    void 0 !== $(this).attr("muted") && (this.muted = !0)
                }), new Customize(me), isMobile || isPoorBrowser || $(document).bind("mousewheel DOMMouseScroll", function(e) {
                    return 1 == e.ctrlKey ? (e.preventDefault(), !0) : void 0
                })
            }
        })
    }, {
        "./animation/css-animation.js": 1,
        "./animation/players.js": 2,
        "./animation/scrolling.js": 3,
        "./app/app.js": 6,
        "./customize/customize.js": 10,
        "./tools/tools.js": 12,
        "./widgets/ajax-form.js": 13,
        "./widgets/change-colors.js": 14,
        "./widgets/counter.js": 15,
        "./widgets/dot-scroll.js": 16,
        "./widgets/fluid.js": 17,
        "./widgets/gallery.js": 18,
        "./widgets/loading.js": 19,
        "./widgets/map.js": 20,
        "./widgets/menu-toggle.js": 21,
        "./widgets/overlay-window.js": 22,
        "./widgets/skillbar.js": 23,
        "./widgets/sliders.js": 24,
        "./widgets/text-bg.js": 25,
        "./widgets/text-fit.js": 26,
        "./widgets/text-fullscreen.js": 27,
        "./widgets/text-mask.js": 28,
        "./widgets/top-nav.js": 29,
        "./widgets/video-bg.js": 30,
        "./widgets/vimeo-bg.js": 31
    }],
    12: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = new function() {
            var a = this,
                t = (e("../script.js"), s("html").hasClass("android-browser-4_3minus"));
            this.animationEnd = "animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", this.transitionEnd = "transitionend webkitTransitionEnd oTransitionEnd otransitionend", this.transition = ["-webkit-transition", "-moz-transition", "-ms-transition", "-o-transition", "transition"], this.transform = ["-webkit-transform", "-moz-transform", "-ms-transform", "-o-transform", "transform"], this.property = function(e, a, t) {
                for (var s = t ? t : {}, i = 0; i < e.length; i++) s[e[i]] = a;
                return s
            }, this.windowYOffset = void 0 !== window.pageYOffset ? function() {
                return window.pageYOffset
            } : "CSS1Compat" === document.compatMode ? function() {
                return document.documentElement.scrollTop
            } : function() {
                return document.body.scrollTop
            }, this.getUrlParameter = function(e) {
                for (var a = window.location.search.substring(1), t = a.split("&"), s = 0; s < t.length; s++) {
                    var i = t[s].split("=");
                    if (i[0] == e) return decodeURI(i[1])
                }
            }, this.selectTextarea = function(e) {
                e.focus(function() {
                    var e = s(this);
                    e.select(), e.mouseup(function() {
                        return e.unbind("mouseup"), !1
                    })
                })
            };
            var i;
            this.time = function(e) {
                if (i) {
                    var a = Date.now();
                    console.log("====" + (a - i) + " ms" + (e ? " | " + e : "")), i = a
                } else i = Date.now(), console.log("====Timer started" + (e ? " | " + e : ""))
            }, this.scrollTo = function(e, t, s) {
                void 0 === s && (s = 1200), new TWEEN.Tween({
                    y: a.windowYOffset()
                }).to({
                    y: Math.round(e)
                }, s).onUpdate(function() {
                    window.scrollTo(0, this.y)
                }).easing(TWEEN.Easing.Quadratic.InOut).onComplete(function() {
                    t && t()
                }).start()
            }, this.androidStylesFix = function(e) {
                t && (e.hide(), e.get(0).offsetHeight, e.show())
            }, this.transformCss = function(e, a) {
                var t = {
                    "-webkit-transform": e,
                    "-moz-transform": e,
                    "-ms-transform": e,
                    "-o-transform": e,
                    transform: e
                };
                return a && (t["-webkit-transform-origin"] = a, t["-moz-transform-origin"] = a, t["-ms-transform-origin"] = a, t["-o-transform-origin"] = a, t["transform-origin"] = a), t
            }, this.snapUrl = function(e) {
                if (skrollexConfig.isCustomizer) {
                    var a = window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.search + "#" + e.attr("id");
                    return "url('" + a + "')"
                }
                return e
            }
        }
    }, {
        "../script.js": 11
    }],
    13: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(a) {
            var t = e("./loading.js"),
                i = s(".gate .loader");
            a.find(".ajax-form").each(function() {
                var e = s(this);
                e.submit(function(a) {
                    e.find(".help-block ul").length < 1 && (i.addClass("show"), t.gate(function() {
                        var a = function(a) {
                            s('<div class="ajax-form-alert alert heading fade in text-center"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> ' + a + "</div>").addClass(e.data("message-class")).appendTo("body"), t.ungate(), i.removeClass("show")
                        };
                        s.ajax({
                            type: e.attr("method"),
                            url: e.attr("action"),
                            data: e.serialize(),
                            success: function(t) {
                                e[0].reset(), a(t)
                            },
                            error: function(e, t) {
                                a("Error: " + e.responseCode)
                            }
                        })
                    }), a.preventDefault())
                })
            })
        }
    }, {
        "./loading.js": 19
    }],
    14: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(a) {
            var t = e("../app/themes.js");
            a.find(".change-colors").each(function() {
                for (var e, a = s(this), i = s(a.data("target")), n = a.find("a"), r = 0; r < t.colors; r++) {
                    var o = "colors-" + String.fromCharCode(65 + r).toLowerCase();
                    i.hasClass(o) && (e = o, n.each(function() {
                        var a = s(this);
                        a.data("colors") === e && a.addClass("active")
                    }))
                }
                n.click(function(a) {
                    a.preventDefault();
                    var t = s(this);
                    return i.removeClass(e), e = t.data("colors"), i.addClass(e), n.removeClass("active"), t.addClass("active"), !1
                })
            })
        }
    }, {
        "../app/themes.js": 8
    }],
    15: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(e, a) {
            var t = s("html").hasClass("poor-browser"),
                i = s("html").hasClass("no-animations");
            t || i || e.find(".counter .count").each(function() {
                var e = s(this),
                    t = parseInt(e.text()),
                    i = {
                        n: 0
                    },
                    n = new TWEEN.Tween(i).to({
                        n: t
                    }, 1e3).onUpdate(function() {
                        e.text(Math.round(this.n))
                    }).easing(TWEEN.Easing.Quartic.InOut),
                    r = function() {
                        n.stop()
                    },
                    o = function() {
                        i.n = 0, n.start()
                    },
                    l = o;
                a.players.addPlayer(e, l, r, o)
            })
        }
    }, {}],
    16: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = new function() {
            var e, a = s("html").hasClass("mobile"),
                t = s(".wrapper-content>.view>.fg[id]");
            if (!a && t.length > 1) {
                var i = s("#dot-scroll");
                t.each(function() {
                    i.append('<li><a href="#' + s(this).attr("id") + '"><span></span></a></li>')
                }), e = i.find("a").data("navigation-group", "dot-scroll")
            } else e = jQuery();
            this.links = function() {
                return e
            }
        }
    }, {}],
    17: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = new function() {
            this.setup = function(e) {
                e.find(".fluid *").each(function() {
                    var e = s(this),
                        a = e.parent(".fluid"),
                        t = a.width(),
                        i = e.attr("data-aspect-ratio");
                    i || (i = this.height / this.width, e.attr("data-aspect-ratio", i).removeAttr("height").removeAttr("width"));
                    var n = Math.round(t * i);
                    e.width(Math.round(t)).height(n), a.height(n)
                })
            }
        }
    }, {}],
    18: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(a, t, i) {
            var n, r, o, l = (e("../tools/tools.js"), e("./overlay-window.js")),
                d = (s("#top-nav"), s(".gallery-overlay")),
                p = d.find(".loader"),
                c = d.find(".overlay-content"),
                u = new l(d, t, i),
                h = d.find(".next"),
                f = d.find(".previos");
            h.click(function(e) {
                e.preventDefault();
                var a = n.nextAll(".gallery-item:not(.hidden-item)").first();
                return a.length < 1 && (a = r.filter(".gallery-item:not(.hidden-item)").first()), o(a), !1
            }), f.click(function(e) {
                e.preventDefault();
                var a = n.prevAll(".gallery-item:not(.hidden-item)").first();
                return a.length < 1 && (a = r.filter(".gallery-item:not(.hidden-item)").last()), o(a), !1
            }), s(".fg").each(function(e) {
                function a(e) {
                    n = e, r = d, o = a, u.show(null, function() {
                        p.removeClass("show"), c.addClass("show")
                    }, function(a) {
                        p.addClass("show");
                        var t = e.find(".gallery-item-content");
                        c.removeClass("show"), c.html(t.html());
                        var i = c.find(".default-slider");
                        i.removeClass("hold");
                        var n = i.find(".swiper-slide");
                        n.each(function(e) {
                            var a = s(this);
                            "yes" === a.data("as-bg") ? a.css({
                                "background-image": "url(" + a.data("hold-img") + ")"
                            }) : a.after('<div class="swiper-slide"><img alt="' + a.data("alt") + '" src="' + a.data("hold-img") + '"/></div>').remove()
                        });
                        var r = c.find("img"),
                            o = r.length;
                        o > 0 ? r.load(function() {
                            o--, 0 === o && a()
                        }) : a()
                    })
                }
                var t = s(this),
                    i = t.find(".gallery-grd"),
                    l = i.find(".item"),
                    d = t.find(".gallery-item");
                if (!(l.length < 1 && d.length < 1)) {
                    var h = t.find(".masonry-grd"),
                        f = !1,
                        m = t.data("default-group") ? t.data("default-group") : "all",
                        g = t.find(".filter a"),
                        v = t.find(".filter a[data-group=all]"),
                        w = m;
                    t.find(".filter a[data-group=" + m + "]").addClass("active"), g.click(function(e) {
                        if (e.preventDefault(), f) return !1;
                        var a = s(this),
                            t = a.hasClass("active"),
                            i = t ? "all" : a.data("group");
                        return w !== i && (w = i, g.removeClass("active"), t ? v.addClass("active") : a.addClass("active"), l.each(function() {
                            var e = s(this),
                                a = e.data("groups"),
                                t = a ? a.split(/\s+/) : [];
                            "all" == i || -1 != s.inArray(i, t) ? e.removeClass("hidden-item") : e.addClass("hidden-item")
                        }), h.masonry("reloadItems"), h.masonry("layout")), !1
                    }), d.click(function(e) {
                        return e.preventDefault(), a(s(this)), !1
                    })
                }
            })
        }
    }, {
        "../tools/tools.js": 12,
        "./overlay-window.js": 22
    }],
    19: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = new function() {
            var a = e("../tools/tools.js"),
                t = s(".gate"),
                i = t.find(".gate-bar"),
                n = s("html").hasClass("android-browser-4_3minus"),
                r = this;
            this.queue = [], this.load = function(e) {
                var a = [],
                    n = !1,
                    o = s(".non-preloading, .non-preloading img");
                skrollexConfig.isCustomizer || (s("img:visible").not(o).each(function() {
                    var e = s(this),
                        t = e.attr("src");
                    t && -1 === s.inArray(t, a) && a.push(t)
                }), s("div.bg:visible").not(o).each(function() {
                    var e = s(this),
                        t = e.css("background-image");
                    if ("none" != t) {
                        var i = t.match(/url\(['"]?([^'")]*)/i);
                        i && i.length > 1 && !i[1].match(/data:/i) && -1 === s.inArray(i[1], a) && a.push(i[1])
                    }
                })), s(".loading-func:visible").not(o).each(function() {
                    var e = s(this),
                        t = e.data("loading");
                    t && a.push(t)
                });
                var l = 0;
                if (0 === a.length) n = !0, e();
                else {
                    var d = setTimeout(function() {
                        n || (n = !0, e())
                    }, 2e4);
                    skrollexConfig.isCustomizer || t.addClass("load-animation"), s("html").addClass("page-loading");
                    for (var p = 0, c = function() {
                        l++, p = l / a.length * 100, i.css({
                            width: p + "%"
                        }), l !== a.length || n || (clearTimeout(d), n = !0, e())
                    }, u = 0; u < a.length; u++)
                        if ("function" == typeof a[u]) a[u](c);
                        else {
                            var h = new Image;
                            s(h).one("load error", function() {
                                r.queue.push(c)
                            }), h.src = a[u]
                        }
                }
            }, this.gate = function(e) {
                s("html").addClass("page-is-gated"), i.css({
                    width: "0%"
                }), t.transitionEnd(function() {
                    e && e()
                }).css({
                    opacity: 1,
                    visibility: "visible"
                })
            }, this.ungate = function(e) {
                s("html").removeClass("page-is-gated page-loading").addClass("page-is-loaded"), t.transitionEnd(function() {
                    n && a.androidStylesFix(s("body")), e && e(), t.removeClass("load-animation")
                }).css({
                    opacity: 0,
                    visibility: "hidden"
                })
            }
        }
    }, {
        "../tools/tools.js": 12
    }],
    20: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(a) {
            var t = e("./overlay-window.js");
            "undefined" != typeof google && a.find(".map-open").each(function() {
                var e = [{
                        elementType: "geometry",
                        stylers: [{
                            color: "#ebe3cd"
                        }]
                    }, {
                        elementType: "labels.text.fill",
                        stylers: [{
                            color: "#523735"
                        }]
                    }, {
                        elementType: "labels.text.stroke",
                        stylers: [{
                            color: "#f5f1e6"
                        }, {
                            weight: 4.5
                        }]
                    }, {
                        featureType: "administrative",
                        elementType: "geometry.stroke",
                        stylers: [{
                            color: "#c9b2a6"
                        }]
                    }, {
                        featureType: "administrative.land_parcel",
                        elementType: "geometry.stroke",
                        stylers: [{
                            color: "#dcd2be"
                        }]
                    }, {
                        featureType: "administrative.land_parcel",
                        elementType: "labels.text.fill",
                        stylers: [{
                            color: "#ae9e90"
                        }]
                    }, {
                        featureType: "administrative.province",
                        elementType: "geometry.stroke",
                        stylers: [{
                            weight: 4.5
                        }]
                    }, {
                        featureType: "administrative.province",
                        elementType: "labels.text",
                        stylers: [{
                            color: "#eb5050"
                        }]
                    }, {
                        featureType: "landscape.natural",
                        elementType: "geometry",
                        stylers: [{
                            color: "#dfd2ae"
                        }]
                    }, {
                        featureType: "poi",
                        elementType: "geometry",
                        stylers: [{
                            color: "#dfd2ae"
                        }]
                    }, {
                        featureType: "poi",
                        elementType: "labels.text.fill",
                        stylers: [{
                            color: "#93817c"
                        }]
                    }, {
                        featureType: "poi.park",
                        elementType: "geometry.fill",
                        stylers: [{
                            color: "#a5b076"
                        }]
                    }, {
                        featureType: "poi.park",
                        elementType: "labels.text.fill",
                        stylers: [{
                            color: "#447530"
                        }]
                    }, {
                        featureType: "road",
                        elementType: "geometry",
                        stylers: [{
                            color: "#f5f1e6"
                        }]
                    }, {
                        featureType: "road.arterial",
                        elementType: "geometry",
                        stylers: [{
                            color: "#fdfcf8"
                        }]
                    }, {
                        featureType: "road.highway",
                        elementType: "geometry",
                        stylers: [{
                            color: "#f8c967"
                        }]
                    }, {
                        featureType: "road.highway",
                        elementType: "geometry.stroke",
                        stylers: [{
                            color: "#e9bc62"
                        }]
                    }, {
                        featureType: "road.highway.controlled_access",
                        elementType: "geometry",
                        stylers: [{
                            color: "#e98d58"
                        }]
                    }, {
                        featureType: "road.highway.controlled_access",
                        elementType: "geometry.stroke",
                        stylers: [{
                            color: "#db8555"
                        }]
                    }, {
                        featureType: "road.local",
                        elementType: "labels.text.fill",
                        stylers: [{
                            color: "#806b63"
                        }]
                    }, {
                        featureType: "transit.line",
                        elementType: "geometry",
                        stylers: [{
                            color: "#dfd2ae"
                        }]
                    }, {
                        featureType: "transit.line",
                        elementType: "labels.text.fill",
                        stylers: [{
                            color: "#8f7d77"
                        }]
                    }, {
                        featureType: "transit.line",
                        elementType: "labels.text.stroke",
                        stylers: [{
                            color: "#ebe3cd"
                        }]
                    }, {
                        featureType: "transit.station",
                        elementType: "geometry",
                        stylers: [{
                            color: "#dfd2ae"
                        }]
                    }, {
                        featureType: "water",
                        elementType: "geometry.fill",
                        stylers: [{
                            color: "#b9d3c2"
                        }]
                    }, {
                        featureType: "water",
                        elementType: "labels.text.fill",
                        stylers: [{
                            color: "#92998d"
                        }]
                    }],
                    a = s(this),
                    i = s(a.data("map-overlay")).clone().appendTo("body");
                i.find(".overlay-view").append(a.parent().find(".map-canvas").clone());
                var n = i.find(".map-canvas"),
                    r = {
                        mapTypeControlOptions: {
                            mapTypeIds: ["Styled"]
                        },
                        center: new google.maps.LatLng(n.data("latitude"), n.data("longitude")),
                        zoom: n.data("zoom"),
                        mapTypeId: "styled"
                    },
                    o = [];
                n.find(".map-marker").each(function() {
                    var e = s(this);
                    o.push({
                        latitude: e.data("latitude"),
                        longitude: e.data("longitude"),
                        text: e.data("text")
                    })
                }), n.addClass("close-map").wrap('<div class="map-view"></div>');
                var l = n.parent(),
                    d = new t(i, !1, !1, function() {
                        new TWEEN.Tween({
                            autoAlpha: 1
                        }).to({
                            autoAlpha: 0
                        }, 500).onUpdate(function() {
                            l.css({
                                opacity: this.autoAlpha,
                                visibility: this.autoAlpha > 0 ? "visible" : "hidden"
                            })
                        }).easing(TWEEN.Easing.Linear.None).start()
                    }),
                    p = !1;
                a.click(function(a) {
                    return a.preventDefault(), d.show(!1, function() {
                        var a = i.find(".overlay-control"),
                            t = i.find(".overlay-view"),
                            d = new google.maps.StyledMapType(e, {
                                name: "styled"
                            });
                        if (l.css({
                                height: s(window).height() - a.height() - parseInt(t.css("bottom").replace("px", "")) + "px"
                            }), l.css({
                                opacity: 1,
                                visibility: "visible"
                            }), !p) {
                            p = !0;
                            for (var c = new google.maps.Map(n[0], r), u = function(e, a) {
                                var t = new google.maps.InfoWindow({
                                    content: a
                                });
                                google.maps.event.addListener(e, "click", function() {
                                    t.open(c, e)
                                })
                            }, h = 0; h < o.length; h++) {
                                var f = new google.maps.Marker({
                                        map: c,
                                        position: new google.maps.LatLng(o[h].latitude, o[h].longitude)
                                    }),
                                    m = o[h].text;
                                c.mapTypes.set("styled", d), m && u(f, m)
                            }
                        }
                    }), !1
                })
            })
        }
    }, {
        "./overlay-window.js": 22
    }],
    21: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(e) {
            var a = ".ext-nav-toggle, .responsive-nav",
                t = s(a),
                i = s("html").hasClass("mobile"),
                n = function() {
                    s(".responsive-nav").removeClass("show"), s("body").removeClass("responsive-nav-show"), i || (s(".textillate").children("span:nth-of-type(1)").css({
                        display: ""
                    }), s(".textillate").children("span:nth-of-type(2)").css({
                        display: "none"
                    }), s(".textillate").children("span:nth-of-type(2)").children().css({
                        display: ""
                    }))
                };
            s(window).resize(function() {
                s("div[class*='off-canvas']").removeClass("open"), n()
            }), t.click(function(t) {
                t.preventDefault();
                var r = s(this);
                if (r.hasClass("ext-nav-toggle")) {
                    var o = r.data("target"),
                        l = s(o),
                        d = s(o + " a,#top-nav a:not(" + a + "),.page-border a, #dot-scroll a"),
                        p = function() {
                            l.removeClass("show"), r.removeClass("show"), s("body").removeClass("ext-nav-show"), e.topNav.unforceState("ext-nav"), s("html, body").css({
                                overflow: "",
                                position: ""
                            }), d.unbind("click", p)
                        };
                    r.hasClass("show") ? (l.removeClass("show"), r.removeClass("show"), s("body").removeClass("ext-nav-show"), d.unbind("click", p), e.topNav.unforceState("ext-nav")) : (l.addClass("show"), r.addClass("show"), s("body").addClass("ext-nav-show"), d.bind("click", p), e.topNav.forceState("ext-nav"))
                } else r.hasClass("show") ? n() : (i || (s(".textillate").children("span:nth-of-type(1)").css({
                    display: "none"
                }), s(".textillate").children("span:nth-of-type(2)").css({
                    display: "inline"
                }), s(".textillate").children("span:nth-of-type(2)").children(":not(.current)").css({
                    display: "none"
                })), r.addClass("show"), s("body").addClass("responsive-nav-show"))
            })
        }
    }, {}],
    22: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(e, a, t, i) {
            function n(e, a) {
                var t = s("html").hasClass("ie9") || s("html").hasClass("ie10");
                t ? (e.find("iframe").attr("src", ""), setTimeout(function() {
                    a()
                }, 300)) : a()
            }
            var r = e.find(".cross"),
                o = s(e.data("overlay-zoom")),
                l = e.find(".overlay-view"),
                d = this;
            this.$overlay = e, this.show = function(t, i, n) {
                var r = function(d) {
                    return n && !d ? void n(function() {
                        r(!0)
                    }) : (o.addClass("overlay-zoom"), void e.transitionEnd(function() {
                        var n = function() {
                            l.find("iframe").each(function() {
                                var e = s(this),
                                    a = e.data("hold-src");
                                a && !e.attr("src") && (e[0].src = a), e.addClass("show")
                            })
                        };
                        if (t) {
                            var r = e.find(".loader"),
                                o = s('<div class="loaded-content"></div>');
                            r.addClass("show"), o.addClass("content-container").appendTo(l), o.load(t, function(t, s, l) {
                                function d() {
                                    n(), a && a(o), o.addClass("show"), r.removeClass("show"), i && i()
                                }
                                if ("success" !== s && "notmodified" !== s) return void o.text(s);
                                e.find("iframe").addClass("show");
                                var p = o.find("img"),
                                    c = p.length;
                                c > 0 ? p.load(function() {
                                    c--, 0 === c && d()
                                }) : d()
                            })
                        } else n(), a && a(l), i && i()
                    }).addClass("show"))
                };
                e.hasClass("show") ? d.hide(r) : r()
            }, this.hide = function(a) {
                o.removeClass("overlay-zoom"), e.removeClass("show"), setTimeout(function() {
                    var s = e.find(".loaded-content");
                    s.length > 0 ? (t && (e.find("iframe").removeClass("show"), t(e)), n(s, function() {
                        s.remove(), i && i(), a && a()
                    })) : (t && (e.find("iframe").removeClass("show"), t(e)), i && i(), a && a())
                }, 500)
            }, r.click(function(e) {
                return e.preventDefault(), d.hide(), !1
            })
        }
    }, {}],
    23: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(e, a) {
            var t = s("html").hasClass("poor-browser"),
                i = s("html").hasClass("no-animations");
            e.find(".skillbar").each(function() {
                var e = s(this),
                    n = e.find(".skillbar-bar"),
                    r = parseInt(e.attr("data-percent").replace("%", ""));
                if (t || i) n.css({
                    width: r + "%"
                });
                else {
                    var o = {
                            width: 0
                        },
                        l = new TWEEN.Tween(o).to({
                            width: r
                        }, 1e3).onUpdate(function() {
                            n.css({
                                width: this.width + "%"
                            })
                        }).easing(TWEEN.Easing.Quartic.Out),
                        d = function() {
                            l.stop()
                        },
                        p = function() {
                            o.width = 0, l.start()
                        },
                        c = p;
                    a.players.addPlayer(e, c, d, p)
                }
            })
        }
    }, {}],
    24: [function(require, module, exports) {
        "use strict";
        var $ = jQuery;
        module.exports = function($context, isRemoved) {
            var Swiper = require("../bower_components/swiper/dist/js/swiper.jquery.js");
            return isRemoved ? void $context.find(".swiper-container.default-slider").each(function() {
                var e = $(this)[0].swiper;
                e.destroy(!0, !0)
            }) : void $context.find(".default-slider:not(.hold)").each(function() {
                var $this = $(this).addClass("swiper-container"),
                    el = $this[0],
                    $el = $(el),
                    sopt = $el.data("swiper-options"),
                    opt = sopt ? eval("(" + sopt + ")") : {},
                    swiper = new Swiper(el, $.extend({
                        pagination: $el.find(".swiper-pagination")[0],
                        paginationClickable: $el.find(".swiper-pagination")[0],
                        nextButton: $el.find(".swiper-button-next")[0],
                        prevButton: $el.find(".swiper-button-prev")[0]
                    }, opt))
            })
        }
    }, {
        "../bower_components/swiper/dist/js/swiper.jquery.js": 9
    }],
    25: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(a, t) {
            var i = e("../tools/tools.js"),
                n = s("html").hasClass("android-browser"),
                r = s("html").hasClass("no-animations"),
                o = s("html").hasClass("poor-browser"),
                l = s("html").hasClass("safari");
            n || r || o || a.find(".text-bg").each(function(e) {
                var a = s(this);
                a.find(a.data("text-effect-selector")).each(function(e) {
                    var n = s(this).addClass("text-bg-svg svg-effect"),
                        r = [];
                    n.contents().filter(function() {
                        return !!s.trim(this.innerHTML || this.data)
                    }).each(function(e) {
                        var a = s(this);
                        r.push(a.text())
                    });
                    var o = function() {
                        var e, o, d = function(a, t, s) {
                                t.attr({
                                    width: "100%",
                                    height: "100%"
                                });
                                var l = t.text(0, 0, r).attr({
                                    fill: i.snapUrl(a)
                                });
                                n.empty().append(s), e = n.width();
                                var d = parseFloat(n.css("line-height").replace("px", "")),
                                    p = n.css("text-align");
                                l.selectAll("tspan").forEach(function(a, t) {
                                    a.attr({
                                        dy: d
                                    }), a.attr("right" === p ? {
                                        x: e,
                                        "text-anchor": "end"
                                    } : "center" === p ? {
                                        x: Math.round(e / 2),
                                        "text-anchor": "middle"
                                    } : {
                                        x: 0,
                                        "text-anchor": "start"
                                    })
                                }), o = Math.round(r.length * d + d / 5), n.height(o), t.attr({
                                    viewBox: "0 0 " + e + " " + o
                                })
                            },
                            p = document.createElementNS("http://www.w3.org/2000/svg", "svg"),
                            c = Snap(p),
                            u = function(e, i, n, r, o) {
                                var d = function() {
                                        this.animate(i, r, o, p)
                                    },
                                    p = function() {
                                        this.animate(n, r, o, d)
                                    };
                                if (-1 !== a.data("text-effect").indexOf("animated")) {
                                    var c = !1,
                                        u = !1;
                                    if (e.animate(n, r, o, d), "custom-animated" === a.data("text-effect") || l) {
                                        var h = null;
                                        s(window).scroll(function(a) {
                                            clearTimeout(h), u || (u = !0, c || e.inAnim()[0].mina.pause()), h = setTimeout(function() {
                                                u = !1, c || (e.animate({
                                                    dummy: 0
                                                }, 1), e.inAnim()[0].mina.resume())
                                            }, 100)
                                        })
                                    }
                                    var f = function() {
                                            c = !0, u || e.inAnim()[0].mina.pause()
                                        },
                                        m = function() {
                                            c = !1, u || (e.animate({
                                                dummy: 0
                                            }, 1), e.inAnim()[0].mina.resume())
                                        },
                                        g = m;
                                    t.players.addPlayer(a, g, f, m)
                                }
                            };
                        if (-1 !== a.data("text-effect").indexOf("custom")) {
                            var h = a.data("text-bg");
                            if (h) {
                                var f = new Image;
                                f.onload = function() {
                                    var a = this.width,
                                        t = this.height,
                                        s = 3e4,
                                        i = mina.easeinout,
                                        n = c.image(h, 0, 0, a, t).toPattern(0, 0, a, t).attr({
                                            patternUnits: "userSpaceOnUse"
                                        });
                                    d(n, c, p);
                                    var r = {
                                            x: e - a,
                                            y: o - t
                                        },
                                        l = {
                                            x: 0,
                                            y: 0
                                        };
                                    u(n, l, r, s, i)
                                }, f.src = h
                            }
                        } else if ("no" !== a.data("text-effect")) {
                            var m = 300,
                                g = 3e4,
                                v = mina.easeinout,
                                w = n.height(),
                                y = c.rect().attr({
                                    width: "100%",
                                    height: w
                                }).addClass("fill-highlight"),
                                x = c.rect().attr({
                                    width: "90%",
                                    height: w
                                }).addClass("fill-heading"),
                                b = c.rect().attr({
                                    width: "50%",
                                    height: w
                                }).addClass("fill-highlight"),
                                C = c.rect().attr({
                                    width: "20%",
                                    height: w
                                }).addClass("fill-heading"),
                                T = c.g(y, x, b, C).toPattern(0, 0, m, w).attr({
                                    patternUnits: "userSpaceOnUse",
                                    preserveAspectRatio: "none"
                                }); - 1 !== a.data("text-effect").indexOf("effect-b-") && T.attr({
                                transform: "rotate(-45deg)"
                            }), d(T, c, p);
                            var S = {
                                    width: 50
                                },
                                k = {
                                    width: m
                                };
                            u(T, k, S, g, v)
                        }
                    };
                    s(window).resize(o), o()
                })
            })
        }
    }, {
        "../tools/tools.js": 12
    }],
    26: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(e) {
            var a = s("html").hasClass("android-browser");
            a || e.find(".text-fit").each(function() {
                var e = s(this);
                e.find(e.data("text-effect-selector")).each(function(e) {
                    var a = s(this).addClass("text-fit-svg svg-effect"),
                        t = a.find("a"),
                        i = t.length > 0 ? s(t[0]) : a,
                        n = [];
                    i.contents().filter(function() {
                        return !!s.trim(this.innerHTML || this.data)
                    }).each(function(e) {
                        var a = s(this);
                        n.push(a.text())
                    }), i.addClass("normal-letter-spacing");
                    var r = function() {
                        var e = document.createElementNS("http://www.w3.org/2000/svg", "svg"),
                            a = Snap(e);
                        i.empty().append(e);
                        var t = i.innerWidth();
                        t > 391 && (t = 391);
                        for (var s = parseFloat(i.css("font-size").replace("px", "")), r = 10, o = 0, l = 20, d = 0; d < n.length; d++) {
                            var p = a.text(0, 0, n[d]),
                                c = p.getBBox().width,
                                u = s * t / c,
                                h = r + u;
                            o += d > 0 ? h : u + l, p.attr({
                                x: 0,
                                y: o,
                                "font-size": u + "px"
                            }).addClass("fill-heading")
                        }
                        var f = o + l;
                        a.attr({
                            width: t,
                            height: f
                        }), i.height(f)
                    };
                    s(window).resize(r), r()
                })
            })
        }
    }, {}],
    27: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(a) {
            var t = (e("../tools/tools.js"), s("html").hasClass("android-browser"));
            t || a.find(".text-fullscreen").each(function() {
                var e = s(this);
                e.find(".fg").addClass("background-transparent"), e.find(e.data("text-effect-selector")).each(function(a) {
                    var t = s(this).addClass("text-fullscreen-svg svg-effect"),
                        i = t.find("a"),
                        n = i.length > 0 ? s(i[0]) : t,
                        r = [];
                    n.contents().filter(function() {
                        return !!s.trim(this.innerHTML || this.data)
                    }).each(function(e) {
                        var a = s(this);
                        r.push(a.text())
                    }), n.addClass("normal-letter-spacing");
                    var o = function() {
                        var a = document.createElementNS("http://www.w3.org/2000/svg", "svg"),
                            t = Snap(a);
                        t.attr({
                            width: "100%",
                            height: "100%"
                        }), n.empty().append(a);
                        var i = t.mask().attr({
                            x: 0,
                            y: 0,
                            width: "100%",
                            height: "100%"
                        });
                        i.toDefs();
                        var o = Math.round(.7 * s(window).width());
                        o > 391 ? o = 391 : 200 > o && (o = 200);
                        for (var l = parseFloat(n.css("font-size").replace("px", "")), d = parseFloat(n.css("line-height").replace("px", "")), p = d / l, c = 0, u = i.g(), h = 0; h < r.length; h++) {
                            var f = t.text(0, 0, r[h]),
                                m = f.getBBox().width,
                                g = l * o / m,
                                v = p * g;
                            c += h > 0 ? v : .85 * g, f.attr({
                                x: 0,
                                y: c,
                                "font-size": g + "px"
                            }), u.add(f)
                        }
                        var w = e.data("text-bg"),
                            y = t.g(w ? t.image(w, 0, 0, "100%", "100%").attr({
                                preserveAspectRatio: "xMidYMid slice"
                            }) : t.rect({
                                x: 0,
                                y: 0,
                                width: "100%",
                                height: "100%",
                                fill: "#ffffff"
                            })).toPattern(0, 0, s(window).width(), s(window).height());
                        t.rect({
                            x: 0,
                            y: 0,
                            width: "100%",
                            height: "100%",
                            fill: y,
                            stroke: "none"
                        }).prependTo(i), t.rect({
                            x: 0,
                            y: 0,
                            width: "100%",
                            height: "100%",
                            mask: i,
                            stroke: "none"
                        }).addClass("fill-bg");
                        var x = Math.round((s(window).width() - o) / 2),
                            b = Math.round((s(window).height() - c) / 2);
                        u.attr({
                            transform: "translate(" + x + ", " + b + ")"
                        })
                    };
                    s(window).resize(o), o()
                })
            })
        }
    }, {
        "../tools/tools.js": 12
    }],
    28: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function(a) {
            var t = (e("../tools/tools.js"), s("html").hasClass("android-browser"));
            t || a.find(".text-mask").each(function() {
                var e = s(this);
                e.find(e.data("text-effect-selector")).each(function(a) {
                    var t = s(this).addClass("text-mask-svg svg-effect"),
                        i = t.find("a"),
                        n = i.length > 0 ? s(i[0]) : t,
                        r = [];
                    n.contents().filter(function() {
                        return !!s.trim(this.innerHTML || this.data)
                    }).each(function(e) {
                        var a = s(this);
                        r.push(a.text())
                    }), n.addClass("normal-letter-spacing");
                    var o = function() {
                        var a = document.createElementNS("http://www.w3.org/2000/svg", "svg"),
                            t = Snap(a);
                        t.attr({
                            width: "100%",
                            height: "100%"
                        }), n.empty().append(a);
                        var s = t.mask().attr({
                            x: 0,
                            y: 0,
                            width: "100%",
                            height: "100%"
                        });
                        s.toDefs();
                        var i = Math.round(.7 * n.innerWidth());
                        i > 391 && (i = 391);
                        for (var o = parseFloat(n.css("font-size").replace("px", "")), l = 10, d = Math.round(.15 * i), p = 0, c = s.g(), u = 0; u < r.length; u++) {
                            var h = t.text(0, 0, r[u]),
                                f = h.getBBox().width,
                                m = o * (i - d - d) / f,
                                g = l + m;
                            p += u > 0 ? g : .85 * m, h.attr({
                                x: d,
                                y: p,
                                "font-size": m + "px"
                            }), c.add(h)
                        }
                        var v = p + 2 * d,
                            w = Math.floor(v > i ? v / 2 : i / 2),
                            y = 2 * w,
                            x = y,
                            b = y,
                            C = e.data("text-bg"),
                            T = t.g(C ? t.image(C, 0, 0, x, b).attr({
                                preserveAspectRatio: "xMidYMid slice"
                            }) : t.rect(0, 0, x, b).attr({
                                fill: "#ffffff"
                            })).toPattern(0, 0, x, b);
                        t.rect({
                            x: 0,
                            y: 0,
                            width: "100%",
                            height: "100%",
                            fill: T,
                            stroke: "none"
                        }).prependTo(s), t.circle(w, w, w).attr({
                            mask: s,
                            stroke: "none"
                        }).addClass("fill-heading text-box"), c.attr({
                            transform: "translate(0, " + Math.round((y - p) / 2) + ")"
                        }), t.attr({
                            viewBox: "0 0 " + y + " " + y,
                            width: y,
                            height: y
                        }), n.height(y)
                    };
                    s(window).resize(o), o()
                })
            })
        }
    }, {
        "../tools/tools.js": 12
    }],
    29: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function() {
            var a = e("../tools/tools.js"),
                t = s("#top-nav:visible"),
                i = s("#top-nav, .page-border"),
                n = s("html"),
                r = s("html").hasClass("site-decoration-b"),
                o = (s("body").hasClass("admin-bar"), s("body").hasClass("page-template-builder")),
                l = t.length > 0,
                d = function() {
                    return l ? 20 : 0
                }(),
                p = !1,
                c = !1,
                u = !1,
                h = [],
                f = this,
                m = t.data("colors-2"),
                g = t.data("colors-1"),
                v = s(window);
            this.isState1 = !1, this.isState2 = !1;
            var w = r ? 40 : 48,
                y = 767;
            s(".page-border.left, .page-border.right"), this.isMode2 = null, this.state2H = l ? w : 0, this.state1Top = function() {
                return d
            }, this.state1 = function() {
                n.addClass("on-top"), o ? !l || f.isState1 && !u || (p || (n.removeClass("state2").addClass("state1"), t.removeClass(g).addClass(m).addClass('dpLogoChange'), a.androidStylesFix(i)), c || (f.isState1 = !0, f.isState2 = !1)) : f.state2(!0)
            }, this.state2 = function(e) {
                e || n.removeClass("on-top"), !l || f.isState2 && !u || (p || (n.removeClass("state1").addClass("state2"), t.removeClass(m).addClass(g), a.androidStylesFix(i)), c || (f.isState1 = !1, f.isState2 = !0))
            }, this.forceState = function(e) {
                h.indexOf(e) < 0 && (h.push(e), c = !0, f.state2(), c = !1, p = !0)
            }, this.unforceState = function(e) {
                var a = h.indexOf(e);
                a >= 0 && (h.splice(a, 1), 0 === h.length && (p = !1, u = !0, f.isState1 ? f.state1() : f.state2(), u = !1))
            };
            var x = function() {
                f.isMode2 = s(window).width() <= y, f.isMode2 ? f.forceState("size") : f.unforceState("size")
            };
            v.resize(x), x()
        }
    }, {
        "../tools/tools.js": 12
    }],
    30: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function() {
            var e = s(".video-bg");
            if (!(e.length < 1)) {
                var a = function() {
                    var e = s("html").hasClass("mobile"),
                        a = document.createElement("video"),
                        t = a.canPlayType ? a.canPlayType("video/mp4") : !1;
                    return t && !e
                }();
                return a ? void e.each(function() {
                    var e = s(this);
                    e.addClass("loading-func").data("loading", function(a) {
                        var t = s('<video class="video-bg"></video>');
                        "yes" === e.data("mute") && (t[0].muted = !0);
                        var i = e.data("volume");
                        void 0 !== i && (t[0].volume = i / 100);
                        var n = function() {
                            var e = t.width(),
                                i = t.height(),
                                n = e / i,
                                r = s(window),
                                o = function() {
                                    var e, a, s = r.width(),
                                        i = r.height(),
                                        o = s / i;
                                    n > o ? (a = Math.ceil(i), e = Math.ceil(a * n)) : (e = Math.ceil(s), a = Math.ceil(e / n)), t.css({
                                        width: e + "px",
                                        height: a + "px",
                                        top: Math.round((i - a) / 2) + "px",
                                        left: Math.round((s - e) / 2) + "px"
                                    })
                                };
                            r.resize(o), o(), t[0].play(), a()
                        };
                        t.on("ended", function() {
                            this.currentTime = 0, this.play(), this.ended && this.load()
                        });
                        var r = !0;
                        t.on("canplaythrough", function() {
                            r ? (r = !1, n()) : this.play()
                        }), t.on("error", function() {
                            r && (r = !1, n())
                        }), t[0].src = e.data("video"), t[0].preload = "auto", e.after(t), e.remove()
                    })
                }) : void e.each(function() {
                    var e = s(this),
                        a = e.data("alternative");
                    if (a) {
                        var t = s('<img alt class="bg" src="' + a + '"/>');
                        e.after(t).remove()
                    }
                })
            }
        }
    }, {}],
    31: [function(e, a, t) {
        "use strict";
        var s = jQuery;
        a.exports = function() {
            var e = s(".vimeo-bg");
            if (!(e.length < 1)) {
                if (s("html").hasClass("mobile")) return void e.each(function() {
                    var e = s(this),
                        a = e.data("alternative");
                    if (a) {
                        var t = s('<img alt class="bg" src="' + a + '"/>');
                        e.after(t).remove()
                    }
                });
                var a = [];
                e.each(function(e) {
                    var t = s(this),
                        i = t.attr("id");
                    i || (i = "vimeo-bg-" + e, t.attr("id", i)), t.addClass("loading-func").data("loading", function(e) {
                        a[i] = e
                    })
                }), s.getScript("https://f.vimeocdn.com/js/froogaloop2.min.js").done(function(t, i) {
                    e.each(function() {
                        var e = s(this),
                            t = e.attr("id"),
                            i = function() {
                                var a = e.data("volume");
                                return void 0 === a ? 0 : a
                            }(),
                            n = e.data("video"),
                            r = s('<iframe class="vimeo-bg" src="https://player.vimeo.com/video/' + n + "?api=1&badge=0&byline=0&portrait=0&title=0&autopause=0&player_id=" + t + '&loop=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
                        e.after(r), e.remove(), r.attr("id", t);
                        var o = $f(r[0]);
                        o.addEvent("ready", function() {
                            var e = function(e) {
                                var a = s(window).width(),
                                    t = s(window).height(),
                                    i = r.width(),
                                    n = r.height(),
                                    o = i / n,
                                    l = a / t,
                                    d = function(s, i) {
                                        var n, l;
                                        o > e ? (l = Math.ceil(i), n = Math.ceil(l * o)) : (n = Math.ceil(s), l = Math.ceil(n / o)), r.css({
                                            width: n + "px",
                                            height: l + "px",
                                            top: Math.round((t - l) / 2) + "px",
                                            left: Math.round((a - n) / 2) + "px"
                                        })
                                    };
                                if (l > e) {
                                    var p = a,
                                        c = p / e;
                                    d(p, c)
                                } else {
                                    var c = t,
                                        p = c * e;
                                    d(p, c)
                                }
                            };
                            o.addEvent("finish", function() {
                                o.api("play")
                            });
                            var n = !0;
                            o.addEvent("play", function() {
                                n && (n = !1,
                                    a[t]())
                            }), o.api("setVolume", i), o.api("getVideoWidth", function(a, t) {
                                var i = a;
                                o.api("getVideoHeight", function(a, t) {
                                    var n = a,
                                        r = i / n;
                                    s(window).resize(function() {
                                        e(r)
                                    }), e(r), o.api("play")
                                })
                            })
                        })
                    })
                }).fail(function(e, a, t) {
                    console.log("Triggered ajaxError handler.")
                })
            }
        }
    }, {}]
}, {}, [11]);