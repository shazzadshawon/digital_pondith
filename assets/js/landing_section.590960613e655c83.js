var SEPARATION = 100, AMOUNTX = 50, AMOUNTY = 50;
var container_wave, containerSize, fullWave_container;
var camera_wave, scene_wave, renderer_wave, wave_cam_control;
var particles, particle, count = 0;
var mouseX_wave = 0, mouseY_wave = 0;
var windowHalfX_wave = window.innerWidth / 2;
var windowHalfY_wave = window.innerHeight / 2;
var particle_material, texture_map;
init_wave();
animate_wave();

function init_wave() {
    container_wave = document.getElementById( 'canvas_perticle_wave' );
    containerSize = document.getElementById( 'layers-widget-skrollex-section-3' );
    fullWave_container = document.getElementById( 'home' );
    camera_wave = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 20000 );
    camera_wave.position.x = -533.5619553605518;
    camera_wave.position.y = 432.651586093233;
    camera_wave.position.z = 1002.0966409697394; //1000

    scene_wave = new THREE.Scene();
    camera_wave.lookAt( scene_wave.position );
    particles = new Array();
    particle_material = new Array();
    texture_map = new Array();
    var PI2 = Math.PI * 2;

    texture_map.push(new THREE.TextureLoader().load( "assets/data/social_sea/facebook_32.ff5ce6b5569e529e.png" ));
    texture_map.push(new THREE.TextureLoader().load( "assets/data/social_sea/twitter_32.b1daccd5ee72a2dc.png" ));
    texture_map.push(new THREE.TextureLoader().load( "assets/data/social_sea/linkedin_32.8fbe97e55b115bf0.png" ));
    texture_map.push(new THREE.TextureLoader().load( "assets/data/social_sea/myspace_32.392b17df0a482e54.png" ));
    texture_map.push(new THREE.TextureLoader().load( "assets/data/social_sea/pandora_32.630d30404d4c9ed5.png" ));
    texture_map.push(new THREE.TextureLoader().load( "assets/data/social_sea/slashdot_32.8cfb4f811cedbbec.png" ));
    texture_map.push(new THREE.TextureLoader().load( "assets/data/social_sea/flickr_32.48e3d0798c39421e.png" ));
    texture_map.push(new THREE.TextureLoader().load( "assets/data/social_sea/apple_32.c5c237485c4c4405.png" ));
    texture_map.push(new THREE.TextureLoader().load( "assets/data/social_sea/skype_32.197346fa1587ca6c.png" ));
    texture_map.push(new THREE.TextureLoader().load( "assets/data/social_sea/squidoo_32.1053aab3062e710d.png" ));
    texture_map.push(new THREE.TextureLoader().load( "assets/data/social_sea/bebo_32.2a6b99091b38e44c.png" ));
    texture_map.push(new THREE.TextureLoader().load( "assets/data/social_sea/google_32.487fb3507c9bfc08.png" ));
    texture_map.push(new THREE.TextureLoader().load( "assets/data/social_sea/google_talk_32.10992ad4bb51aeff.png" ));
    texture_map.push(new THREE.TextureLoader().load( "assets/data/social_sea/wordpress_32.bcabe75135606386.png" ));

    for(var s = 0; s < texture_map.length; s++){
        var temp_mat = new THREE.SpriteMaterial( { map: texture_map[s], fog: false } );
        particle_material.push(temp_mat);
    }

    var i = 0;
    var tmm = 0
    for ( var ix = 0; ix < AMOUNTX; ix ++ ) {
        for ( var iy = 0; iy < AMOUNTY; iy ++ ) {
            if(tmm >= particle_material.length){
                tmm = 0;
            }
            var max = particle_material.length - 1;
            var tmId = Math.floor(Math.random() * (max - 0 + 1)) + 0;
            particle = particles[ i ++ ] = new THREE.Sprite( particle_material[tmId] );
            particle.position.x = ix * SEPARATION - ( ( AMOUNTX * SEPARATION ) / 2 );
            particle.position.z = iy * SEPARATION - ( ( AMOUNTY * SEPARATION ) / 2 );
            scene_wave.add( particle );
            tmm++;
        }
    }
    renderer_wave = new THREE.CanvasRenderer({ alpha: true });
//        renderer_wave.setClearColorHex( 0xffffff, 0 );
//        renderer_wave.setClearColor(new THREE.Color(0xcecccc));
    renderer_wave.setClearColor(new THREE.Color(0xffffff));
    renderer_wave.setPixelRatio( window.devicePixelRatio );
    renderer_wave.setSize( window.innerWidth, window.innerHeight);
    container_wave.appendChild( renderer_wave.domElement );

    if(!isMobile){
        wave_cam_control = new THREE.OrbitControls( camera_wave, fullWave_container );
        wave_cam_control.enableDamping = true;
        wave_cam_control.dampingFactor = 0.25;
        wave_cam_control.enableZoom = false;
        wave_cam_control.rotateSpeed = 0.07;
    }

    window.addEventListener( 'resize', onWindowResize, false );
//        containerSize.addEventListener( 'mousemove', onDocumentMouseMove, false );
//        containerSize.addEventListener( 'touchstart', onDocumentTouchStart, false );
//        containerSize.addEventListener( 'touchmove', onDocumentTouchMove, false );
}
function onWindowResize() {
    windowHalfX_wave = window.innerWidth / 2;
    windowHalfY_wave = window.innerHeight / 2;
    camera_wave.aspect = window.innerWidth / window.innerHeight;
    camera_wave.updateProjectionMatrix();
    renderer_wave.setSize( window.innerWidth, window.innerHeight );
}
//.....    ON MOUSE MOVE CAMERA ROTAION FUNCTIONALITY
//    function onDocumentMouseMove( event ) {
//        mouseX_wave = event.clientX - windowHalfX_wave;
//        mouseY_wave = event.clientY - windowHalfY_wave;
//    }
//    function onDocumentTouchStart( event ) {
//        if ( event.touches.length === 1 ) {
//            event.preventDefault();
//            mouseX_wave = event.touches[ 0 ].pageX - windowHalfX_wave;
//            mouseY_wave = event.touches[ 0 ].pageY - windowHalfY_wave;
//        }
//    }
//    function onDocumentTouchMove( event ) {
//        if ( event.touches.length === 1 ) {
//            event.preventDefault();
//            mouseX_wave = event.touches[ 0 ].pageX - windowHalfX_wave;
//            mouseY_wave = event.touches[ 0 ].pageY - windowHalfY_wave;
//        }
//    }
//......    ON MOUSE MOVE CAMERA ROTAION FUNCTIONALITY
function animate_wave() {
    requestAnimationFrame( animate_wave );
    if(!isMobile){
        wave_cam_control.update(); // required if controls.enableDamping = true, or if controls.autoRotate = true
    }
    render_wave();
}
function render_wave() {
//        console.log(camera_wave.position);
//        camera_wave.position.x += ( mouseX_wave - camera_wave.position.x ) * .05;
//        camera_wave.position.y += ( - mouseY_wave - camera_wave.position.y ) * .05;
//        camera_wave.lookAt( scene_wave.position );
    var i = 0;
    for ( var ix = 0; ix < AMOUNTX; ix ++ ) {
        for ( var iy = 0; iy < AMOUNTY; iy ++ ) {
            particle = particles[ i++ ];
            particle.position.y = ( Math.sin( ( ix + count ) * 0.3 ) * 50 ) +
                ( Math.sin( ( iy + count ) * 0.5 ) * 50 );
            particle.scale.x = particle.scale.y = ( Math.sin( ( ix + count ) * 0.3 ) + 1.2 ) * 8 +
                ( Math.sin( ( iy + count ) * 0.5 ) + 1.2 ) * 8;
        }
    }
    renderer_wave.clear();
    renderer_wave.render( scene_wave, camera_wave );
    count += 0.05;
}