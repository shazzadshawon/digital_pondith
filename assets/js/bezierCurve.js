(function() {


    var addCurveSegment;
    var canvas;
    var changeCubicSlider;
    var changeLanczosSlider;
    var cx;
    var distance;
    var getHandlePoint;
    var getHandles;
    var getPoints;
    var handleDoubleClick;
    var hitTest;
    var hit_cx;
    var makeHandle;
    var plotBox;
    var plotBoxDoubleClick;
    var redraw;
    var smoothConfig;
    var updateConfigBox;
    var __slice = Array.prototype.slice;

    plotBox = null;

    canvas = null;

    cx = null;

    hit_cx = null;

    smoothConfig = {
        clip : 'mirror',
        method : 'cubic',
        lanczosFilterSize : 2,
        cubicTension : 0
    };

    distance = function(a, b) {
        return Math.sqrt(Math.pow(a[0] - b[0], 2) + Math.pow(a[1] - b[1], 2));
    };

    jQuery(function() {

        var canvasW = window.innerWidth;
        var canvasH = window.innerWidth * 0.26;

        plotBox	= jQuery('#bezier-curve');

        generateCanvas = function(){
            canvas 	= jQuery("<canvas width='" + canvasW + "' height='" + canvasH + "' />").appendTo(plotBox);
            cx 		= canvas[0].getContext('2d');
            hit_cx 	= jQuery("<canvas width='" + canvasW + "' height='" + canvasH + "' />");
            hit_cx.css({display: 'none'}).appendTo(plotBox)[0].getContext('2d');
            plotBox.dblclick(plotBoxDoubleClick);
        };
        generateCanvas();

        makeHandle = function(x, y, id, isInteractive, novel, textInside, textLabel) {
            var handle;

            handle = jQuery('<div class="center" data-x="' + x + '" data-y="' + y + '" data-id="' + id + '" ></div>');
            handle.addClass('handle');
            handle.appendTo(plotBox);
            handle.css({
                'top' : y,
                'left' : x,
                position : 'absolute'
            });

            if(isInteractive){
                handle.text(textInside);
                handle.addClass('handle-move');
                handle.attr('data-novel', novel);

                generateInteractiveHandle(x, y, id, novel, textLabel);
            }

            return handle;
        };


        generateInteractiveHandle = function(x, y, id, novel, textLabel){
            var interactiveHandle;

            interactiveHandle = jQuery('<div class="interactive-handle" data-handle="' + id + '"><div class="novel-circle" data-novel="' + novel + '"></div><div data-novel="' + novel + '" class="novel-link">' + textLabel + '</div></div>');
            interactiveHandle.css({
                'left': (id * 12.5) + '%',
                'width': '25%',
            });

            interactiveHandle.appendTo(plotBox);
        };


        getHandles = function() {
            return plotBox.children('div.handle');
        };



        getHandlePoint = function(handle) {
            var left, top, _ref;
            _ref = jQuery(handle).position(), top = _ref.top, left = _ref.left;
            return [left + 6, top + 6];
        };

        getPoints = function() {
            var handle, _i, _len, _ref, _results;
            _ref = getHandles();
            _results = [];
            for ( _i = 0, _len = _ref.length; _i < _len; _i++) {
                handle = _ref[_i];
                _results.push(getHandlePoint(handle));
            }
            return _results;
        };

        plotBoxDoubleClick = function(ev) {
            /* Add a new handle at the clicked location
             */
            var first, last, middle, newHandle, newPoint, offset, segmentStartHandle, x, y, _i, _ref, _ref2;
            offset = plotBox.offset();
            _ref = [ev.pageX - offset.left, ev.pageY - offset.top], x = _ref[0], y = _ref[1];
            segmentStartHandle = getHandles()[hitTest(x, y)];
            newHandle = makeHandle(x, y);
            if (segmentStartHandle != null) {
                newHandle.insertAfter(jQuery(segmentStartHandle));
            } else {
                _ref2 = getPoints(), first = _ref2[0], middle = 4 <= _ref2.length ? __slice.call(_ref2, 1, _i = _ref2.length - 2) : ( _i = 1, []), last = _ref2[_i++], newPoint = _ref2[_i++];
                if (distance(first, newPoint) < distance(last, newPoint)) {
                    newHandle.insertBefore(jQuery(getHandles()[0]));
                }
            }
            redraw();
            return false;
        };

        handleDoubleClick = function(handle) {
            handle.remove();
            redraw();
            return false;
        };

        updateConfigBox = function() {
            jQuery('div.method-config').hide();
            return jQuery("div#" + smoothConfig.method + "-config").show();
        };

        changeLanczosSlider = function(ev, ui) {
            smoothConfig.lanczosFilterSize = ui.value;
            jQuery("#lanczosfiltersize").text(smoothConfig.lanczosFilterSize);
            return redraw();
        };

        changeCubicSlider = function(ev, ui) {
            smoothConfig.cubicTension = ui.value;
            jQuery("#cubictension").text(smoothConfig.cubicTension.toFixed(1));
            return redraw();
        };

        addCurveSegment = function(context, i, points) {
            var averageLineLength, du, end, pieceCount, pieceLength, s, start, t, u, _ref, _ref2, _ref3;
            s = Smooth(points, smoothConfig);
            averageLineLength = 1;
            pieceCount = 2;
            for ( t = 0, _ref = 1 / pieceCount; t < 1; t += _ref) {
                _ref2 = [s(i + t), s(i + t + 1 / pieceCount)], start = _ref2[0], end = _ref2[1];
                pieceLength = distance(start, end);
                du = averageLineLength / pieceLength;
                for ( u = 0, _ref3 = 1 / pieceCount; 0 <= _ref3 ? u < _ref3 : u > _ref3; u += du) {
                    context.lineTo.apply(context, s(i + t + u));
                }
            }
            return context.lineTo.apply(context, s(i + 1));
        };

        redraw = function() {
            var i, lastIndex, points;
            cx.clearRect(0, 0, canvas.width(), canvas.height());
            points = getPoints();
            if (points.length >= 2) {
                cx.beginPath();
                cx.moveTo.apply(cx, points[0]);
                lastIndex = points.length - 1;
                if (smoothConfig.clip === 'periodic')
                    lastIndex++;
                for ( i = 0; 0 <= lastIndex ? i < lastIndex : i > lastIndex; 0 <= lastIndex ? i++ : i--) {
                    addCurveSegment(cx, i, points);
                }
                cx.lineWidth = 2;
                cx.strokeStyle = '#000000';
                cx.setLineDash([5, 10]);
                cx.lineJoin = 'round';
                cx.lineCap = 'round';
                return cx.stroke();
            }
        };

        hitTest = function(x, y) {
            var i, points, _ref;
            points = getPoints();
            hit_cx.clearRect(0, 0, canvas.width(), canvas.height());
            for ( i = 0, _ref = points.length; 0 <= _ref ? i < _ref : i > _ref; 0 <= _ref ? i++ : i--) {
                hit_cx.beginPath();
                hit_cx.moveTo(points[i]);
                addCurveSegment(hit_cx, i, points);
                hit_cx.color = "#FFFFFF";
                hit_cx.lineWidth = 20;
                hit_cx.lineCap = 'round';
                hit_cx.lineJoin = 'round';
                hit_cx.stroke();
                if (hit_cx.getImageData(x, y, 1, 1).data[3] === 255)
                    return i;
            }
        };



        generateHandles = function(){

            var _handles = [];
            // X				// Y				// ID		// INTERATTIVO		// NOVEL		// TEXT INSIDE			// TEXT LABEL
            _handles.push(makeHandle(0, 				canvasH * .5, 		0,								'',				'',						''								));
            _handles.push(makeHandle(canvasW * .125, 	canvasH * .5, 		1,								'',				'',						''								));
            _handles.push(makeHandle(canvasW * .25, 	canvasH * .5, 		2, 			true,				'lake',			'I.',					'the color<br />of the LAKE'	));
            _handles.push(makeHandle(canvasW * .375, 	canvasH * .5, 		3,								'',				'',						''								));
            _handles.push(makeHandle(canvasW * .5, 		canvasH * .5, 		4, 			true,				'garden',		'II.',					'a small secrect<br />GARDEN'	));
            _handles.push(makeHandle(canvasW * .625, 	canvasH * .5, 		5,								'',				'',						''								));
            _handles.push(makeHandle(canvasW * .75, 	canvasH * .5, 		6, 			true,				'eyes',			'III.',					'the journey<br />of the EYES'	));
            _handles.push(makeHandle(canvasW * .875, 	canvasH * .5, 		7,								'',				'',						''								));
            _handles.push(makeHandle(canvasW, 			canvasH * .5, 		8,								'',				'',						''								));
        };
        generateHandles();

        onMouseEnterInteractiveHandle = function(){
            jQuery('.interactive-handle').on('mouseenter', function(){
                var idHandle = jQuery(this).attr('data-handle');
                var jQueryhandle = jQuery('.handle[data-id=' + idHandle + ']');

                handleGoDecenter(jQuery('.handle-move').not(jQueryhandle));
                handleGoUp(jQueryhandle);
            });
        };
        onMouseLeaveInteractiveHandle = function(){
            jQuery('.interactive-handle').on('mouseleave', function(){
                handleGoCenter(jQuery('.handle-move'));
            });
        };
        onClickInteractiveHandleText = function(){
            jQuery('.interactive-handle .novel-link, .interactive-handle .novel-circle').on('click', function(){
                var redirectNovel = jQuery(this).attr('data-novel');
                jQuery('body').trigger('redirectNovel', [redirectNovel]);
            });
        };
        offClickInteractiveHandleText = function(){
            jQuery('.interactive-handle .novel-link, .interactive-handle .novel-circle').off('click');
        };
        if(isMobile) onMouseEnterInteractiveHandle();
        if(!isMobile) onMouseLeaveInteractiveHandle();
        onClickInteractiveHandleText();


        handleGoUp = function(p_jQueryhandle){

            var jQueryhandle = p_jQueryhandle;
            var endY	= canvasH * .20;

            TweenMax.killTweensOf(jQueryhandle);
            TweenMax.to(jQueryhandle, 7, {
                top: endY,
                ease: Elastic.easeOut,
                onUpdate: function(){
                    redraw();
                }
            });
        };
        handleGoCenter = function(p_jQueryhandle){

            var jQueryhandle = p_jQueryhandle;
            var endY	= canvasH * .5;

            TweenMax.killTweensOf(jQueryhandle);
            TweenMax.to(jQueryhandle, 7, {
                top: endY,
                ease: Elastic.easeOut,
                onUpdate: function(){
                    redraw();
                }
            });
        };
        handleGoDecenter = function(p_jQueryhandle){

            var jQueryhandle = p_jQueryhandle;
            var endY	= canvasH * .4;

            TweenMax.killTweensOf(jQueryhandle);
            TweenMax.to(jQueryhandle, 7, {
                top: endY,
                ease: Elastic.easeOut,
                onUpdate: function(){
                    redraw();
                }
            });
        };
        handleGoDown = function(p_jQueryhandle){

            var jQueryhandle = p_jQueryhandle;
            var endY	= canvasH * .80;

            TweenMax.killTweensOf(jQueryhandle);
            TweenMax.to(jQueryhandle, 7, {
                top: endY,
                ease: Elastic.easeOut,
                onUpdate: function(){
                    redraw();
                }
            });
        };




        resizeCanvas = function(){
            canvasW = window.innerWidth;
            canvasH = window.innerWidth * 0.26;
        };

        var _timeoutToggleCurva;
        restartCanvas = function(){

            jQuery('#bezier-curve canvas').remove();
            jQuery('#bezier-curve .handle').remove();
            jQuery('#bezier-curve .interactive-handle').remove();
            resizeCanvas();
            generateCanvas();
            generateHandles();

            if(App.DEVICETYPE == 'desktop') onMouseEnterInteractiveHandle();
            if(App.DEVICETYPE == 'desktop') onMouseLeaveInteractiveHandle();
            offClickInteractiveHandleText();
            onClickInteractiveHandleText();

            if(App.DEVICETYPE == 'mobile'){
                if(_timeoutToggleCurva) _timeoutToggleCurva = null;
                _timeoutToggleCurva = setTimeout(function(){
                    toggleCurva();
                }, 50);
            }

            redraw();
        };


        jQuery(window).resize(function(){
            restartCanvas();
        });


        destroy = function(){
            jQuery('.interactive-handle').off('mouseenter');
            jQuery('.interactive-handle').off('mouseleave');
            jQuery('.interactive-handle .novel-link, .interactive-handle .novel-circle').off('click');
            jQuery('#bezier-curve canvas').remove();
            jQuery('#bezier-curve .handle').remove();
            jQuery('#bezier-curve .interactive-handle').remove();
        };

        jQuery('body').on('completeScrollTopNovel', function(){
            destroy();
        });



        if(isMobile){

            var footerUp = false;

            jQuery('.interactive-handle').off('mouseenter');
            jQuery('.interactive-handle').off('mouseleave');

            jQuery('footer').on('is-inview', function(){
                toggleCurva();
            });
        }




        toggleCurva = function(){

            if(footerUp){
                footerUp = false;
                handleGoDown(jQuery('.handle-move')[0]);
                handleGoUp(jQuery('.handle-move')[1]);
                handleGoDown(jQuery('.handle-move')[2]);
            } else {
                footerUp = true;
                handleGoUp(jQuery('.handle-move')[0]);
                handleGoDown(jQuery('.handle-move')[1]);
                handleGoUp(jQuery('.handle-move')[2]);
            }
        };


        updateConfigBox();
        return redraw();
    });

}).call(this);