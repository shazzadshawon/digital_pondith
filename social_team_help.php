<section id="our_social_media" class="layout-boxed archive">
    <div class="pure-g">
        <div class="pure-u-1 pure-u-md-20-24">
            <div class="view x40-widget widget text-bg peraRemoval" id="layers-widget-skrollex-section-9" data-text-effect-selector="h1,h2,h3,h4" data-text-effect="effect-a-animated">

                <div class="pure-g">
                    <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">

                            <h1 class="post-title">
                                <a href="#">How our <span>Social Media</span> team can help</a>
                            </h1>
                            <div class="copy">
                                <p class="excerpt">We make sure that you will never fall into a well - publicized #epical campaign or a compilation of 'The 10 Biggest Social Media disaster…..' We also won’t entirely take over your accounts because we know that customer wants to talk to you and not listen to constant, one way sales pushed from your marketing agency. As part of our social media marketing service we provide to help & guidance with:</p>
                            </div>
                        </article>
                    </div>
                </div>

<!--                <div class="pure-g">-->
<!--                    <div class="post-meta pure-u-1 pure-u-md-8-24 text-right background-transparent colors-v">-->
<!--                        <a href="#">-->
<!--                            <div class="post-day heading"><span>Social</span></div>-->
<!--                            <div class="post-year heading">Consultancy</div>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <div class="colors-w post-body pure-u-1 pure-u-md-16-24 article-blog">-->
<!--                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">-->
<!--                            <div class="post-image push-bottom">-->
<!--                                <a href="#">-->
<!--                                    <img width="100" height="100" src="assets/data/services/smm/scon.6577be0a179bef7b.6577be0a179bef7b.d84561f7696bc138.png" alt="Social Consultancy icon"/>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                            <div class="copy">-->
<!--                                <p class="excerpt">We'll help you to develop a social media strategy with you based winning end goals. You may have already in-house marketing teams who just necessitate guidance or you may need us to help construct your social media presence from scrape. We'll help you to find the best approach.</p>-->
<!--                            </div>-->
<!--                        </article>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div class="pure-g">-->
<!--                    <div class="post-meta pure-u-1 pure-u-md-8-24 text-right background-transparent colors-v">-->
<!--                        <a href="#">-->
<!--                            <div class="post-day heading"><span>Brand</span></div>-->
<!--                            <div class="post-year heading">Management</div>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <div class="colors-w post-body pure-u-1 pure-u-md-16-24 article-blog">-->
<!--                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">-->
<!--                            <div class="post-image push-bottom">-->
<!--                                <a href="#">-->
<!--                                    <img width="100" height="100" src="assets/data/services/smm/brandM.8221ff5f2cd528f4.8221ff5f2cd528f4.ce55ab730cb774ab.png" class="img-responsive" alt="Social Consultancy icon"/>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                            <div class="copy">-->
<!--                                <p class="excerpt">Finding &amp; Securing the perfect usernames &amp; profiles across the social channels. Your brand is very important and so our social media team will always be on offer to help to turn potential and target customer problems into command.</p>-->
<!--                            </div>-->
<!--                        </article>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div class="pure-g">-->
<!--                    <div class="post-meta pure-u-1 pure-u-md-8-24 text-right background-transparent colors-v">-->
<!--                        <a href="#">-->
<!--                            <div class="post-day heading"><span>Social Tone</span></div>-->
<!--                            <div class="post-year heading">&amp; Nature</div>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <div class="colors-w post-body pure-u-1 pure-u-md-16-24 article-blog">-->
<!--                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">-->
<!--                            <div class="post-image push-bottom">-->
<!--                                <a href="#">-->
<!--                                    <img width="100" height="100" src="assets/data/services/smm/social_TN.d7bb1e9d69a00c38.d7bb1e9d69a00c38.ebdff9ae7edf1f6f.png" class="img-responsive" alt="Social Consultancy icon"/>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                            <div class="copy">-->
<!--                                <p class="excerpt">Which businesses can be used in social media differs particularly in the proper way. We will be there to help to find the right quality of voice and approach for you, social making sure that your business is portrayed exactly how you want it.</p>-->
<!--                            </div>-->
<!--                        </article>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div class="pure-g">-->
<!--                    <div class="post-meta pure-u-1 pure-u-md-8-24 text-right background-transparent colors-v">-->
<!--                        <a href="#">-->
<!--                            <div class="post-day heading"><span>Social</span></div>-->
<!--                            <div class="post-year heading">Media Monitoring</div>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <div class="colors-w post-body pure-u-1 pure-u-md-16-24 article-blog">-->
<!--                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">-->
<!--                            <div class="post-image push-bottom">-->
<!--                                <a href="#">-->
<!--                                    <img width="100" height="100" src="assets/data/services/smm/social_MM.7780994e13503f2d.7780994e13503f2d.937c43e6ad983066.png" class="img-responsive" alt="Social Consultancy icon"/>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                            <div class="copy">-->
<!--                                <p class="excerpt">We will search the social networks, making sure that you know who is talking about you and what they are saying and what they think, and also whether it is positive. This ensures that you are always in front of the discussion, ready to give whenever you need to.</p>-->
<!--                            </div>-->
<!--                        </article>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div class="pure-g">-->
<!--                    <div class="post-meta pure-u-1 pure-u-md-8-24 text-right background-transparent colors-v">-->
<!--                        <a href="#">-->
<!--                            <div class="post-day heading"><span>Social</span></div>-->
<!--                            <div class="post-year heading">PPC</div>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <div class="colors-w post-body pure-u-1 pure-u-md-16-24 article-blog">-->
<!--                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">-->
<!--                            <div class="post-image push-bottom">-->
<!--                                <a href="#">-->
<!--                                    <img width="100" height="100" src="assets/data/services/smm/social_PPc.a9c1dd852fa73c30.a9c1dd852fa73c30.9961581f97804832.png" class="img-responsive" alt="Social Consultancy icon"/>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                            <div class="copy">-->
<!--                                <p class="excerpt">Social ads can be used to improve your attendance or make interest in a new product as the standard updates. This could be through promoted Twitter accounts or trends, Facebook & LinkedIn Ads, or sponsored YouTube, Instagram or Pinterest content.</p>-->
<!--                            </div>-->
<!--                        </article>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div class="pure-g">-->
<!--                    <div class="post-meta pure-u-1 pure-u-md-8-24 text-right background-transparent colors-v">-->
<!--                        <a href="#">-->
<!--                            <div class="post-day heading"><span>Social</span></div>-->
<!--                            <div class="post-year heading">PR</div>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <div class="colors-w post-body pure-u-1 pure-u-md-16-24 article-blog">-->
<!--                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">-->
<!--                            <div class="post-image push-bottom">-->
<!--                                <a href="#">-->
<!--                                    <img width="100" height="100" src="assets/data/services/smm/social_PR.b49eee1fa9dc2125.b49eee1fa9dc2125.3f9e04c4136f02e7.png" class="img-responsive" alt="Social Consultancy icon"/>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                            <div class="copy">-->
<!--                                <p class="excerpt">Our close relationships & contacts with some of the most reliable publishers, journalists, and bloggers mean that we can help suspension bridge that gap between offline and online content that really means your content gets the treatment it deserves.</p>-->
<!--                            </div>-->
<!--                        </article>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div class="pure-g">-->
<!--                    <div class="post-meta pure-u-1 pure-u-md-8-24 text-right background-transparent colors-v">-->
<!--                        <a href="#">-->
<!--                            <div class="post-day heading"><span>Creative</span></div>-->
<!--                            <div class="post-year heading">Discussion</div>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <div class="colors-w post-body pure-u-1 pure-u-md-16-24 article-blog">-->
<!--                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">-->
<!--                            <div class="post-image push-bottom">-->
<!--                                <a href="#">-->
<!--                                    <img width="100" height="100" src="assets/data/services/smm/creative_dis.98b9a4e27db04dc9.98b9a4e27db04dc9.b77bfab9fb03d539.png" class="img-responsive" alt="Social Consultancy icon"/>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                            <div class="copy">-->
<!--                                <p class="excerpt">Our social media team will help you to create fast, reactive updates that absolutely hit the target mark & sparks to the further conversation that gets your brand awareness outside of your immediate industry.</p>-->
<!--                            </div>-->
<!--                        </article>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div class="pure-g">-->
<!--                    <div class="post-meta pure-u-1 pure-u-md-8-24 text-right background-transparent colors-v">-->
<!--                        <a href="#">-->
<!--                            <div class="post-day heading"><span>Report</span></div>-->
<!--                            <div class="post-year heading">&amp; Analysis</div>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <div class="colors-w post-body pure-u-1 pure-u-md-16-24 article-blog">-->
<!--                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">-->
<!--                            <div class="post-image push-bottom">-->
<!--                                <a href="#">-->
<!--                                    <img width="100" height="100" src="assets/data/services/smm/reporting_ana.10ebc7dd1c6230fe.10ebc7dd1c6230fe.1ae6fe7c66a6bb0b.png" class="img-responsive" alt="Social Consultancy icon"/>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                            <div class="copy">-->
<!--                                <p class="excerpt">Our social media reporting will help you to identify and inform future activity not only in your social circles but also across all your marketing channels.</p>-->
<!--                            </div>-->
<!--                        </article>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!--                <div class="pure-g">-->
<!--                    <div class="post-meta pure-u-1 pure-u-md-8-24 text-right background-transparent colors-v">-->
<!--                        <a href="#">-->
<!--                            <div class="post-day heading"><span>Cross</span></div>-->
<!--                            <div class="post-year heading">Channel Promotion</div>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                    <div class="colors-w post-body pure-u-1 pure-u-md-16-24 article-blog">-->
<!--                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">-->
<!--                            <div class="post-image push-bottom">-->
<!--                                <a href="#">-->
<!--                                    <img width="100" height="100" src="assets/data/services/smm/cross_chn.7dec2e439eebc95e.7dec2e439eebc95e.e09ca677987a7d9c.png" class="img-responsive" alt="Social Consultancy icon"/>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                            <div class="copy">-->
<!--                                <p class="excerpt">Using the approaching from our reporting and analysis we make sure that your message and brand reach your target audience and fits across all your social marketing channels, giving you a logical and right away recognizable voice through the internet.</p>-->
<!--                            </div>-->
<!--                        </article>-->
<!--                    </div>-->
<!--                </div>-->

            </div>
        </div>

    </div>
</section>