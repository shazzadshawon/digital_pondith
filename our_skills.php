<div class="view x40-widget widget   text-bg" id="layers-widget-skrollex-section-14" data-text-effect-selector="h1,h2,h3,h4" data-text-effect="effect-a-animated">
    <div data-src="http://skrollex2.x40.ru/mary/wp-content/uploads/sites/42/2015/11/bg-stocksnap-219FB68281.jpg" data-alt="" class="bg-holder"></div>
    <div data-src="http://skrollex2.x40.ru/mary/wp-content/uploads/sites/42/2015/11/bg-stocksnap-219FB68281.jpg" data-alt="" class="bg-holder"></div>
    <div id="skills" class="fg colors-c ">
        <div class="layout-boxed section-top"><h3 class="heading-section-title">Our Skills</h3>
            <p class="header-details"><span>Our Main</span> Skills</p>
            <p class="lead">Digital Pondith provides a coherent approach to all your digital marketing media requirements.</p>
        </div>
        <div class="section-cols layout-boxed">
            <div class="pure-g">
                <div class="layers-widget-skrollex-section-55d518df8751f716032540 pure-u-1 pure-u-lg-12-24  col-padding">
                    <p class="col-details"><span>Where we prepare all marketing plan which we have done and perform. In this step, we will find out what you really need and what we do.</span><span></span>
                    </p>
                    <p>It is now a proven fact that to survive the competitive landscape, companies have to invest in a significant proportion of their time in digital marketing services. It is precisely for this reason that service providers like the SEO company India are so much in demand.</p>
                </div>
                <div class="layers-widget-skrollex-section-55d51c26899a5000556162 pure-u-1 pure-u-lg-12-24  col-padding">
                    <div class="skillbar clearfix background-h player" data-percent="60%">
                        <div class="skillbar-title background-i heading-i"><span
                                class="skillbar-label">Writing &amp; Editing</span></div>
                        <div class="skillbar-bar background-i"></div>
                        <div class="skill-bar-percent heading-h">60%</div>
                    </div>
                    <div class="skillbar clearfix background-h player" data-percent="95%">
                        <div class="skillbar-title background-j heading-j"><span
                                class="skillbar-label">SEO</span></div>
                        <div class="skillbar-bar background-j"></div>
                        <div class="skill-bar-percent heading-h">95%</div>
                    </div>
                    <div class="skillbar clearfix background-h player" data-percent="70%">
                        <div class="skillbar-title background-i heading-i"><span
                                class="skillbar-label">Design Skills</span></div>
                        <div class="skillbar-bar background-i"></div>
                        <div class="skill-bar-percent heading-h">70%</div>
                    </div>
                    <div class="skillbar clearfix background-h player" data-percent="80%">
                        <div class="skillbar-title background-j heading-j"><span
                                class="skillbar-label">Social Paid Advertising</span></div>
                        <div class="skillbar-bar background-j"></div>
                        <div class="skill-bar-percent heading-h">80%</div>
                    </div>
                    <div class="skillbar clearfix background-h player" data-percent="60%">
                        <div class="skillbar-title background-i heading-i"><span class="skillbar-label">Video Content Marketing</span>
                        </div>
                        <div class="skillbar-bar background-i"></div>
                        <div class="skill-bar-percent heading-h">60%</div>
                    </div>
                    <div class="skillbar clearfix background-h player" data-percent="78%">
                        <div class="skillbar-title background-j heading-j"><span
                                class="skillbar-label">Community Management</span></div>
                        <div class="skillbar-bar background-j"></div>
                        <div class="skill-bar-percent heading-h">78%</div>
                    </div>
                    <div class="skillbar clearfix background-h player" data-percent="60%">
                        <div class="skillbar-title background-i heading-i"><span class="skillbar-label">Top-Notch Personal Brand</span>
                        </div>
                        <div class="skillbar-bar background-i"></div>
                        <div class="skill-bar-percent heading-h">60%</div>
                    </div>

                    <div class="skillbar clearfix background-h player" data-percent="90%">
                        <div class="skillbar-title background-i heading-i"><span class="skillbar-label">Social Media Marketing</span>
                        </div>
                        <div class="skillbar-bar background-i"></div>
                        <div class="skill-bar-percent heading-h">90%</div>
                    </div>
                    <div class="skillbar clearfix background-h player" data-percent="70%">
                        <div class="skillbar-title background-i heading-i"><span class="skillbar-label">Email Marketing &amp; Newsletters</span>
                        </div>
                        <div class="skillbar-bar background-i"></div>
                        <div class="skill-bar-percent heading-h">70%</div>
                    </div>
                    <div class="skillbar clearfix background-h player" data-percent="70%">
                        <div class="skillbar-title background-i heading-i"><span class="skillbar-label">Google Adwords &amp; PPC</span>
                        </div>
                        <div class="skillbar-bar background-i"></div>
                        <div class="skill-bar-percent heading-h">70%</div>
                    </div>
                    <div class="skillbar clearfix background-h player" data-percent="50%">
                        <div class="skillbar-title background-i heading-i"><span class="skillbar-label">Conversion Rate Optimization (CRO)</span>
                        </div>
                        <div class="skillbar-bar background-i"></div>
                        <div class="skill-bar-percent heading-h">50%</div>
                    </div>
                    <div class="skillbar clearfix background-h player" data-percent="91%">
                        <div class="skillbar-title background-i heading-i"><span class="skillbar-label">Marketing Automation</span>
                        </div>
                        <div class="skillbar-bar background-i"></div>
                        <div class="skill-bar-percent heading-h">91%</div>
                    </div>
                    <div class="skillbar clearfix background-h player" data-percent="60%">
                        <div class="skillbar-title background-i heading-i"><span class="skillbar-label">Influencer Marketing</span>
                        </div>
                        <div class="skillbar-bar background-i"></div>
                        <div class="skill-bar-percent heading-h">60%</div>
                    </div>
                    <div class="skillbar clearfix background-h player" data-percent="90%">
                        <div class="skillbar-title background-i heading-i"><span class="skillbar-label">HTML &amp; CSS</span>
                        </div>
                        <div class="skillbar-bar background-i"></div>
                        <div class="skill-bar-percent heading-h">90%</div>
                    </div>
                    <div class="skillbar clearfix background-h player" data-percent="70%">
                        <div class="skillbar-title background-i heading-i"><span class="skillbar-label">Creative &amp; Analytical Abilities</span>
                        </div>
                        <div class="skillbar-bar background-i"></div>
                        <div class="skill-bar-percent heading-h">70%</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>