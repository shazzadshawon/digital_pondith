<?php error_reporting(0); ?>
<?php
$mail_flag = 2;

if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['message']) ){
    if ( !empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['message']) ){
        $user_name = $_POST['name'];
        $user_email = $_POST['email'];
        $user_message = $_POST['message'];
        $contact_addr = 'sofwaresolutions@agvcorp.biz';
//        $contact_addr = 'contact@kababiabd.com';

        $subject = 'Contact Page Mail from '.$user_email;
//        $message = $user_message.'<br>'.'regards'.'<br>'.$user_name;
        $message_body = "This is a mail from AGV website Contact-us page. <hr>".
                        "<br/>".
                        "<br/>".
                        $user_message.
                        "<br>".
                        "<hr>".
                        "Name: ".$user_name."<br>".
                        "Email: ".$user_email."<br/>";

        $message = '
                    <style>
                        body {
                            padding: 0;
                            margin: 0;
                        }
                    
                        html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
                        @media only screen and (max-device-width: 680px), only screen and (max-width: 680px) {
                            *[class="table_width_100"] {
                                width: 96% !important;
                            }
                            *[class="border-right_mob"] {
                                border-right: 1px solid #dddddd;
                            }
                            *[class="mob_100"] {
                                width: 100% !important;
                            }
                            *[class="mob_center"] {
                                text-align: center !important;
                            }
                            *[class="mob_center_bl"] {
                                float: none !important;
                                display: block !important;
                                margin: 0px auto;
                            }
                            .iage_footer a {
                                text-decoration: none;
                                color: #929ca8;
                            }
                            img.mob_display_none {
                                width: 0px !important;
                                height: 0px !important;
                                display: none !important;
                            }
                            img.mob_width_50 {
                                width: 40% !important;
                                height: auto !important;
                            }
                        }
                        .table_width_100 {
                            width: 680px;
                        }
                    </style>
                    <div id="mailsub" class="notification" align="center">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">
                            <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
                                <tr><td>                
                                </td></tr>
                                <tr><td align="center" bgcolor="#ffffff">
                                    <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                        <tr><td align="center">
                                            <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; float:left; width:100%; padding:20px;text-align:center; font-size: 13px;">
                                                <font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
                                                    <img src="http://www.basis.org.bd/logo/a3ff7c89595fa36153d8ae1c8ab071a1.png" width="250" alt="Metronic" border="0"  /></font></a>
                                        </td>
                                            <td align="right">
                                            </td></tr>
                                        <tr><td align="center" bgcolor="#fbfcfd">
                                            <font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 15px;">
                                                <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr><td>
                                                        '.$message_body.'
                                                    </td></tr>
                                                </table>
                                            </font>
                                        </td></tr>
                                        <tr><td class="iage_footer" align="center" bgcolor="#ffffff">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr><td align="center" style="padding:20px;flaot:left;width:100%; text-align:center;">
                                                    <font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
                                    <span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
                                        2017 - 2020 © AGV. ALL Rights Reserved.
                                    </span></font>
                                                </td></tr>
                                            </table>
                                        </td></tr>
                                        <tr><td>
                                        </td></tr>
                                    </table>
                                </td></tr>
                            </table>';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        $headers .= $user_email. "\r\n";

        if ( mail($contact_addr, $subject, $message, $headers) ){
            $mail_flag = 1;
        } else {
            $mail_flag = 0;
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en-US" class="state2 page-is-gated scroll-bar site-decoration-b" data-skrollex-config="{isInitColorPanel: false, isCustomizer: false, adminUrl: &#039;http://skrollex2.x40.ru/mary/wp-admin/&#039;, ajaxUrl: &#039;http://skrollex2.x40.ru/mary/wp-admin/admin-ajax.php&#039;, homeUri: &#039;http://skrollex2.x40.ru/mary&#039;, themeUri: &#039;http://skrollex2.x40.ru/mary/&#039;, permalink: &#039;http://skrollex2.x40.ru/mary&#039;, colors: &#039;colors-preset-mary.css&#039;}">
<!--HEAD-->
<head>
    <title>Contact &#8211; Digital Pondith</title>
    <?php require('head.php'); ?>
    <!--ADDITIONAL STYLES-->
    <link rel="stylesheet" href="assets/css/service.9300b0c83579906f.9300b0c83579906f.9300b0c83579906f.css?v=2.0" type="text/css" media="screen" />
    <!--//ADDITIONAL STYLES-->
</head>
<!--//HEAD-->

<body id="skrollex-body" class="blog no-colors-label background-k body-header-logo-left">
<!--    PRELOADER    -->
<?php require('preloader.php');?>
<!--    //PRELOADER    -->

<div class="page-border  heading top colors-a main-navigation"></div>
<div class="page-border  heading bottom colors-a main-navigation"><a href="#top" class="to-top hover-effect">To <span>Top</span></a><a href="#scroll-down" class="scroll-down hover-effect">Scroll <span>Down</span></a></div>
<div class="page-border  heading left colors-a main-navigation border-pad"></div>
<div class="page-border  heading right colors-a main-navigation border-pad"></div>
<div class="page-border  heading left colors-a main-navigation">
    <!--Side Border Social Links-->
    <?php include('side_border_socialLink.php'); ?>
    <!--Side Border Social Links-->
</div>
<div class="page-border  heading right colors-a main-navigation">
    <ul>
        <li><a href="#contact"><i class="fa fa-info-circle" aria-hidden="true"></i></a></li>
    </ul>
</div>

<!--    TOP HEADER-->
<?php include('top_header.php'); ?>

<!--    RIGHT SIDE DOT NAVIGATOR-->
<?php include('top_menu_mobile.php'); ?>
<section class="wrapper-site">

    <!--        MAIN MENU SECTION-->
    <?php include('main_menu.php'); ?>

    <section id="wrapper-content" class="wrapper-content">

        <img class="bg" src="assets/images/contact_banner.0da83898aa60585c.0da83898aa60585c.581d0a6806446d2f.jpg" alt=""/>
        <img class="bg" src="assets/images/contact_banner.0da83898aa60585c.0da83898aa60585c.581d0a6806446d2f.jpg" alt=""/>
        <div class="default-page-wrapper background-v">

        </div>
    </section>

    <!--CONTACT US-->
    <div class="view x40-widget widget  " id="layers-widget-skrollex-section-15">
        <div id="contact" class="fg colors-d  full-size">
            <div class="layout-boxed section-top"><h3 class="heading-section-title">Keep In Touch</h3>
                <p class="header-details"> <h1 style="color: #d29934; text-align: center">HELLO@DIGITALPONDITH.COM </h1> </p>
            </div>
            <div class="section-cols layout-boxed">
                <div class="pure-g">
                    <div class="layers-widget-skrollex-section-55e76a38d8c72733306009 pure-u-1 pure-u-lg-12-24  col-padding scroll-in-animation"
                         data-animation="fadeInLeft">
                        <p class="text-big text-right">Mobile.: <span>01678504912 / 3 / 4</span></p>
                        <p class="text-big text-right">Tel.: <span>+88-029850613 / 4</span></p>
                        <p class="text-big text-right">HOUSE - 3, ROAD - 28, BLOCK - K <br/>
                            BANANI, DHAKA-1213, BANGLADESH</p>
                        <p class="text-right"><a href="mailto:info@ouraddress.com">info@digitalpondith.com</a><br/>
                            <a href="http://www.digitalpondith.com/">www.digitalpondith.com</a></p>
                        <p class="text-right"><a href="https://www.facebook.com/" target="_blank"><i
                                        class="fa fa-2x fa-facebook"></i></a> <a href="https://twitter.com/"><i
                                        class="fa fa-2x fa-twitter"></i></a> <a href="https://soundcloud.com/"
                                                                                target="_blank"><i
                                        class="fa fa-2x fa-soundcloud"></i></a></p>

                        <?php
                        if ($mail_flag == 1){
                            echo "<div class='wpcf7-response-output text-big text-right' style='color: green; font-weight: 700;'>Your Mail Successfully Sent to hello@digitalpondith.com</div>";
                        } else if ($mail_flag == 2){
                            echo "<div class='wpcf7-response-output text-big text-right'></div>";
                        } else if ($mail_flag == 0){
                            echo "<div class='wpcf7-response-output text-big text-right' style='color: red; font-weight: 700;'>Server Overloaded! Message sending failed.</div>";
                        }
                        ?>
                    </div>
                    <div class="layers-widget-skrollex-section-55e78dea6c13d201303792 pure-u-1 pure-u-lg-12-24  col-padding scroll-in-animation" data-animation="fadeInRight">
                        <div role="form">
                            <div class="screen-reader-response"></div>
                            <form action="contact_us.php" method="POST" class="wpcf7-form">
                                <div class="pure-g contact-form-content">
                                    <div class="pure-u-1 pure-u-sm-12-24">
                                                <span>
                                                    <input type="text" name="contact-name" size="40" aria-required="true" aria-invalid="false" placeholder="Name"/>
                                                </span>
                                    </div>
                                    <div class="pure-u-1 pure-u-sm-12-24">
                                                <span>
                                                    <input type="email" name="contact-email" size="40" aria-required="true" aria-invalid="false" placeholder="Email"/>
                                                </span>
                                    </div>
                                    <div class="pure-u-1">
                                                <span>
                                                    <textarea name="contact-message" cols="40" rows="10" aria-invalid="false" placeholder="Message"></textarea>
                                                </span>
                                    </div>
                                    <div class="pure-u-1">
                                        <input type="submit" value="Send"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55e8d6e813a15761004876 pure-u-1 pure-u-lg-24-24  col-padding">
                        <p class="text-center">
                        <span class="google-map-button">
                            <a class="map-open button background-g border-normal-g heading-g" data-map-overlay=".map-overlay" href="#">Locate Us On Map</a>
                            <span
                                    class="map-canvas" data-latitude="23.7944022" data-longitude="90.4090476" data-zoom="17">
                                <span class="map-marker" data-latitude="23.7944022" data-longitude="90.4090476" data-text="Digital Pondith"></span>
                                <!--                                <span class="map-marker" data-latitude="23.794402" data-longitude="90.4068593" data-text="Our sales office"></span>-->
                            </span>
                        </span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3ZisuuEiULh72jtyAv5TBfEnt0HMnm0s" type="text/javascript"></script>
    <?php require('javacsript.php'); ?>
    <!--//CONTACT US-->

    <!--FOOTER-->
    <?php include('footer.php'); ?>
    <!--//FOOTER-->


</section>

</body>
</html>
