<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css?v=2.0" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<section id="web_pricing" class="layout-boxed archive">
    <div class="pure-g">
        <div class="pure-u-1 pure-u-md-24-24">
            <div class="view x40-widget widget   text-bg peraRemoval" id="layers-widget-skrollex-section-9" data-text-effect-selector="h1,h2,h3" data-text-effect="effect-a-animated">

                <div class="pure-g">
                    <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">

                            <h1 class="post-title">
                                <a href="#"><span>SEO</span> Marketing Packages</a>
                            </h1>
                            <div class="copy">
                                <p class="excerpt">kindly select Search Engine Optimization Package from below mention packages which meet your requirements.</p>
                            </div>
                        </article>
                    </div>
                </div>

                <div class="pure-g">

                    <div class="post-meta pure-u-1 pure-u-md-6-24 text-center colors-v" style="background: rgba(223, 245, 255, 0.82) !important;">
                        <section class="accordion">
                            <h1>BASIC</h1>
                            <h6>$199/month</h6>
                            <p>15 Keywords Optimization</p>
                            <div class="item">
                                <h4>INITIAL REVIEW &amp; ANALYSIS</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    Website Analysis <hr>
                                    Content Duplicacy Check <hr>
                                    Initial Backlinks analysis <hr>
                                    Google Penalty Check <hr>
                                    Competition Analysis <hr>
                                    Keyword Research
                                </div>
                            </span>

                            <div class="item">
                                <h4>ON PAGE OPTIMIZATION</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    Onpage Changes Upto 15 Pages<hr>
                                    Title Tags Optimization <hr>
                                    META Tags Optimization <hr>
                                    Content Optimization <hr>
                                    HTML Code Optimization <hr>
                                    Website Responsive Check* <hr>
                                    Website Permalinks Analysis* <hr>
                                    Schema Markup Analysis* <hr>
                                    Page Speed Analysis* <hr>
                                    Internal Linking Optimization <hr>
                                    Heading Tags Optimization <hr>
                                    Canonicalization/301 Redirect <hr>
                                    Images Optimization <hr>
                                    Hyperlink Optimization
                                </div>
                            </span>

                            <div class="item">
                                <h4>CONTENT MARKETING &amp; LINK BUILDING</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    Blog Writing (500-1000 Words) OR Web Pages <hr>
                                    Guest Blogging <hr>
                                    Custom graphics and/or illustrations to support your blogs <hr>
                                    Contextual links <hr>
                                    Keyword used in anchor text <hr>
                                    Add SEO data to each post (Meta tags etc.) <hr>
                                    Share each blog post across social media networks <hr>
                                    Search Engine Submissions <hr>
                                    Blogs Social Bookmarking Links <hr>
                                    Blogs Social Bookmarking Links <hr>
                                    Classified Submissions
                                </div>
                            </span>

                            <div class="item">
                                <h4>LOCAL OPTIMIZATION</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    Location Optimization <hr>
                                    NAP Syndication <hr>
                                    Google Business Page Creation <hr>
                                    Bing Local Listing Creation <hr>
                                    Local Business Listings
                                </div>
                            </span>

                            <div class="item">
                                <h4>VIDEO MARKETING</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    Youtube Account Setup <hr>
                                    Vimeo Account Setup <hr>
                                    Dailymotion Account Setup
                                </div>
                            </span>

                        </section>
                    </div>


                    <div class="post-meta pure-u-1 pure-u-md-6-24 text-center colors-v" style="background: rgba(217, 218, 255, 0.76) !important;">
                        <section class="accordion">

                            <h1>STANDARD</h1>
                            <h6>$399/month</h6>
                            <p>25 Keywords Optimization</p>

                            <div class="item">
                                <h4>INITIAL REVIEW &amp; ANALYSIS</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    Website Analysis <hr>
                                    Content Duplicacy Check <hr>
                                    Initial Backlinks analysis <hr>
                                    Google Penalty Check <hr>
                                    Competition Analysis <hr>
                                    Keyword Research
                                </div>
                            </span>

                            <div class="item">
                                <h4>ON PAGE OPTIMIZATION</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    Onpage Changes Upto 25 Pages <hr>
                                    Title Tags Optimization <hr>
                                    META Tags Optimization <hr>
                                    Content Optimization <hr>
                                    HTML Code Optimization <hr>
                                    Website Responsive Check* <hr>
                                    Website Permalinks Analysis* <hr>
                                    Schema Markup Analysis* <hr>
                                    Page Speed Analysis* <hr>
                                    Internal Linking Optimization <hr>
                                    Heading Tags Optimization <hr>
                                    Canonicalization/301 Redirect <hr>
                                    Images Optimization <hr>
                                    Hyperlink Optimization <hr>
                                    Robots.txt Creation/Optimization <hr>
                                    Sitemap Creation <hr>
                                    Google Webmaster Tools Setup <hr>
                                    Bing Webmaster Tools Setup <hr>
                                    Google Analytics Setup &amp; Integration
                                </div>
                            </span>

                            <div class="item">
                                <h4>CONTENT MARKETING &amp; LINK BUILDING</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    3 Blog Writing (500-1,000 Words) OR Web Pages <hr>
                                    3 Guest Blogging <hr>
                                    Custom graphics and/or illustrations to support your blogs <hr>
                                    Contextual links <hr>
                                    Keyword used in anchor text <hr>
                                    Add SEO data to each post (Meta tags etc.) <hr>
                                    Share each blog post across social media networks <hr>
                                    25 Search Engine Submissions <hr>
                                    18 Blogs Social Bookmarking Links <hr>
                                    18 Social Bookmarking Links <hr>
                                    15 Classified Submissions
                                </div>
                            </span>

                            <div class="item">
                                <h4>LOCAL OPTIMIZATION</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    2 Location optimization <hr>
                                    NAP Syndication <hr>
                                    Google Business Page Creation <hr>
                                    Bing Local Listing Creation <hr>
                                    5 Local Business Listings
                                </div>
                            </span>

                        </section>
                    </div>



                    <div class="post-meta pure-u-1 pure-u-md-6-24 text-center colors-v" style="background: rgba(247, 184, 159, 0.69) !important;">
                        <section class="accordion">

                            <h1>PROFESSIONAL</h1>
                            <h6>$699/month</h6>
                            <p>40 Keywords Optimization</p>

                            <div class="item">
                                <h4>INITIAL REVIEW &amp; ANALYSIS</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    Website Analysis <hr>
                                    Content Duplicacy Check <hr>
                                    Initial Backlinks analysis <hr>
                                    Google Penalty Check <hr>
                                    Competition Analysis <hr>
                                    Keyword Research
                                </div>
                            </span>

                            <div class="item">
                                <h4>ON PAGE OPTIMIZATION</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    Onpage Changes Upto 40 Pages <hr>
                                    Title Tags Optimization <hr>
                                    META Tags Optimization <hr>
                                    Content Optimization <hr>
                                    HTML Code Optimization <hr>
                                    Website Responsive Check* <hr>
                                    Website Permalinks Analysis* <hr>
                                    Schema Markup Analysis* <hr>
                                    Page Speed Analysis* <hr>
                                    Internal Linking Optimization <hr>
                                    Heading Tags Optimization <hr>
                                    Canonicalization/301 Redirect <hr>
                                    Images Optimization <hr>
                                    Hyperlink Optimization <hr>
                                    Robots.txt Creation/Optimization <hr>
                                    Sitemap Creation <hr>
                                    Google Webmaster Tools Setup <hr>
                                    Bing Webmaster Tools Setup <hr>
                                    Google Analytics Setup & Integration
                                </div>
                            </span>

                            <div class="item">
                                <h4>CONTENT MARKETING &amp; LINK BUILDING</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    5 Blog Writing (500-1,000 Words) OR Web Pages <hr>
                                    3 Guest Blogging <hr>
                                    2 Blog Posts on Client's Website <hr>
                                    Custom graphics and/or illustrations to support your blogs <hr>
                                    Contextual links <hr>
                                    Keyword used in anchor text <hr>
                                    Add SEO data to each post (Meta tags etc.) <hr>
                                    Share each blog post across social media networks <hr>
                                    Infographics Creation 1 in Every 6th Month <hr>
                                    Share each Infographic on social media networks <hr>
                                    40 Search Engine Submissions <hr>
                                    25 Blogs Social Bookmarking Links <hr>
                                    25 Social Bookmarking Links <hr>
                                    20 Classified Submissions
                                </div>
                            </span>

                            <div class="item">
                                <h4>LOCAL OPTIMIZATION</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    4 Location optimization <hr>
                                    NAP Syndication <hr>
                                    Google Business Page Creation <hr>
                                    Bing Local Listing Creation <hr>
                                    5 Local Business Listings
                                </div>
                            </span>

                            <div class="item">
                                <h4>VIDEO MARKETING</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    Youtube Account Setup <hr>
                                    Vimeo Account Setup <hr>
                                    Dailymotion Account Setup <hr>
                                    Video / PPT Creation 1 in Every 6th Month <hr>
                                    Video / PPT Distribution <hr>
                                    Share Videos on Social Media Networks
                                </div>
                            </span>

                        </section>
                    </div>

                    <div class="post-meta pure-u-1 pure-u-md-6-24 text-center colors-v" style="background: rgba(202, 202, 202, 0.38) !important;">
                        <section class="accordion">

                            <h1>ENTERPRISE</h1>
                            <h6>$999/month</h6>
                            <p>100 Keywords Optimization</p>

                            <div class="item">
                                <h4>INITIAL REVIEW &amp;amp; ANALYSIS</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    Website Analysis <hr>
                                    Content Duplicacy Check <hr>
                                    Initial Backlinks analysis <hr>
                                    Google Penalty Check <hr>
                                    Competition Analysis <hr>
                                    Keyword Research
                                </div>
                            </span>

                            <div class="item">
                                <h4>ON PAGE OPTIMIZATION</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    Onpage Changes Upto 100 Pages <hr>
                                    Title Tags Optimization <hr>
                                    META Tags create and Optimization <hr>
                                    Content Optimization <hr>
                                    HTML Code Optimization <hr>
                                    Website Responsive Check* <hr>
                                    Website Permalinks Analysis* <hr>
                                    Schema Markup Analysis* <hr>
                                    Page Speed Analysis* <hr>
                                    Internal Linking Optimization <hr>
                                    Heading Tags Optimization <hr>
                                    Canonicalization/301 Redirect <hr>
                                    Images Optimization <hr>
                                    Hyperlink Optimization <hr>
                                    Robots.txt Creation/Optimization <hr>
                                    Sitemap Creation <hr>
                                    Google Webmaster Tools Setup <hr>
                                    Bing Webmaster Tools Setup <hr>
                                    Google Analytics Setup &amp; Integration
                                </div>
                            </span>

                            <div class="item">
                                <h4>CONTENT MARKETING &amp; LINK BUILDING</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    10 Blog Writing (500-1,000 Words) OR Web Pages <hr>
                                    8 Guest Blogging <hr>
                                    4 Blog Posts on Client's Website <hr>
                                    Custom graphics and/or illustrations to support your blogs <hr>
                                    Contextual links <hr>
                                    Keyword used in anchor text <hr>
                                    Add SEO data to each post (Meta tags etc.) <hr>
                                    Share each blog post across social media networks <hr>
                                    Infographics Creation 1 in Every 3rd Month <hr>
                                    Share each Infographic on social media networks <hr>
                                    Premium Press Release 1 in Every 6th Month <hr>
                                    100 Search Engine Submissions <hr>
                                    35 Blogs Social Bookmarking Links <hr>
                                    35 Social Bookmarking Links <hr>
                                    30 Classified Submissions
                                </div>
                            </span>

                            <div class="item">
                                <h4>LOCAL OPTIMIZATION</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    8 Location optimization <hr>
                                    NAP Syndication <hr>
                                    Google Business Page Creation <hr>
                                    Bing Local Listing Creation <hr>
                                    10 Local Business Listings
                                </div>
                            </span>

                            <div class="item">
                                <h4>VIDEO MARKETING</h4>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    Youtube Account Setup <hr>
                                    Vimeo Account Setup <hr>
                                    Dailymotion Account Setup <hr>
                                    Video / PPT Creation 1 in Every 3rd Month <hr>
                                    Video / PPT Distribution <hr>
                                    Share Videos on Social Media Networks <hr>
                                </div>
                            </span>

                        </section>
                    </div>

                </div>


                <div class="pure-g" style="margin: 0 auto !important;">

                </div>

            </div>
        </div>

    </div>
</section>

<script type="text/javascript">
    jQuery(document).ready(function(){
        'use strict';
        jQuery('.accordion .item').on("click", function () {
            jQuery(this).next().slideToggle(100);
//            jQuery('p').not(jQuery(this).next()).slideUp('fast');
            jQuery('.details').not(jQuery(this).next()).slideUp('fast');
        });
    });
</script>