<ul id="dot-scroll" class="colors-a no-colors-label"></ul>
<div class="overlay-window gallery-overlay colors-q" data-overlay-zoom=".fg">
    <div class="overlay-control">
        <a class="previos" href="#"></a>
        <a class="next" href="#"></a>
        <a class="cross" href="#"></a>
    </div>
    <div class="overlay-view scroll-bar">
        <div class="layout-boxed overlay-content">
        </div>
    </div>
    <ul class="loader">
        <li class="background-highlight"></li>
        <li class="background-highlight"></li>
        <li class="background-highlight"></li>
    </ul>
</div>
<div class="overlay-window map-overlay colors-q">
    <div class="overlay-control">
        <a class="cross" href="#"></a>
    </div>
    <div class="overlay-view scroll-bar">
    </div>
</div>
<div class="wrapper invert off-canvas-right scroll-bar colors-k" id="off-canvas-right">
    <a class="close-canvas" data-toggle="#off-canvas-right" data-toggle-class="open">
        <i class="l-close"></i>
        Close
    </a>
    <div class="content-box nav-mobile page-transition">
        <nav class="nav nav-vertical">
            <ul id="menu-skrollex-menu-2" class="menu">
                <li id="menu-item-796" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-796"><a href="index.php">Home</a></li>
<!--                <li id="menu-item-797" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-797"><a href="about_us_page.php">About</a></li>-->
                <li id="menu-item-806" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-806">
                    <a href="index.php#solutions">Solutions</a>
                    <ul class="sub-menu">
                        <li id="menu-item-804" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-804">
                            <a href="solution_categoryPage.php">Solutions Category</a>
                            <ul>
                                <li><a href="solution_details_page.php">Solutions Name</a></li>
                                <li><a href="solution_details_page.php">Solutions Name</a></li>
                                <li><a href="solution_details_page.php">Solutions Name</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-805" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-805">
                            <a href="solution_categoryPage.php">Solutions Category</a>
                            <ul>
                                <li><a href="solution_details_page.php">Solutions Name</a></li>
                                <li><a href="solution_details_page.php">Solutions Name</a></li>
                                <li><a href="solution_details_page.php">Solutions Name</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-802" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-802">
                            <a href="solution_categoryPage.php">Solutions Category</a>
                            <ul>
                                <li><a href="solution_details_page.php">Solutions Name</a></li>
                                <li><a href="solution_details_page.php">Solutions Name</a></li>
                                <li><a href="solution_details_page.php">Solutions Name</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li id="menu-item-807" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-807">
                    <a href="index.php#services">SERVICES</a>
                    <ul class="sub-menu">
                        <li id="menu-item-804" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-804">
                            <a href="service_category_page.php">Search Engine optimisation</a>
                            <ul>
                                <li><a href="service_details_page.php">Service Name</a></li>
                                <li><a href="service_details_page.php">Service Name</a></li>
                                <li><a href="service_details_page.php">Service Name</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-805" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-805">
                            <a href="service_category_page.php">Search engine Marketing</a>
                            <ul>
                                <li><a href="service_details_page.php">Service Name</a></li>
                                <li><a href="service_details_page.php">Service Name</a></li>
                                <li><a href="service_details_page.php">Service Name</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-802" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-802">
                            <a href="service_category_page.php">Social Media Marketing</a>
                            <ul>
                                <li><a href="service_details_page.php">Facebook marketing</a></li>
                                <li><a href="service_details_page.php">Twitter Marketing</a></li>
                                <li><a href="service_details_page.php">Printerest Marketing</a></li>
                                <li><a href="service_details_page.php">LinkedIn Marketing</a></li>
                            </ul>
                        </li>

                        <li id="menu-item-802" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-802"><a href="service_category_page.php">Email Marketing</a></li>
                        <li id="menu-item-802" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-802"><a href="service_category_page.php">SMS Marketing</a></li>
                        <li id="menu-item-802" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-802"><a href="service_category_page.php">Content Marketing</a></li>
                        <li id="menu-item-802" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-802"><a href="service_category_page.php">Video Marketing</a>
                            <ul>
                                <li><a href="service_details_page.php">Youtube Marketing</a></li>
                            </ul>
                        </li>

                    </ul>
                </li>

                <li id="menu-item-800" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-800"><a href="portfolio_page.php">Clients</a></li>

                <li id="menu-item-799" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-799"><a href="blog.php">Blog</a></li>

                <li id="menu-item-803" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-803"><a href="letsDiscuss.php">Let's Discuss</a></li>

                <li id="menu-item-803" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-803"><a href="contact_us.php">Contact</a></li>
            </ul>
        </nav>
    </div>
</div>