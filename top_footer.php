<style type="text/css">
    .topFooterCon{
        background: #d89825;
        /*background: #90fffa !important;*/
        padding: 0 !important;
        margin: 0 !important;
    }

    .topFooterinner{
        padding: 5% !important;
        margin: 0 !important;
    }
    .background-digital{
        background-color: #906925 !important;
        /*background-color: #4f9994 !important;*/
    }
    .countlike{
        font-size: 45px;
        line-height: 78px;
        font-weight: 600;
    }
    .fontSize{
        font-size: 100%;
    }
</style>

<div class="view x40-widget widget  " id="layers-widget-skrollex-section-13">
    <div id="numbers" class="fg colors-d topFooterCon">
        <div class="section-cols layout-fullwidth">
            <div class="pure-g">
                <div class="layers-widget-skrollex-section-55d50056042d8357324106 pure-u-12-24 pure-u-md-8-24  ">
                    <div class="counter background-f heading-f topFooterinner" style="height: 100%;">
                        <div class="countlike player background-10-light">
                            <i class="fas fa-mobile"></i>
                        </div>
                        <div class="caption heading-g" style="font-size: 45px;">01741228971</div>
                    </div>
                </div>
                <div class="layers-widget-skrollex-section-55d504c904a53344045984 pure-u-12-24 pure-u-md-8-24  ">
                    <div class="counter background-digital heading-g topFooterinner" style="height: 100%;">
                        <a href="mailto:contact@agvcorp.com" target="_top" >
                            <div class="countlike player background-10-light">
                                <i class="fas fa-envelope-square"></i>
                            </div>
                        </a>
                        <div class="caption heading-g fontSize">contact@agvcorp.com</div>
                    </div>
                </div>
                <div class="layers-widget-skrollex-section-55d504ce404de389549092 pure-u-12-24 pure-u-md-8-24  ">
                    <div class="counter background-f heading-f topFooterinner" style="height: 100%;">
                        <a href="contact_us.php">
                            <div class="countlike player background-10-light">
                                <i class="fas fa-map-marker-alt"></i>
                            </div>
                        </a>
                        <div class="caption heading-g fontSize">Get In Touch</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>