<!DOCTYPE html>
<html lang="en-US" class="state2 page-is-gated scroll-bar site-decoration-b" data-skrollex-config="{isInitColorPanel: false, isCustomizer: false, adminUrl: &#039;http://digitalpondith.com/&#039;, ajaxUrl: &#039;http://digitalpondith.com/&#039;, homeUri: &#039;http://digitalpondith.com/&#039;, themeUri: &#039;http://digitalpondith.com/&#039;, permalink: &#039;http://digitalpondith.com/&#039;, colors: &#039;colors-preset-mary.css&#039;}">

<!-- HEAD -->
<head>
    <?php require_once('head.php'); ?>
    <title>Blog|Digital Pondith</title>
    <!--ADDITIONAL STYLES-->
    <link rel="stylesheet" href="assets/css/service.9300b0c83579906f.9300b0c83579906f.9300b0c83579906f.css?v=2.0" type="text/css" media="screen" />
    <!--//ADDITIONAL STYLES-->
</head>
<!-- //HEAD -->

<body id="skrollex-body" class="blog no-colors-label background-k body-header-logo-left">

<!--    PRELOADER    -->
<?php require('preloader.php'); ?>
<!--    //PRELOADER    -->

<div class="page-border  heading top colors-a main-navigation"></div>
<div class="page-border  heading bottom colors-a main-navigation"><a href="#top" class="to-top hover-effect">To <span>Top</span></a><a href="#scroll-down" class="scroll-down hover-effect">Scroll<span>Down</span></a></div>
<div class="page-border  heading left colors-a main-navigation border-pad"></div>
<div class="page-border  heading right colors-a main-navigation border-pad"></div>
<div class="page-border  heading left colors-a main-navigation">
    <!--Side Border Social Links-->
    <?php include('side_border_socialLink.php'); ?>
    <!--Side Border Social Links-->
</div>
<div class="page-border  heading right colors-a main-navigation"></div>

<!--    TOP HEADER-->
<?php include('top_header.php'); ?>

<!--    RIGHT SIDE DOT NAVIGATOR-->
<?php include('top_menu_mobile.php'); ?>


<section class="wrapper-site">
    <!--MAIN MENU SECTION-->
    <?php include('main_menu.php'); ?>

    <section id="wrapper-content" class="wrapper-content">
        <img class="bg" src="assets/images/bg-picjumbo.com_IMG_7432.jpg" alt=""/>
        <img class="bg" src="assets/images/bg-picjumbo.com_IMG_7432.jpg" alt=""/>
        <div class="post-page-wrapper colors-v">
            <section id="post-242" class="content-blog clearfix post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music container">
                <div class="pure-g">
                    <article class="pure-u-1 pure-u-md-18-24">
                        <div class="pure-g">
                            <div class="post-meta pure-u-1 pure-u-md-6-24 text-right background-transparent colors-v">
                                <a href="blogDetailsPage.php">
                                    <div class="post-day heading"><span>23</span></div>
                                    <div class="post-year heading">Apr 2016</div>
                                </a>
                                <div class="post-author"><i class="fa fa-user"></i>
                                    <span>author name</span>
                                </div>
                                <div class="post-comments"><i class="fa fa-comments-o"></i>
                                    <a href="blogDetailsPage.php#comments">2 Comments</a>
                                </div>
                                <div class="post-categories"><i class="l-folder-open-o"></i>
                                    <a href="blog.php" title="View all posts in blog category">Blog category</a></div>
                                <div class="post-permalink"><i class="fa fa-link"></i>
                                    <a href="blogDetailsPage.php" class="page-transition">Permalink</a>
                                </div>
                            </div>
                            <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-post">
                                <div class="post-image push-bottom"><img width="660" height="337" src="assets/images/blog_demoGIF_2.gif" class="attachment-large size-large" alt=""/></div>
                                <h1 class="post-title">Blog <span>Title</span></h1>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
                                <p>Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit amet consectetur adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur?</p>
                                <p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>
                                <div id="comments" class="non-preloading">
                                    <h3 id="comments-title">2 Responses to <em>Post Name</em></h3>
                                    <ol class="commentlist">

                                        <?php include('blog_details_comment.php'); ?>
                                        <?php include('blog_details_comment.php'); ?>
                                        <?php include('blog_details_comment.php'); ?>
                                        <?php include('blog_details_comment.php'); ?>

                                    </ol>

                                    <?php include('blog_details_make_response.php'); ?>

                                </div>
                            </div>
                        </div>
                    </article>


                    <div class="colors-v background-transparent sidebar pure-u-1 pure-u-md-6-24">
                        <aside id="recent-posts-3" class="content well push-bottom-large widget widget_recent_entries"> <h5 class="section-nav-title">Recent Posts</h5> <ul>
                                <li>
                                    <a href="blogDetailsPage.php">Recent Post Title</a>
                                </li>
                                <li>
                                    <a href="blogDetailsPage.php">Recent Post Title</a>
                                </li>
                                <li>
                                    <a href="blogDetailsPage.php">Recent Post Title</a>
                                </li>
                                <li>
                                    <a href="blogDetailsPage.php">Recent Post Title</a>
                                </li>
                                <li>
                                    <a href="blogDetailsPage.php">Recent Post Title</a>
                                </li>
                            </ul>
                        </aside>

                        <aside id="search-3" class="content well push-bottom-large widget widget_search">
                            <form role="search" method="get" class="search-form" action="blog.php">
                                <label>
                                    <span class="screen-reader-text">Search for:</span>
                                    <input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s"/>
                                </label>
                                <input type="submit" class="search-submit" value="Search"/>
                            </form>
                        </aside>

                        <aside id="categories-3" class="content well push-bottom-large widget widget_categories"><h5 class="section-nav-title">Categories</h5> <ul>
                                <li class="cat-item cat-item-4"><a href="blog.php">Blog Category One</a></li>
                                <li class="cat-item cat-item-6"><a href="blog.php">Blog Category Two</a></li>
                                <li class="cat-item cat-item-5"><a href="blog.php">Blog Category Three</a></li>
                                <li class="cat-item cat-item-5"><a href="blog.php">Blog Category Four</a></li>
                                <li class="cat-item cat-item-5"><a href="blog.php">Blog Category Five</a></li>
                                <li class="cat-item cat-item-5"><a href="blog.php">Blog Category Six</a></li>
                            </ul>
                        </aside

                        <aside id="tag_cloud-1" class="content well push-bottom-large widget widget_tag_cloud"><h5 class="section-nav-title">Tags</h5><div class="tagcloud">
                                <a href='blog.php' class='tag-link-8 tag-link-position-1' title='4 topics' style='font-size: 22pt;'>Audio</a>
                                <a href='blog.php' class='tag-link-9 tag-link-position-2' title='2 topics' style='font-size: 8pt;'>Design</a>
                                <a href='blog.php' class='tag-link-13 tag-link-position-3' title='4 topics' style='font-size: 22pt;'>Life</a>
                                <a href='blog.php' class='tag-link-10 tag-link-position-4' title='2 topics' style='font-size: 8pt;'>Music</a>
                                <a href='blog.php' class='tag-link-11 tag-link-position-5' title='3 topics' style='font-size: 15.636363636364pt;'>Photo</a>
                                <a href='blog.php' class='tag-link-7 tag-link-position-6' title='4 topics' style='font-size: 22pt;'>Text</a>
                                <a href='blog.php' class='tag-link-12 tag-link-position-7' title='4 topics' style='font-size: 22pt;'>Video</a></div>
                        </aside>

                    </div>
                </div>
            </section>
        </div>
    </section>

    <!--FOOTER-->
    <?php include('footer.php'); ?>
</section>


<!--        <script type="application/javascript" src="assets/js/digitalPondith_down.min.411e408751f3b106.411e408751f3b106.411e408751f3b106.js?v=2.0"></script>-->
<?php require('javacsript.php'); ?>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpcf7 = {"recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}};
    /* ]]> */
</script>

</body>
</html>