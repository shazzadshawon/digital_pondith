<div class="view x40-widget widget background_blueDark" id="layers-widget-skrollex-section-10" style="min-height: 500px;">
    <div class="fg colors-d  half-size background_blueDark" style="box-sizing: border-box;">
        <div class="layout-boxed section-top">
            <div class="slogan text_black">
                <span class="textillate" data-textillate-options="{loop:true, in:{effect:'fadeInRight', reverse:true}, out:{effect:'fadeOutLeft', sequence:true}}">
                    <span class="texts">
                        <span>Your growth Strategy Together</span>
                        <span>Building Branding Brands For the Future</span>
                        <span>Content is the Atomic Particle of all Digital Marketing</span>
                        <span>Team Works Makes the Dream Works</span>
                        <span>Marketing is no longer about the stuff that you make, but about the stories you tell</span>
                        <span>It’s not what you upload; it’s the strategy with which you upload</span>
                    </span>
                </span>
            </div>
            <p class="text-center">— Digital|Pondith —</p>
        </div>
    </div>
</div>