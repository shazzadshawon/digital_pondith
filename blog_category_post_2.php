<div class="pure-g">
    <div class="post-meta pure-u-1 pure-u-md-6-24 text-right background-transparent colors-v">
        <a href="blog.php">
            <div class="post-day heading"><span>10th</span></div>
            <div class="post-year heading">April 2018</div>
        </a>
        <div class="post-author"><i class="fa fa-user"></i>
            <span>author name</span>
        </div>
        <div class="post-comments"><i class="fa fa-comments-o"></i>
            <a href="blogDetailsPage.php#comments">2Comments</a>
        </div>
        <div class="post-categories"><i class="l-folder-open-o"></i>
            <a href="blog.php" title="View all posts in Web Design">Blog category</a>
        </div>
        <div class="post-permalink"><i class="fa fa-link"></i>
            <a href="blogDetailsPage.php" class="page-transition">Permalink</a>
        </div>
    </div>
    <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">
            <div class="post-image push-bottom">
                <a class="slider" href="blogDetailsPage.php">
                    <img width="660" height="337" src="assets/images/blog_demoGIF_2.gif" class="attachment-large size-large" alt=""/>
                </a>
            </div>

            <h1 class="post-title"><a href="blogDetailsPage.php"><span>Blog Post </span>Title</a></h1>
            <div class="copy">
                <p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                    minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                    velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat. Ut enim ad
                    minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                    ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                    velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat [&hellip;]</p>
            </div>
            <p><a href="blogDetailsPage.php" class="page-transition post-read-more">Read More...</a></p>
            <p class="post-tags"><i class="l-tags"></i> <a class="page-transition" href="blog.php" title="View all posts tagged Design">tag1</a>,
                <a class="page-transition" href="blog.php" title="View all posts tagged Life">tag2</a>,
                <a class="page-transition" href="blog.php" title="View all posts tagged Music">tag3</a>
            </p></article>
    </div>
</div>