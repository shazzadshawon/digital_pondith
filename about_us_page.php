<!DOCTYPE html>
<html lang="en-US" class="state2 page-is-gated scroll-bar site-decoration-b" data-skrollex-config="{isInitColorPanel: false, isCustomizer: false, adminUrl: &#039;http://skrollex2.x40.ru/mary/wp-admin/&#039;, ajaxUrl: &#039;http://skrollex2.x40.ru/mary/wp-admin/admin-ajax.php&#039;, homeUri: &#039;http://skrollex2.x40.ru/mary&#039;, themeUri: &#039;http://skrollex2.x40.ru/mary/&#039;, permalink: &#039;http://skrollex2.x40.ru/mary&#039;, colors: &#039;colors-preset-mary.css&#039;}">
    <!--HEAD-->
    <head>
        <title>About Us &#8211; Digital Pondith</title>
        <?php require('head.php'); ?>
        <!--ADDITIONAL STYLES-->
        <link rel="stylesheet" href="assets/css/service.9300b0c83579906f.9300b0c83579906f.9300b0c83579906f.css?v=2.0" type="text/css" media="screen" />
        <!--//ADDITIONAL STYLES-->
    </head>
    <!--//HEAD-->

    <body id="skrollex-body" class="blog no-colors-label background-k body-header-logo-left">
        <!--    PRELOADER    -->
        <?php require('preloader.php');?>
        <!--    //PRELOADER    -->

        <div class="page-border  heading top colors-a main-navigation"></div>
        <div class="page-border  heading bottom colors-a main-navigation"><a href="#top" class="to-top hover-effect">To <span>Top</span></a><a href="#scroll-down" class="scroll-down hover-effect">Scroll <span>Down</span></a></div>
        <div class="page-border  heading left colors-a main-navigation border-pad"></div>
        <div class="page-border  heading right colors-a main-navigation border-pad"></div>
        <div class="page-border  heading left colors-a main-navigation">
            <!--Side Border Social Links-->
            <?php include('side_border_socialLink.php'); ?>
            <!--Side Border Social Links-->
        </div>
        <div class="page-border heading right colors-a main-navigation">
            <ul>
                <li><a href="#how-we-work"><i class="fa fa-briefcase" aria-hidden="true"></i></a></li>
                <li><a href="#why_choose_us"><i class="fa fa-paper-plane" aria-hidden="true"></i></a></li>
                <li><a href="#numbers"><i class="fa fa-sort-numeric-desc" aria-hidden="true"></i></a></li>
                <li><a href="#skills"><i class="fa fa-gavel" aria-hidden="true"></i></a></li>
            </ul>
        </div>

        <!--    TOP HEADER-->
        <?php include('top_header.php'); ?>

        <!--    RIGHT SIDE DOT NAVIGATOR-->
        <?php include('top_menu_mobile.php'); ?>
        <section class="wrapper-site">

            <!--        MAIN MENU SECTION-->
            <?php include('main_menu.php'); ?>

            <section id="wrapper-content" class="wrapper-content">
                <div class="view x40-widget widget" id="layers-widget-skrollex-section-2">
                    <div data-src="assets/images/about_cover.ba0ea706854a7cfd.ba0ea706854a7cfd.ba0ea706854a7cfd.jpg" data-alt="" class="bg-holder"></div>
                    <div data-src="assets/images/about_cover.ba0ea706854a7cfd.ba0ea706854a7cfd.ba0ea706854a7cfd.jpg" data-alt="" class="bg-holder"></div>
                    <div class="fg colors-u ">
                        <div class="layout-boxed section-top"><h3 class="heading-section-title"><span>About</span> Us</h3>
        <!--                    <p class="header-details"><span>Search Engine Optimization</span> Marketing</p>-->
        <!--                    <p class="header-caption">We offer a proper incorporated approach to <span>SEO</span> marketing which looks at the bigger picture to go beyond businesses needs. The two popular way of search engine marketing are search engine optimization (SEO) &amp; pay-per-click (PPC) advertising.</p>-->
                        </div> </div>
                </div>
                <img class="bg" src="assets/images/smm_bannner.23035219b9f2ac82.23035219b9f2ac82.298e49249b78bbb5.png" alt=""/>
                <img class="bg" src="assets/images/smm_bannner.23035219b9f2ac82.23035219b9f2ac82.298e49249b78bbb5.png" alt=""/>
                <div class="default-page-wrapper background-v">

                    <!--who we are-->
                    <div class="view x40-widget widget text-bg" id="layers-widget-skrollex-section-5" data-text-effect-selector="h1,h2,h3,h4" data-text-effect="effect-a-animated" style="background: transparent">
                        <div id="about" class="fg ">
                            <div class="layout-boxed section-top">
                                <h1 class="post-title center">
                                    <a href="#">Who <span>We</span> Are</a>
                                </h1>
                                <p class="lead">To be honest there are a great deal of organizationhothat do what we do.
                                    Probably, they even share a similar what and how, yet our accomplice works with us for our why and our who.
                                    We're wits and marketers with marketing prudence and innovative slashes,
                                    set out to associate individuals with what is important most <strong><span>the experience.</span></strong>
                                    And more, we spend every day doing as such by honing the tools of the advanced Digital Marketing Trade.</p>
                            </div>
                        </div>
                    </div>
                    <!--who we are-->


                    <!--HOW WE WORK-->
                    <?php include('how_we_work.php'); ?>
                    <!--HOW WE WORK-->

                    <!--WHY CHOOSE US-->
                    <?php include('why_choose_us.php'); ?>
                    <!--WHY CHOOSE US-->

                    <!--OUR NUMBERS-->
                    <?php include('our_number.php'); ?>

                    <!--OUR SKILLS-->
                    <?php include('our_skills.php'); ?>

                    <!--LOWER IMAGE SLIDER SLOGAN-->
                    <?php include('slogan.php') ?>

                </div>
            </section>

            <!--FOOTER-->
            <?php include('footer.php'); ?>

        </section>

        <?php require('javacsript.php'); ?>

    </body>
</html>
