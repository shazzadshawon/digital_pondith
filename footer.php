<?php require('top_footer.php'); ?>

<!--<div id="bezier-curve" style="background: #fff"></div>-->
<style>
    .footer_image_dimention{
        width: 81px; height: 81px; margin: 3px;
    }
</style>

<!--<footer id="footer" class="animated page-transition non-preloading" style="background: #ffbb42;">-->
<footer id="footer" class="animated page-transition non-preloading background_yello_dark">
    <div class="footer-widgets clearfix">
        <div class="pure-g">
            <div class="pure-u-1 pure-u-md-24-24">
                <div class="view x40-widget widget peraRemoval" id="layers-widget-skrollex-section-1">
                    <!--                    <div class="section-cols layout-fullwidth" style="padding: 0;">-->
                    <!--                        <img class="img-responsive" src="assets/images/footer_banner.08f6087516e6dfa3.08f6087516e6dfa3.08f6087516e6dfa3.08f6087516e6dfa3.png" style="width: 100%;" />-->
                    <!--                    </div>-->
                    <div class="fg no-bottom-padding">
                        <div class="section-cols layout-boxed">
                            <div class="pure-g">
                                <div class="layers-widget-skrollex-section-5605431a66a7f615865511 pure-u-12-24 pure-u-md-8-24  col-padding">
<!--                                    <img src="assets/images/Social-Media-Profile.9c6882b0848a5f01.9c6882b0848a5f01.d5df7eb057c8288a.png" style="width: 30%;">-->
                                    <h5 class="heading-col-title">Digital<span>Pondith</span></h5>
                                    <p>Digital Marketing is not a theory but a science. The science where technics create a chemical reaction.  </p>
                                    <p>Digital Marketing is not a theory but a science. The science where technics create a chemical reaction.  </p>
                                    <p>Digital Marketing is not a theory but a science. The science where technics create a chemical reaction.  </p>
                                    <p>Digital Marketing is not a theory but a science. The science where technics create a chemical reaction.  </p>
                                    <h5 class="heading-col-title">
                                        <a href="https://www.facebook.com/Digital-Pondith-114644899181020/" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/DigitalPondith" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/channel/UCJ5LZ-6oVHjcighMPubIAKg" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://vimeo.com/digitalpondith" target="_blank"><i class="fa fa-vimeo-square"></i></a>
                                        <a href="https://www.linkedin.com/" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/digitalpondith/" target="_blank"><i class="icon-instagram"></i></i></a>
                                    </h5>
                                </div>
                                <div class="layers-widget-skrollex-section-5605450875fbd690551588 pure-u-12-24 pure-u-md-8-24  col-padding">
                                    <h6 class="heading-block-title">Additional Links</h6>
                                    <ul>
                                        <li><a href="about_us_page.php">About Us</a></li>
                                        <li><a href="blog.php">Our Blogs</a></li>
                                        <li><a href="https://www.linkedin.com/in/digital-pondith" target="_blank">Our Portfolio on Linked In</a></li>
                                        <li><a href="https://digitalpondith.blogspot.com/" target="_blank">ContactUs on Blogspot</a></li>
                                        <li><a href="#" target="_blank">Subscribe to RSS feed</a></li>
                                        <li><a href="mailto:contact@digitalpondith.com" target="_blank">Our Email</a></li>
                                    </ul>
                                </div>
                                <!-- <div class="layers-widget-skrollex-section-5605450f3001b812805779 pure-u-12-24 pure-u-md-6-24  col-padding">
                                    <h6 class="heading-block-title">Recent Posts</h6>
                                    <ul>
                                        <li><a href="2016/04/23/post-about-life/index.html">Post one</a></li>
                                        <li><a href="2016/04/23/my-next-post-3/index.html">Post Two</a></li>
                                        <li><a href="2016/04/23/simple-example/index.html">Post Three</a></li>
                                        <li><a href="2016/04/23/next-important-text/index.html">Post Four</a></li>
                                        <li><a href="2016/04/23/simple-post/index.html">Post Five</a></li>
                                        <li><a href="blog/index.html">More...</a></li>
                                    </ul>
                                </div> -->
                                <!--<div class="layers-widget-skrollex-section-56054514d6dd9579769748 pure-u-24-24 pure-u-md-6-24  col-padding">
                                    <h6 class="heading-block-title">Photo Stream</h6>
                                    <p class="photo-stream">
                                        <a class="pure-img" style="display: inline-block"
                                           href="assets/data/footer_ps/1.c9c19e9f9c1ccf71.c9c19e9f9c1ccf71.09954dfc1cda07bf.jpg"
                                           target="_blank"><img class="footer_image_dimention" alt=""
                                                                src="assets/data/footer_ps/1.c9c19e9f9c1ccf71.c9c19e9f9c1ccf71.09954dfc1cda07bf.jpg"/></a>
                                        <a class="pure-img" style="display: inline-block"
                                           href="assets/data/footer_ps/2.736bd241ff10c5e4.736bd241ff10c5e4.50f151aa7cb55480.jpg"
                                           target="_blank"><img class="footer_image_dimention" alt=""
                                                                src="assets/data/footer_ps/2.736bd241ff10c5e4.736bd241ff10c5e4.50f151aa7cb55480.jpg"/></a>
                                        <a class="pure-img" style="display: inline-block"
                                           href="assets/data/footer_ps/3.7d74e7d21b95c21d.7d74e7d21b95c21d.fb92376d68693d2f.jpg"
                                           target="_blank"><img class="footer_image_dimention" alt=""
                                                                src="assets/data/footer_ps/3.7d74e7d21b95c21d.7d74e7d21b95c21d.fb92376d68693d2f.jpg"/></a>
                                        <a class="pure-img" style="display: inline-block"
                                           href="assets/data/footer_ps/4.3b72a3e4248107c6.3b72a3e4248107c6.9a26659d92126b2a.jpg"
                                           target="_blank"><img class="footer_image_dimention" alt=""
                                                                src="assets/data/footer_ps/4.3b72a3e4248107c6.3b72a3e4248107c6.9a26659d92126b2a.jpg"/></a>
                                        <a class="pure-img" style="display: inline-block"
                                           href="assets/data/footer_ps/5.4207c3b5ed437b32.4207c3b5ed437b32.3e8fb707c913b679.jpg"
                                           target="_blank"><img class="footer_image_dimention" alt=""
                                                                src="assets/data/footer_ps/5.4207c3b5ed437b32.4207c3b5ed437b32.3e8fb707c913b679.jpg"/></a>
                                        <a class="pure-img" style="display: inline-block"
                                           href="assets/data/footer_ps/6.002b5ff4483b88e1.002b5ff4483b88e1.6562c92d1bbed80d.jpg"
                                           target="_blank"><img class="footer_image_dimention" alt=""
                                                                src="assets/data/footer_ps/6.002b5ff4483b88e1.002b5ff4483b88e1.6562c92d1bbed80d.jpg"/></a>
                                    </p>
                                </div>-->

                                <div class="layers-widget-skrollex-section-56054514d6dd9579769748 pure-u-24-24 pure-u-md-8-24  col-padding">
                                    <h6 class="heading-block-title">Certifications</h6>
                                    <p class="photo-stream">
                                        <div class="pure-g">
                                            <div class="pure-u-12-24 pure-u-md-12-24">
                                                <a class="pure-img" style="display: inline-block"
                                                   href="https://clutch.co/user/74292"
                                                   target="_blank"><img class="footer_image_dimention" alt="" src="assets/data/footer_credits/clutch.3763e5c5716eb572.3763e5c5716eb572.64a4d6f5dd9c1953.png"/></a>
                                            </div>

                                            <div class="pure-u-12-24 pure-u-md-12-24">
                                                <span class="pure-img" style="display: inline-block">
                                                <img alt="" src="assets/data/footer_credits/bbb.fed4f48f850b6a60.fed4f48f850b6a60.a3aa1ea0af144b54.png" style="margin: 3px; width: 100%;"/></span>
                                            </div>
                                        </div>

                                        <div class="pure-g">
                                            <div class="pure-u-12-24 pure-u-md-12-24">
                                                <span class="pure-img" style="display: inline-block">
                                                <img alt="" src="assets/data/footer_credits/3.2928ccba5d0f0fad.2928ccba5d0f0fad.8108fe5382ecc9a6.png" style="margin: 3px; width: 80%;"/></span>
                                            </div>

                                            <div class="pure-u-12-24 pure-u-md-12-24">
                                                <span class="pure-img" style="display: inline-block">
                                                <img alt="" src="assets/data/footer_credits/4.1046a05ce590e5af.1046a05ce590e5af.768e03668a9af7aa.png" style="margin: 3px; width: 80%;"/></span>
                                            </div>
                                        </div>
                                                                                <!--<a class="pure-img" style="display: inline-block"
                                                                                   href="#"
                                                                                   target="_blank"><img alt=""
                                                                                                        src="assets/data/footer_credits/5.ae332e5d14e3b2a7.ae332e5d14e3b2a7.ae332e5d14e3b2a7.ae332e5d14e3b2a7.png"
                                                                                                        style="margin: 3px; width: auto; height: 81px;"/></a>
                                                                                <a class="pure-img" style="display: inline-block"
                                                                                   href="#"
                                                                                   target="_blank"><img alt=""
                                                                                                        src="assets/data/footer_credits/6.901efeea1eba6f05.901efeea1eba6f05.901efeea1eba6f05.901efeea1eba6f05.png"
                                                                                                        style="margin: 3px; width: auto; height: 81px;"/></a>-->
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 952.9 186.3" style="enable-background:new 0 0 952.9 186.3;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#13281C;}
    .st1{fill-rule:evenodd;clip-rule:evenodd;fill:none;stroke:#50DB89;stroke-miterlimit:10;}
    .st2{fill-rule:evenodd;clip-rule:evenodd;fill:#50DB89;}
    .st3{fill-rule:evenodd;clip-rule:evenodd;fill:#13281C;}
    .st4{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_1_);}
    .st5{fill-rule:evenodd;clip-rule:evenodd;fill:url(#SVGID_2_);}
</style>
            <path class="st0" d="M920.1,79.1l-4.6-3.9c1.7-1.9,3.4-3.5,5.3-4.9c-2.8-1.6-5.8-3.5-8.8-5.6l3.4-5c4.3,2.9,8.1,5.2,11.5,7
    c3.9-1.7,7.5-2.5,10-2.9c8.4-1.2,13.8,2.1,15.5,5.8c1.1,2.5,0.5,5.2-1.6,6.9c-4.2,3.5-12.8,2.3-23.9-3.2
    C924.5,74.8,922.2,76.6,920.1,79.1z M934.7,70.4c7.7,3,11.4,2.3,12.2,1.7c-0.1-0.2-0.3-0.5-0.8-0.9c-0.7-0.5-3.2-2.1-8.3-1.4
    C936.9,70,935.8,70.2,934.7,70.4z"/>
            <path class="st1" d="M421.4,136.8c-4.5,4.5-1.4,10.2,1.8,14.5c2.3,3.1,10.1,4.2,11.8,10c2.7,9.1-14.4,19.2-76.1,24.5"/>
            <g>
                <g>
                    <path class="st2" d="M435.5,122.5c0,0-0.3-0.9-1.1-1.5c-1.1-0.9-2.7-1.7-2.7-1.7s-1,1.3-1.6,2.8c-0.4,1.1-0.5,2.4-0.5,2.4l1.8,1.3
            c0,0,1,0.9-2.1,5.2c-2.9,4.1-4,3.3-4,3.3l-1.8-1.3c0,0-1.2,0.5-2.2,1.3c-1.2,1-2.1,2.4-2.1,2.4s1.3,1.1,2.6,1.7
            c0.9,0.4,1.9,0.4,1.9,0.4s4.2-0.4,8.1-5.9C436,127.2,435.5,122.5,435.5,122.5z M415.4,123.4l0.9-2.7h-1.8L415.4,123.4z
             M412.6,126.1l1.8-1.8c0,0-1.8,0.4-2.7,0.9C410.8,125.7,412.6,126.1,412.6,126.1z"/>
                </g>
            </g>
            <g>
                <g>
                    <path class="st3" d="M3.2,182.3c-1.8,0-3.2,1.2-3.2,2.7c0,0.8,1.4,0.8,3.2,0.8s3.2,0,3.2-0.8C6.3,183.5,4.9,182.3,3.2,182.3z
             M52.3,181.3c-5.1,0.2-5.4,1.5-5.4,1.5s-4,0.5-5.4-1.5c-1.4-2-1.4,0-1.4,0s1.9,2.3,3.7,3.1c1.6,0.7,3.1-0.1,3.1-0.1l1.4,1.5H59
            v-1.5C59,184.3,57.4,181.1,52.3,181.3z M118.7,176.6c-1.4,0-5.7-2.5-10.5-2.6c-4.8-0.2-9.2,2.6-9.2,2.6l-11.8,2.6v1.3l11.8-1.3
            l-1.7,5.7l1.3,0.4l1.7-4.8l1.3,5.2h2.6c0,0-0.7-0.8-1.3-1.3c-0.7-0.5,0-3.9,0-3.9h6.6l2.6,5.2h1.3l-1.3-3.9h2.6l2.6,3.9h1.3
            l-2.6-5.2h3.9c0,0,3.8,3.3,5.3,3.9c1.5,0.6,2.6-2.6,2.6-2.6S125,176.8,118.7,176.6z M192.1,160.5c0,0,0.8-1.3,0-1.8
            c-0.8-0.5-2.7,0.9-2.7,0.9s-3.3-0.6-4.5,1.8c-1.3,2.4-0.9,3.6-0.9,3.6s-6.2,0.5-8.2,0.9c-1.9,0.4-7.3,2.7-7.3,2.7
            s-3.6,0.2-6.3-1.8c-2.8-2.1-7.9-7.2-13.6-7.3c-5.7,0-12.4,5.3-10,7.3c2.4,1.9,6-3.9,10-3.6c4,0.3,11.3,5.8,12.7,6.3
            c1.4,0.5,6.3,0.9,6.3,0.9s-1,2.4,0.9,5.4c0.8,1.4,0.9,1.8,0.9,1.8l-0.9,7.3l4.6,0.8l-2.8-1.7l0.9-4.5l4.6,6.2h3.6l0-0.8l-2.7-0.9
            l-2.7-8.2c0,0,4.5-0.7,5.4-0.9c1-0.3,2.7-0.9,2.7-0.9l0.9,10.9c0,0,1.3,0.8,1.8,0.8c0.5,0,1.8,0,1.8,0l-0.9-0.8l-0.9-0.9V175
            l0.9,0.9l5.4,3.6l0.9,0.9v-1.8l-4.5-4.5v-6.3L192.1,160.5z"/>
                </g>
            </g>
            <path class="st3" d="M263.7,184.8l-5.3-0.6l-3,1.5c0,0-2.4-5-2.5-6.5c-0.1-1.5-0.5-3-0.5-3l-3,2.5l-6.4,2.5l5.2,3.6
    c0,0-3.8,1.1-4.5,0.9s-5.9-3.9-5.6-5c0.3-1.1,8.1-5.6,8.9-6c0.8-0.4,4.1-2.5,3.5-3.5s-6.1-4.4-6.9-5.5c-0.8-1.1-1.5-2.5-1.5-2.5
    s-3.5,0.6-4.9,0c-5-1.9-3.5-10.1-4.9-10.5c-3-0.9-3.2,4.9-4.4,4.5c-2.9-0.8,0.8-7.5,3.3-7.8c7-0.6,4,10.5,7.3,10.9
    c3.5,0.4,4.5-0.9,4.5-0.9l1.3-3.2l2-10c0,0-5,2.5-6.3,1.5c-1.4-1,0.9-13.5,0.9-16c0-2.7,0.2-3.5-0.9-3.5c-1.1,0-1,3.8-2.1,3.5
    c-2.9-0.9-0.2-7.8,4.3-5.8c4.5,2,0.2,15.6,2.2,14.8c1.9-0.8,5-4.6,5.9-5c0.9-0.4,2-1,2-1s0.4-0.7,1-1.5c0.9-1.1,2.2-2.4,3.8-2.4
    c5.4,0,4.9,3.3,5,5.9c0.1,2.6-3.4,4.5-3.4,4.5v5.5c0,0,7.2-5.7,8.9-5.5c1.7,0.2,5.4,3.5,5.4,3.5s-0.1,3.7-1,4c-0.9,0.3-1.5,0-1.5,0
    l0.5-2.5c0,0-1.2-1.7-2.6-1.6c-2.5,0.2-7.7,8.2-9.3,8.1c-1.6-0.1-2-1-2-1l-3.5,4.5c0,0-2.6,2.1-2,3.5c0.6,1.4,5.1,3.5,5.4,4.5
    c0.4,1,1,17.1,1,17.1l3-0.1l2.7,1.8V184.8z"/>
            <g>
                <g>
                    <path class="st2" d="M147.9,175.8l-2.7,10l20.8-5.4l-3.6-4.5H147.9z M229.5,128.7l-2.7,10.9l20.8-6.3l-3.6-4.5H229.5z
             M194.1,169.3l13.9,5.6l10.5-11.2l-20-1.5L194.1,169.3z M275.6,165.7l-3.4,5.5l10.6,4.3l8.1-8.7L275.6,165.7z M208.6,150.4
            l13.6,2.7v-9.1l-16.3-10L208.6,150.4z"/>
                </g>
            </g>
            <ellipse class="st3" cx="237.9" cy="130" rx="1.4" ry="2.3"/>
            <g>
                <g>
                    <path class="st3" d="M347.3,131.5l0.4-0.4l-1-0.5c-0.6-0.8-1.5-1.6-2.6-2.2v-0.9l-1.5,0.3c-0.3-0.1-0.6-0.2-0.9-0.3
            c-1.3-0.3-2.6,0.2-3.7,0.9h-1.1l0.1,0.6c-0.9,0.6-1.6,1-2.1,1c-1.2-0.1-1.5-1.4-8.6-1.2c-1.1,0-2.2,0.2-3.3,0.6
            c-1.3,0-2.4-0.1-2.4-0.1l1.1,0.5c-0.8,0.3-1.5,0.7-2.2,1.1l-4.3,1.1l1.1,1.1c-0.5,0.4-0.9,0.7-1.3,1.1l-3.4,0.6l0.9,1.8
            c0,0-0.1,0.1-0.1,0.1l-4.4,2.7l0.8,0.4c-0.9,0.8-1.8,1.7-2.8,2.6l-4.4,2.4l0.9,1.7c-0.3,0.5-0.6,1-0.8,1.5
            c-2.4,6.9,2.5,9.3,2.5,9.3l1.8,6.8c0,0-0.3,0-2.5,1.9c-2.2,1.8-3.1,4.3-3.1,4.3s-4.3,3.8-4.3,4.3c0,1.7,3,4.3,3.7,6.8
            c0.7,2.5,2.5,4.3,2.5,4.3h6.8l-0.6-2.5l-3.7-2.5l-2.5-5.6c0,0,7.3-4.7,8.6-5.6c2.7-1.8,1.2-5.6,1.2-5.6l2.5,3.7c0,0-1.4,2.2-2.5,5
            c-1.1,2.7-1.9,7.4-2.5,9.3c-0.6,1.9,1.8,3.7,1.8,3.7h13.6l-1.2-2.5l-5.5-1.9l-2.5-1.9c0,0,6.3-10.9,6.8-12.4
            c1.4-4.2-4.9-13.6-4.9-13.6s1.6,0.3,3.7,0c2.1-0.3,5.5-1.2,5.5-1.2s-0.3,6.1-0.6,7.4c-0.3,1.3,5.5,15.5,5.5,15.5s0,0.5-0.6,1.9
            c-0.6,1.4-0.6,4.3-0.6,4.3l-1.8,0.6v1.2l1.2,2.5h5.5l0.6-3.1l0.6-3.7l-1.2-8.7l3.7,6.2v5.6l-1.8,1.9c0,0,0.1,1.8,1.2,1.9
            c1.1,0.1,4.3,0.1,6.2,0c1.9-0.1,0-9.3,0-9.3s-2-5.4-3.7-9.9c-1.7-4.5-5.5-8.7-5.5-8.7s-0.7-6.4-0.6-11.1c0.1-4.7,2.5-4.3,2.5-4.3
            s1.8,2.4,3.7,3.7c1.3,0.9,4.9-3.2,4.9-3.2l1.3,0.7l0.6-1.2l-0.6-3.7c0,0-1-2.7,0.6-2.5C348.3,136.3,348.6,133.8,347.3,131.5z"/>
                </g>
            </g>
            <g>
                <g>
                    <path class="st3" d="M431.3,132.9c0,0-0.4,0.5-0.8,1c-3.4-4.8-7.6,6.2-5.5,4.5c-1.9,1.5-8.2,5.4-8.2,5.4l-8-7.6
            c0,0,1.1-5.4,0.6-6.8c-0.7-1.9-0.7-2.8-0.7-2.8s2.6,0.9,3.5,0.3c0.9-0.6-1.6-3.2-0.8-4.9c0.6-1.2,2.7-0.9,3.6-0.9
            c1.5,0,1.7,0.3,1.8,0c0.3-1.2,0-2.7,0-2.7l-1.3-0.4l0.6-1.2l-1.2-1.2l0.1-1.6c0,0-0.1-0.9-1.8-1.8c-0.6-0.3-4.3-9.1-10.9-1.8
            c-2.3,2.5-1,7-2.8,8.6c-1,0.9-2,1.9-3.1,3.1l-3.2,1.9l0.9,0.9c-0.1,0.1-0.2,0.3-0.3,0.4l-3.3,3.3l0.7,0.7
            c-1.8,3.1-4.5,8.5-5.9,12.1c-0.6,1.6-1.8,3.5-2.9,5.4h-2.8l2,1.3c-0.1,0.2-0.3,0.5-0.4,0.7l-3.4,0.7l2.7,0.7
            c-0.9,1.9-1.4,3.6-0.7,4.9c1.7,3.2,7.5,7.4,8.6,8c1.1,0.7,1,1.6,0.6,2.5c-0.4,0.9-3.7,2.8-4.3,5c-0.7,2.2-3,9.5-3.7,10.5
            c-0.6,1-1.7,5-0.6,5c2.7-0.1,12.3,0,12.3,0v-1.9l-6.2-2.5c0,0-0.4-1.3,0.6-1.9c1-0.6,5.8-5.3,6.2-6.2c0.4-0.9,0.6-1.9,0.6-1.9
            l1.2,5v7.4c0,0,0.8,1.7,1.2,1.9c0.4,0.2,9.9,0,9.9,0h4.3c0,0,0.5-1.3-0.6-1.9c-1.1-0.5-6.2-1.9-6.2-1.9s-2.8-2.7-2.5-4.3
            c0.3-1.6,3.1-10.1,3.1-12.4c-0.1-2.3-8-14.2-8-14.2l4.9-3.1l2.5,5l12.3,8.7l0.6,3.1l-3.1,5.6c0,0,3.5,1.9,4.3,1.2
            c0.8-0.6,3.1-5,3.1-5s0.7-5.2,0-6.2c-0.7-1-8.8-11.1-8.8-11.1l5.4,0.9c0,0,8.2-8,10-9.1c-0.7,0.4,4.8-1.6,4.5-2.7L431.3,132.9z"/>
                </g>
            </g>
            <path class="st3" d="M493,185.8h-10.8c0,0-3.2-0.2-3.2-2.3c0-2.1,0.4-3.3,0-5.7c-0.4-2.4-0.7-9.2-0.5-15.3c0-1.2-1,0-1,0
    s-5,6.5-8.5,9.4c-3.6,2.9-5.6,4.3-5.6,4.3s2,1.4,5.4,7.5c1.1,1.9-0.2,1.8-0.2,1.8l-1-0.4c0,0-1.9,0-2.5-0.6
    c-0.4-0.5-1.7-1.5-1.7-1.5s-1.1,1.6-2,1.4c-1.6-0.3-0.3-1.9-0.6-3c-0.3-1.1-6.3-4.1-4.6-5.7c2.5-2.5,6.7-7.3,8.1-10
    c0.7-1.4,6-6.1,6-6.1l-6.1-11.5c0,0-4.2-1.9-2.5-7.2c1.6-5.3,1.4-3.5,2-5.7c0.6-2.2,7-19.4,7.1-20.1s2.7-6.6,8.1-9.1
    c5.4-2.5,4-1.2,5.6-2.4c1.6-1.2,1.8-4.3,5.5-4.9c3.3-0.5,5.6,1.1,6.7,3c1.1,1.8,2.8,5.3,2.8,5.3l-1.3,0.9l2,1.9l-0.5,1l1.6,1.4
    l-1.6,1.4c0,0-0.1,0.9-1.5,1.4c-2.2,0.9-4.4,1.1-5.6,0.5c-1.2-0.6-3.1-2.4-3.1-2.4l-1.7,1.9c0,0,0.3,7.4,0,8.7
    c-0.4,1.6-1.8,1.4-1.8,1.4l-1.5,5.7c0,0,3.8,4.8,5.6,9.1c1.8,4.3,1.6,5.5,2.5,7.6c1,2.1,1,5.7,1,5.7l-2.5,4.3l-1,1.4l-2.5-0.5
    l1.5-6.7l-0.5-2.9c0,0-5.7-5.8-6.1-7.2c-0.4-1.4-1.5-4.3-1.5-4.3l-4.6,6.2c0,0,9.5,9.5,10.2,12.4c0.7,2.9-1.5,24.4-1.5,24.4
    s3.5,3.2,4.6,3c1.1-0.2,3.2,0.8,3.3,1.7C493,185.5,493,185.8,493,185.8z"/>
            <g>
                <g>
                    <path class="st3" d="M476.7,106v1.8l5.4-2.7L476.7,106z M475.8,108.7c-1.1,0.3-5.4,3.6-5.4,3.6l0.8,1.6c-1.3,1.3-2.6,3-2.6,3
            l1.8,2.7c0,0,3.8-7.5,2.7-7.3c0,0-0.1,0-0.1,0.1C474.5,110.8,476.5,108.6,475.8,108.7z"/>
                </g>
            </g>
            <g>
                <g>
                    <path class="st3" d="M575.5,123c-0.1,0-0.2,0.1-0.3,0.1c-0.7-0.5-1.4-0.9-1.8-0.8c-1.2,0.1-4.7,0.5-4.7,0.5l-8.5-2.4l0.9-3.3
            l1.4-1.9c0,0-1.3-12.1,1.9-11.8c1,0.1,3.5,2.4,5.6,2.4c2.2-0.1,3.8-2.8,3.8-2.8s-0.2-0.3,0-0.5c0.2-0.2,0.5-1.9,0.5-1.9l-0.9-1.9
            c0,0,0.3-1.1,0-1.4c-0.3-0.3-0.3-1,0-1.4s0-1.4,0-1.4s-3.1-2.2-3.8-3.3c-0.3-0.5-1.2-1.1-2.2-1.5l1.8-1.8l-4.5,0.9v-1.8l-2.4,2.4
            c-0.2,0.1-0.4,0.3-0.7,0.4l-3.3,0.8l0.6,1.2c-0.9,0.7-1.7,1.4-2.4,2l-0.9,0.5l0.2,0.2c-0.7,0.6-1.2,1.1-1.2,1.1s-0.4,0.4-1,0.9
            l-4.4,1.5l1.4,1.4l-4.1,1.4l1.2,1.2c-1.3,1.3-3.1,6-3.1,6s-6.5,7.5-7.1,8.1c-0.6,0.6,2.8,6.6,2.8,6.6l-0.9,4.7
            c0,0-1.2,1.5-2.8,4.7c-1.6,3.3,1.9,8.1,1.9,8.1s0,1.7,0.5,6.2c0.5,4.4,1.4,9,1.4,9s-1.4,0.5-4.2,1.4c-2.8,0.9-13.7,8.6-15.1,9.5
            c-1.4,0.9,1.4,5.2,1.4,5.2l0.9,5.2c0,0,2.8,3.8,3.3,3.8c0.5,0,0.5-1.4,0.5-1.4s1.9,1.1,2.4,0.9c0.5-0.2,1.6-2.1,1.4-2.8
            c-0.2-0.7-2.1-1.3-2.8-1.4c-0.7-0.1-1.4-6.6-0.5-7.6c1-1,18.6-6.7,20.2-7.6c1.6-0.9,2.4-3.8,2.4-3.8s1.1,1.9,1.4,2.8
            c0.3,0.9-2.1,8.6-2.8,12.3c-0.7,3.7-0.3,8.7-0.9,10.9c-0.7,2.2,1.9,3.3,1.9,3.3H564c0,0-0.4-1.4-1.9-2.4c-1.5-1-4.8-1.3-6.6-1.9
            c-1.8-0.6-0.5-2.8-0.5-2.8s3.4-17,4.2-18c0.8-1,0.9-6.2,0-10c-0.9-3.7-7.1-13.7-7.1-13.7l5.2-10c0,0,7.5,1,8.9,0.9
            c1.4,0,2.4,0.5,2.4,0.5s4.1,2.2,5.6,2.8c0.4,0.2,0.8,0,1.2-0.3c0.1,0,0.3,0.1,0.4,0.1c1.2,0.1,2.9-2.9,3.1-5.4
            C579.4,123.4,577.1,122.8,575.5,123z"/>
                </g>
            </g>
            <path class="st2" d="M579.5,117.4l-2.7-0.7l1.6-6.1l-1.7-0.4l-1.5,6.2l-4.4-1.1l-0.8,3.3l1.3,2.4l-1.6,6.2l-2.2,1.3l-0.9,3.4
    l8.8,2.2L579.5,117.4z"/>
            <path class="st3" d="M575.6,122.5c1.6-0.3,3.8,0.3,3.6,2.6c-0.2,2.5-2,5.5-3.1,5.4c-1.6-0.1-1.9-2.2-2.3-4.4
    C573.4,123.8,574,122.7,575.6,122.5z"/>
            <path class="st3" d="M652.6,182.4l-15,3.4l-1.3-5.5c0,0-1.3-7.5-1.7-8.4c-0.4-0.9-2.3-5.3-1.7-10.5c0.6-5.2,0-4.6,0-4.6l-6.5-5.9
    l-2.9,7.1l-13.4,17.2l4.6,7.1l2.9,0.8v2.1h-3.3c0,0-3.6-0.4-4.2-1.3c-0.5-0.9-2.4-2.6-3.3-2.9c-0.9-0.3-4.2-1.2-4.2-2.1
    c0-0.9,2.5-5,2.5-5s3.2-13.2,6.7-15.5c3.4-2.3,5.1-3.2,5-3.8c-0.1-0.6,1.3-16.8,1.3-16.8s-3.2-4.6-2.1-7.1c1.1-2.5,2.5-6.3,2.5-6.3
    s-1.5-12.6-0.8-15.5c0.7-2.9,1.8-12.6,4.6-15.5c2.8-2.9,4.3-4.3,4.6-5.5c0.3-1.2,1.4-7.2,4.2-8.4c2.8-1.2,7.6-1.3,10,0.8
    c0.7,0.6,0.4,2.5,0.6,2.8c0.9,1.3,1.8,2.7,1.8,2.7s-1.1,0.4-1.2,0.7c0,0.4,0.4,1.7,0.4,1.7v1.3c0,0,0.3,1.5,0,1.7
    c-0.3,0.2-0.8,0.8-0.8,0.8s-0.1,1.6-0.8,2.1c-0.8,0.5-2.1,0.6-2.9,0.4c-0.8-0.2-1.2-0.9-2.1-0.4c-0.9,0.5-0.4,0.8-0.4,0.8
    s4.1,9.9,4.2,10.5c0,0.6-1,4.5-1.3,4.6c-0.2,0.2,0,5,0,5s5.7,10.6,6.7,13.9c1,3.3,2.5,12.2,2.5,12.2s-2.5,4.6-4.6,3.8
    s-2.1-2.1-2.1-2.1l2.1-2.9l-0.8-1.7l0.8-2.9l-7.1-8.8l-0.4-1.3c0,0-3,6.9-3.3,8c-0.3,1.1-0.3,1.7,0,2.9c0.3,1.3,3,4.2,5,9.7
    c2,5.5,2.5,5.1,2.7,8c0.2,2.9,1.3,12.2,1.3,12.2l0.4,10.9c0,0,1.2,2.9,3.3,2.9c2.2,0.1,5.4-0.3,6.3,0
    C652.2,180.2,652.6,182.4,652.6,182.4z"/>
            <g>
                <g>
                    <path class="st2" d="M644.4,95.2c-1.3,0-2.3,0.8-2.3,1.8s1,1.8,2.3,1.8s2.3-0.8,2.3-1.8S645.6,95.2,644.4,95.2z M634,85.2
            l-2.4-7.3c0,0-0.7-0.7-1.1-0.6c-0.5,0.1-0.7,1.2-0.7,1.2l2.4,7.4c-0.6,0.5-1,1.3-1,2.1c0,1.5,1.2,2.7,2.7,2.7
            c1.5,0,2.7-1.2,2.7-2.7C636.7,86.4,635.5,85.2,634,85.2z"/>
                </g>
            </g>
            <path class="st2" d="M643.5,97c-2.7,0-4.5,0-7.3-1.8"/>
            <g>
                <g>
                    <path class="st3" d="M722.9,181.4l-4.9-0.9l-4.4-2.2c0,0,0.3-6.2,0.9-10.6c0.6-4.3,1.3-9.2,1.3-9.2s0.5-1.9,0.9-4
            c0.3-2.1-0.9-13.2-0.9-13.2l1.3-0.4c0,0,0.9-1.7,0.9-1.8c-0.1,0-0.9-4-0.9-4s-4.6-5-4.9-6.6c-0.3-1.6,2.2-3.3,2.7-10.1
            c0.1-1.4,0.8-2.4,1.3-5.3c0.6-2.9-0.4-7.9-0.4-7.9l0.9-3.5c0,0-5-11.8-5.3-12.8c-0.3-1,1.4-2.6,1.8-3.1c0.4-0.4,1.8,0.8,3.5,1.3
            c1.8,0.5,3.5-2.6,3.5-2.6v-1.8l1.3-0.9l-0.4-1.3l0.9-1.3l-2.2-2.2c0,0,0.4-1.6,0-2.2c0,0,0,0,0,0l0.8-2.4l-2.7-2.7v-0.9h-4.5v-1.8
            l-4.5,1.8L706,68l-0.9,2.7l-2.7,1.8v1.8l-1.8,2.7v1.8l-0.9,1.8v2.5c-0.9,1.6,0.6,1.4-4.3,7.7c-5.1,6.6,1.9,23.8,2.2,25.5
            c0.4,1.7-3.4,4.8-4.9,9.7c-1.5,4.9,4,7.9,4,7.9s0.3,1.1,0.4,5.7c0.2,4.6,2.7,12.8,2.7,12.8s-0.4,0.3-4.4,2.2c-4,1.9-7.5,10-8,11
            c-0.5,1-5.5,6.3-6.6,7.9c-1.1,1.7,3.5,5,4.9,7c1.4,2.1,5.6,3.9,7.5,4c1.9,0,3.1-1.8,3.1-1.8l-3.5-1.8l-4-7.5l19.5-16.7
            c0,0-0.3,1.6-1.3,4.4c-1,2.8,0.9,11.4,0.9,11.4v6.6c0,0-1.2,5.1-0.4,6.2c0.7,1.1,5.8-0.4,5.8-0.4l11.5-0.9l0.4-1.8L722.9,181.4z"
                    />
                </g>
            </g>
            <g>
                <g>
                    <polygon class="st2" points="730.7,138.1 719.3,140.4 718.6,136.8 710.5,138.5 711.3,142.1 697,145.1 701.2,165.5 734.9,158.5
            "/>
                </g>
            </g>
            <path class="st3" d="M711,136.9c0.4,1.1,0.8,2.3,1.2,3.4c0.3,0.2,0.7,0.4,1,0.6c3,0,6.7-4,3-5.8c-1.1,0.2-2.3,0.3-3.4,0.5
    c-0.5,0.5-0.9,1.1-1.4,1.6c0.1,0.1,0.3,0.3,0.4,0.4"/>
            <g>
                <g>
                    <path class="st3" d="M914,24.4c-4.2-1.8-9,0.2-10.7,4.4L898.1,41c-0.7,1.7,0.1,3.6,1.8,4.3l9.1,3.8c1.7,0.7,3.6-0.1,4.3-1.8
            l5.1-12.1C920.1,31,918.2,26.2,914,24.4z M894.9,48.1c-8.4,0.8-14.2,6.1-13.1,11.8c0.4,1.9,1.4,3.6,3,5l-10.3,41.2l0.6,0.2
            c-0.3,0.6-0.4,1.3-0.3,2.3c0.8,6.7,7.7,11.2,14.4,8.8c2.8-1,5-3.2,6.1-5.8l0,0l0-0.1c0.4-1,0.7-2,0.8-3.1l10.5-41.6
            c4-2.2,6.2-5.8,5.5-9.5C910.9,51.4,903.3,47.3,894.9,48.1z"/>
                </g>
            </g>
            <path class="st3" d="M917.7,16.3c0.4,0.2,0.6,0.7,0.4,1.1l-3.8,9.1c-0.2,0.4-0.7,0.6-1.1,0.4c-0.4-0.2-0.6-0.7-0.4-1.1l3.8-9.1
    C916.8,16.3,917.3,16.1,917.7,16.3z"/>
            <rect x="912.5" y="76.3" transform="matrix(0.9275 0.3738 -0.3738 0.9275 96.8457 -336.614)" class="st3" width="7.4" height="10.2"/>
            <polygon class="st3" points="924.8,181.5 913.5,185.8 902.8,147.9 909.6,145 "/>
            <polygon class="st3" points="855.1,185.8 843.1,185.8 869.1,147.7 877.5,148.2 "/>
            <polygon class="st3" points="933.7,178.8 916.1,184.7 916.1,178.8 933.1,177.1 "/>
            <polygon class="st3" points="866.3,185.8 845.9,185.8 849.6,180.2 866.3,183.9 "/>
            <rect x="837" y="128.6" transform="matrix(-1.595151e-02 0.9999 -0.9999 -1.595151e-02 987.7933 -704.7778)" class="st3" width="7.4" height="10.2"/>
            <rect x="876.7" y="53.4" transform="matrix(0.7271 0.6865 -0.6865 0.7271 283.0999 -588.0048)" class="st3" width="9" height="17.4"/>
            <rect x="905.6" y="54.4" transform="matrix(0.6716 -0.7409 0.7409 0.6716 254.545 693.9992)" class="st3" width="9" height="11"/>
            <circle class="st2" cx="916.9" cy="36.7" r="2.3"/>
            <circle class="st2" cx="910.4" cy="34.3" r="2.3"/>
            <ellipse class="st2" cx="918.9" cy="72.3" rx="4.3" ry="3.5"/>
            <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="908.1178" y1="9.176802e-10" x2="908.1178" y2="72.46">
                <stop  offset="0" style="stop-color:#4DD685;stop-opacity:0"/>
                <stop  offset="1" style="stop-color:#4DD685;stop-opacity:0.6"/>
            </linearGradient>
            <path class="st4" d="M887.7,1.9L928.6,0l-6.5,71.5l-6.5,0.9L887.7,1.9z"/>
            <circle class="st2" cx="917.6" cy="16" r="1.6"/>
            <path class="st0" d="M843.9,137.6l-1.5-5.8c5.6-1.5,10.6-6.6,13.8-14.1c1.6-3.7,2.1-6.8,2.7-11.1c0.4-2.7,0.9-5.9,1.8-9.7
    c2.1-9.5,7.2-23.6,19.3-36.7l4.4,4.1c-11.2,12.1-15.9,25.1-17.8,33.9c-0.8,3.7-1.3,6.7-1.7,9.3c-0.7,4.4-1.3,8.2-3.1,12.6
    C857.7,129.3,851.2,135.7,843.9,137.6z"/>
            <path class="st3" d="M881.2,111.5"/>
            <rect x="873.7" y="110.9" transform="matrix(0.9808 0.1948 -0.1948 0.9808 42.101 -168.2446)" class="st3" width="5.6" height="38"/>
            <rect x="894.7" y="111.8" transform="matrix(0.9027 -0.4303 0.4303 0.9027 31.0672 398.9152)" class="st3" width="5.6" height="38"/>
            <ellipse class="st3" cx="906.3" cy="146.3" rx="6.5" ry="5.1"/>
            <ellipse class="st3" cx="872.8" cy="148.2" rx="6.5" ry="5.1"/>
            <path class="st2" d="M820.5,87.4L844,72.8l2,2.6L822.5,90L820.5,87.4z"/>
            <g>
                <g>
                    <path class="st3" d="M824.3,85.4c-0.5-0.6-0.8-0.5-0.8-0.5l-1.6,1.2c0,0-3.1,2.4-3.9,2.6c-0.7,0.2-1.8,1.5-1.8,1.5l-9.6,1.8h-4.8
            c0.1-0.7,0.2-1.5,0.2-2.3l4.6-3.2c0,0,1-0.4,1.5-0.4c0.6,0,1.4-0.4,2.3-0.8c1.1-0.5,1.8-0.6,1.6-0.3c-0.5,0.7,0.3,1.4,0.3,1.4
            l1-0.6l1,0.3l1.3-4l-0.4-0.7c0,0,0.7,0.1,2,0.4s1.6-0.8,1.6-0.8l-4.3-1.7l-4-0.4l-4.9,4.1l-4.5,1.7c-1.7-3.8-6.2-11.2-6.2-11.2
            l0.5-3.1l-1.9-2.4c0,0,0.8-1.3,1.4-1.4c1.5-0.3,0.8,0.3,3.4,0.7c1.4,0.2,2.1-1.4,2.1-1.4l0.9-0.7l0.3-1.2l0.7-0.5l0.2-1.1
            c0,0,1.8,0,1.7-0.8c-0.2-1.2-0.3-2.6-0.4-3.3c0-0.6,0.7-1.4,0.7-1.4s0.1-0.1,0.3-0.9c0.5,0.4,0.6,0.4,0.6,0.4s0.9-0.2,0.5-3.5
            c-0.1-1.5-0.7-2.7-1.4-3.5c-0.7-2-2.5-4.3-6.3-5.6c-3.4-1.2-5.7-0.4-7.3,0.7c-0.6-1.8-2.2-3.1-4.2-3.1c-2.5,0-4.5,2-4.5,4.5
            c0,2.4,1.9,4.3,4.2,4.4l-0.2,0.2c0,0-1.3,0.8-1.9,2.2c-2.3,5.1,2.5,6.9,2.8,7.2c0.8,0.9,0.2,2.6,0.2,2.6l-1.9-0.4
            c0,0-2.5,2.7-5.2,7c-2.7,4.4-2.1,17.2-2.1,17.2l-5.5,1.4l-6.2,6.9l1.4,4.8c0,0,0,1.6,0,5.5c0.1,10.7,10.3,6.9,10.3,6.9l-1.9,8.2
            l1.9,0.7c0,0,3.5,7.2,4.1,8.2c0.6,1,2.6,12.7,2.6,12.7l0.9,0.3v3.4c0,0-0.7,0.6-5.5,2.4c-5.8,2.1-11.4,9.3-11.4,9.3l-7.2,5.4
            c0,0-4.4,0.1-5.5,0c-1.1-0.1-1.4,1.4-1.4,1.4l0.7,12.4c0,0,2,7.3,3.4,6.9c1.5-0.4,1.4-5.5,1.4-5.5l3.3-8.1c0,0,3.3-2.6,10.5-7.9
            s16.7-8.6,16.7-8.6s-0.9,5.1-0.9,7.4c0.1,10.1,2.2,16.2,1.1,19.7c-0.1,0.3-1.1,1.6-1.4,2.6s-0.4,3.1-0.4,3.1h21c0,0,1-2.4-4.1-2.9
            c-2.6-0.3-7-3.9-7-3.9h-0.9c0,0-0.5-4.8-0.5-7.6c0-2.8,1.9-17.9,1.9-17.9l1.4-7.6c0,0,0.7-7.2,0.7-13s-1.4-13.7-1.4-13.7l4.1-2.1
            c0,0-0.1-0.4-1.4-2.7c-1.4-2.5-2.4-9.3-2.7-13.9c2.4-0.1,4.3-0.2,4.4-0.3c0.4-0.1,1.2-2.6,1.2-2.6l10-2.7c0,0,0.5,0.1,1.5,0.3
            s2.5-0.5,3.8-1c1.3-0.5,1.5-1.8,2-2.8c0.6-1,1.7-3.3,1.7-3.3S826,87.4,824.3,85.4z"/>
                </g>
            </g>
            <linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="876.0076" y1="4.0596" x2="911.1841" y2="64.9871" gradientTransform="matrix(0.9968 -7.966986e-02 7.966962e-02 0.9968 -78.7881 86.2383)">
                <stop  offset="0" style="stop-color:#4DD685;stop-opacity:0"/>
                <stop  offset="1" style="stop-color:#4DD685;stop-opacity:0.6"/>
            </linearGradient>
            <path class="st5" d="M771.1,36.8l47.6-32.1l21.9,69.6L822.1,86L771.1,36.8z"/>
</svg>

        <div class="footer-bottom colors-p">
            <div class="text-center"> Made with <i class="fa fa-heart-o" aria-hidden="true" style="color: red;"></i> - by <a href="http://shobarjonnoweb.com/" target="_blank"><strong style="color: #fd7979;"> Shobar Jonno Web </strong></a> </div>
            <div class="text-center scroll-in-animation" data-animation="fadeInUp"> Powered by - <a href="http://agvcorp.biz/" target="_blank"> <strong style="color: #f92828;">Asian Global Ventures Bangladesh Company Limited</strong> </a> </div>
            <div id="counterContainer" class="text-center" style="font-size:10px;"><a id="online-visitors-counter"></a></div>
        </div>
    </div>
</footer>

<script type="text/javascript" src="ovc/counter.js"></script>
<!--<script type="text/javascript" src="assets/js/twin.min.46285f855d909ed7.46285f855d909ed7.46285f855d909ed7.46285f855d909ed7.js?v=2.0"></script>-->
<!--<script type="text/javascript" src="assets/js/smooth.a5d2b4b7a2061895.a5d2b4b7a2061895.a5d2b4b7a2061895.a5d2b4b7a2061895.js?v=2.0"></script>-->
<!--<script type="text/javascript" src="assets/js/bezierCurve.a8d641966141e751.a8d641966141e751.a8d641966141e751.a8d641966141e751.js?v=2.0"></script>-->
