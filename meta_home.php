<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<meta charset="UTF-8">
<meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no">
<meta name="generator" content="Digital Pondith"/>
<meta name="twitter:card" content="Digital Pondith" />
<meta name="twitter:site" content="@Digital Pondith" />
<meta name="twitter:creator" content="@Digital Pondith" />
<meta property="og:title" content="Digital Pondith"/>
<meta property="og:description" content="Digital Pondith provides a coherent approach to all your digital marketing media requirements. Advertising/Marketing of the product, application, games and services through the right channels to the customers that will affect an increase in demand."/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="http://digitalpondith.com"/>
<meta property="og:image" content="http://digitalpondith.com/assets/preset-images/bg_menu_big.jpg"/>
<meta property="og:image" content="http://digitalpondith.com/assets/images/bg_service_details.png"/>
<meta property="og:image" content="http://digitalpondith.com/assets/images/smm_bannner.png"/>