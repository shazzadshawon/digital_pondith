<!DOCTYPE html>
<html lang="en-US" class="state2 page-is-gated scroll-bar site-decoration-b" data-skrollex-config="{isInitColorPanel: false, isCustomizer: false, adminUrl: &#039;http://digitalpondith.com/&#039;, ajaxUrl: &#039;http://digitalpondith.com/&#039;, homeUri: &#039;http://digitalpondith.com/&#039;, themeUri: &#039;http://digitalpondith.com/&#039;, permalink: &#039;http://digitalpondith.com/&#039;, colors: &#039;colors-preset-mary.css&#039;}">


    <!--Blue Dark - #75cecb
        Blue Light - #90fffa
        Yellow Light - #ffbb42
    -->

    <!-- HEAD -->
    <head>
        <title>Digital Pondith</title>
        <?php require_once('head.php'); ?>
    </head>
    <!-- //HEAD -->

    <body id="skrollex-body" class="home page-template page-template-builder page-template-builder-php page page-id-26 no-colors-label background-k body-header-logo-left">
    <!--    PRELOADER    -->
    <?php require('preloader.php'); ?>
    <!--    //PRELOADER    -->


    <div class="page-border  heading top colors-a main-navigation"></div>
    <div class="page-border  heading bottom colors-a main-navigation">
        <a href="#top" class="to-top hover-effect">To <span>Top</span></a>
        <a href="#scroll-down" class="scroll-down hover-effect">Scroll <span>Down</span></a>

        <a class="border_hotline" href="contact_us.php">Hotline  +88-02-985-0613/4</a>
        <a class="border_mail" href="mailTo: contact@agvcorp.com">contact@agvcorp.com</a>
    </div>
    <div class="page-border  heading left colors-a main-navigation border-pad"></div>
    <div class="page-border  heading right colors-a main-navigation border-pad"></div>
    <div class="page-border  heading left colors-a main-navigation">
        <!--Side Border Social Links-->
        <?php include('side_border_socialLink.php'); ?>
        <!--Side Border Social Links-->
    </div>
    <div class="page-border heading right colors-a main-navigation"></div>

    <!--    TOP HEADER-->
    <?php include('top_header.php'); ?>

    <!--    RIGHT SIDE DOT NAVIGATOR-->
    <?php include('top_menu_mobile.php'); ?>


    <section class="wrapper-site">

        <!--        MAIN MENU SECTION-->
        <?php include('main_menu.php'); ?>

        <section id="wrapper-content" class="wrapper-content">
            <!--            LANDING SECTION-->
            <?php include('landing_section.php'); ?>

            <!--WHO WE ARE-->
            <?php include('about_us.php'); ?>

            <!--HOW WE WORK-->
            <?php include('how_we_work.php'); ?>

            <!--SERVICES-->
            <!--            --><?php //include('services.php'); ?>

            <!--WHY CHOOSE US-->
            <!--            --><?php //include('why_choose_us.php'); ?>

            <!--LOWER IMAGE SLIDER SLOGAN-->
            <!--            --><?php //include('slogan.php') ?>

            <!--OUR WORK GALLERY-->
            <!--            --><?php //include('work_gallery.php'); ?>

            <!--PROCESS-->
            <?php include('DM_trailler.php'); ?>

            <!--OUR SKILLS-->
            <!--            --><?php //include('our_skills.php'); ?>


            <!--        LOWER IMAGE SLIDER SLOGAN-->
            <?php include('slogan.php') ?>

            <!--SERVICES-->
            <?php include('services.php'); ?>

            <!--OUR NUMBERS-->
            <?php include('our_number.php'); ?>

            <!--CONTACT US-->
<!--            --><?php //include('contact.php'); ?>
        </section>

        <!--FOOTER-->
        <?php include('footer.php'); ?>

    </section>

    <!--        <script type='text/javascript' src='assets/js/digitalPondith_up.min.1edb9f4ef812f737.1edb9f4ef812f737.1edb9f4ef812f737.js?v=1.0'></script>-->
    <!--        <script type="text/javascript" src="assets/js/digitalPondith_down.min.411e408751f3b106.411e408751f3b106.411e408751f3b106.js?v=2.0"></script>-->
    <!--        <script type="text/javascript" src="assets/js/landing_section.590960613e655c83.590960613e655c83.js?v=2.0"></script>-->
    <script type="text/javascript" src="assets/js/digitalPondith.min.0869ef84014f44c2.0869ef84014f44c2.js?v=2.0"></script>


    <!--        <script type="text/javascript" src="assets/digitalpondith_2.a3fcfdca7e06764c.a3fcfdca7e06764c.a3fcfdca7e06764c.a3fcfdca7e06764c.js?v=2.0"></script>-->
    <!--        <script type="text/javascript" src="assets/sitescript.bb4604cd95833e56.bb4604cd95833e56.bb4604cd95833e56.bb4604cd95833e56.js?v=2.0"></script>-->
    <!--        <script type="text/javascript" src="assets/js/pera_removal.25fd0c2a70df58ae.25fd0c2a70df58ae.25fd0c2a70df58ae.25fd0c2a70df58ae.js?v=2.0"></script>-->
    <!--    <script type='text/javascript' src='plugins/contact-form-7/includes/js/jquery.form.mind03d.f448c593c242d134.js'></script>-->
    <script type='text/javascript'>
        /* <![CDATA[ */
        var _wpcf7 = {"recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}};
        /* ]]> */
    </script>

    <!--    <script type='text/javascript' src='plugins/contact-form-7/includes/js/scripts4906.b64315176b0dd79e.js'></script>-->
    <!--    <script type='text/javascript' src='assets/lib/bower_components/jquery-cookie/jquery.cookief4ef.16387a76475a91af.16387a76475a91af.16387a76475a91af.16387a76475a91af.js?ver=27e1af5b410de903825f58485ec0fd40'></script>-->
    <!--    <script type='text/javascript' src='assets/js/imagesloaded.min55a0.d0c2c0d7e37652e6.d0c2c0d7e37652e6.d0c2c0d7e37652e6.d0c2c0d7e37652e6.js?ver=3.2.0'></script>-->
    <!--    <script type='text/javascript' src='assets/js/masonry.mind617.5420b6516c14245b.5420b6516c14245b.5420b6516c14245b.5420b6516c14245b.js?ver=3.3.2'></script>-->
    <!--    <script type='text/javascript' src='assets/lib/bower_components/less/dist/less.minf4ef.ed926034cc41d320.ed926034cc41d320.ed926034cc41d320.js.ed926034cc41d320.js?ver=27e1af5b410de903825f58485ec0fd40'></script>-->
    <!--    <script type='text/javascript' src='assets/lib/tween/tween.minf4ef.0333be8d916c9e91.0333be8d916c9e91.0333be8d916c9e91.0333be8d916c9e91.js?ver=27e1af5b410de903825f58485ec0fd40'></script>-->
    <!--    <script type='text/javascript' src='assets/lib/bower_components/modernizr/modernizrf4ef.e741a78646adb336.e741a78646adb336.e741a78646adb336.e741a78646adb336.js?ver=27e1af5b410de903825f58485ec0fd40'></script>-->
    <!--    <script type='text/javascript' src='assets/lib/bower_components/vimeo-player-js/dist/player.minf4ef.2efb021ec4088975.2efb021ec4088975.2efb021ec4088975.2efb021ec4088975.js?ver=27e1af5b410de903825f58485ec0fd40'></script>-->
    <!--    <script type='text/javascript' src='assets/lib/bower_components/snap.svg/dist/snap.svg-minf4ef.b92f25bc9c56c69a.b92f25bc9c56c69a.b92f25bc9c56c69a.b92f25bc9c56c69a.js?ver=27e1af5b410de903825f58485ec0fd40'></script>-->
    <!--    <script type='text/javascript' src='assets/lib/bower_components/minicolors/jquery.minicolors.minf4ef.9e5315b8ba5ce983.9e5315b8ba5ce983.9e5315b8ba5ce983.9e5315b8ba5ce983.js?ver=27e1af5b410de903825f58485ec0fd40'></script>-->
    <!--    <script type='text/javascript' src='assets/lib/bower_components/textillate/assets/jquery.letteringf4ef.4434237a4aaf85c2.4434237a4aaf85c2.4434237a4aaf85c2.4434237a4aaf85c2.js?ver=27e1af5b410de903825f58485ec0fd40'></script>-->
    <!--    <script type='text/javascript' src='assets/lib/bower_components/textillate/assets/jquery.fittextf4ef.17871d671095bc41.17871d671095bc41.17871d671095bc41.17871d671095bc41.js?ver=27e1af5b410de903825f58485ec0fd40'></script>-->
    <!--    <script type='text/javascript' src='assets/lib/bower_components/textillate/jquery.textillatef4ef.7bce45002ec5ec6d.7bce45002ec5ec6d.7bce45002ec5ec6d.7bce45002ec5ec6d.js?ver=27e1af5b410de903825f58485ec0fd40'></script>-->
    <!--    <script type='text/javascript' src='assets/lib/stringencoders-v3.10.3/javascript/base64f4ef.a3f04aa72c4525a4.a3f04aa72c4525a4.a3f04aa72c4525a4.a3f04aa72c4525a4.js?ver=27e1af5b410de903825f58485ec0fd40'></script>-->
    <!--    <script type='text/javascript' src='assets/js/script-bundle.minf4ef.e48abb28eac1a191.e48abb28eac1a191.e48abb28eac1a191.e48abb28eac1a191.js?ver=27e1af5b410de903825f58485ec0fd40'></script>-->
    <!--    <script type='text/javascript' src='assets/js/wp-embed.min66f2.5a03f97cc479b9f5.5a03f97cc479b9f5.5a03f97cc479b9f5.5a03f97cc479b9f5.js?ver=4.7.5'></script>-->
    <!--    <script type='text/javascript' src='plugins/easy-fancybox/fancybox/jquery.fancybox-1.3.8.min82fc.5fec00649cf6e3fb.js'></script>-->
    <!--    <script type='text/javascript' src='plugins/easy-fancybox/js/jquery.easing.min9e1e.508803480aeda9d6.js'></script>-->
    <!--    <script type='text/javascript' src='plugins/easy-fancybox/js/jquery.mousewheel.min4830.639d1c35a685d111.js'></script>-->
    <!--Inline 2-->
    <script type="text/javascript">
        jQuery(document).on('ready post-load', function () {
            //jQuery('.nofancybox,a.pin-it-button,a[href*="pinterest.com/pin/create/button"]').addClass('nolightbox');
        });
        //        jQuery(document).on('ready post-load', easy_fancybox_handler);
        //        jQuery(document).on('ready', easy_fancybox_auto);
    </script>

    </body>
</html>