<style type="text/css">
    .colors-b.main-navigation *:not(span) {
        color: #7d7d7d !important;
    }

    .colors-b .ext-nav-toggle span, .colors-b .ext-nav-toggle span:before, .colors-b .ext-nav-toggle span:after {
        background-color: #7d7d7d !important;
    }
</style>
<section id="top-nav" class="page-transition main-navigation heading colors-a top-nav-logo-left"
         data-colors-1="colors-c" data-colors-2="colors-b">
    <div class="layout-boxed top-nav-inner clearfix">
        <span class="menu-toggle ext-nav-toggle" data-target=".ext-nav"><span></span></span>
        <nav class="nav nav-horizontal">
            <ul id="menu-skrollex-menu-1" class="menu">

                <!--HOME-->
                <li id="menu-item-796" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-796">
                    <a href="index.php">Home</a>

                </li>
<!--                <li id="menu-item-797" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-797">-->
<!--                    <a href="about_us_page.php">About</a></li>-->

                <!--SOLUTIONS-->
                <li id="menu-item-806" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-806">
                    <a href="index.php#solutions">Solutions</a>
                    <ul class="sub-menu">
                        <li>
                            <a href="solution_categoryPage.php">Industry Wise Solution</a>
                            <ul class="sub-menu">
                                <li><a href="solution_details_page.php">Solution Name</a></li>
                                <li><a href="solution_details_page.php">Solution Name</a></li>
                                <li><a href="solution_details_page.php">Solution Name</a></li>
                                <li><a href="solution_details_page.php">Solution Name</a></li>
                                <li><a href="solution_details_page.php">Solution Name</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="solution_categoryPage.php">Solution Category</a>
                            <ul class="sub-menu">
                                <li><a href="solution_details_page.php">Solution Name</a></li>
                                <li><a href="solution_details_page.php">Solution Name</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="solution_categoryPage.php">Solution Category</a>
                            <ul class="sub-menu">
                                <li><a href="solution_details_page.php">Solution Name</a></li>
                                <li><a href="solution_details_page.php">Solution Name</a></li>
                                <li><a href="solution_details_page.php">Solution Name</a></li>
                                <li><a href="solution_details_page.php">Solution Name</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="solution_categoryPage.php">Solution Category</a>
                            <ul class="sub-menu">
                                <li><a href="solution_details_page.php">Solution Name</a></li>
                            </ul>
                        </li>
                        <li><a href="solution_categoryPage.php">Solution Category</a></li>
                    </ul>
                </li>

                <!--SERVICES-->
                <li id="menu-item-807" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-807">
                    <a href="index.php#h_services">SERVICES</a>
                    <ul class="sub-menu">
                        <li id="menu-item-804" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-804">
                            <a href="service_category_page.php">Search Engine optimisation</a>
                            <ul class="sub-menu">
                                <li><a href="service_details_page.php">Service Name</a></li>
                                <li><a href="service_details_page.php">Service Name</a></li>
                                <li><a href="service_details_page.php">Service Name</a></li>
                                <li><a href="service_details_page.php">Service Name</a></li>
                                <li><a href="service_details_page.php">Service Name</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-805" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-805">
                            <a href="service_category_page.php">Search engine Marketing</a>
                            <ul class="sub-menu">
                                <li><a href="service_details_page.php">Service Name</a></li>
                                <li><a href="service_details_page.php">Service Name</a></li>
                                <li><a href="service_details_page.php">Service Name</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-802" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-802">
                            <a href="service_category_page.php">Social Media Marketing</a>
                            <ul class="sub-menu">
                                <li><a href="service_details_page.php">Facebook marketing</a></li>
                                <li><a href="service_details_page.php">Twitter Marketing</a></li>
                                <li><a href="service_details_page.php">Printerest Marketing</a></li>
                                <li><a href="service_details_page.php">LinkedIn Marketing</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-802" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-802"><a href="service_category_page.php">Email Marketing</a></li>
                        <li id="menu-item-802" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-802"><a href="service_category_page.php">SMS Marketing</a></li>
                        <li id="menu-item-802" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-802"><a href="service_category_page.php">Content Marketing</a></li>
                        <li id="menu-item-802" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-802"><a href="service_category_page.php">Video Marketing</a>
                            <ul class="sub-menu">
                                <li><a href="service_details_page.php">Youtube Marketing</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-802" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-802"><a href="service_category_page.php">Affiliate Marketing</a></li>
                    </ul>
                </li>

                <!--CLIENTS-->
                <li id="menu-item-800" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-800">
                    <a href="portfolio_page.php">Clients</a>
                </li>
                <!--<li id="menu-item-798" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-798">
                    <a href="index.php#team">Team</a></li>-->
                <!--<li id="menu-item-799" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-799">
                    <a href="index.php#services">Services</a></li>-->
<!--                <li id="menu-item-802" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-802">-->
<!--                    <a href="blog.php">Blog</a></li>-->

                <li id="menu-item-803" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-803">
                    <a href="blog.php">Blog</a>
                </li>

                <li id="menu-item-803" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-803">
                    <a href="letsDiscuss.php">Let's Discuss</a>
                </li>

                <li id="menu-item-803" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-803">
                    <a href="contact_us.php">Contact</a>
                </li>
            </ul>
            <a class="responsive-nav" data-toggle="#off-canvas-right" data-toggle-class="open">
                <span class="l-menu"></span>
            </a></nav>
        <div class="logo">
            <div class="site-description">
                <h3 class="sitename sitetitle"><a href="index.php"> <img id="dpLogoImg" onError="this.onerror=null;this.src='assets/images/initialHeaderLogo_a.png';" src="assets/images/initialHeaderLogo_a.png" alt="Digital|Pondith"> </a></h3>
<!--                <h3 class="sitename sitetitle"><a href="http://digitalpondith.com">Digital<span>Pondith</span></a></h3>-->
            </div>
        </div>
    </div>
</section>