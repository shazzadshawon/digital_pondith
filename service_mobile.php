<!DOCTYPE html>
<html lang="en-US" class="state2 page-is-gated scroll-bar site-decoration-b" data-skrollex-config="{isInitColorPanel: false, isCustomizer: false, adminUrl: &#039;http://skrollex2.x40.ru/mary/wp-admin/&#039;, ajaxUrl: &#039;http://skrollex2.x40.ru/mary/wp-admin/admin-ajax.php&#039;, homeUri: &#039;http://skrollex2.x40.ru/mary&#039;, themeUri: &#039;http://skrollex2.x40.ru/mary/&#039;, permalink: &#039;http://skrollex2.x40.ru/mary&#039;, colors: &#039;colors-preset-mary.css&#039;}">
    <!--HEAD-->
    <head>
        <title>MM &#8211; Digital Pondith</title>
        <?php require('head.php'); ?>
        <!--ADDITIONAL STYLES-->
        <link rel="stylesheet" href="assets/css/service.9300b0c83579906f.9300b0c83579906f.9300b0c83579906f.css?v=2.0" type="text/css" media="screen" />
        <!--//ADDITIONAL STYLES-->
    </head>
    <!--//HEAD-->

    <body id="skrollex-body" class="blog no-colors-label background-k body-header-logo-left">
        <!--    PRELOADER    -->
        <?php require('preloader.php');?>
        <!--    //PRELOADER    -->

        <div class="page-border  heading top colors-a main-navigation"></div>
        <div class="page-border  heading bottom colors-a main-navigation"><a href="#top" class="to-top hover-effect">To <span>Top</span></a><a href="#scroll-down" class="scroll-down hover-effect">Scroll <span>Down</span></a></div>
        <div class="page-border  heading left colors-a main-navigation border-pad"></div>
        <div class="page-border  heading right colors-a main-navigation border-pad"></div>
        <div class="page-border  heading left colors-a main-navigation">
            <!--Side Border Social Links-->
            <?php include('side_border_socialLink.php'); ?>
            <!--Side Border Social Links-->
        </div>
        <div class="page-border  heading right colors-a main-navigation">
        <!--    <ul>-->
        <!--        <li><a href="#our_web"><i class="fa fa-users" aria-hidden="true"></i></a></li>-->
        <!--        <li><a href="#web_pricing"><i class="fa fa-ticket" aria-hidden="true"></i></a></li>-->
        <!--    </ul>-->
        </div>

        <!--    TOP HEADER-->
        <?php include('top_header.php'); ?>

        <!--    RIGHT SIDE DOT NAVIGATOR-->
        <?php include('top_menu_mobile.php'); ?>
        <section class="wrapper-site">

            <!--        MAIN MENU SECTION-->
            <?php include('main_menu.php'); ?>

            <section id="wrapper-content" class="wrapper-content">
                <div class="view x40-widget widget  " id="layers-widget-skrollex-section-2">
                    <div data-src="assets/images/service_mm.275c154172902b13.275c154172902b13.275c154172902b13.jpg" data-alt="" class="bg-holder"></div>
                    <div data-src="assets/images/service_mm.275c154172902b13.275c154172902b13.275c154172902b13.jpg" data-alt="" class="bg-holder"></div>
                    <div class="fg colors-u ">
                        <div class="layout-boxed section-top"><h3 class="heading-section-title"><span>Mobile</span> Marketing</h3>
        <!--                    <p class="header-details"><span>Search Engine Optimization</span> Marketing</p>-->
        <!--                    <p class="header-caption">We offer a proper incorporated approach to <span>SEO</span> marketing which looks at the bigger picture to go beyond businesses needs. The two popular way of search engine marketing are search engine optimization (SEO) &amp; pay-per-click (PPC) advertising.</p>-->
                        </div> </div>
                </div>
                <img class="bg" src="assets/images/bg_service_details.fc0dcb00150d0032.fc0dcb00150d0032.b83e3d25011828d6.png" alt=""/>
                <img class="bg" src="assets/images/bg_service_details.fc0dcb00150d0032.fc0dcb00150d0032.b83e3d25011828d6.png" alt=""/>
                <div class="default-page-wrapper background-v">

                    <!--COMMING SOON PAGE-->
                    <?php include('comming_soon.php'); ?>
                    <!--COMMING SOON PAGE-->

                </div>
            </section>

            <!--FOOTER-->
            <?php include('footer.php'); ?>

        </section>


        <?php require('javacsript.php'); ?>

    </body>
</html>

