<div class="view x40-widget widget  " id="layers-widget-skrollex-section-15">
    <div id="contact" class="fg colors-d  full-size">
        <div class="layout-boxed section-top"><h3 class="heading-section-title">Keep In Touch</h3>
            <p class="header-details"> <h1 style="color: #d29934; text-align: center">HELLO@DIGITALPONDITH.COM </h1> </p>
        </div>
        <div class="section-cols layout-boxed">
            <div class="pure-g">
                <div class="layers-widget-skrollex-section-55e76a38d8c72733306009 pure-u-1 pure-u-lg-12-24  col-padding scroll-in-animation"
                     data-animation="fadeInLeft">
                    <p class="text-big text-right">Mobile.: <span>01678504912 / 3 / 4</span></p>
                    <p class="text-big text-right">Tel.: <span>+88-029850613 / 4</span></p>
                    <p class="text-big text-right">HOUSE - 3, ROAD - 28, BLOCK - K <br/>
                        BANANI, DHAKA-1213, BANGLADESH</p>
                    <p class="text-right"><a href="mailto:info@ouraddress.com">info@digitalpondith.com</a><br/>
                        <a href="http://www.digitalpondith.com/">www.digitalpondith.com</a></p>
                    <p class="text-right"><a href="https://www.facebook.com/" target="_blank"><i
                                class="fa fa-2x fa-facebook"></i></a> <a href="https://twitter.com/"><i
                                class="fa fa-2x fa-twitter"></i></a> <a href="https://soundcloud.com/"
                                                                        target="_blank"><i
                                class="fa fa-2x fa-soundcloud"></i></a></p>

                    <?php
                    if ($mail_flag == 1){
                        echo "<div class='wpcf7-response-output text-big text-right' style='color: green; font-weight: 700;'>Your Mail Successfully Sent to contact@digitalpondith.com</div>";
                    } else if ($mail_flag == 2){
                        echo "<div class='wpcf7-response-output text-big text-right'></div>";
                    } else if ($mail_flag == 0){
                        echo "<div class='wpcf7-response-output text-big text-right' style='color: red; font-weight: 700;'>Server Overloaded! Message sending failed.</div>";
                    }
                    ?>
                </div>
                <div class="layers-widget-skrollex-section-55e78dea6c13d201303792 pure-u-1 pure-u-lg-12-24  col-padding scroll-in-animation"
                     data-animation="fadeInRight">
                    <div role="form" class="wpcf7" id="wpcf7-f839-o1" lang="en-US" dir="ltr">
                        <div class="screen-reader-response"></div>
                        <form action="contact_us.php" method="GET"
                              class="wpcf7-form" novalidate="novalidate">
                            <div style="display: none;">
                                <input type="hidden" name="_wpcf7" value="839"/>
                                <input type="hidden" name="_wpcf7_version" value="4.7"/>
                                <input type="hidden" name="_wpcf7_locale" value="en_US"/>
                                <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f839-o1"/>
                                <input type="hidden" name="_wpnonce" value="e47f824e7a"/>
                            </div>
                            <div><input type="hidden" name="your-subject" value="Contact Form"></div>
                            <div class="pure-g contact-form-content">
                                <div class="pure-u-1 pure-u-sm-12-24"><span
                                        class="wpcf7-form-control-wrap your-name"><input type="text"
                                                                                         name="contact-name"
                                                                                         size="40"
                                                                                         class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                         aria-required="true"
                                                                                         aria-invalid="false"
                                                                                         placeholder="Name"/></span>
                                </div>
                                <div class="pure-u-1 pure-u-sm-12-24"><span
                                        class="wpcf7-form-control-wrap your-email"><input type="email"
                                                                                          name="contact-email"
                                                                                          size="40"
                                                                                          class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                          aria-required="true"
                                                                                          aria-invalid="false"
                                                                                          placeholder="Email"/></span>
                                </div>
                                <div class="pure-u-1"><span
                                        class="wpcf7-form-control-wrap your-message"><textarea
                                            name="contact-message" cols="40" rows="10"
                                            class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"
                                            placeholder="Message"></textarea></span></div>
                                <div class="pure-u-1"><input type="submit" value="Send"
                                                             class="wpcf7-form-control wpcf7-submit button"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="layers-widget-skrollex-section-55e8d6e813a15761004876 pure-u-1 pure-u-lg-24-24  col-padding">
                    <p class="text-center">
                        <span class="google-map-button">
                            <a class="map-open button background-g border-normal-g heading-g" data-map-overlay=".map-overlay" href="#">Locate Us On Map</a>
                            <span
                                class="map-canvas" data-latitude="23.7944022" data-longitude="90.4090476" data-zoom="17">
                                <span class="map-marker" data-latitude="23.7944022" data-longitude="90.4090476" data-text="Digital Pondith"></span>
<!--                                <span class="map-marker" data-latitude="23.794402" data-longitude="90.4068593" data-text="Our sales office"></span>-->
                            </span>
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>


<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3ZisuuEiULh72jtyAv5TBfEnt0HMnm0s" type="text/javascript"></script>
<?php require('javacsript.php'); ?>