<div class="view x40-widget widget   text-bg" id="layers-widget-skrollex-section-11"
     data-text-effect-selector="h1,h2,h3" data-text-effect="effect-a-animated">
<!--    <div data-src="http://skrollex2.x40.ru/mary/wp-content/uploads/sites/42/2015/11/bg-stocksnap-219FB68281.jpg" data-alt="" class="bg-holder"></div>-->
<!--    <div data-src="http://skrollex2.x40.ru/mary/wp-content/uploads/sites/42/2015/11/bg-stocksnap-219FB68281.jpg" data-alt="" class="bg-holder"></div>-->
    <div id="work" class="fg">
<!--        <div class="layout-boxed section-top"><h3 class="heading-section-title">Our Work</h3>-->
<!--            <p class="header-details"><span>Some of our Recent</span> Projects</p>-->
<!--            <p class="lead">Digital Pondith is a Digital marketing agency helps business to increase targeting consumer reliability and find new customers online. Here, you will find a collection of our expert work. Take a look at some of the results we have delivered.</p>-->
<!--        </div>-->
        <ul class="filter">
            <li><a class="hover-effect" data-group="all" href="#">All</a></li>
            <li><a class="hover-effect" data-group="web" href="#">Web Marketing</a></li>
            <li><a class="hover-effect" data-group="smm" href="#">Social Media Marketing</a></li>
            <li><a class="hover-effect" data-group="mobile" href="#">Mobile Marketing</a></li>
        </ul>
        <div class="section-cols layout-fullwidth gallery-grd">
<!--        <div class="section-cols layout-boxed gallery-grd">-->
            <div class="masonry-grd">
                <div class="pure-g">

                    <!--                    WEB-->
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="web">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/web/1.82e14128aa3bc8c7.82e14128aa3bc8c7.82e14128aa3bc8c7.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center"><span>yale</span> appliance</h4>
                                        <p class="col-details text-center">https://www.yaleappliance.com</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                <span>yale</span> appliance</h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/web/1.82e14128aa3bc8c7.82e14128aa3bc8c7.82e14128aa3bc8c7.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="https://www.yaleappliance.com/" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="web">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/web/2.87bc9ab08ce98b62.87bc9ab08ce98b62.87bc9ab08ce98b62.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center"><span>eastern</span> housing</h4>
                                        <p class="col-details text-center">http://www.easternhousing.com</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                <span>eastern</span> housing</h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/web/2.87bc9ab08ce98b62.87bc9ab08ce98b62.87bc9ab08ce98b62.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="http://www.easternhousing.com/" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="web">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/web/3.44e899e7b2acc093.44e899e7b2acc093.44e899e7b2acc093.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center"><span>shops</span>evenoaks</h4>
                                        <p class="col-details text-center">http://www.shopsevenoaks.com</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                <span>shops</span>evenoaks</h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/web/3.44e899e7b2acc093.44e899e7b2acc093.44e899e7b2acc093.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="http://www.shopsevenoaks.com/" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="web">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/web/4.5d8ee721cfeff25f.5d8ee721cfeff25f.5d8ee721cfeff25f.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center"><span>cascade</span> aerospace</h4>
                                        <p class="col-details text-center">http://www.cascadeaerospace.com</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                <span>cascade</span> aerospace</h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide" web-hold-img="assets/data/portfolio/smm/4.94ec54985fde65de.94ec54985fde65de.94ec54985fde65de.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="http://www.cascadeaerospace.com/" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="web">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/web/5.1304ebcca2e8ecc9.1304ebcca2e8ecc9.1304ebcca2e8ecc9.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center">wrap<span> guys</span></h4>
                                        <p class="col-details text-center">https://wrapguys.com</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                wrap<span> guys</span></h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/web/5.1304ebcca2e8ecc9.1304ebcca2e8ecc9.1304ebcca2e8ecc9.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="https://wrapguys.com/" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="web">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/web/6.6e07ea88aea8c391.6e07ea88aea8c391.6e07ea88aea8c391.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center"> <span>duraramp</span></h4>
                                        <p class="col-details text-center">http://www.duraramp.com</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                <span>duraramp</span></h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/web/6.6e07ea88aea8c391.6e07ea88aea8c391.6e07ea88aea8c391.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="http://www.duraramp.com/" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="web">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/web/7.f734379da48d8791.f734379da48d8791.f734379da48d8791.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center"><span>fvad</span></h4>
                                        <p class="col-details text-center">http://fvad.ca</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                <span>fvad</span></h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/web/7.f734379da48d8791.f734379da48d8791.f734379da48d8791.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="http://fvad.ca/" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="web">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/web/8.1f839d265f536c9e.1f839d265f536c9e.1f839d265f536c9e.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center"><span>bc </span>greenhouses</h4>
                                        <p class="col-details text-center">https://www.bcgreenhouses.com</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                <span>bc </span>greenhouses</h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/web/8.1f839d265f536c9e.1f839d265f536c9e.1f839d265f536c9e.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="https://www.bcgreenhouses.com/" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="web">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/web/9.a3c3f76a1243dad9.a3c3f76a1243dad9.a3c3f76a1243dad9.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center">snow <span> crewtracker</span></h4>
                                        <p class="col-details text-center">http://www.snowcrewtracker.com</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                snow <span> crewtracker</span></h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide" web-hold-img="assets/data/portfolio/smm/9.f633c9f763019cb0.f633c9f763019cb0.f633c9f763019cb0.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="http://www.snowcrewtracker.com/" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="web">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/web/10.a4da3d019614a854.a4da3d019614a854.a4da3d019614a854.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center"> <span>cd </span> logistics</h4>
                                        <p class="col-details text-center">https://www.cdlogistics.ca</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                <span>cd </span> logistics</h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/web/10.a4da3d019614a854.a4da3d019614a854.a4da3d019614a854.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="https://www.cdlogistics.ca/" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--                    //WEB-->


<!--                    SMM-->
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="smm">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/smm/1.fc1ef745ac9fb7e3.fc1ef745ac9fb7e3.fc1ef745ac9fb7e3.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center">need<span>24</span>shop</h4>
                                        <p class="col-details text-center">www.facebook.com/need24shop/?fref=ts</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                Need<span>24</span>Shop</h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/smm/1.fc1ef745ac9fb7e3.fc1ef745ac9fb7e3.fc1ef745ac9fb7e3.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                    <a class="btn btn-info" href="https://www.facebook.com/need24shop/?fref=ts" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="smm">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/smm/2.333208416d001d3a.333208416d001d3a.333208416d001d3a.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center"><span>stickon</span> bd</h4>
                                        <p class="col-details text-center">www.facebook.com/stickon.bd</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                <span>stickon</span> bd</h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/smm/2.333208416d001d3a.333208416d001d3a.333208416d001d3a.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="https://www.facebook.com/stickon.bd/" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="smm">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/smm/3.814e5dbbb0a42615.814e5dbbb0a42615.814e5dbbb0a42615.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center"><span>dress</span> station</h4>
                                        <p class="col-details text-center">www.facebook.com/dress-station-1068273079927477</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                <span>dress</span> station</h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/smm/3.814e5dbbb0a42615.814e5dbbb0a42615.814e5dbbb0a42615.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="https://www.facebook.com/Dress-Station-1068273079927477/" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="smm">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/smm/4.94ec54985fde65de.94ec54985fde65de.94ec54985fde65de.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center"><span>kenakata</span> zone</h4>
                                        <p class="col-details text-center">https://www.facebook.com/kenakatazonecom-627558324114668/?fref=ts</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                <span>kenakata</span> zone</h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/smm/4.94ec54985fde65de.94ec54985fde65de.94ec54985fde65de.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="https://www.facebook.com/Kenakatazonecom-627558324114668/?fref=ts" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="smm">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/smm/5.c878d0d758ab3142.c878d0d758ab3142.c878d0d758ab3142.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center">block<span>b</span></h4>
                                        <p class="col-details text-center">https://www.facebook.com/blockb.001</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                block<span>b</span></h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/smm/5.c878d0d758ab3142.c878d0d758ab3142.c878d0d758ab3142.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="https://www.facebook.com/blockb.001/" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="smm">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/smm/6.21c6ad87f4abd7f6.21c6ad87f4abd7f6.21c6ad87f4abd7f6.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center">myshop 24 <span> live</span></h4>
                                        <p class="col-details text-center">https://www.facebook.com/myshop24live</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                myshop 24 <span> live</span></h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/smm/6.21c6ad87f4abd7f6.21c6ad87f4abd7f6.21c6ad87f4abd7f6.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="https://www.facebook.com/myshop24live/" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="smm">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/smm/7.7ac691cf42e96d8c.7ac691cf42e96d8c.7ac691cf42e96d8c.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center">queen <span> mart</span></h4>
                                        <p class="col-details text-center">https://www.facebook.com/queen-mart-1503526446623873</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                queen <span> mart</span></h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/smm/7.7ac691cf42e96d8c.7ac691cf42e96d8c.7ac691cf42e96d8c.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="https://www.facebook.com/Queen-Mart-1503526446623873/" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="smm">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/smm/8.7c9d3df6f5e22334.7c9d3df6f5e22334.7c9d3df6f5e22334.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center"><span> zeliz</span></h4>
                                        <p class="col-details text-center">https://www.facebook.com/zeliz1826/?fref=ts</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                <span> zeliz</span></h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/smm/8.7c9d3df6f5e22334.7c9d3df6f5e22334.7c9d3df6f5e22334.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="https://www.facebook.com/zeliz1826/?fref=ts" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="smm">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/smm/9.f633c9f763019cb0.f633c9f763019cb0.f633c9f763019cb0.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center">furniture <span> solution</span></h4>
                                        <p class="col-details text-center">https://www.facebook.com/furnituresolution2015/?fref=ts</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                furniture <span> solution</span></h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/smm/9.f633c9f763019cb0.f633c9f763019cb0.f633c9f763019cb0.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="https://www.facebook.com/furnitureSolution2015/?fref=ts" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item  gallery-item item"
                         data-groups="smm">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <img src="assets/data/portfolio/smm/10.b02219f2ca9e53f2.b02219f2ca9e53f2.b02219f2ca9e53f2.PNG" alt="1PNG"/>
                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">
                                    <div>
                                        <h4 class="heading-subsection-title text-center"> <span> navila</span> furniture bd</h4>
                                        <p class="col-details text-center">https://www.facebook.com/navilafurniturebd/?fref=ts</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                <span> navila</span> furniture bd</h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/smm/10.b02219f2ca9e53f2.b02219f2ca9e53f2.b02219f2ca9e53f2.PNG"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">
                                        <a class="btn btn-info" href="https://www.facebook.com/NavilaFurniturebd/?fref=ts" target="_blank">Go To Page</a>
                                </div>
                            </div>
                        </div>
                    </div>
<!--                    //SMMM-->


<!--                    MOBILE-->
                    <div class="layers-widget-skrollex-section-55d261b267ac7676057367 pure-u-12-24 pure-u-md-6-24 masonry-item" data-groups="mobile" style="width: 100%">
                        <a href="#" class="gallery-link">
                            <div class="hover-overlay">
                                <div class="comming-soon"></div>
<!--                                <img src="assets/data/portfolio/mobile/comming.ee0495e37d54dc41.ee0495e37d54dc41.ee0495e37d54dc41.ee0495e37d54dc41.gif" alt="1PNG"/>-->
<!--                                <div class="overlay background-e heading-e link-heading-e internal-highlight-e">-->
<!--                                    <div>-->
<!--                                        <h4 class="heading-subsection-title text-center"> <span>Comming </span> soon</h4>-->
<!--                                        <p class="col-details text-center">https://www.cdlogistics.ca</p>-->
<!--                                    </div>-->
<!--                                </div>-->
                            </div>
                        </a>
                        <div class="gallery-item-content"><h4 class="heading-subsection-title text-center">
                                <span>Comming </span> soon</h4>
                            <div class="row responsive-wide">
                                <div class="col-padding pure-u-1 pure-u-lg-18-24">
                                    <div class="default-slider hold"
                                         data-swiper-options="{touchRatio: 0, speed: 300}">
                                        <div class="swiper-wrapper">
                                            <div class="swiper-slide"
                                                 data-hold-img="assets/data/portfolio/mobile/comming.ee0495e37d54dc41.ee0495e37d54dc41.a599e2df35b8dbdb.gif"
                                                 data-alt="" data-as-bg="no"></div>
                                        </div>
                                    </div>
                                </div>
<!--                                <div class="col-padding pure-u-1 pure-u-lg-6-24"><h5 class="heading-col-title">-->
<!--                                        <a class="btn btn-info" href="https://www.cdlogistics.ca/" target="_blank">Go To Page</a>-->
<!--                                </div>-->
                            </div>
                        </div>
                    </div>

<!--                    //MOBILE-->

                </div>
            </div>
        </div>
    </div>
</div>