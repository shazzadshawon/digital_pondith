<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css?v=2.0" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<section id="smm_pricing" class="layout-boxed archive">
    <div class="pure-g">
        <div class="pure-u-1 pure-u-md-24-24">
            <div class="view x40-widget widget text-bg peraRemoval" id="layers-widget-skrollex-section-9" data-text-effect-selector="h1,h2,h3,h4" data-text-effect="effect-a-animated">

                <div class="pure-g">
                    <div class="colors-w post-body pure-u-1 pure-u-md-18-24 article-blog">
                        <article id="post-242" class="post-242 post type-post status-publish format-standard has-post-thumbnail hentry category-web-design tag-design tag-life tag-music">

                            <h1 class="post-title">
                                <a href="#">Our <span>Social Media</span> Advertising Packages</a>
                            </h1>
                            <div class="copy">
                                <p class="excerpt">Social Media Packages Plan</p>
                            </div>
                        </article>
                    </div>
                </div>

                <div class="pure-g">
                    <div id="one" class="post-meta pure-u-1 pure-u-md-8-24 text-center background-transparent colors-v">
                        <section class="accordion">
                            <div class="item">
                                <h2>Startup</h2>
                                <h5>$100 Per Month</h5>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-heading">$10 spends for boosting and branding on Facebook per post</div>
                                <div class="panel-body">
                                    4 posting per Month
                                    <ul class="list-group">
                                        <li class="list-group-item">Service Provide Within 24hrs</li>
                                        <li class="list-group-item">Account create and Sharing 4 Popular Social Media(Twitter, Linkedin, Pinterest, Google  pluse)</li>
                                        <li class="list-group-item">4 Trending Keywords</li>
                                        <li class="list-group-item">Product / Service Description if it’s needed.</li>
                                        <li class="list-group-item">Maximum 2 (#) Hash tag in description</li>
                                        <li class="list-group-item">Analysis Report</li>
                                    </ul>
                                </div>
                                <div class="panel-footer"> *Condition apply in reach and impression ratio (According to the industry and demographics)</div>
                            </span>
                            <div class="item">
                                <h2>Startup</h2>
                                <h5>$115 Per Month</h5>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-heading">$15 spend for boosting and branding per week (Boosting facebook and Instagram both)</div>
                                <div class="panel-body">
                                    5 post image design/month and posting
                                    <ul class="list-group">
                                        <li class="list-group-item">Service Provide Within 24hrs</li>
                                        <li class="list-group-item">Account Create and Sharing 4 Social Media without Facebook (Twitter, Linkedin, Pinterest, Google Pluse)</li>
                                        <li class="list-group-item">5 Trending Keywords Provide</li>
                                        <li class="list-group-item">Product / Service Description if it’s needed</li>
                                        <li class="list-group-item">Maximum 2 (#) Hash tag in description</li>
                                        <li class="list-group-item">Analysis Report</li>
                                    </ul>
                                    Possible reach: 1,00,000 – 7,60,000 (per month)
                                </div>
                                <div class="panel-footer"> *Condition apply in reach and impression ratio (According to the industry and demographics)</div>
                            </span>
                            <div class="item">
                                <h2>STANDARD</h2>
                                <h5>$150 Per Month</h5>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-heading">$10 spend for boosting and branding per post, (Boosting facebook and Instagram)</div>
                                <div class="panel-body">
                                    6 posting per Month </br> 1 Video posting in a Month
                                    <ul class="list-group">
                                        <li class="list-group-item">Service Provide Within 24hrs</li>
                                        <li class="list-group-item">Account create and Sharing 7 Popular Social Media (Twitter, Linkedin, Pinterest, Google +,  Youtube,  Vimeo / Dailymotion)</li>
                                        <li class="list-group-item">6 Trending Keywords</li>
                                        <li class="list-group-item">Product / Service Description if it’s needed</li>
                                        <li class="list-group-item">Maximum 2 (#) Hash tag in description</li>
                                        <li class="list-group-item">Analysis Report</li>
                                    </ul>
                                </div>
                                <div class="panel-footer"> *Condition apply in reach and impression ratio (According to the industry and demographics)</div>
                            </span>
<!--                            <div class="item">-->
<!--                                <h3>Books</h3>-->
<!--                            </div>-->
<!--                            <p>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>-->
<!--                            <div class="item">-->
<!--                                <h3>Tendances</h3>-->
<!--                            </div>-->
<!--                            <p>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>-->
                        </section>
                    </div>
                    <div id="two" class="post-meta pure-u-1 pure-u-md-8-24 text-center background-transparent colors-v">
                        <section class="accordion">
                            <div class="item">
                                <h2>Social Mid</h2>
                                <h5>$160 Per Month</h5>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-heading">$20 spend for boosting and branding on Facebook and Instagram per week</div>
                                <div class="panel-body">
                                    <ul class="list-group">
                                        <li class="list-group-item">7 post image design/month and posting</li>
                                        <li class="list-group-item">1 video creation and sharing per month</li>
                                        <li class="list-group-item">Free: Twitter, Instagram, Linkedin, Youtube, Vimeo/dailymotion posting or sharing per week, Analytic Report Monthly</li>
                                    </ul>
                                    Possible reach: 120000 - 940,000 (per month)
                                </div>
                                <div class="panel-footer"> *Condition apply in reach and impression ratio (According to the industry and demographics)</div>
                            </span>
                            <div class="item">
                                <h2>PREMIUM</h2>
                                <h5>$220 Per Month</h5>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-heading">$5 spend for boosting and branding per week (Boosting facebook and Instagram)</div>
                                <div class="panel-body">
                                    <ul>
                                        <li>18 posting per Month</li>
                                        <li>9 Posting per month</li>
                                        <li>$10 spend for boosting and branding per week</li>
                                        <li>2 Videos  posting in a Month</li>
                                    </ul>
                                    <ul class="list-group">
                                        <li class="list-group-item">Service Provide Within 24hrs</li>
                                        <li class="list-group-item">Account create and Sharing 7 Social Media without Facebook (Twitter, Linkedin, Pinterest, Google +,  Youtube,  Vimeo / Dailymotion)</li>
                                        <li class="list-group-item">9 Trending Keywords</li>
                                        <li class="list-group-item">Product / Service Description if its needed</li>
                                        <li class="list-group-item">Maximum 2 (#) Hash tag in description</li>
                                        <li class="list-group-item">Analysis Report</li>
                                    </ul>
                                    Possible reach: 140000 - 960,000 (per month)
                                </div>
                                <div class="panel-footer"> *Condition apply in reach and impression ratio (According to the industry and demographics)</div>
                            </span>
                            <div class="item">
                                <h2>Sales Growth / Give Me More Clients</h2>
                                <h5>$240/Month (3 to 6 month continue with us)</h5>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-heading">$30 spend for post boosting on Facebook</div>
                                <div class="panel-body">
                                    <ul>
                                        <li>12 image design per month and posting</li>
                                        <li>2 products showcase video creation per month and sharing</li>
                                    </ul>
                                    <ul class="list-group">
                                        <li class="list-group-item">Free: Twitter, Instagram, Linkedin, Youtube, Vimeo/dailymotion posting or sharing per week</li>
                                        <li class="list-group-item">Analytic Report Monthly</li>
                                    </ul>
                                    Possible reach: 180000 - 1200,000 (per month)
                                </div>
                                <div class="panel-footer"> *Condition apply in reach and impression ratio (According to the industry and demographics)</div>
                            </span>
<!--                            <div class="item">-->
<!--                                <h3>Books</h3>-->
<!--                            </div>-->
<!--                            <p>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>-->
<!--                            <div class="item">-->
<!--                                <h3>Tendances</h3>-->
<!--                            </div>-->
<!--                            <p>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>-->
                        </section>
                    </div>
                    <div id="three" class="post-meta pure-u-1 pure-u-md-8-24 text-center background-transparent colors-v">
                        <section class="accordion">
                            <div class="item">
                                <h2>Branding with Maintenance</h2>
                                <h5>$270/Month (3 to 6 month continue with us)</h5>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-heading">$20 spend for boosting and branding on Facebook Per week</div>
                                <div class="panel-body">
                                    <ul>
                                        <li>12 (3post/w X 4week) post image monthly posting</li>
                                        <li>2 videos creation and sharing per month</li>
                                        <li>Create and Proper Maintain Facebook Page.</li>
                                    </ul>
                                    <ul class="list-group">
                                        <li class="list-group-item">Free: Twitter, Instagram, Linkedin, Youtube, Vimeo/dailymotion posting or sharing per week</li>
                                        <li class="list-group-item">Analytic Report Monthly</li>
                                    </ul>
                                    Possible reach: 140000 - 960,000 (per month)
                                </div>
                                <div class="panel-footer"> *Condition apply in reach and impression ratio (According to the industry and demographics)</div>
                            </span>
                            <div class="item">
                                <h2>Build My Brand</h2>
                                <h5>$415/Month (3 to 6 month continue with us)</h5>
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-heading">$50 spend for branding / brand awareness per week</div>
                                <div class="panel-body">
                                    <ul>
                                        <li>25 images design per month and posting with Details Description</li>
                                        <li>4 videos creation and sharing</li>
                                    </ul>
                                    <ul class="list-group">
                                        <li class="list-group-item">Free: Twitter, Instagram, Linkedin, Youtube, Vimeo/dailymotion posting or sharing per week</li>
                                        <li class="list-group-item">Analytic Report Monthly</li>
                                    </ul>
                                    Possible reach: 260000 - 1600,000 (per month)
                                </div>
                                <div class="panel-footer"> *Condition apply in reach and impression ratio (According to the industry and demographics)</div>
                            </span>
                            <div class="item">
                                <h2>Customize Package</h2>
                                <!-- <h5>$100 Per Month</h5> -->
                            </div>
                            <span class="details panel panel-info">
                                <div class="panel-body">
                                    X = Total Posting Amounts <hr>
                                    X*25% = Share 7 Social Media Sites <hr>
                                    X*25% = Analysis, Reporting, Trending Keywords, Product / Service Description Check, Tag used in Description
                                </div>
                            </span>
<!--                            <div class="item">-->
<!--                                <h3>Books</h3>-->
<!--                            </div>-->
<!--                            <p>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>-->
<!--                            <div class="item">-->
<!--                                <h3>Tendances</h3>-->
<!--                            </div>-->
<!--                            <p>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using</p>-->
                        </section>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>

<script type="text/javascript">
    jQuery(document).ready(function(){
        'use strict';
        jQuery('.accordion .item').on("click", function () {
            jQuery(this).next().slideToggle(100);
//            jQuery('p').not(jQuery(this).next()).slideUp('fast');
            jQuery('.details').not(jQuery(this).next()).slideUp('fast');
        });
    });
</script>