<div class="ext-nav scroll-bar page-transition heading non-preloading background-t">
    <div class="view half-height">
        <img alt class="bg static" src="assets/preset-images/bg_menu_big.1ba42063c64d81e3.1ba42063c64d81e3.33c7eaa53fbbc1dd.jpg"/>
        <div class="fg no-top-padding no-bottom-padding  full-height">
            <div class="full-height">
                <div class="pure-g full-height">
                    <a href="index.php#process" class="position-relative pure-u-1 pure-u-sm-12-24 colors-r full-height">
                        <div>
                            <span class="side-label highlight">Home</span>
                            <span class="side-title heading">Our Coverages</span>
                        </div>
                    </a>
                    <a href="portfolio_page.php" class="position-relative pure-u-1 pure-u-sm-12-24 colors-s full-height">
                        <div>
                            <span class="side-label highlight">Home</span>
                            <span class="side-title heading">Portfolio</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="half-height">

        <div class="pure-g full-height">
            <a href="service_smm.php#smm_pricing" class="position-relative pure-u-1 pure-u-sm-12-24 pure-u-lg-6-24 colors-t full-height border-bottom border-right border-lite-t">
                <div>
                    <span class="side-label highlight">Social Media Marketing</span>
                    <span class="side-title heading">Social Media Marketing Package Plan</span>
                </div>
            </a>
            <a href="service_smm.php" class="position-relative pure-u-1 pure-u-sm-12-24 pure-u-lg-2-24 colors-t full-height border-bottom border-right border-lite-t">
                <div>
                    <span class="side-label highlight">Social Media Marketing</span>
                    <span class="side-title heading">SMM</span>
                </div>
            </a>
            <a href="service_web.php#web_pricing" class="position-relative pure-u-1 pure-u-sm-12-24 pure-u-lg-6-24 colors-t full-height border-bottom border-right border-lite-t">
                <div>
                    <span class="side-label highlight">SEO Marketing</span>
                    <span class="side-title heading">SEO Marketing Packages</span>
                </div>
            </a>
            <a href="service_web.php" class="position-relative pure-u-1 pure-u-sm-12-24 pure-u-lg-2-24 colors-t full-height border-bottom border-right border-lite-t">
                <div>
                    <span class="side-label highlight">Search Engine Optimization</span>
                    <span class="side-title heading">SEO</span>
                </div>
            </a>
            <a href="service_mobile.php#mobile_pricing" class="position-relative pure-u-1 pure-u-sm-12-24 pure-u-lg-6-24 colors-t full-height border-bottom border-right border-lite-t">
                <div>
                    <span class="side-label highlight">Mobile Marketing</span>
                    <span class="side-title heading">Mobile Marketing Packag Plan</span>
                </div>
            </a>
            <a href="service_mobile.php" class="position-relative pure-u-1 pure-u-sm-12-24 pure-u-lg-2-24 colors-t full-height border-bottom border-right border-lite-t">
                <div>
                    <span class="side-label highlight">Mobile Marketing</span>
                    <span class="side-title heading">MM</span>
                </div>
            </a>
        </div>
    </div>
</div>